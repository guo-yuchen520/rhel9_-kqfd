1.清除当前课程，并设置课程为RH294    *以kiosk身份在主目录位置执行指令 [kiosk@foundation0 ~]$ 

清除当前课程

```bash
rht-clearcourse 0
```

设置课程为RH294 

```
rht-setcourse rh294
```

2.保证所有机器RUNNING状态

```bash
rht-vmctl status all；rht-vmctl status classroom
```

3.检查所有主机是否处于登录状态，多等待一会，检查完毕后关闭窗口即可

```bash
for i in bastion classroom servera serverb serverc serverd utility workstation;do rht-vmview view $i;done
```

4.下载环境部署包

```
wget  https://gitee.com/guo-yuchen520/rhel9_-kqfd/blob/master/RHEL9_Exercise/RHEL9_EX200v1.16.tar.gz
```

5.解压缩

```
tar xf
```

