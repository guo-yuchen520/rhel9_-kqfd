郭玉晨



# 课程表

|      第一天       | 第二天             | 第三天             | 第四天               | 第五天               |
| :---------------: | ------------------ | :----------------- | -------------------- | -------------------- |
|   线下环境介绍    | 创建、查看编辑文本 | 控制服务和守护进程 | RHEL网络管理         | 访问Linux文件系统    |
| 红帽企业Linux入门 | 管理本地用户和组   | OpenSSH服务        | 归档与系统间复制文件 | 分析服务器和获取支持 |
|    访问命令行     | 控制文件的访问     | 日志分析与存储     | 安装与升级软件包     | 回顾复习             |
| 从命令行管理文件  | 进程监控及管理     |                    |                      |                      |
| 在RHEL中获取帮助  |                    |                    |                      |                      |

# 认证

## RHCSA   初级

| 课程  | 项目   | 版本    | 时间 |
| ----- | ------ | ------- | ---- |
| RH124 | basic  | RHCSA8  | 5    |
| RH134 | system | RHCSA85 | 5    |

## RHCE  中级

| 课程  | 项目                | 版本  |
| ----- | ------------------- | ----- |
| RH254 | 安全+服务           | RHCE7 |
| RH294 | ansible 2.9         | RHCE8 |
| RH294 | ansible 自动化平台2 | RHCE9 |



## 旧RHCA- 高级

| 课程      | 内容                                      |
| --------- | ----------------------------------------- |
| CL210     | 红帽openstack认证系统管理员               |
| DO407 2.7 | Ansible网络自动化，配置和管理网络基础架构 |
| DO280     | 红帽平台即服务专业技能证书openshift  k8s  |
| RH318     | 红帽认证虚拟化管理员                      |
| RH236     | 红帽混合云存储专业技能证书                |

## 2021新RHCA云方向

推荐的云方向CA认证

| 课程       | 内容                                     |
| ---------- | ---------------------------------------- |
| CL210*     | 红帽openstack认证系统管理员              |
| DO447-374* | 高级自动化：Ansible 最佳实践             |
| DO280*     | 红帽平台即服务专业技能证书openshift  k8s |
| RH236-     | 红帽混合云存储专业技能证书               |

| 以上四项外加以下三选一 |                                  |
| ---------------------- | -------------------------------- |
| RH358*                 | 红帽认证服务管理和自动化专家考试 |
| RH442                  | 红帽认证性能调优专家考试         |
| RH415                  | 红帽认证 Linux 安全专家考试      |
| 125-260*               | ceph                             |



# 第零章 介绍环境搭建过程

## 课程环境方案：

- 如何制作自己的练习环境（微信群共享环境，）

1. 网盘下载（虚拟机）

2. 安装vmware软件并且加载虚拟机程序

3.  随时随地可练


-    瑞通提供了远程环境（面授不提供）

1. 注意： foundation 不要关机

 环境为all in one环境  即所有内容虚拟机都在一个虚拟机里

## vmware安装、加载虚拟机

vmware软件-功能：开启、关闭、挂起、启动、快照

9卸载  再装更高版本，这种方法不推荐

当前9版本，直接升级到更高版本，比如15，推荐方法

## 环境配置

| ID       | 内容                                           |
| -------- | ---------------------------------------------- |
| 硬件     | cpu：VT-X、mem：32G（最低16G），disk 100G      |
| 软件     | OS：x64 APP：VMware17+（workstation\|Fustion） |
| 文件     | folder：RH294.vmx                              |
| 压缩软件 | 7z/win ，keka/mac                              |

## 线下环境机器模块

| VM 虚拟机名称    | 功能                             | 是否必须 | 普通用户：密码  | 超级用户密码 |
| ---------------- | -------------------------------- | -------- | --------------- | ------------ |
| foundation0      | 宿主机                           | 1        | kiosk：redhat   | root：Asimov |
| classroom        | 分配dhcp、dns、软件              | 1        |                 | root：Asimov |
| utility          | DNS等                            | 1        | student:student | root:redhat  |
| bastion          | 网关设备，连接其他机器的网络     | 1        | student:student | root:redhat  |
| workstation      | 提供图形界面连接其他机器：如abcd | 0        | student:student | root:redhat  |
| servera、b、c、d | 练习机，nic/mac+IP+hostname      | 1        | student:student | root:redhat  |

一 切换课程

```bash
课程： RH124(当前录播) RH134 RH294
环境： RH124 		   RH134 RH294

1、查询当前课程
-f0
$ cat /etc/rht
RHT_COURSE=rh294   #证明当前是294课程
2、切换课程
rht-clearcourse 0  #清除当前课程环境
rht-setcourse rh124 #切换课程
3、验证
Course set to rh124
[kiosk@foundation0 ~]$ echo $?   #查看上一条命令的返回值，如果为0证明执行正确，非0证明执行有错误
0
```

二 ping通 classroom

```bash
kiosk@foundation0 ~]$  su - root
Password: Asimov
[root@foundation0 ~]# chattr -i /etc/resolv.conf    #去掉隐藏权限，变成可编辑状态		
[root@foundation0 ~]# gedit /etc/resolv.conf        #编辑文件内容，
[root@foundation0 ~]# cat /etc/resolv.conf 
nameserver 172.25.254.250							#nameserver 172.25.254.250行放到12.2的上面，点击save保存退出即可。
nameserver 192.168.12.2						

```

三 开机顺序

```bash
日常练习时开机顺序：
classroom 
bastion
utility
servera、b、c、d   #RH124、RH134也就是容器之前的课程，基本开一个servera即可。
[kiosk@foundation0 ~]$ for i in classroom utility bastion servera serverb;do rht-vmctl start $i;done

```

## 每台服务器的IP地址分别是什么

```bash
[root@foundation0 ~]# vim /etc/hosts
172.25.250.254 bastion.lab.example.com bastion
172.25.250.9   workstation.lab.example.com workstation
172.25.250.10  servera.lab.example.com servera
172.25.250.11  serverb.lab.example.com serverb
172.25.250.12  serverc.lab.example.com serverc
172.25.250.13  serverd.lab.example.com serverd
172.25.254.254 classroom.lab.example.com classroom    #是我手动自己添加的，默认环境里没有
```

安装之前su -  root  输入密码：Asimov   ，yum install -y langpacks-zh_CN  注销切换一下kisok用户，选择中文，注销即可。

win键+空格=切换输入法

## 宿主机联网

（课程默认不需要上网，有上网需求可以自行配置）

```shell
需要选择f0  添加硬件网络适配器2 的nat的模式，并点击已连接，然后进系统
[kiosk@foundation0 ~]$  ping www.baidu.com  #发现不通
[kiosk@foundation0 ~]$  ifconfig #查看到其中有网卡名称是ens192
[kiosk@foundation0 ~]$  nmcli connection up ens192  #让网卡获取IP
[kiosk@foundation0 ~]$  ping www.baidu.com   #发现可以连通
```

## 学习技巧：

| STEP |         | 含义       |
| ---- | ------- | ---------- |
| 1    | word    | 单词       |
| 2    | tab     | 列出、补全 |
| 3    | man     | 查帮助     |
| 4    | echo $? | 结果是0    |

## 虚拟机控制命令

rht-vmcli 管理线下虚拟机环境

```bash
【foundation0】  #简称f0机器
语法：
rht-vmctl start|stop|reset|status|   #主机名称

rht-vmctl status all   		  #查看所有虚拟机状态及名称
rht-vmctl status classroom    #单独查看classroom 
rht-vmctl start classroom     #开启某台虚拟机
rht-vmctl start bastion 	  #开启某台虚拟机  开启某台虚拟机
rht-vmctl start servera       #开启某台虚拟机
rht-vmctl stop servera        #停止某台虚拟机
rht-vmctl reset servera       #重置虚拟机（恢复初始状态）

vim /etc/hosts				  #查看IP和域名、主机名的映射

ping -c 4 172.25.250.10       #用ping命令测试和servera的连通性
ping  servera
ctrl+c  终端操作

ssh  root@servera 	          #以root身份远程servera  
exit 或者ctrl+d
```

# 第一章  红帽企业Linux入门	

### 开源

### Linux

```
linus  内核linux----莱纳斯
kernel.org 网站
```

### Linux系列发行版

```
公司特定的软件+linux内核=操作系统
红帽：GNU/Linux，各种开源应用+Linux内核  redhat
Centos 
fedora
debian
ubuntu
```

### 红帽培训和教育方案

RHCSA、RHCE、RHCA

红帽企业Linux

# 第二章 访问命令行

## 1.登录Linux系统

```bash
图形化
​	系统菜单-注销-或切换用户

字符界面
​	Ctrl+alt+F2-F6   
在本机上切换用户给

su - root 切换用户
ctrl+d 退出登录(ctrl+d or exit or logout)

网络登录
​     ssh  ip 或 主机名
[kiosk@foundation0 ~]$ ssh servera
​     ssh  用户名@ip/主机名
[kiosk@foundation0 ~]$ ssh root@servera
[kiosk@foundation0 ~]$ ssh student@servera
```

## 2.终端切换

|      |             |               |
| ---- | ----------- | ------------- |
| CLI  | Ctrl+alt+Fx | x in （2，6） |
| GUI  | Ctrl+alt+F1 |               |

## 3.Shell简介

概念：它是一个解释器，可以帮助用户将指令信息传递内核

红帽企业Linux中为用户提供默认shell是bash，bash是与UNIX类似的系统上使用的其中一个最成功的shell改进版本

## 4.基本组成：

```
[kiosk@foundation0 ~]$     				  $普通用户

[kiosk@foundation0 ~]$ su  -  root        切换用户su - 用户名
Password: Asimov  						  输入用户登录密码：Asimov
Last login: Sat Feb 22 15:11:13 CST 2020 on tty3
[root@foundation0 ~]#      				 #超级用户

ctrl+d or exit 							 退出登录
```

注：bash shell在概念上与微软的cmd相似，但bash具有更加复杂的脚本语言。与win系统powershell类似， mac的管理终端使用工具也是使用的bash shell

## 5.GNOME Shell

### 启动终端

```
ALT+F2  输入 gnome-terminal
```

### 锁定  win+l

### 关闭和重启

|      | 关机               | 重启             |
| ---- | ------------------ | ---------------- |
| 1    | init 0             | init 6           |
| 2    | poweroff           | reboot           |
| 3    | systemctl poweroff | systemctl reboot |
| 4    | shutdown -h 20：00 | shutdown -r 0    |

### 工作区切换

| ctrl+alt   上\|下  箭头 |
| ----------------------- |
|                         |

## 6.Shell的特性

### linux命令语法：

完成具体功能的命令

扩展该命令功能的选项

命令要操作的对象

cmd 【-option】 【arg1】 【arg2】

```bash
简单的命令示例：
whoami
date
touch file1；mkdir dir1

完成某些工作的指令
扩展命令功能的选项
参数

ls
ls -a    
ls -a    ~/.bashrc
ls -a  -l  ~/.bashrc
ls -al  ~/.bashrc

```

### 回显式命令

```bash
 date +%Y%m%d
 date +%Y-%m-%d
```

### 交互式命令

```shell
passwd
```

### tab补全   

```
1 按一下是补全
2 按两下列出可用命令
3 tab键  输入单词或命令前面几个首字母后，保证唯一可补全，不唯一可列出能选择的命令
```

### 历史命令-history

```shell
env--能容纳1000条
[root@servera ~]# env | grep SIZE
HISTSIZE=1000

```

```shell
[root@servera ~]# history -w 
[root@servera ~]# vim ~/.bash_history     记录历史命令文件，vim是一个文本工具，可以打开后面的文件，进入后:q退出
[root@servera ~]# history -c   清除
[root@servera ~]# history 
    1  history
其他方法：
！！
！23    历史命令的编号
！h      命令首字母

 （以下内容了解即可）
当前历史命令支持的最大条数
[root@foundation0 /]# grep ^HISTSIZE /etc/profile
HISTSIZE=1000

历史命令存放文件路径
[root@foundation0 /]# set | grep HISTFILE
HISTFILE=/root/.bash_history
vim /etc/profile
export HISTFILE=/root/.newfile
source /etc/profile
history -w
cat /root/.newfile

ctrl+R 搜索历史命令
```

## 7.命令行快捷键

|      |                        |                        |
| ---- | ---------------------- | ---------------------- |
| 1    | ctrl+shift+t           | 当前画面添加一个标签   |
| 2    | ctrl+shift+n           | 打开一个新的标签       |
| 3    | alt+1，alt+2           | 切换标签               |
| 4    | ctrl+shift+ =，ctrl+ - | 扩大终端字体，缩小字体 |
| 5    | ctrl+shift+w           | 关闭标签               |

## 8.Bash Shell常用的快捷键

| ctrl+a              | 光标跳至行首                                      |
| ------------------- | ------------------------------------------------- |
| ctrl+e              | 光标跳至行尾                                      |
| ctrl+u              | 从光标所在位置清空至行首                          |
| ctrl+k              | 从光标所在位置清空至行末                          |
| ctrl+左箭头         | 光标向左跳一个单词                                |
| ctrl+右箭头         | 光标向右跳一个单词                                |
| ctrl+w              | 回删一个单词                                      |
| alt+d               | 删除光标后一个单词                                |
| esc + .  或 alt + . | 调用之前使用过的路径，alt+.一直点可以向上翻阅路径 |

# 第三章 从命令行管理文件

## 1.Linux系统目录结构

```shell
/目录下每个目录的作用:
bin 用户可执行目录(命令 root 和 普通)
sbin 系统可执行目录(命令 root)
lib 库文件目录(32位)
lib64 库文件目录(64位)
dev 设备文件目录dev 设备文件目
usr 应用程序目录
var 服务器数据目录(数据 日志)
srv 服务器数据目录
etc 配置文件目录
tmp 临时文件目录
boot 服务器启动目录(内核和启动文件)
media 媒介目录(u盘,cdrom)
mnt 其他挂载点
opt 第三方应用程序目录
proc 伪文件系统(内核参数,进程信息,硬件信息)
sys 伪文件系统(c 配置文件目录 内核参数,进程信息,硬件信息)
run 进程锁目录
root root管理员家目录
home 普通用户家目录
```

## 2.文件类型

```bash
文件类型:(7种)
- 普通文件 file
d 目录文件 directory
c 字符设备文件 character
b 块设备文件 block
s 套接字文件 socket
p 管道文件 pipe
l 符号链接文件(软链接) symbolic
```

## 3.通过文件名定位文件

```bash
[root@foundation0 home]# cd /
[root@foundation0 /]# cd /etc/
```

## 4.路径的表示

```
绝对路径 ,通常以/开头
例子：
根开头    cd /etc/sysconfig
相对路径
非根开头 cd  ..
```

## 5.导航路径	

pwd、cd、ls

```shell
pwd
[root@foundation0 yum.repos.d]# pwd
/etc/yum.repos.d
```

```bash
cd -   返回之前的目录
cd or cd ~	家目录
cd .	当前目录
cd ..    上一级目录
```

```shell
ls
ls -a 
ls -a /home 
ls -a -l 
ls -al
[root@foundation0 ~]# ls -a .viminfo
.viminfo
[root@foundation0 ~]# ls -a -l .viminfo
-rw-------. 1 root root 2545 Mar 13 13:12 .viminfo
[root@foundation0 ~]# ls -al .viminfo
-rw-------. 1 root root 2545 Mar 13 13:12 .viminfo
[root@foundation0 /]# ls -l -d /home
drwxr-xr-x. 4 root root 30 Mar 13 11:38 /home

ls -ld /opt
ll -d  /opt

alias
```

## 6.查看文件内容

| cat  | cat /etc/passwd                                              | 将文件内容打印到屏幕上                                       |
| ---- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| tail | tail  /var/log/message  ,tail -n 5 或 tail -5  /var/log/message | 默认查看文件后10行。-F （追踪）指定文件不存在时再创建相同名称文件 |
| head | head  /var/log/message  ,head -5  /var/log/message           | 默认查看文件头10行                                           |
| less | less  /var/log/message                                       | space。q，page up，page down,ctrl+f,ctrl+b                   |
| more | more /var/log/message                                        | space ，q                                                    |
| vim  | vim  /etc/passwd                                             | 文本编辑器                                                   |

## 7.使用命令行管理文件/目录

|      | file  | folder   |
| ---- | ----- | -------- |
| 创建 | touch | mkdir -p |
| 改名 | mv    | mv       |
| 移动 | mv    | mv       |
| 拷贝 | cp    | cp -r    |
| 删除 | rm    | rm -r    |

touch、mkdir、rm、cp、mv

```shell
touch：
[root@servera ~]# cd /opt/
[root@servera opt]# pwd
/opt
[root@servera opt]# ls
[root@servera opt]# man touch
[root@servera opt]# touch file1
[root@servera opt]# ls
file1
[root@servera opt]# touch file2 file3
[root@servera opt]# ls
file1  file2  file3
[root@servera opt]# touch /file4 /tmp/file5
[root@servera opt]# ls /file4;ls /tmp/file5
/file4
/tmp/file5
[root@servera opt]# touch file{10..20}
[root@servera opt]# ls
file1   file11  file13  file15  file17  file19  file20
file10  file12  file14  file16  file18  file2   file3
a1.txt a2.txt a3.txt b1.txt.....c3.txt
```

```shell
mkdir
选项：
-p：递归创建
-v：显示过程
[root@servera opt]# rm -f file* 
[root@servera opt]# ls
[root@servera opt]# man mkdir
[root@servera opt]# mkdir dir1
[root@servera opt]# ls
dir1
[root@servera opt]# mkdir dir2 /dir3
[root@servera opt]# ls
dir1  dir2
[root@servera opt]# ls /
bin   dir3   home   media  proc  sbin  tmp
boot  etc    lib    mnt    root  srv   usr
dev   file4  lib64  opt    run   sys   var
[root@servera opt]# ls
dir1  dir2
[root@servera opt]# mkdir dir3/dir4
mkdir: cannot create directory ‘dir3/dir4’: No such file or directory
[root@servera opt]# mkdir -pv dir3/dir4
mkdir: created directory 'dir3'
mkdir: created directory 'dir3/dir4'
[root@servera opt]# ls -R dir3  #-R递归查看，可以查看多级目录内容
dir3:
dir4

dir3/dir4:
[root@servera opt]# ll -R dir3/dir4
dir3/dir4:
total 0
[root@servera opt]# ll  dir3
total 0
drwxr-xr-x. 2 root root 6 Mar 13 22:23 dir4
[root@servera opt]# ll  dir3/dir4/
total 0
[root@servera opt]# ll  dir3/dir4/ -d
drwxr-xr-x. 2 root root 6 Mar 13 22:23 dir3/dir4/
```

```shell
rm
[root@servera opt]# man rm
[root@servera opt]# ls
dir1  dir2  dir3  file1  file2  file3
[root@servera opt]# rm file1
rm: remove regular empty file 'file1'? y    询问是否删除y删除n不删除
[root@servera opt]# ls
dir1  dir2  dir3  file2  file3
[root@servera opt]# rm file2
rm: remove regular empty file 'file2'? n
[root@servera opt]# rm -f file2 		强制删除不询问
[root@servera opt]# ls
dir1  dir2  dir3  file3
[root@servera opt]# rm -f file*         *代表一个或多个字符
[root@servera opt]# ls
dir1  dir2  dir3
[root@servera opt]# rm dir1				
rm: cannot remove 'dir1': Is a directory
[root@servera opt]# rm -r dir1			删除目录需要-r表示递归
rm: remove directory 'dir1'? y
[root@servera opt]# rm -rf dir2
[root@servera opt]# ls
dir3

copy
[root@servera opt]# ls
dir1  dir2  file1  file2  file3
[root@servera opt]# cp file1 /tmp/
[root@servera opt]# ls /tmp/file1
/tmp/file1
[root@servera opt]# cp file1 /tmp/file10
[root@servera opt]# ls /tmp/file10
/tmp/file10
[root@servera opt]# cp /etc/man_db.conf .
[root@servera opt]# ls
dir1  dir2  file1  file2  file3  man_db.conf
[root@servera opt]# cp file2 file3 /tmp/
[root@servera opt]# ls /tmp/file*
/tmp/file1   /tmp/file2  /tmp/file5
/tmp/file10  /tmp/file3
[root@servera opt]# cp -r dir1 /tmp/
[root@servera opt]# cp -r dir1 /tmp/di10

[root@servera opt]# cp /home/student/.bashrc  /opt/
[root@servera opt]# ll -a 
[root@servera opt]# cp -p /home/student/.bashrc ./.bashrc1
[root@servera opt]# ll -a

mv
[root@servera opt]# ls
dir1  dir2  file1  file2  file3  man_db.conf
[root@servera opt]# mv file1 /
[root@servera opt]# ls
dir1  dir2  file2  file3  man_db.conf
[root@servera opt]# mv file2 /file20
[root@servera opt]# ls
dir1  dir2  file3  man_db.conf
[root@servera opt]# mv file3 file30
[root@servera opt]# ls
dir1  dir2  file30  man_db.conf
[root@servera opt]# mv dir1 /
[root@servera opt]# ls
dir2  file30  man_db.conf
[root@servera opt]# mv dir2 dir20
[root@servera opt]# ls
dir20  file30  man_db.conf

```

##   8.通配符

通配符

```
1		*匹配0个或多个任意字符
   ？		匹配1个任意字符
   [ ]		匹配中括号内一个字符
   [ - ] 	        匹配中括号内连续范围的一个字符
   [ ^ ]		取反。匹配非中括号内的字符，表示一定有一个字符，但不是中括号内出现的。【^ab】
2		{a,b}或{a..c} 匹配号中的字符，或连续的字符
```

```shell
[root@servera opt]# rm -rf *
[root@servera opt]# touch {a..c}{1..3}.txt
[root@servera opt]# ls
a1.txt  a3.txt  b2.txt  c1.txt  c3.txt
a2.txt  b1.txt  b3.txt  c2.txt
[root@servera opt]# ls *.txt
a1.txt  a3.txt  b2.txt  c1.txt  c3.txt
a2.txt  b1.txt  b3.txt  c2.txt
[root@servera opt]# ls a*
a1.txt  a2.txt  a3.txt
[root@servera opt]# ls a?.txt
a1.txt  a2.txt  a3.txt
[root@servera opt]# ls ?2*
a2.txt  b2.txt  c2.txt
[root@servera opt]# ls ?[23]*
a2.txt  a3.txt  b2.txt  b3.txt  c2.txt  c3.txt
[root@servera opt]# ls ?[^23]*
a1.txt  b1.txt  c1.txt
[root@servera opt]# ls ?[!23]*
a1.txt  b1.txt  c1.txt
[root@servera opt]# ls {a1,b1}*
a1.txt  b1.txt
[root@servera opt]# ls {a..c}*
a1.txt  a3.txt  b2.txt  c1.txt  c3.txt
a2.txt  b1.txt  b3.txt  c2.txt
[root@servera opt]# ls [[:alpha:]]*
a1.txt  a3.txt  b2.txt  c1.txt  c3.txt
a2.txt  b1.txt  b3.txt  c2.txt
[root@servera opt]# ls [a-z]*
a1.txt  a3.txt  b2.txt  c1.txt  c3.txt
a2.txt  b1.txt  b3.txt  c2.txt
[root@servera opt]# ls [A-Z]*
b1.txt  b2.txt  b3.txt  c1.txt  c2.txt  c3.txt

```



# 第四章  在线获取帮助

## 1.MAN帮助手册

```
man 1 用户命令 *
man 2 系统调用
man 3 库调用
man 4 特殊文件
man 5 配置文件 *
man 6 游戏
man 7 杂项
man 8 系统命令 *
```

## 2.--help

```bash
man passwd
man -k passwd 
mandb
man -k passwd
man 1 passwd
man 5 passwd

  
ls --help
  
man tar
man setfacl | grep lisa
man setfacl | grep -B 1  lisa
setfacl --help | grep \\-a
setfacl --help | grep -w \\-a
  
```

## 3.Pinfo

```
info
pinfo   回车  u
pinfo  ls


```

## 4.rpm包中提供帮助

```
yum install -y httpd

rpm -qa | grep httpd
rpm -ql 软件包名称
rpm -qc

http tools install 
servera
classroom和bastion
yum install -y httpd-manual
systemctl start servera
允许http，或关闭防火墙

found：http：//172.25.250.10/manual
```

## 5.在线帮助

 https://access.redhat.com/ 

# 第五章 创建、查看编辑文本

## 1.VIM文本编辑器

| 模式     | 功能               |      |
| -------- | ------------------ | ---- |
| 命令模式 | 光标移动、复制删除 | cmd  |
| 输入模式 | 输入文本内容       | a    |
| 末行模式 | 保存退出、设置环境 | ：   |

## 2.命令模式

| 命令       | 解释                                                         |
| ---------- | ------------------------------------------------------------ |
| h j k l    | 左下上右                                                     |
| 方向键     | 上下左右                                                     |
| 1G、nG     | n代表一个数字，去第1行或n行                                  |
| gg         | 将光标定位到文章的顶端                                       |
| G          | 将光标定位到文章的底端 （$定位光标到行尾，0和^定位光标到行首） |
| x，X       | 向后删除，向前删除一个字符                                   |
| dd ，  ndd | 删除1行，n是一个数字，n行   。 例子：dgg dG  d$ d0 D         |
| yy，nyy    | 复制1行，n行                                                 |
| p，P       | 粘贴到下一行，粘贴到上一行                                   |
| u          | 撤销                                                         |
| ZZ         | 保存退出 按键ZZ                                              |

## 3.插入模式

| a    | 字符后进入插入模式             |
| ---- | ------------------------------ |
| i    | 当前字符位置进入插入模式       |
| o    | 在下一行新创建一行进入插入模式 |
| A    | 在行尾进入插入模式             |
| I    | 在行首进入插入模式             |
| O    | 在上一行新创建一行进入插入模式 |
| s    | 删除光标位置字符并进入插入模式 |
| S    | 删除光标所在行并进入插入模式   |

## 4.末行模式

| w             | 保存                                                  |
| ------------- | ----------------------------------------------------- |
| q             | 退出                                                  |
| wq            | 退出并保存                                            |
| q！           | 强制退出                                              |
| x             | 保存退出                                              |
| set nu        | 设置行号                                              |
| set nonu      | 取消行号                                              |
| ：w /newfile  | 另存为其他文件       例子：":w /man.txt"              |
| ：r /newfile  | 读取/newfile到本文件中   例子：     ":r  /etc/passwd" |
| ：！  command | vim编辑过程中，查询linux  ":! ls /"                   |
| :  e！        | 重新读取文件                                          |

## **5.其他模式**

| v、V或Ctrl+V  | 可视模式                                   |
| ------------- | ------------------------------------------ |
| R             | 替换模式                                   |
| /word，？word | /向下查找，？向上查找                      |
| n，N          | 定位到下一个匹配字符，定位到上一个匹配字符 |

## 6.试图模式(重点)

```shell
视图模式修改方法：
ctrl+v ， jjj，I，  写入#号，esc
```

## 7.缩进+永久保存

```bash
：set all         查看末行模式的帮助
：set autoindent  保存上下缩进
：set tabstop=2   调整tab键缩进
：set nu          设置行号

[root@foundation0 ~]# vim ~/.vimrc    仅对当前用户生效

set nu
set tabstop=2

[root@foundation0 ~]# vim /etc/vimrc  全局设置，每个用户使用vim工具都有行号

set nu
```

## 8.替换

```bash
:s///   @@@  AAA  ; ;;
:s/old/new/
:s/old/new/g
:#,#s/old/new/g     #井号代表一个数字比如：1,5s/old/new/g
:%s/old/new/g
:#,$s/old/new/g    #井号代表数字，比如1，$s ，$代表末行，该命令为1行至末行

修改某一段ip地址: %s/192.168.1/172.25.250/g

取消文本中某个字段：:%s/10.10.10.10//g
```

## 9.重定向

```bash
1=stand，2=error，&=1+2
pwd
ls
echo hello
echo $SHELL
echo 12345
echo 123456 > file1
cat file1
grep root /etc/passwd
grep root /etc/passwd > /opt/a.txt
cat /opt/a.txt
grep apache /etc/passwd
grep apache /etc/passwd > /opt/a.txt
cat a.txt
echo 123456
echo 123456 >> a.txt
cat a.txt
echo abcd 1>> a.txt
cat a.txt
ls
cd dir1
cd dir1 2>  err
cat err
cd dir1 2>  /dev/null
find / -user studnet
find / -user student
find / -user student 2> err
vim err
find / -user student 2> err > sta
vim sta
find / -user student &> share
vim share
grep na /etc/resolv.conf
vim /etc/resolv.conf
grep ^$ /etc/resolv.conf
cat -n /etc/resolv.conf
cat -n /etc/resolv.conf  | grep ^$
cat -n /etc/resolv.conf
vim ~/.vimrc
cat -n /etc/resolv.conf
cat  /etc/resolv.conf
grep -n ^$ /etc/resolv.conf
grep na /etc/resolv.conf
ls /root/
ls
grep na /etc/resolv.conf
grep na /etc/resolv.conf   > /root/lines.txt
cat /root/lines.txt
history -w
tc/resolv.conf   > /root/lines.txt
cat /root/lines.txt                   
```

# 第六章 管理本地用户和组

## 1.USER

基本概念：用户用于访问计算机资源

```
0 超级用户 
1000以下系统用户
1000以上普通用户 
组与用户ID对应（自然创建）
```

### useradd

```bash
语法：useradd 选项 选项参数  用户名

option：
-u：指定用户uid
-g：指定主要群组
-G：指定附加群组
-s：指定shell环境 /bin/bash  /sbin/nolgoin /bin/false
-c：指定描述
-d：指定用户家目录（通常不更改，且如设置需要是未存在的目录）


例：
useradd user1   创建user1
passwd user1    为user1设置密码
id user1 		查询用户信息
```

### 用户配置文件

```bash
/etc/passwd
[kiosk@foundation0 ~]$ vim /etc/passwd
root:x:0:0:root:/root:/bin/bash
用户名：密码占位符：UID：GID：描述：家目录：shell环境
```

### 练习：

```bash
练习1：   
[root@servera /]# useradd -u 2000 user1
[root@servera /]# id user1

[root@servera /]# groupadd group1
[root@servera /]# tail -1 /etc/group
[root@servera /]# useradd -g group1 user2
[root@servera /]# tail -1 /etc/passwd

[root@servera /]# useradd -G wheel user3
[root@servera /]# useradd -c student -d /user4dir -s /sbin/nologin user4
[root@servera /]# tail -1 /etc/passwd

[root@servera /]# su - user1
[user1@servera /]# ctrl+d
[root@servera /]# su - user1
[root@servera /]# su - user2  需要密码
[root@servera /]# ctrl+d  退出用户

[root@servera /]# passwd user2  超级用户设置密码
123456
123456
[root@servera /]# su - user1
[root@servera /]# su - user2  输入密码
[root@servera /]# useradd -G root,tom user5   #将user5同时加入到组root和tom组中

练习2：tom10，uid 3000 ，gid devops，shell环境为/bin/false,描述 student，家目录/tom10dir,附加组 root。
答案：
useradd -u 3000 -g devops -s /bin/false -c student -d /tom10dir -G root  tom10
```

### usermod

```bash
语法：usermod 选项 选项参数  用户名

option：
-u：指定用户uid
-g：指定主要群组
-G：指定附加群组
-s：指定shell环境 /bin/bash  /sbin/nolgoin /bin/false
-c：指定描述
-d：指定用户家目录（通常不更改，且如设置需要是未存在的目录）
-a：额外指定附加组
```

### 练习：

```bash
[root@servera /]# usermod -u 3000 user1
[root@servera /]# usermod -g group1 user1
[root@servera /]# usermod -G root user1
[root@servera /]# grep user1 /etc/group
[root@servera /]# usermod -G root,wheel user1
[root@servera /]# grep user1 /etc/group
[root@servera /]# groupadd group2
[root@servera /]# usermod -a -G group2 user1
[root@servera /]# grep user1 /etc/group
[root@servera /]# usermod -s /bin/false user1    #shell环境为/bin/false的用户和系统无任何交互
[root@servera /]# su - user1
[root@servera /]# usermod -c heihei user1


练习3：tom11的附加组，root。想额外添加一个附加组为devops。
答案：
[root@servera opt]# usermod -a -G root tom11
[root@servera opt]# usermod -G root,devops tom11
```

### userdel

```bash
语法：userdel 选项 选项参数  用户名

option：
-r：删除用户同时删除邮箱和家目录
```

### 练习：

```bash
[root@servera /]# useradd user5
[root@servera /]# find / -user user5
[root@servera /]# userdel -r user5
[root@servera /]# find / -user user5
```

## 2.PASSWORD

### passwd

```shell
语法：
passwd 用户名
例：
方法1：
[root@foundation0 /]# useradd zhangsan   #创建用户
[root@foundation0 /]# echo 123456 | passwd --stdin zhangsan  非交互式
Changing password for user zhangsan.
passwd: all authentication tokens updated successfully.
方法2：
[root@foundation0 /]# passwd zhangsan  交互式

```

### 密码配置文件/etc/shadow

```bash
第一列: 用户名
第二列: 密码(有密码状态,无密码状态,!!帐号锁定,* 该帐号永久不能登陆系统)
第三列: 密码的最后一次修改时间（从1970年1月1日至今的天数）18834=今天
第四列: 密码的最小时间(和第三列比较，密码修改相隔时间，或理解为密码自最后一次修改后多少天内不能再重复修改)
第五列: 密码的最大时间(密码有效期) 99999表示永久不过期(和第3列比，相当于自最后一次修改多久后必须变更密码，否则过期)
第六列: 密码过期前警告时间（和第5列比，在过n天你的密码就过期了，需要重设密码。）
第七列: 密码过期后帐号（宽限时间，第五列密码的最大时间到期后，还可以使用系统的宽限时间，该期间中可以继续使用系统，但是再次登入系统时强制修改密码，否则无法进入）
第八列: 帐号有效期（账号失效后，无论密码是否过期都不能使用。）
第九列: 保留列
```

### 练习：

```bash
设置密码：
1.交互式
语法：passwd 用户名
[root@servera /]# passwd user1

2.非交互式
语法：echo xxx | passwd --stdin username 
[root@servera /]# echo 123456 | passwd --stdin user1
Changing password for user user1.
passwd: all authentication tokens updated successfully.
```

## 3.GROUP

### groupadd、groupmod、groupdel

```
1.groupadd 
语法：
groupadd 选项 选项参数 组名
-g：指定组ID
例：
[root@servera /]# groupadd group10
[root@servera /]# groupadd -g 3000 group11

2.groupmod
语法：
groupmod 选项 选项参数 组名
-n：更改组名 groupmod -n 新组名 旧组名
例：
[root@servera /]# groupmod -n group100 group10
[root@servera /]# 

3.删除组信息；
groupdel groupname
[root@foundation0 ~]# groupdel haha1
[root@foundation0 ~]# grep haha1 /etc/group

4.加入群组与清除群组成员
gpasswd
-a：添加用户到群组
-d：从组中清除用户

[root@foundation0 ~]# useradd -G upup user5  添加用户时指定附加组（次要群组）
[root@foundation0 ~]# usermod -G upup user1  修改用户时指定附加组（次要群组） 
[root@foundation0 ~]# gpasswd -a user2 root
Adding user user2 to group root
[root@foundation0 ~]# gpasswd -d user2 root
Removing user user2 from group root
```

### 用户组配置文件/etc/group

```

[root@localhost ~]# vim /etc/group
upup:x:2006:
第一段: 组名
第二段: 组密码占位符号
第三段: gid
第四段: 用户列表
```

### su

```bash
su  -
su  - root
su  - user1     su - user2  需要密码
```

### 模拟练习

```
groupadd sysmgrs
useradd -G    sysmgrs  natasha
useradd -G    sysmgrs  harry
useradd -s /bin/false sarah
echo  flectrag  | passwd --stdin natahsa
echo  flectrag  | passwd --stdin harry
echo  flectrag  | passwd --stdin sarah

验证方式：通过切换用户，id username，vim /etc/passwd
```

```bash
修改密码重设默认天数：
[root@servera ~]# useradd user10
[root@servera ~]# chage -l user10
Maximum number of days between password change		: 99999
[root@servera ~]# vim /etc/login.defs 
PASS_MAX_DAYS   20
[root@servera ~]# useradd user20
[root@servera ~]# chage -l user20
Maximum number of days between password change		: 20
```

## 4.课内重点总结

```bash
useradd  usermod userdel  ，id 用户名，cat /etc/passwd，
groupadd groupmod  groupdel 
gpasswd -a   , -d  

passwd 用户名  
echo xxx | passwd --stdin username 

用户 /etc/passwd, 用户组，/etc/group 密码 /etc/shadow
创建用户时的一些默认设置，/etc/default/useradd 
/etc/login.defs 
```



# 第七章 控制对文件的访问

## 关于系统安全的技术点对比

| Linux操作系统涉及的安全部分              |                   |
| ---------------------------------------- | ----------------- |
| 防火墙                                   | semanage port ，  |
| selinux                                  | semanage ...      |
| 软件app                                  | semanage boolean  |
| 文件系统权限   特殊权限  facl   隐藏权限 | semanage fcontext |

## 1.Linux文件权限简介

| 权限分类 |         |
| -------- | ------- |
| r        | read    |
| w        | write   |
| x        | execute |

## 2.Linux系统的权限表示

```bash
# ls -l test
-rw-r--r--. 1 stu1 class1 35 May 21 14:09 test
rw-r--r--   #中间9位是权限，逻辑分三组，所有者 所属组 其他人权限
stu1 所有者
class1 所属组

tom
```

## 3.Linux系统的权限的作用

| 权限 | 对文件的影响 | 对目录的影响     |
| ---- | ------------ | ---------------- |
| r    | cat          | ls               |
| w    | vim          | touch，rm，mkdir |
| x    | ./script     | cd               |

## **4.使用符号方式修改文件权限**

| 对象     | 设置方式    | 权限          |
| -------- | ----------- | ------------- |
| u(user)  | +  (添加)   | r             |
| g(group) | -   (减去） | w             |
| o(other) | =  (设置）  | x             |
| a(all)   |             | s(SUID、SGID) |
|          |             | t(Sbit)       |

## 5.**使用数字方式修改文件权限**

| rwx  | 8进制表示 | 数字表示 |
| ---- | --------- | -------- |
| r--  | 100       | 4        |
| -w-  | 010       | 2        |
| --x  | 001       | 1        |

## **6.文件权限设置实例**chmod

```bash
语法：
chmod 权限 文件名
u g o a   + - =   r w x s t
例：
[root@node1 /]# cd /opt/
[root@node1 opt]# ls
[root@node1 opt]# touch test
[root@node1 opt]# ll test 
-rw-r--r--. 1 root root 0 Nov 24 04:55 test
[root@node1 opt]# chmod u+x test 
[root@node1 opt]# ll test 
-rwxr--r--. 1 root root 0 Nov 24 04:55 test
```

### 练习：文件权限修改

```bash
文件权限修改
[root@node1 opt]# touch aa.txt
[root@node1 opt]# ll aa.txt   #每次修改权限去自己检验查询

[root@node1 opt]# chmod u+x aa.txt
[root@node1 opt]# chmod u-rw aa.txt
[root@node1 opt]# chmod u+r,g+w,o+x aa.txt
[root@node1 opt]# chmod ug+rw aa.txt
[root@node1 opt]# chmod ugo+rwx aa.txt   # chmod a+rwx , chmod a=rwx, ugo+rwx
[root@node1 opt]# chmod a-rwx aa.txt
[root@node1 opt]# chmod o=--- aa.txt     # o=-
[root@node1 opt]# chmod u=r aa.txt
[root@node1 opt]# chmod u=rw aa.txt
[root@node1 opt]# chmod u=r,g=rw,o=x aa.txt
[root@node1 opt]# chmod ugo=rwx aa.txt
[root@node1 opt]# chmod a=r aa.txt
```

### 练习：数字修改方法

```bash
r  4
w  2
x  1

[root@node1 opt]# mkdir dir1
[root@node1 opt]# ll -d dir1
drwx r-x  r-x. 2 root root 6 Nov 24 05:04 dir1
[root@node1 opt]# chmod 775 dir1
[root@node1 opt]# ll -d dir1
drwxrwxr-x. 2 root root 6 Nov 24 05:04 dir1
```

### 设置文件属主和属组 chown 

### chown

```bash
语法：
chown  所有者:所属组   文件名 
chown  该命令可以作用域文件、目录，修改时保证所有者的用户，及组都是存在的。
例：
chown user2:user2 newfile
```

### 练习：

```bash
[root@node1 opt]# ll test
-rwxr--r--. 1 root root 0 Nov 24 04:55 test
[root@node1 opt]# id student
uid=1000(student) gid=1000(student) groups=1000(student),10(wheel)

[root@node1 opt]# useradd harry

[root@node1 opt]# chown student test;ll test
-rwxr--r--. 1 student root 0 Nov 24 04:55 test

[root@node1 opt]# chown :harry test;ll test
-rwxr--r--. 1 student harry 0 Nov 24 04:55 test

[root@node1 opt]# useradd sally
[root@node1 opt]# chown sally:sally test ; ll test
-rwxr--r--. 1 sally sally 0 Nov 24 04:55 test


-R
[root@node1 opt]# ls
dir1  test
[root@node1 opt]# touch dir1/test2
[root@node1 opt]# ll -d dir1
drwxrwxr-x. 2 root root 19 Nov 24 05:15 dir1
[root@node1 opt]# ll  dir1
total 0
-rw-r--r--. 1 root root 0 Nov 24 05:15 test2
[root@node1 opt]# chown -R sally:sally dir1
[root@node1 opt]# ll -d dir1
drwxrwxr-x. 2 sally sally 19 Nov 24 05:15 dir1

[root@node1 opt]# ll dir1/test2 
-rw-r--r--. 1 sally sally 0 Nov 24 05:16 dir1/test2
```

## 7.文件默认权限 umask

```bash
系统默认定义权限对于文件666对于目录来说777

查看umask值方法
[root@servera /]# umask
0022

修改方法umask
[root@servera /]# umask 0002
修改完后，可以去文件和目录查看权限，看是否和之前不一样，看完改回来

永久生效
[root@servera /]# echo 'umask 0002' >> ~/.bash_profile
[root@servera /]# source ~/.bash_profile  #source 将后面文件中的值加载到当前shell中。系统登录会读取~/.bash_profile文件自动加载
```

练习：

```bash
文件默认权限666
umask后三位022
快捷方法：变成权限后相减
rw-rw-rw-  =  666  文件默认权限
----w--w-  =  022  umask值
------------------------------
rw-r--r--  =  644

文件默认权限是644
默认目录权限777
rwxrwxrwx  =  777  目录默认权限
----w--w-  =  022  umask值
------------------------------
rwxr-xr-x  =  755




参考umask计算方法：https://www.cnblogs.com/wyllearning/p/16482006.html

要求文件权限结果 r--r--r--
rw-rw-rw- =   666  默认
xx 		      xxx  umask
-----------------------
r--r--r-- =    结果


rw-rw-rw- =   666  默认
r--r--r-- =    结果
______________________
-w--w--w-=     0222 



rwxrwxrwx =  777
r-xr-xr-x =  555
-----------------
-w--w--w- =  0222


umask 0222 
```

## 8.特殊权限

文件系统权限可以完成一些基本权限功能设置，但有些特殊要求是达不到的，可能需要特殊权限来完成。Linux系统中特殊权限有三个 SUID 4  、SGID 2  、SBIT 1 

```bash
SUID  4 
    通常设置在二进制可执行文件（命令）上，并具有执行权限的情况下。作用：设置了该权限的命令，被其他用户执行时，会临时获取文件所有者权限。
[student@clear ~]$ su - studnet
[student@clear ~]$ cat /etc/shadow   #普通用户无法查看/etc/shadow
[student@clear ~]$ su - root         #切换root身份
[root@clear ~]# chmod u+s /usr/bin/cat   #数字修改方式：chmod 4755 /usr/bin/cart
[root@clear ~]# ll /usr/bin/cat
-rwsr-xr-x. 1 root root 34512 Aug 13  2018 /usr/bin/cat
[student@clear ~]$ su - studnet
[student@clear ~]$ cat /etc/shadow #能够看见内容，临时获取拥有者权限

chmod 4755 /usr/bin/cat

SGID  2
该权限通常设置在`目录`上，设置了该权限的目录，在该目录中创建`子文件及目录`时会`继承`父目录所属组。
[root@clear ~]# cd /opt/
[root@clear opt]# ls
[root@clear opt]# mkdir dir1
[root@clear opt]# chown student:student dir1
[root@clear opt]# ll -d dir1
drwxr-xr-x. 2 student student 6 Nov 19 04:50 dir1
[root@clear opt]# touch dir1/root.txt
[root@clear opt]# ll dir1/root.txt
-rw-r--r--. 1 root root 0 Nov 19 04:51 dir1/root.txt
[root@clear opt]# 
[root@clear opt]# chmod g+s dir1
[root@clear opt]# ll -d dir1
drwxr-sr-x. 2 student student 22 Nov 19 04:51 dir1
[root@clear opt]# touch dir1/root1.txt
[root@clear opt]# ll dir1/root1.txt
-rw-r--r--. 1 root student 0 Nov 19 04:52 dir1/root1.txt

数字修改法： chmod 2755 dir1


SBIT（粘滞位） 1
该权限通常设置在目录上，设置了该权限的目录，其他用户在该目录中只能删除所有者是自己的文件。
[root@clear opt]# rm -rf *
[root@clear opt]# ls
[root@clear opt]# mkdir share
[root@clear opt]# chmod 777 share/
[root@clear opt]# id student
uid=1000(student) gid=1000(student) groups=1000(student),10(wheel)
[root@clear opt]# useradd tom
[root@clear opt]# id tom
uid=1005(tom) gid=1005(tom) groups=1005(tom)
[root@clear opt]# su - student
Last login: Sat Nov 19 04:46:47 EST 2022 on pts/0
[student@clear ~]$ touch /opt/share/student.txt
[student@clear ~]$ logout
[root@clear opt]# su - tom
[tom@clear ~]$ rm -f /opt/share/student.txt
[tom@clear ~]$ logout
[root@clear opt]# chmod 1777 /opt/share/
[root@clear opt]# ll -d /opt/share/
drwxrwxrwt. 2 root root 6 Nov 19 04:56 /opt/share/
[root@clear opt]# su - student
Last login: Sat Nov 19 04:55:24 EST 2022 on pts/0
[student@clear ~]$ touch /opt/share/student.txt.haha
[student@clear ~]$ logout
[root@clear opt]# su - tom
Last login: Sat Nov 19 04:55:38 EST 2022 on pts/0
[tom@clear ~]$ rm -f /opt/share/student.txt.haha
rm: cannot remove '/opt/share/student.txt.haha': Operation not permitted


check：
创建两个不同用户登录操作系统，进入dirt目录分别创建文件，尝试互相删除对方文件，结果应不能互相删除文件。

大S和小s区别，
执行权限位大S是，没有x
执行权限位小s是该位，有x
```

# 第八章 进程监控及管理

```
yum install -y psmisc
pstree -p
一程序被开启会产生一个或多个进程，他们都有对应父进程与子进程，每个进程都有进程号PID
systemd 1 不能被杀死，除非重启，关机。
```

## 1.ps

```bash
以静态的方式查看系统进程
ps -l
ps  aux  
ps aux | grep http

[root@servera ~]# ps -l
F S   UID   PID  PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
4 S     0 27392 27367  0  80   0 - 85532 -      pts/0    00:00:00 su
4 S     0 27396 27392  0  80   0 - 59008 -      pts/0    00:00:00 bash
4 T     0 27822 27396  1  80   0 - 63962 -      pts/0    00:00:00 vim
0 R     0 27823 27396  0  80   0 - 63625 -      pts/0    00:00:00 ps
```

### 练习：

```
查看httpd进程
1、【node1】：
yum install -y httpd
2、systemctl start httpd
3、ps aux |grep httpd
4、【foundation】
firefox  172.25.250.10
```

```bash
模拟考试环境yum源配置：
[root@node1 /]# cat /etc/yum.repos.d/rhel.repo 
[rhel]
name=rhel
baseurl=http://foundation0.ilt.example.com/dvd/BaseOS
gpgcheck=0
[rhel1]
name=rhel1
baseurl=http://foundation0.ilt.example.com/dvd/AppStream
gpgcheck=0

```

## 2.top

```
以同态的形式查看进程
top
M
P
h
k  pid    9/15  
q 退出
```

## 终止进程

```
kill

语法：
kill -s 信号名称 或-n 信号编号

Options:
-s sig	SIG is a signal name
-n sig	SIG is a signal number

例子:
kill -s SIGKILL httpd
kill -n 9 httpd   # 或 kill -9 httpd

练习：
[root@node1 /]# kill -l
 1) SIGHUP	 2) SIGINT	 3) SIGQUIT	 4) SIGILL	 5) SIGTRAP
 6) SIGABRT	 7) SIGBUS	 8) SIGFPE	 9) SIGKILL	10) SIGUSR1

[root@node1 /]# vim 1 &
[1] 1372
[root@node1 /]# ps -l
F S   UID     PID    PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
0 S     0    1313    1312  0  80   0 - 59084 -      pts/1    00:00:00 bash
0 T     0    1372    1313  4  80   0 - 60816 -      pts/1    00:00:00 vim
0 R     0    1373    1313  0  80   0 - 63799 -      pts/1    00:00:00 ps

[1]+  Stopped                 vim 1
[root@node1 /]# kill -n 9 1372
[root@node1 /]# ps -l
F S   UID     PID    PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
0 S     0    1313    1312  0  80   0 - 59084 -      pts/1    00:00:00 bash
0 R     0    1374    1313  0  80   0 - 63799 -      pts/1    00:00:00 ps
[1]+  Killed                  vim 1


killall
语法：
killall 守护进程名称

#yum install -y httpd
systemctl start httpd
ps aux | grep httpd
killall httpd
yum provides killall
yum install -y psmisc-23.1-3.el8.x86_64
killall httpd
ps aux | grep httpd

pkill
```

## 3.作业控制jobs

```bash
[root@servera ~]# vim file2   （ctrl+z）

[1]-  Stopped                 vim file1

[2]+  Stopped                 vim file2
[root@servera ~]# jobs
[1]-  Stopped                 vim file1
[2]+  Stopped                 vim file2

[root@servera ~]# dd if=/dev/zero of=./bigfile bs=1M count=1000
ctrl + z 
[root@servera ~]# jobs
[1]+  Stopped                 dd if=/dev/zero of=./bigfile bs=1M count=1000
[root@servera ~]# bg ％1
[1]+ dd if=/dev/zero of=./bigfile bs=1M count=1000 &
[root@servera ~]# jobs
[1]+  Running                 dd if=/dev/zero of=./bigfile bs=1M count=1000 &
[root@servera ~]# fg %1
[root@servera ~]# 
1000+0 records in
1000+0 records out
1048576000 bytes (1.0 GB, 1000 MiB) copied, 65.0049 s, 16.1 MB/s


[root@servera ~]# kill -9 %2
[2]-  Stopped                 vim file2
[root@servera ~]# jobs
[2]-  Killed                  vim file2
[3]+  Stopped                 nice -n -10 vim file4
```

## 4.进程优先级调整

```
nice值
超级用户root 可以修改nice值范围 -20~19
普通用户user 可以修改nice值范围  0-19

进程优先级数字越小，优先级越高。
优先级不能直接改，可以通过nice值来影响优先级。

旧优先级 + nice值 = 新优先级
80       -10   = 70

两种方法：
一、产生新进程时，设置nice值
nice  -n -5  vim file2 &
二、修改现有进程nice值
renice -n  10  PID
ps -l 查看需要更改的进程号
renice -n 10 28183
```



# 第九章 控制服务与守护进程

## 1.服务状态关键字段

| 字段     | 描述                           |
| -------- | ------------------------------ |
| Loaded   | 服务单元是否加载到内存         |
| Active   | 服务单元是否在运行，运行了多久 |
| Main PID | 服务的主进程ID，包括命令名称   |
| Status   | 有关该服务的其他信息。         |

```bash
systemctl -t help

列入.service扩展名，代表服务，如web服务
systemctl list-units --type service  列出当前服务器加载的服务单元
systemctl  status  httpd.service   查看某个服务



# yum install -y httpd  安装apache服务
服务运行状态
[root@servera system]# systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor pr>
   Active: inactive (dead)
     Docs: man:httpd.service(8)
     
[root@servera system]# systemctl start httpd     
[root@servera system]# systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor pr>
   Active: active (running) since Sat 2020-02-29 04:34:47 CST; 1s ago

查看服务是否启动
[root@servera system]# systemctl  is-active httpd
active
查看服务是否开机启动
[root@servera system]# systemctl enable httpd
[root@servera system]# systemctl is-enabled httpd
enabled
[root@servera system]# systemctl disable httpd
Removed /etc/systemd/system/multi-user.target.wants/httpd.service.
[root@servera system]# systemctl is-enabled httpd
disabled

常见特征（了解）：
1、安装  yum install -y httpd
2、启动  systemctl  start  httpd.service （单元文件）/usr/lib/systemd/system/
3、查进程 ps aux | grep httpd , 每个服务有自己的守护进程/usr/sbin/httpd
4、查端口 netstat -ntlp ，找到80端口，对应Listen监听状态 对应httpd服务

vim /etc/service  改文件记录了系统服务的端口和协议的对应关系
```

## 2.服务状态分类（了解）

| 关键字            | 描述                                     |
| ----------------- | ---------------------------------------- |
| loaded            | 单元配置文件已处理                       |
| active（running） | 正在通过一个或多个持续进程与运行         |
| active（exited）  | 已成功完成一次性配置                     |
| active（waiting） | 运行中，但正在等待事件                   |
| inactive          | 不在运行                                 |
| enabled           | 在系统引导时启动                         |
| disabled          | 未设为在系统引导时启动                   |
| static            | 无法启动，但可以由某一启动的单元自动启动 |

## 3.管理系统服务（考点）

| 语法：systemctl  管理命令  unitname |                               |
| ----------------------------------- | ----------------------------- |
| 管理命令                            | 描述                          |
| status                              | 查看状态                      |
| start                               | 开启                          |
| stop                                | 关闭                          |
| restart                             | 重启                          |
| reload                              | 加载配置文件                  |
| enable                              | 开机启动                      |
| disable                             | 关闭开机启动                  |
| is-active                           | 查看服务状态是否启动          |
| is-enabled                          | 查看服务是否开机自启动        |
| list-dependencies  【unitname】     | 查看单元依赖                  |
| mask                                | 禁止服务，无法启动或开机 启动 |
| unmask                              | 解除mask                      |

### 练习

```bash
练习时，重启服务前，先关闭以下两个应用
setenforce 0 关闭seliunx
systemctl stop firewalld  关闭防火墙 


yum install -y httpd
systemctl start httpd.service
systemctl status httpd
ps aux |grep httpd
netstat -ntlp
systemctl is-active httpd
systemctl is-enabled httpd
systemctl status httpd
systemctl --help
systemctl --help | grep \\--system
systemctl start httpd
systemctl --system start httpd
systemctl --help| grep \\--user
systemctl enable httpd
systemctl status httpd
systemctl disable httpd
systemctl status httpd
systemctl mask httpd  注销服务
systemctl unmask httpd  取消注销
systemctl enable --now httpd  开启服务并且开机自启动

  
可以用做练习的服务httpd，sshd，autofs，samba。
ftp服务器服务开机自启
1、安装 vsftpd
2、启动  vsftpd.service
3、设置开机自启
  
寻找service文件的方法：
[root@servera ~]# rpm -qa | grep autofs
libsss_autofs-2.4.0-9.el8.x86_64
autofs-5.1.4-48.el8.x86_64
[root@servera ~]# rpm -ql autofs | grep service
[root@servera ~]# rpm -ql autofs | grep service
/usr/lib/systemd/system/autofs.service
```

# 第十章 OPENSSH服务

## 1.ssh的常用功能

```bash
[root@servera ~]# ssh serverb
root@serverb's password: 
[root@servera ~]# vim /etc/hosts  或者系统是否做了dns,ip和域名及主机名的映射
[root@servera ~]# ssh 172.25.250.11
[root@servera ~]# ssh root@172.25.250.11

[root@servera opt]# scp rhcetext root@172.25.250.11:/
root@172.25.250.11's password: 
rhcetext                                                 100%    0     0.0KB/s   00:00

[root@serverb /]# scp root@172.25.250.10:/opt/newfile  .
root@172.25.250.10's password: 
newfile                                                  100%    0     0.0KB/s   00:00 

[root@servera opt]# ssh root@172.25.250.11 'yum install -y httpd'
ssh root@172.25.250.11 'yum install -y httpd'
```

## 2.ssh免密登录

```bash
【servera】
[root@servera ssh]# ssh-keygen  后面三个回车
[root@servera ssh]# ssh-copy-id  root@serverb
【serverb】
[root@serverb /]# cd /root/.ssh/
[root@serverb .ssh]# ls
authorized_keys  known_hosts
【servera】
[root@servera ssh]# ssh root@serverb

a免密远程b，如果想b远程a免密，需要相同的配置

课上练习：b远程免密登录a
```

## 3.ssh服务控制 

拒绝root登录

```bash
[root@serverb ~]# vim /etc/ssh/sshd_config
PermitRootLogin no
[root@serverb ~]# systemctl reload sshd（或restart）

[root@servera ~]# ssh root@serverb
```

### 4.sudo

```bash
一、将用户设置为特权用户
1、
[student@servera ~]$ yum remove -y httpd
Error: This command has to be run under the root user.

2、
[root@servera /]# vim /etc/sudoers    或者 visudo
## Allow root to run any commands anywhere 
root    ALL=(ALL)       ALL
student ALL=(ALL)       ALL

3、
[student@servera ~]$ sudo yum remove -y httpd
[sudo] password for student: student

二、将账号添加到特权用户组中，培训环境默认特权用户组是wheel组，在/etc/sudoers文件中用%wheel来表示
usermod -G wheel tom

三、练习：
添加一个特权组admin，而且组内有一个成员是harry。最终harry账号应当为特权账号。
[root@serverb ~]# groupadd admin
[root@serverb ~]# visudo
[root@serverb ~]# useradd -G admin  harry
[root@serverb ~]# su - harry
[harry@serverb ~]$ sudo -i
[sudo] password for harry: 

四、设置特权组中用户切换时不需要密码
%admin  ALL=(ALL)       NOPASSWD: ALL
```

# 第十一章 日志分析与存储

## 一 .系统中的日志文件

| 日志文件          | 存储的消息类型                           |
| ----------------- | ---------------------------------------- |
| /var/log/messages | 大多数系统日志消息处存放处。             |
| /var/log/secure   | 与安全性和身份验证时间相关的syslog消息。 |
| /var/log/maillog  | 与邮件服务器相关的syslog消息。           |
| /var/log/cron     | 与计划任务执行相关的syslog消息           |
| /var/log/boot.log | 与系统启动相关的消息。                   |

### 2.rsyslog服务管理的日志配置文件

```bash
[root@haha log]# yum provides /etc/rsyslog.conf  #查看文件是哪个软件包提供的
[root@clear log]# rpm -qc rsyslog-8.1911.0-3.el8.x86_64 
/etc/logrotate.d/syslog
/etc/rsyslog.conf       #一般服务文件以.conf结尾，改文件是日志服务的配置文件
/etc/sysconfig/rsyslog
[root@clear log]# vim /etc/rsyslog.conf
```

### 3.记录日志的规则

```bash
日志文件配置格式:
mail.info  /var/log/vsftpd.log  #.点代表包含后面级别及以上级别

AAAA.BBBB CCCC

AAAA 产生日志的设备（类别）   #如何产生的日志
BBBB 日志的级别             #日志有不同安全级别，类似轻重缓急的严重程度，发出警告
CCCC 保存日志的位置          #在系统中保存日志文件的路径
```

### 4.rsyslog配置文件类别（产生日志的设备）

| 类别(facility) |                     |
| -------------- | ------------------- |
| Kern           | 内核                |
| authpriv       | 授权和安全          |
| cron           | 计划任务            |
| mail           | 邮件                |
| daemon         | 系统守护进程        |
| user           | 普通用户级别的      |
| syslog         | 由rsyslog生成的信息 |
| local0~local7  | 自定义本地策略      |
| *              | 所有类别            |

### 5.日志的等级（日志级别）

```
man 3  syslog
```

| 等级            | 解释                       |
| --------------- | -------------------------- |
| EMERG（紧急）   | 会导致主机系统不可用的情况 |
| ALERT（警告）   | 必须马上采取措施解决的问题 |
| CRIT（严重）    | 比较严重的情况             |
| ERR（错误）     | 运行出现错误               |
| WARNING（提醒） | 可能会影响系统功能的事件   |
| NOTICE（注意）  | 不会影响系统但值得注意     |
| INFO（信息）    | 一般信息                   |
| DEBUG（调试）   | 程序或系统调试信息等       |
| *               | 所有等级                   |
| none            | 不记录日志                 |

```bash
解释：
*.info;mail.none;authpriv.none;cron.none                /var/log/messages
*.info   *所有设别    .点代表后面的等级及以上等级，也就是info以上的等级全记录
；分号是不同设备等级的分隔符号
mail.*                                                  -/var/log/maillog
- 代表先记录缓存，再记录硬盘，减轻硬盘i/o读写压力。
```

### 6.使用logger发送测试日志信息（考点）

```bash
1 查看rsyslog服务是否开启 (默认系统已开启)
[root@servera ~]# systemctl status rsyslog
2 编辑rsyslog配置文件
[root@servera ~]# vim /etc/rsyslog.conf
# Save boot messages also to boot.log
local7.*                                                /var/log/boot.log
.
.
*.debug                                                 /var/log/messages.debug
3 重启rsyslog日志服务让配置生效
[root@servera ~]# systemctl restart rsyslog

4 开另一个窗口 ctrl+shift+t
[root@servera ~]# tail -n 0 -f /var/log/messages.debug   

5 使用logger命令生成一个user类别，debug级别的日志内容为“Debug test messages”  #（考点）
[root@servera ~]# logger -p user.debug "Debug test messages"    

6 在第4步的窗口中查看新生成日志信息
[root@servera ~]# tail -n 0 -f /var/log/messages.debug
Jun 18 14:45:31 servera root[29174]: messages haha

```

## 二.journalctl知识点

```bash
传统日志服务rsyslog。而新添加的日志服务是systemd-journal，它也是一个日志管理服务，可以收集来自内核、系统早期启动阶段的日志，以及系统进程在启动和运行中的一些标准输出与错误输出。此日志一旦重启既消失，因为保存在了/run/log/journal/*/*.journal结尾，该文件是一个二进制日志文件，需要用journalctl命令查看。

tail -n 5

journalctl 查看系统日志
journalctl -n   通过q或ctrl接触观看  ，此命令显示方式类似与tail -n
journalctl -n 5 

journalctl -p err  日志等级
journalctl -f 
journalctl -p err 
journalctl -p info
  （deubg、info、notice、warning、err、crit、alert、emerg）

journalctl --since "2020-02-28 22:53:35" --until "2020-02-28 22:53:40"
```

| 常用字段     | 含义                    |
| ------------ | ----------------------- |
| _COMM        | 命令名称                |
| _EXE         | 进程的可执行文件的路径  |
| _PID         | 进程的PID               |
| _UID         | UID                     |
| _SYSTEM_UNIT | 启动该进程的systemd单元 |

```bash
journalctl -o verbose 
journalctl _HOSTNAME=localhost
journalctl _HOSTNAME=localhost _PID=1
```

### 1.设置永久保存journal服务文件方式（考点）

```bash
mandb
man -k journal
man 5 journald.conf  ， Storage=

[root@clear journal]# ll -d /run/log/journal
[root@clear journal]# cp -a /run/log/journal/ /var/log/journal
[root@clear journal]# ll -d /var/log/journal
drwxr-sr-x. 4 root systemd-journal 86 Nov 19 04:30 /var/log/journal
[root@clear journal]# systemctl restart systemd-journald
[root@clear journal]# ll /var/log/journal/3a2b4da8dabb4729935c193e58ad052d/  #字符串目录名字每个人的可能不一样。不要复制我的笔记。
total 8192
-rw-r-----. 1 root root 8388608 Nov 19 04:30 system.journal
[root@clear journal]# journalctl
```

## 三.保持准确的系统时间

```
RHEL6 ntp服务
RHEL8 chrony服务
还是使用同样的协议标准ntp（network time protocol）

UTC：通用协调时
   （UTC时间0点是北京时间8点,因为中国、新加坡、马来西亚、菲律宾等国的时间与UTC的时差均为+8,也就是UTC+8,所以当UTC时间0点,北京时间即为0+8=8点）
GMT：格林威治标准时间
CST：中国标准时间 (China Standard Time）
    （中国大陆、中国香港、中国澳门、中国台湾、蒙古国、新加坡、马来西亚、菲律宾、西澳大利亚州的时间与UTC的时差均为＋8，也就是UTC+8。）
RTC：(Real-Time Clock)也称为硬件时间：RTC是芯片内置的硬件时钟，只要芯片不断电，即使操作系统关机的时候，RTC时钟也是正常在走的，所以当操作系统关机重启后，可通过读取RTC时间来更新系统时间。
    （可通过hwclock命令来获取具体的时间 
      -r 查看硬件时间
      -s 硬件时间设置到系统
      -w 系统设置到硬件 ）
```

```bash
[root@servera log]# timedatectl 
               Local time: Sat 2020-02-29 08:51:49 CST
           Universal time: Sat 2020-02-29 00:51:49 UTC
                 RTC time: Sat 2020-02-29 08:11:07
                Time zone: Asia/Shanghai (CST, +0800)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
          
[root@servera log]# timedatectl list-timezones     
[root@servera log]# timedatectl set-timezone Asia/Hong_Kong 
[root@servera log]# timedatectl 
               Local time: Sat 2020-02-29 08:55:48 HKT
           Universal time: Sat 2020-02-29 00:55:48 UTC
                 RTC time: Sat 2020-02-29 08:15:06
                Time zone: Asia/Hong_Kong (HKT, +0800)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no

修改时间方法
timedatectl set-time "2020-02-30 10:00:00"
Failed to set time: NTP unit is active
timedatectl set-ntp false 
timedatectl set-time "2020-02-30 10:00:00"
timedatectl set-ntp true 
```

### 1.chrony（考点）

```
server
===server选项格式===
server host  [ key n ] [ version n ] [ prefer ] [ mode n ] [ minpoll n ] [ maxpoll n ] [ iburst ]
其中host是上层NTP服务器的IP地址或域名，随后所跟的参数解释如下所示：
◆ key： 表示所有发往服务器的报文包含有秘钥加密的认证信息，n是32位的整数，表示秘钥号。
◆ version： 表示发往上层服务器的报文使用的版本号，n默认是3，可以是1或者2。
◆ prefer： 如果有多个server选项，具有该参数的服务器有限使用。
◆ mode： 指定数据报文mode字段的值。
◆ minpoll： 指定与查询该服务器的最小时间间隔为2的n次方秒，n默认为6，范围为4-14。
◆ maxpoll：  指定与查询该服务器的最大时间间隔为2的n次方秒，n默认为10，范围为4-14。
◆ iburst： 当初始同步请求时，采用突发方式接连发送8个报文，时间间隔为2秒。
```



```bash

[root@servera ~]# systemctl enable --now chronyd    #--now启动服务 enable 开机自动
[root@servera ~]# systemctl status chronyd
[root@servera ~]# vim /etc/chrony.conf   #大概第7行 server 后面添加服务器'地址'或'域名'。
server classroom.exmaple.com iburst
[root@servera ~]# systemctl  restart chronyd.service
[root@servera ~]# chronyc sources -v
210 Number of sources = 1

  .-- Source mode  '^' = server, '=' = peer, '#' = local clock.
 / .- Source state '*' = current synced, '+' = combined , '-' = not combined,
| /   '?' = unreachable, 'x' = time may be in error, '~' = time too variable.
||                                                 .- xxxx [ yyyy ] +/- zzzz
||      Reachability register (octal) -.           |  xxxx = adjusted offset,
||      Log2(Polling interval) --.      |          |  yyyy = measured offset,
||                                \     |          |  zzzz = estimated error.
||                                 |    |           \
MS Name/IP address         Stratum Poll Reach LastRx Last sample               
===============================================================================
^* classroom.example.com         8   6   377    25  -3837ns[  +21us] +/-  627us

[root@node1 ~]# timedatectl 
               Local time: Sat 2022-11-19 05:03:30 EST
           Universal time: Sat 2022-11-19 10:03:30 UTC
                 RTC time: Sat 2022-11-26 06:07:04
                Time zone: America/New_York (EST, -0500)
System clock synchronized: yes   #表示时间服务同步
              NTP service: active
          RTC in local TZ: no
```

# 第十二章 RHEL网络管理

## 一.认识IPv4地址

|      |                      |                                                      |
| ---- | -------------------- | ---------------------------------------------------- |
| 1    | IP/(NETMASK\|PREFIX) | 172.25.0.9/255.255.0.0 \| 172.25.0.9/16              |
| 2    | GATEWAY              | 172.25.x.x                                           |
| 3    | DNS                  | 正向解析 # host servera， 反向解析 # host 172.25.0.9 |

```bash
私有地址：
A　１－１２７
B　１２８－１９１
C　１９２－２２３

IP地址分类默认对应的子网掩码掩码：
Ａ：２５５．０．０．０        11111111.00000000.00000000.00000000     /8
Ｂ：２５５．２５５．０．０     11111111.11111111.00000000.00000000     /16
Ｃ：２５５．２５５．２５５．０  11111111.11111111.11111111.00000000     /24


```

### 网段：IP与掩码二进制与运算 

| 网络地址 | 172.25.0.0     | 主机位全0 |
| -------- | -------------- | --------- |
| 广播地址 | 172.25.255.255 | 主机位全1 |

### 查看ip4,ip6

```bash
查看ip地址方法1：
[root@servera ~]#ifconfig 
[root@servera ~]#ifconfig  eth0
查看ip地址方法2：
[root@servera ~]# ip addr show eth0
[root@servera ~]# ip a  s eth0
[root@servera ~]# ip -s link show enp1s0 
```

ipv4 ipv6 mac

|              |  ipv4  |   ipv6   |   mac    |
| :----------: | :----: | :------: | :------: |
| 二进制（位） |   32   |   128    |    48    |
|  符号（分）  |   .    |    :     |    :     |
|     进制     | 十进制 | 十六进制 | 十六进制 |
|      组      |   4    |    8     |    6     |

### 2.端口与服务

```bash
查看服务端口是否被占用
lsof -i:80
lsof -i:80
或
netstat
-n：显示接口和端口编号
-t：tcp信息
-u：udp信息
-l：监听状态信息
-a：显示所有信息
-p：显示协议名称而不是端口

netstat -ntlp | grep 22
netstat -ntlp | grep 80

参考ss和netstat区别：https://blog.csdn.net/qq_37863891/article/details/107283415
```

### 3.标准服务端口

```bash
/etc/services
```

## 二.网络管理工具（考点）

### 1.nmcli

```bash
使用nmcli管理网络服务NetworkManager，从rhel8开始便不提供network服务的支持前者为替代品
nmcli工具功能：
	查看网络设备
	创建网络连接
	修改网络配置

特点及概念：
nmcli工具 可以对网卡操作，或网卡配置文件操作
device ---- 网卡设备
connection --- 连接  指的就是网卡配置文件
一个device可以拥有多个connection，同一时间只能启用一个connection，且一个connection只能属于一个device

举例：
device-----eth0
connection1  ----  dhcp    自动获取IP  家
connection2  ----  static  静态IP   公司
```

使用nmcli管理网络

```bash
mandb
man -k nmcli
nmcli (1) 
nmcli-examples (7)
man nmcli | grep -A 2 'nmcli connection add'



-basic
[root@servera ~]# nmcli connection show 
[root@servera ~]# nmcli connection show --active 
[root@servera ~]# nmcli device status 

-add
dhcp方式: #创建一个名为default的手动链接，绑定至eth0网卡。
[root@servera ~]# nmcli connection add  con-name 'default' type ethernet ifname eth0  autoconnect yes    
[root@servera ~]# nmcli con show  #查看一下新生成的链接，#练习环境中没有dhcp服务器，无法测试该方法

静态：#创建一个名为static的静态链接，绑定至eth1网卡。（考点必须掌握）
[root@servera ~]# nmcli connection add con-name static type ethernet ifname eth0 autoconnect yes ipv4.addresses 192.168.0.1/24 ipv4.gateway 192.168.0.254 ipv4.dns 8.8.8.8 ipv4.method manual  
[root@servera ~]# nmcli connection show
NAME                UUID                                  TYPE      DEVICE 
Wired connection 1  1f5ad5ae-e926-3f54-9805-33174e63af47  ethernet  eth0   
static              980f6712-86b7-4d92-bc84-62e677ccabfc  ethernet  eth1     #此处
dhcp                82c4a93f-1ca2-432b-94da-59a6c4f5aaca  ethernet  --     
Wired connection 2  e801f880-78a6-3344-857f-588f7495bb26  ethernet  --     
[root@servera /]# nmcli connection up static   启动static网卡
[root@servera /]# ip a s eth1 | grep -w inet

-modify
#将链接static网络信息更改： IP：192.168.0.2  mask：/24  gw：192.168.0.200 dns：114.114.114.114
[root@servera ~]# nmcli connection modify static ipv4.addresses 192.168.0.2/24 ipv4.gateway 192.168.0.200 ipv4.dns 114.114.114.114 autoconnect yes ipv4.method manual
[root@servera ~]# nmcli connection up  static
[root@servera ~]# ip a s eth0
    inet 192.168.0.2/24 brd 192.168.0.255 scope global noprefixroute eth1
   
[root@servera ~]# route -n
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         172.25.250.254  0.0.0.0         UG    100    0        0 eth0
0.0.0.0         192.168.0.200   0.0.0.0         UG    101    0        0 eth1

[root@servera ~]# cat /etc/resolv.conf 
# Generated by NetworkManager
search lab.example.com example.com
nameserver 172.25.250.254
nameserver 114.114.114.114
 
[root@serverb ~]# nmcli connection delete static  #删除一个链接
Connection 'static' (b85e6a57-b8f7-421f-8d15-9ff5e27cbb85) successfully deleted.

-up_down
[root@servera /]# nmcli connection down static   关闭static网卡

-off
[root@servera /]# nmcli networking off       关闭网络服务，慎重使
```

### 2.图形化管理工具 nmtui

```bash
nmtui-edit
图形化管理配置
通过点击设置--network--网卡设置，ipv4address  netmask  dns  gateway
nmtui-edit
[root@servera /]# nmcli connection up static
```

### 3.网卡配置文件（必考）

```bash
# grep -r IPADDR /usr/share/ #找到下面手册的指令
vim /usr/share/doc/initscripts/sysconfig.txt 帮助手册

修改配置文件方式修改IP
[root@servera ~]# vim /etc/sysconfig/network-scripts/ifcfg-Wired_connection_1

BOOTPROTO=none       #获取IP的方式  static 静态 none不设置 dhcp自动获取 ，手动配IP选前两个中任意一个
ONBOOT=yes           #开机自动连接
IPADDR=172.25.250.100   #ip地址
PREFIX=24               #子网掩码  PREFIX=24 （等效于255.255.255.0） ，mask=255.255.255.0 netmask=255.255.255.0
GATEWAY=172.25.250.254  #网关
DNS1=xxxx               #dns，dns可以有三个 DNS1= DNS2= DNS3=

加载网卡配置文件方法一：
[root@serverb network-scripts]# nmcli connection reload  ifcfg-Wired_connection_1 或 nmcli connection reload
[root@serverb network-scripts]# nmcli connection up static

systemctl restart NetworkManager
```

## 三 更改网络信息

### 1 主机名（考点）

```bash
[root@servera ~]# hostname
servera.lab.example.com
[root@servera ~]# hostnmae  www.example.com 临时
[root@servera ~]# vim /etc/hostname  永久（重启系统，reboot ，init 6）
[root@servera ~]# hostnamectl set-hostname hostname　永久
```

### 2 配置网关（gateway）

```bash
一、使用nmcli
nmcli con add con-name xxx ipv4.gateway xxx.xxx.xxx.xxx  配置网关
nmcli con mod xxx ipv4.gateway xxx.xxx.xxx.xxx			修改网关
以上两种改完之后，需要nmcli con up xxx
二、修改配置文件
vim /etc/sysconfig/network-scripts/ifcfg-xxxx
GATEWAY=xxx.xxx.xxx.xxx  
修改完后要nmcli con reload ，再nmcli con up xxxx
```

### 3 查看路由及网关信息

```bash
[root@servera ~]# ip route 
default via 172.25.250.254 dev enp1s0 proto static metric 100 
172.25.250.0/24 dev enp1s0 proto kernel scope link src 172.25.250.10 metric 100

[root@servera ~]# route -n
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         172.25.250.254  0.0.0.0         UG    100    0        0 enp1s0
172.25.250.0    0.0.0.0         255.255.255.0   U     100    0        0 enp1s0

[root@servera ~]# nmcli connection show Wired\ connection\ 1 | grep ipv4.ga
ipv4.gateway:                           172.25.250.254

[root@servera ~]# netstat -nr
Kernel IP routing table
Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
0.0.0.0         172.25.250.254  0.0.0.0         UG        0 0          0 enp1s0
172.25.250.0    0.0.0.0         255.255.255.0   U         0 0          0 enp1s0
```

### 4 指定DNS

```bash
vim /etc/resolv.conf   #该文件指定dns和域名的 /etc/sysconfig/network-scripts/ifcfg-xxx中的DNS字段会同步到/etc/resolv.conf，后者优先。
nameserver 172.25.250.254
```

### 5 测dns域名解析是否正常

```bash
[root@servera ~]# host classroom.example.com  （至少掌握1个）
classroom.example.com has address 172.25.254.254
[root@servera ~]# nslookup classroom.example.com
Server:		172.25.250.254
Address:	172.25.250.254#53

Name:	classroom.example.com
Address: 172.25.254.254

[root@servera ~]# dig classroom.example.com
```

# 第十三章 归档与系统间复制文件

## **一 归档及压缩** （考点）

```bash
语法：
tar 选项  归档文件名    源文件 源文件2 源文件N
-c  创建
-t  查看
-f  指定文件名
-v  显示详细信息
-x  解包
-C  指定解包路径

例：
man tar
tar -cvf /root/etc.tar /etc/
```

### 1.文件及目录打包、解包

```bash
将文件打包归档
[root@servera opt]# touch file{1..3}
[root@servera opt]#
etc.tar  file1  file2  file3
[root@servera opt]# tar -cvf file.tar file1 file2 file3
file1
file2
file3
[root@servera opt]# tar -tf file.tar
file1
file2
file3

[root@servera opt]# ls
etc.tar  file1  file2  file3  file.tar

为文件解包
[root@servera opt]# tar -xvf file.tar -C /tmp/
file1
file2
file3
[root@servera opt]# ls /tmp/
file1  rclocal.log
file2  rht-bastion
file3  rht-default
NIC1   rht-vm-hosts
NIC2   systemd-private-ef2feb022cd2465c9dd920878a1d962b-chronyd.service-kRKFp0
[root@servera opt]# 

将目录打包归档
[root@servera opt]# tar -cvf etc.tar /etc
[root@servera opt]# ls
etc.tar  file1  file2  file3  file.tar
[root@servera opt]# cp etc.tar /home
[root@servera opt]# cd /home 

为目录解包
[root@servera opt]# tar -xvf etc.tar 
```

### 2.压缩

```bash
只压缩文件：
[root@servera opt]# gzip file1
[root@servera opt]# ls
etc.tar  file1.gz  file2  file3  file.tar
[root@servera opt]# file file1.gz 
file1.gz: gzip compressed data, was "file1", last modified: Sun Mar  1 05:54:06 2020, from Unix, original size 0
[root@servera opt]# bzip2 file2
[root@servera opt]# ls
etc.tar  file1.gz  file2.bz2  file3  file.tar
[root@servera opt]# file file2.bz2 
file2.bz2: bzip2 compressed data, block size = 900k
[root@servera opt]# xz file.tar 
[root@servera opt]# ls
etc.tar  file1.gz  file2.bz2  file3  file.tar.xz
```

### 3.tar打包并压缩（考点）

```bash
tar的压缩选项
man tar | grep gzip
-z  gzip
-j	bzip2
-J	xz
打包并压缩
 tar -zcvf /root/etc.tar.gz /etc/
   47  cd /root/
   48  ls
   49  file etc.tar.gz 
   50  tar -jcvf /opt.tar.bz2 /opt/
   51  ls /
   tar -Jcvf /root/etc.tar.gz /etc/

解包解压缩并指定路径
tar -zxvf etc.tar.g 
tar -zxvf etc.tar.gz -C /opt/
tar xf etc.tar.gz -C /opt/
```

## 二 远程传输

### 1.使用scp实现远程文件传输

```bash
# scp servra.txt root@bastion:/opt/
# ls
# scp root@bastion:/opt/bastion.txt .
# ls
```

### 2.使用sftp实现远程文件传输

|  ID  |  app  | roles          |
| :--: | :---: | -------------- |
|  1   |  ftp  | client         |
|  2   | sftp  | ssh SubService |
|  3   | vsftp | service        |

```bash
sftp instructor@classroom.example.com
instructor@classroom.example.com's password:   Asimov
sftp> cd /tmp
sftp> ls
NIC1                                                          NIC1.old                                                       NIC2                                                           NIC2.old                                                         
sftp> get testfile.txt 
Fetching /tmp/testfile.txt to testfile.txt
sftp> exit
[root@servera opt]# 
[root@servera opt]# ls
etc  testfile.txt

[root@servera opt]# touch put.txt
[root@servera opt]# sftp instructor@classroom.example.com
instructor@classroom.example.com's password: 
Connected to instructor@classroom.example.com.
sftp> cd /tmp/
sftp> put /opt/put.txt 
Uploading /opt/put.txt to /tmp/put.txt
/opt/put.txt                                    100%    0     0.0KB/s   00:00    
sftp> ls
NIC1                  
NIC1.old                   
NIC2
NIC2.old
put.txt

```

### 3.使用rsync实现同步文件内容

```bash
-v  显示详细信息
-a  相当于存档模式
  
本地同步
[root@servera tmp]# rsync -av /var/log/* /tmp

远程同步
[root@servera tmp]# rsync -av /var/log/* serverb:/tmp
[root@servera tmp]# ssh root@serverb ls /tmp

问题：将serverb上的/var/log/同步到，servera当前目录下
[root@servera tmp]# rsync -av serverb:/var/log/  .
```

# 第十四章 安装和升级软件包

## 1.RPM包管理

### 1.rpm包语法

```bash
A--B--C


软件的获取方式：
1、互联网 （下载光盘镜像.iso，或直接使用网络yum源）
2、光盘

rpm包语法：
rpm 选项 包名
选项：
-i  安装
-v  显示过程
-h  以易读方式显示进度条
-e  卸载
例：
rpm -ivh  xxx.rpm
```

### 2.练习

```bash
1. 在f0中进入软件包的存储位置
[root@foundation0 /]# cd /content/rhel8.0/x86_64/dvd/AppStream/Packages/
[root@foundation0 Packages]# pwd
/content/rhel8.0/x86_64/dvd/AppStream/Packages
2.安装软件
[root@foundation0 Packages]# rpm -ivh lftp-4.8.4-1.el8.x86_64.rpm
```

### 3.rpm包查询命令（重点）

```bash
语法：
rpm -q 软件包名称
选项：
-q: query    查询  和其他参数配合
-l：list		列出软件包安装后给系统带来的所有文件
-a：all		查看所有已安装的软件包ee
-c: configure 查看软件包提供的配置文件
```

### 4.练习

```bash
[root@foundation0 dvd]# pwd
/content/rhel8.4/x86_64/dvd

f0---classroom--web---servera

【servera】
[root@servera yum.repos.d]# vim /etc/yum.repos.d/rhel_dvd.repo
 http://content.example.com/rhel8.4/x86_64/dvd/BaseOS
 http://content.example.com/rhel8.4/x86_64/dvd/AppStream
通过浏览器打开以上地址，可以查询到lftp或telnet相关软件，网页中ctrl+f搜索，输入lftp找到对应的软件，右键copy Link location，再到命令行中粘贴即可。
rpm -ivh http://foundation0.ilt.example.com/dvd/AppStream/Packages/telnet-0.17-73.el8.x86_64.rpme
rpm -q telnet
rpm -q ssh
rpm -q openssh
rpm -qa
rpm -qa | grep telnet
rpm -qa | grep ssh
rpm -ql telnet
rpm -qc telnet
rpm -qc openssh
rpm -qc openssh-server
rpm -qa  | grep ssh
rpm -qc openssh-server-8.0p1-4.el8_1.x86_64
vim /etc/ssh/sshd_config
rpm -qf /etc/ssh/sshd_config
rpm -qi openssh-server-8.0p1-4.el8_1.x86_64

卸载RPM包
[root@node1 /]# rpm -q telnet
telnet-0.17-73.el8.x86_64
[root@node1 /]# rpm -e telnet-0.17-73.el8.x86_64
[root@node1 /]# rpm -q telnet
package telnet is not installed
```

## 2.YUM工具 （考点）

### 1.管理yum源文件（vim编辑方法）

```bash
yum源软件配置方式
[root@servera /]# cd /etc/yum.repos.d/
[root@servera yum.repos.d]# mkdir old
[root@servera yum.repos.d]# mv * old     #将系统默认的yum源文件移动到old中，可以再该文件中查看原来的yum源路径
[root@servera yum.repos.d]# man 5 yum.conf
[root@servera yum.repos.d]# vim rhel.repo
[AppStream]  #id名称自定义
name=AppStream   #描述自定义，和id不必一样
baseurl=http://content.example.com/rhel8.4/x86_64/dvd/AppStream   #file:///中://是url格式，第三个/是根目录
gpgcheck=0   #gpgchek=1 要进行公钥验证，需要再添加选项gpgkey=http://content.example.com/rhel8.4/x86_64/dvd/RPM-GPG-KEY-redhat-release
enabled=1

[BaseOS]
name=BaseOS
baseurl=http://content.example.com/rhel8.4/x86_64/dvd/BaseOS
gpgcheck=0
enabled=1
[root@servera yum.repos.d]# yum clean all     #清除缓存，避免沿用之前缓存的软件
[root@servera yum.repos.d]# yum makecache     #和当前yum源建立缓存关联
[root@servera yum.repos.d]# yum repolist all  #查看当前yum源状态
[root@servera yum.repos.d]# yum install -y telnet  # 测试安装软件telnet
[root@servera yum.repos.d]# rpm -q telnet    #使用rpm方式查询测试
```

### 2.yum-config-manager（命令配置方法）

```bash
1 找到提供yum-config-manager命令的软件包名称
[foundation0]
[kiosk@foundation0 ~]$ yum provides yum-config-manager
dnf-utils-4.0.2.2-3.el8.noarch #发现提供yum-config-manager命令的包叫dnf-utils（8.0版本里的名字），如果8.4版本名称为yum-utils

2 安装yum-utils软件
打开浏览器输入yum源仓库地址，找到yum-utils的软件包，并且通过rpm命令安装网络上的yum-utils软件包，来提通yum-config-manager命令。
[root@servera ]# rpm -ivh http://content.example.com/rhel8.4/x86_64/dvd/BaseOS/Packages/yum-utils-4.0.18-4.el8.noarch.rpm

3  通过yum-config-manager命令部署yum源
[root@servera ]# yum-config-manager --help
[root@servera ]# yum-config-manager --add-repo=http://content.example.com/rhel8.4/x86_64/dvd/AppStream
[root@servera ]#  yum-config-manager --add-repo=http://content.example.com/rhel8.4/x86_64/dvd/BaseOS

4 命令制作的yum源中没有gpgcheck选项，如何配置？可以通过以下三种方法：
   1、此处可以在/etc/yum.repos.d/xx.repo文件里添加 gpgcheck=0 （推荐方案）
   2、或者配置/etc/yum.conf,配置gpgcheck=1，改为0 （只在练习考试时使用）
   3、或者rpm --import ‘公钥地址’   导入公钥`  （推荐方案）
[root@servera yum.repos.d]# rpm --import http://content.example.com/rhel8.4/x86_64/dvd/RPM-GPG- KEY-redhat-release  


培训环境里/etc/pki/rpm-gpg/保存了公钥
[root@servera ]# find / -name *KEY*
[root@servera ]# rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
[root@servera ]# yum clean all
[root@servera ]# yum repolist all

对称
非对称加密
公钥（解密）  私钥（加密）红帽发布软件包到互联网


开启或关闭
[root@servera /]# yum-config-manager --disable rhel-8.0-for-x86_64-appstream-rpms（yum 池ID）
[root@servera /]# yum repolist all
[root@servera /]# yum-config-manager --enable rhel-8.0-for-x86_64-appstream-rpms
[root@servera /]# yum repolist all
```

连接外网的方法：

```bash
让servera联网方法：
1、f0宿主机上ens192选择nat模式并获取IP，保证可以上网
2、在f0宿主机上执行ssh root@172.25.254.254 rht-config-nat
3、servera上ping www.baidu.com测试
```

### 3.练习

```bash
yum常见命令

yum list httpd
yum list http*
yum search httpd
yum search ssh
yum info httpd-manual
[root@servera /]# yum provides /var/www/html
yum update
yum install  包名
yum remove   包名
yum install -y httpd

[root@servera /]# yum install  -y autofs
[root@servera /]# yum remove -y autofs
[root@servera /]# yum history
yum clean all 清除缓存
yum list
yum repolist 
yum repolist all
servera:
yum group list
yum groupinfo 'Server with GUI'
yum groupinstall -y 'Server with GUI'
startx 切图形
```

## 3.第三方YUM源

```bash
【基础环境foundation】
1.虚拟机联网
虚拟机设置里面NAT选择已连接
nmcli connection up ens192
2.百度搜索（阿里源、华为源...），将.repo文件下载到系统
curl -o /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-vault-8.5.2111.repo
3.验证
cd /etc/yum.repos.d/;ls
yum repolist all
yum install -y vsftpd
```

# 第十五章 访问Linux文件系统

##  一 教学环境切换

```bash
红帽培训环境all in one （rh124 rh134 rh294）
rh124课程---切换rh124环境  课程和环境是对应
切换环境方法：
1 恢复init初始快照--环境就变成rh294了
2 【foundation0】 清除课程、并设置课程
[kiosk@foundation0 ~]$ rht-clearcourse 0  #清除课程  意义为清除掉当前的rh294课程
[kiosk@foundation0 ~]$ rht-setcourse rh124 #设置课程 意义为设置当前课程为rh124环境
.
.
.
Course set to rh124  #看到该提示证明切换成功
[kiosk@foundation0 ~]$ cat /etc/rht
RHT_VENUE=ilt
RHT_ENROLLMENT=0
RHT_ROLE=foundation
RHT_COURSE=rh124   #当前为rh124课程
[kiosk@foundation0 ~]$ for i in classroom bastion servera;do rht-vmctl start $i;done
```



## 二.存储管理概念

### 1.文件系统和挂载点

### 2.文件系统、存储和块设备

块设备命名

|                            |                                       |      |
| -------------------------- | ------------------------------------- | ---- |
| /dev/sda、/dev/sdb         | STAT/SAS（新SCSI技术）/USB 附加存储   |      |
| /dev/vda、/dev/vdb         | virtio-blk 超虚拟化存储（部分虚拟机） |      |
| /dev/nvme0，/dev/nvme1     | 附加存储 （SSD）                      |      |
| /dev/mmcblk0、/dev/mmcblk1 | SD卡                                  |      |

### 3.磁盘分区

```bash
分区（可选）--格式化--挂载--使用
mount命令挂载是临时的，意味着重启系统后将取消挂载。需要手动重新挂载。永久挂载需要将挂载项记入/etc/fstab中（下次基本存储课讲）

1 分区，gpt方案 ，分2个区，每个1G
[root@servera ~]# fdisk /dev/vdb
Command (m for help): m
   d   delete a partition    #删除分区
   n   add a new partition   #创建分区
   p   print the partition table  #打印分区表
   w   write table to disk and exit   #保存并退出
  Create a new label
   g   create a new empty GPT partition table  #指定分区方案gpt，分区前指定一次即可 
   o   create a new empty DOS partition table  #指定分区方位mbr

Command (m for help): g   #指定分区方案gpt
Created a new GPT disklabel (GUID: D29B3E19-BA51-1042-BFE6-0FD975D1B7DB).
Command (m for help): n
Partition number (1-128, default 1): #回车
First sector (2048-10485726, default 2048):  #回车
Last sector, +sectors or +size{K,M,G,T,P} (2048-10485726, default 10485726): +1G  #指定分区大小1G

Created a new partition 1 of type 'Linux filesystem' and of size 1 GiB.

Command (m for help): p
Disklabel type: gpt  #查看分区方案

Device     Start     End Sectors Size Type  #分区表
/dev/vdb1   2048 2099199 2097152   1G Linux filesystem  #/dev/vdb 分区为1G

Command (m for help): n   #创建第二个分区
Partition number (2-128, default 2): 
First sector (2099200-10485726, default 2099200): 
Last sector, +sectors or +size{K,M,G,T,P} (2099200-10485726, default 10485726): +1G

Command (m for help): p
Device       Start     End Sectors Size Type
/dev/vdb1     2048 2099199 2097152   1G Linux filesystem
/dev/vdb2  2099200 4196351 2097152   1G Linux filesystem

Command (m for help): w  #保存退出
[root@servera ~]# fdisk -l /dev/vdb  #查看/dev/vdb分区表
Device       Start     End Sectors Size Type
/dev/vdb1     2048 2099199 2097152   1G Linux filesystem
/dev/vdb2  2099200 4196351 2097152   1G Linux filesystem

[root@servera ~]# lsblk /dev/vdb   #lsblk查看块设备
NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
vdb    252:16   0   5G  0 disk 
├─vdb1 252:17   0   1G  0 part 
└─vdb2 252:18   0   1G  0 part 
[root@servera ~]# 

2 格式化，将两个分区分别格式化为ext4和xfs文件系统

语法：
mkfs 选项  设备名
-t 指定文件系统类型
例子：
mkfs -t ext4 /dev/vdb1  #方法1
mkfs.ext4 /dev/vdb1     #方法2

[root@servera /]# mkfs -t ext4 /dev/vdb1   #mkfs格式化 ext4是文件系统类型 /dev/vdb1是要格式化的磁盘分区
[root@servera /]# echo $?  #    $?是看上一条命令返回值，0为正确，非0为错误
0
[root@servera /]# mkfs.xfs /dev/vdb2
[root@servera /]# echo $?
0
[root@servera ~]# lsblk -f
[root@servera ~]# lsblk -f /dev/vdb  #查看文件系统类型，
NAME   FSTYPE LABEL UUID                                 MOUNTPOINT
vdb                                                      
├─vdb1 ext4         ecb332da-5bf4-4b86-b92e-d9da25f22a07 
└─vdb2 xfs          b538bf38-2b33-4d53-a785-372627587c52 


3 挂载
创建挂载点
mkdir /mnt/disk1
文件系统：格式化后的设备或分区
挂载点：linux中的空目录

语法：
挂载
mount  文件系统  挂载点
mount  /dev/vdb1  /mnt/disk1
卸载
umount 文件系统/挂载点
umount /dev/vdb1 or umount /mnt/disk1


#挂载
[root@servera ~]# lsblk -f /dev/vdb
NAME   FSTYPE LABEL UUID                                 MOUNTPOINT
vdb                                                      
├─vdb1 ext4         ecb332da-5bf4-4b86-b92e-d9da25f22a07 
└─vdb2 xfs          b538bf38-2b33-4d53-a785-372627587c52 
[root@servera ~]# mkdir /mnt/{disk1,disk2}  # 创建挂载点
[root@servera ~]# ls /mnt
disk1  disk2
[root@servera ~]# mount /dev/vdb1 /mnt/disk1  #将/dev/vdb1 挂载到/mnt/disk1目录上
[root@servera ~]# df 
[root@servera ~]# df -Th  #-T 显示文件系统，-h以易读单位显示
Filesystem     Type      Size  Used Avail Use% Mounted on
/dev/vdb1      ext4      976M  2.6M  907M   1% /mnt/disk1
/dev/vdb2      xfs      1014M   40M  975M   4% /mnt/disk2
[root@servera ~]# tree /mnt/
/mnt/
├── disk1
│   ├── haha.txt
│   └── lost+found
└── disk2
    └── heihei.txt
 卸载   
[root@servera ~]# cd /mnt/disk2
[root@servera disk2]# umount /dev/vdb2      #使用时不能卸载
umount: /mnt/disk2: target is busy.
[root@servera disk2]# cd /    #需要退出挂载点
[root@servera /]# umount /dev/vdb2   #卸载
[root@servera /]# df -h | tail -2
tmpfs           183M     0  183M   0% /run/user/0
/dev/vdb1       976M  2.6M  907M   1% /mnt/disk1


```

### 4.检查文件系统

```bash
df 查看系统挂载状态
-T 查看文件系统类型
-h 以易读方式列出容量单位

du 查看文件大小
[root@servera /]# du /etc/
[root@servera /]# du -h /etc/
[root@servera /]# du -sh  /etc/
24M	/etc/

[root@servera /]# du /etc/man_db.conf 
8	/etc/man_db.conf
[root@servera /]# du /etc/man_db.conf  -h  #占用了的块大小，linux默认一个块4k
8.0K	/etc/man_db.conf
[root@servera /]# ll /etc/man_db.conf 
-rw-r--r--. 1 root root 5165 Nov  7  2018 /etc/man_db.conf
```



## 三.文件查找locate和find

#### locate

```bash
locate
updatedb    #收集所有文件元数据
locate passwd
locate -i image
locate -n 5  image   #显示前5行
```

#### find(考点)

```bash
语法：
find  查找范围  查找条件   动作(可选)

例：
find /  -name  passwd


选项：
-name： 以文件名的形式查找
-size： 根据文件大小 -size 1k ：大小为1k的文件，+1k大于1k的文件，-1k小于1k的文件
-user /  -uid： 文件所有者   -user studnet  ：student是用户名，查找student拥有的文件
-group / -gid
-perm： 权限查找     -perm 700 ：搜索权限为700的文件
-type： 按文件类型   -type f ： f表示文件，d表示目录
动作：
-exec： 选项后接Linux指令，操作查找到的文件  command {} ; 


```



```bash
find / -name sshd_config
find /etc -name sshd_config
find /etc -name '*pass*'
find / -iname '*pass*' -iname 不区分大小写
find / -user student
find / -group student
find /home -uid 1000
find /home/ -gid 1000


find / -user student | xargs ls -l
find / -perm 700 | xargs ls -l
ll -d /usr/share/selinux/targeted/default/active/modules/disabled
find / -perm 700 -type f
ll /boot/efi/EFI/redhat/shimx64.efi
find / -perm 700 -type d -user student
find / -perm 700 -type d -user student | xargs ls -ld
find / -type f -size +10k


cd /home
find /home/ -perm 700

find /etc/ -size 10M
find /etc/ -size +10M
find /etc/ -size -10M
查找一个‘文件’，大于3k小于10k
find /etc -type f -size +3k  -and -size -10k


ll -a
cd /etc/
ll -h
find ./ -size +1k    如果是小于1k   用-1k
du -sh man_db.conf 

find /etc -size +1k -and -size -10k > /root/test2.txt
find / -size +1k  -and -size -100k -type f | xargs du -sh
find / -perm -g=s | xargs ls -ld     
find / -perm -4000 | xargs ls -ld

SUID 4     
SGID 2
SBIT 1
chmod 2700 dir

rwxr-sr-- 2754
将系统中student用户的文件复制到/root/studentdir目录中，并且保留权限
[root@servera ~]# find / -user student -exec cp -a {} /root/studentdir/ \;

将系统中student用户的文件列表保存到/root/studentdir文件中
find / -user student > /root/studentdir
```

## 四.ln 软链接与硬链接

```bash
软链接：使用范围广，方便访问源文件
硬链接：节省系统空间

文件链接创建方式：

软链接（符号链接）
ln -s 源文件   链接文件
硬链接：
ln 源文件  链接文件
```

### 1.软链接：

```bash
ln -s 源文件  链接文件
cd /opt
touch file1
mkdir dir
ln -s file1 linkfile1
ln -s dir1 linkdir1
```

### 2.硬链接：

```bash
cd /

[root@servera opt]# echo 123 > /opt/sou_file.txt
[root@servera opt]# cat /opt/sou_file.txt 
ln /opt/sou_file.txt /opt/link_file.txt

取消链接
[root@servera opt]# unlink /opt/link_file.txt
[root@servera opt]# ls
sou_file.txt
 
```

### 3.软链接和硬链接区别

``` bash
1.命令相同 参数不同
2.硬链接的权限和源文件完全一致   
软链接的链接文件权限永远是777 和源文件权限不同
3.硬链接可以删除，移动源文件
软链接不可以删除，移动源文件
4.软链接inode和源文件不同
硬链接的inode的源文件相同
5.软链接可以对目录及文件生效
硬链接只可以对文件操作
6.软链接可以跨文件系统
硬链接不可以跨文件系统
```



# 第十六章 分析服务器获取支持

cockpit 

```bash
[root@servera /]# yum install -y cockpit
[root@servera /]# systemctl start cockpit
[root@servera /]# systemctl status cockpit

添加开机自启动方式：
vim /usr/lib/systemd/system/cockpit.service
[Install]
WantedBy=multi-user.target
systemctl enable cockpit

[root@serveraaa /]# firewall-cmd --permanent --add-service=cockpit
success
[root@serveraaa /]# firewall-cmd --reload
success
[root@serveraaa /]# firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp1s0
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
 
 netstat -ntlp  | grep 9090

连接cockpit
foundation  连接servera
浏览器  http：//172.25.250.10:9090
添加信任
输入用户名密码  root  redhat
```



