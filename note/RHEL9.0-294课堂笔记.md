#  大纲

| 第一天       | 第二天                       | 第三天                | 第四天                |
| ------------ | ---------------------------- | --------------------- | --------------------- |
| 介绍Ansible  | 管理变量和事实               | 管理大项目            | 自动执行Linux管理任务 |
| 部署Ansible  | 实施任务控制                 | 利用角色简化Playbook  | QA                    |
| 实施Playbook | 在被管理节点上创建文件或目录 | 对Ansible进行故障排除 |                       |
|              |                              |                       |                       |





# 第1章 介绍Ansible



**目标**：

描述Ansible 基本概念和使用方法，并从红帽Ansible 自动化平台安装开发工具



## 一 在控制节点上安装Ansible

```bash
#准备练习环境，
- foundation0
$ cat /etc/rht
RHT_COURSE=rh294
$ rht-vmctl start classroom
$ rht-vmctl start all
$ ssh workstation 
#登录workstation后默认为student普通用户，根据实际使用和考试结合建议使用普通用户操作。以下本课程所有操作均为普通用户操作。
```

```bash
#练习安装Ansible，推荐使用教材中环境练习，15页
- workstation
$ lab start intro-install
$ sudo dnf -y install ansible-navigator ansible-core
student
```

```bash
#检查ansible的版本
$ ansible --version
ansible [core 2.13.0]
  config file = /etc/ansible/ansible.cfg

$ ansible-navigator --version
ansible-navigator 2.1.0

$ cat ~/.ansible-navigator.yml 
---
ansible-navigator:
  execution-environment:
    image: utility.lab.example.com/ee-supported-rhel8:latest
    pull:
      policy: missing
  playbook-artifact:
    enable: false 
```

```bash
#配置镜像仓库不进行https验证,可以使用全局或个人的，考试已设置好。
$ mkdir ~/.config/containers
$ cp /etc/containers/registries.conf ~/.config/containers
$ vim  ~/.config/containers
unqualified-search-registries = ["utility.lab.example.com"]
[[registry]]
insecure = true
blocked = false
location = "utility.lab.example.com"
```

```bash
#登录容器镜像服务器，为下载自动化执行环境容器镜像做准备
$ ping utility 
$ podman login -u admin -p redhat utility.lab.example.com 
$ ansible-navigator collections  #如果发现镜像服务器登录失败，可重置一下utility服务器。
$ ansible-navigator images       #如果发现镜像服务器登录失败，可重置一下utility服务器。
esc退出
```





# 第2章 实施Ansible Playbook



## [一 构建Ansible清单]() 



**培训目标**
描述Ansible 清单概念并管理静态清单文件



### 1.系统默认的清单文件

```bash
指定清单范围格式：通配符或正则表达式的方法
[START:END]  			   开始:结束范围
192.168.[0:15].[0:255]     表示   192.168.0.0-192.168.15.255
server[a:c].example.com    表示   a-c
server[01:15].example.com  表示   server01.example.com-server15
all： 					  表示   所有主机
ungrouped: 		           表示   指定组/未分配组的主机
```

```bash
$ rpm -qc ansible-core
/etc/ansible/ansible.cfg  #默认全局配置文件
/etc/ansible/hosts        #默认全局清单文件
```



### 2.自定义清单文件

```bash
1.创建工作目录
[student@workstation ~]$ mkdir ~/ansible
[student@workstation ~]$ cd ~/ansible/
```

```bash
#清单实例文件
$ ansible-doc -t inventory -l
$ ansible-doc -t inventory ini
$ ansible-navigator doc -t inventory ini
```

```bash
2.编辑清自定义单文件+嵌套组

[student@workstation ansible]$ vim inventory 
server0 				#未在组内的主机

[dev]  					#主机组，主机组名称需要使用中括号括起来[]，dev是组名称
servera					#主机组成员，dev组内的主机

[test]
serverb

[prod]
server[c:d]  			#表示两个主机serverc至serverd

[webservers:children]   #嵌套组名webservers是自定义的，但是:children是固定语法，表示prod在webservers组中，嵌套组成员应为组，不应为主机
prod

#注意：不要在清单里书写无用的符号，及一些特殊符号。主机名称不要和主机组冲突，组名尽量不要用数字开头。
```

```bash
3.验证清单：
-RHEL<=8
-i inventory  #指定清单文件的位置
--list-hosts  #列出清单中的主机  
--graph       #通过ansible-inventory命令列出清单整个pattern
$ ansible all -i inventory --list-hosts
$ ansible-inventory -i inventory --graph #推荐，RHEL9考试可用

-RHEL<=9
$ ansible-navigator inventory -i inventory  -m stdout --list
$ ansible-navigator inventory -i inventory  -m stdout --graph
$ ansible-navigator inventory -i inventory  -m stdout --graph webservers

重要：清单中含有名称相同的主机和主机组，ansible命令显示警告并以主机作为其目标，组被忽略
```



### 3.覆盖清单位置

```bash
/home/student/ansible/inventory 优   #用于针对某个用户设置清单（考试使用）
/etc/ansible/hosts 劣    		    #用于全局设置
```



### 4.多清单

```bash
[student@workstation ansible]$ cp inventory inventory2    #额外创建一份清单名为inventory2
[student@workstation ansible]$ echo servere > inventory2  #inventory2内指定一个主机servere
[student@workstation ansible]$ mkdir invdir				  #创建清单存储目录，名称自定义
[student@workstation ansible]$ mv inv* invdir/ 			  #将所有清单移动至清单存储目录
mv: cannot move 'invdir' to a subdirectory of itself, 'invdir/invdir'
[student@workstation ansible]$ ansible-inventory -i invdir --graph #查看主机模式结构
@all:
  |--@dev:
  |  |--servera
  |--@test:
  |  |--serverb
  |--@ungrouped:
  |  |--server0
  |  |--servere
  |--@webservers:
  |  |--@prod:
  |  |  |--serverc
  |  |  |--serverd
```

```bash
练习P25
```



## [二  管理Ansible配置文件]()



**培训目标**
描述Ansible配置文件的位置、Ansible 如何选择这些文件，并编辑它们以对默认设置应用更改。



### 1.配置Ansible

```bash
1.ansible.cfg：用于配置多个Ansible工具的行为

2.ansible-navigator，用于更改ansible-navigator命令默认选项
```

```bash
$ rpm -qc ansible-core
/etc/ansible/ansible.cfg
/etc/ansible/hosts
```

```bash
$ ls -a ~/.ansible-navigator.yml
/home/student/.ansible-navigator.yml
```





### 2.管理Ansible设置

| 指令            | 描述                                                         |
| --------------- | ------------------------------------------------------------ |
| inventory       | 指定清单文件的路径。                                         |
| remote_user     | 指定Ansible用于连接受管主机的用户名。如果未指定，则使用当前用户的名称。(在由ansible-navigator 运行的基于容器的自动化执行环境中，始终为 root。) |
| ask_pass        | 指示是否提示输入SSH 密码。 (可以是 false，如果使用SSH公身份验证，则此为默认值。) |
| become          | 指定连接后是否在受管主机上自动切换用户(一般切换为root)。这也可以通过 play 来指定。 |
| become_method   | 指定如何切换用户(通常为 sudo，此为默认值，但也可选择su)。    |
| become_user     | 指定要在受管主机上切换到哪个用户(通常为 root，此为默认值)。  |
| become_ask_pass | 指示是否提示输入become_method 参数密码。默认为false。        |



### 3.自定义配置文件

```bash
1.普通用户身份远程 #重点掌握，考点
$ cat /etc/ansible/ansible.cfg | grep ansible.cfg
$ ansible-config init --disabled > ~/ansible/ansible.cfg 
$ vim ~/ansible/ansible.cfg
[defaults]
inventory=/home/student/ansible/inventory   #139行  工作目录清单位置
remote_user=student						    #222行  远程用户可选root/普通用户 
host_key_checking=false					    #318行  不进行公钥记录

[privilege_escalation]    					#搜/become，n向下查找
become=True									#430行  开启特权功能，
become_ask_pass=False						#433行  远程免特权密码，需要对端添加sudo免密	
become_method=sudo							#442行  远程功能启用sudo
become_user=root							#445行  特权用户为root
```

```bash
2.#了解：ansible配置文件可以存在于不同位置其中包括：
$ /etc/ansible/ansible.cfg  #默认路径
$ ~/.ansible.cfg  			#家目录
$ ~/ansible/ansible.cfg     #ansible为工作目录（练习和考试时使用）
$ grep  ANSIBLE_CONFIG /etc/profile #环境变量
export  ANSIBLE_CONFIG=/opt/ansible.cfg   （此时/opt下需要有ansible.cfg配置文件）
source /etc/profile   加载

优先级 ：变量＞当前目录＞用户家目录＞/etc
```

```bash
3.#了解：其他指定远程用户及密码的方法，以root用户身份远程
方法一：
ansible.cfg
remote_user=root
inventory
[all:vars]
ansible_password=redhat

方法二：
inventory
[all:vars]
ansible_user=root
ansible_password=redhat
```

```bash
4.练习
根据以上笔记将清单路径记录至inventory=后，要求远程用户为普通用户，并查看清单主机模式结构。
1.创建工作目录，创建并完善清单
2.在工作目录中制作配置文件，并完善
3.保证远程和sudo都免密
#练习环境默认student远程免密，所以不用做。
#但是student未做sudo免密，以下命令为servera~d设置sudo免密。
[workstation]
$ for i in {a..d};do ssh root@server$i 'sed -i s/^%wheel.*$/"%wheel  ALL=(ALL) NOPASSWD: ALL"/ /etc/sudoers';done
$ for i in {a..d};do ssh root@server$i 'grep ^%wheel /etc/sudoers';done
4.测试远程部署
$ ansible all -m ping
servera | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/libexec/platform-python"
    },
    "changed": false,
    "ping": "pong"
```

```bash
5.练习答案：#请根据课上学习内容，自己完成并填写
```



### 4.管理自动化内容导航器的设置

```bash
创建导航器配置文件
$ rpm -ql ansible-navigator | grep temp
/usr/lib/python3.9/site-packages/ansible_navigator/package_data/settings-sample.template.yml

```

```bash
#自动化内容导航器按以下顺序查找设置文件，并使用它找到的第一个文件:
1.ANSIBLE_NAVIGATORCONFIG	#环境变量，则使用所指定位置处的配置文件。
2.当前Ansible项目目录中的ansible-navigator.yml文件。
3.~/.ansible-navigator.yml文件(在您的家目录中)。 #请注意，其文件名开头有一个“点”。

$ vim ~/.ansible-navigator.yml 
---
ansible-navigator:
  execution-environment:
    image: utility.lab.example.com/ee-supported-rhel8:latest
    pull:
      arguments:
      - "--tls-verify=false"
      policy: missing
  playbook-artifact:
    enable: false   #关闭运行playbook时生成日志
```

```bash
#了解anisble远程连接时的必要条件
1.配置链接：
如何选择远程的用户
2.清单位置：
相对路径、绝对路径、多清单等。
3.链接设置
remote_user= ，~/.ansible-navigator.yml中playbook-artifact：
4.ssh免密
ssh-key-gen，ssh-copy-id
5.升级特权
visudo，/etc/sudoers
```

### 5.练习

```
选做：P38管理Ansible配置文件
```



## [三 编写和运行Playbook]()



**培训目标**
编写基础Ansible Playbook，然后使用自动化内容导航器来运行。



###  1.Playbook-yaml语法

```bash
$ ansible-doc -l | grep yum
$ ansible-doc yum  						#进入之后，搜索/EXAMPLE
$ vim ~/ansible/web.yml
---										#yaml语法，---开头
- name: install httpd					#play任务的描述：描述部分name字段是可选的选项
  hosts: servera						#主机模式：任务目标主机
  tasks:								#任务列表：下面通常为任务模块，有两格缩进
  - name: Install Apache				#任务模块：注意和tasks有两格缩进，name是可选字段，模块描述
    ansible.builtin.yum:				#模块名称：
      name: httpd						#模块选项1
      state: latest						#模块选项2
```



### 2.调整tab键缩进

```bash
vim ~/.vimrc
set tabstop=2   #将vim的tab键缩进调至两格
set nu			#设置行号
set autoindent  #回车时调整至上一行文本缩进  

:set all        #末行模式下set all 查看所有环境设置帮助

#整体缩进 视图模式
ctrl+v ， jjj(+G) ，I，(空格、空格)，esc
```



### 3.查找用于任务的模块

```bash
-RHEL<=8
$ ansible-doc -l 
$ ansible-doc -l | grep user
$ ansible-doc user

-RHEL=9
$ ansible-navigator collections
$ ansible-navigator doc ansible.posix.firewalld
$ ansible-navigator doc ansible.posix.firewalld -m stdout
$ ansible-navigator collections -m stdout | grep firewalld

#两个考试都可用，推荐优先用RHEL8版本的，用法方便。
#找不到模块时就需要用RHEL9版本的，怕麻烦可以选择统一都用RHEL9版本的方法，省事。
```



### 4.运行Playbook

```bash
#上节实验做了多清单实验，恢复为最初配置
$ cd  ~/ansible
$ mv invdir/inventory .     
$ rm -rf invdir
```

```bash
#运行playbook
$ ansible-navigator run web.yml
$ ansible-navigator run web.yml -m stdout
```

```yaml
练习P51
```



## [四 实施多个Play]()



**培训目标**
编写一个使用多个play且每个play都有特权升级的playbook，有效使用自动化内容导航器查找可用Ansible内容集合中的新模块，并使用新模块为 play 执行任务。

### 1.编写多个Play

```bash
---
- name: PLAY1
  hosts: servera
  tasks:
  - name: Install httpd
    ansible.builtin.yum:
      name: httpd
      state: latest
  - name: create index.html
    ansible.builtin.copy:
      content: 'haha'
      dest: /var/www/html/index.html
  - name: Enable a httpd
    ansible.builtin.systemd:
      name: httpd
      state: started
      enabled: yes
  - name: Enable a firealld
    ansible.builtin.systemd:
      name: firewalld
      state: started
      enabled: yes
  - name: permit http service
    ansible.posix.firewalld:    #ansible-navigator collections,ansible-navigator doc  ansible.posix.firewalld -m stdout
      service: http
      permanent: yes
      state: enabled
      immediate: yes      
      
#- name: PLAY2
#  hosts: serverb
#  tasks:
#  - name:
#  	ansible.builtin.yum
#  	name: httpd
#  	state: latest

--syntax-check    
[student@workstation playbook]$ ansible-navigator  run -m stdout web.yml --syntax-check

playbook: playbook.yml

-v -vv -vvv
[student@workstation playbook]$ ansible-navigator  run -m stdout web.yml --syntax-check -v  

[student@workstation playbook]$ ansible-navigator  run -m stdout web.yml

以上实验，用ansible-doc 查询课程中的所有模块
yum        安装软件
service    管理服务
shell模块  管理防火墙
copy       拷贝，1有拷贝功能，2 可以将一段文本，复制到某个文件中，如文件不存在，则生成文件。
uri        网站连接测试
```

### 2.选择模块

| 类别     | 模块                                                         |
| -------- | ------------------------------------------------------------ |
| 文件     | ansible.builtin.copy:              将本地文件复制到受管主机<br/>ansible.builtin.file:                 设置文件的权限和其他属性<br/>ansible.builtin.lineinfile:       确保特定行是否在文件中<br/>ansible.posix.synchronize:  使用rsync 同步内容 |
| 软件     | ansible.builtin.package:       使用操作系统自带的自动检测软件包管理器管理软件包。<br/>ansible.builtin.dnf:                使用DNF软件包管理器管理软件包<br/>ansible.builtin.apt:                使用APT 软件包管理器管理软件包<br/>ansible.builtin.pip:                 从PyPI管理Python 软件包。 |
| 系统     | ansible.posix.firewalld:         使用firewalld 管理任意端口和服务<br/>ansible.builtin.reboot:          重新启动计算机。<br/>ansible.builtin.service:          管理服务<br/>ansible.builtin.user:               添加、删除和管理用户帐户 |
| 网络工具 | ansible.builtin.get_url:           通过HTTP、HTTPS或FTP 下载文件<br/>ansible.builtin.uri:                  与Web 服务交互 |



# 第3章 管理变量和事实



## [一 管理变量]()



**培训目标**
创建和引用影响特定主机或主机组、play 或全局环境的变量，并描述变量优先级的工作方式。



### 1.Ansible变量简介

```bash
#key：vaule  

变量可以重复的应用到项目中，简化管理，应用对象可以是：
要创建的用户
要安装的软件包
要启动的服务
要删除的文件
互联网的文档等。
```



### 2.命名变量

| 无效变量名称    | 有效变量名称                   |
| --------------- | ------------------------------ |
| web server      | web_server                     |
| remote.file     | remote_file                    |
| 1st  file       | file_1，file1                  |
| remoteserver $1 | remote_server_1,remote_server1 |



### 3.定义变量

```bash
#Ansible的变量可以定义在不同位置，根据需要设定，其中也有优先度。
```

| 应用场景 | 描述                                               | 优先度 |
| -------- | -------------------------------------------------- | ------ |
| 全局范围 | 命令行执行临时命令时指定的变量 -e key=vaule        | 高     |
| play范围 | playbook的Play部分或模块内部指定变量信息key: vaule | 中     |
| 主机范围 | 清单中主机或主机组指定变量（主机 优先 主机组）     | 低     |



#### 1 全局范围-命令行

```bash
#命令行使用变量优先级最高
$ ansible dev -m shell -a whoami -e ansible_user=root -e ansible_password=redhat 

#在执行playbook时指定变量
$ ansible-navigator run -m stdout web.yml -e ansible_user=root -e ansible_password=redhat
```



#### 2 PlAY范围-Playbook

```bash
#playbook中可以在play位置使用vars直接定义变量，也可以通过vars_files加载包含变量的文件。

-vars
[student@workstation ansible]$ vim web.yml
---
- name: PLAY1
  hosts: servera
  vars: 			  		#vars关键字就是在playbook中设置自定义变量
  - package: httpd    		#key：vaule    键值间：冒号隔开，冒号后有一个空格
  tasks:
  - name: install {{ package }}
    ansible.builtin.yum:
      name: "{{ package }}" #使用变量时，变量两边有空格，并且用双大括号括起来，变量开头要加“”双引号，非变量开头不用加双引号
      state: latest

-vars_files      
#生成变量文件
$ vim /home/greg/ansible/var.yml   
---
package: httpd   #定义变量

$ vim web.yml
---
- name: haha
  hosts: servera
  vars_files:   					#vars_files 加载变量文件到剧本中
  - /home/greg/ansible/var.yml     
  tasks:
  - name: install "{{ package }}"
    ansible.builtin.yum:
      name: "{{ package }}"
      state: latest
```



#### 3 主机范围-清单中

```bash
$ vim inventory 
172.25.250.9   ansible_password=redhat     给主机定义变量

[test]
172.25.250.10 

[test:vars]   #主机组变量中vars是固定语法
ansible_password=redhat   给主机组定义变量

[prod]
172.25.250.[11:12]

[balancers]
172.25.250.13

[all:vars]   给所有主机和主机组组定义变量
ansible_user=root
ansible_password=redhat
```



### 4.字典形式表示变量

```
除了将同一元素相关的配置数据分配到多个变量外，字典形式表示变量的其中一种应用场景是方便Ansible在调用大量变量时，通过一个变量名可以调用多组变量。
```



#### 1 同一元素相关变量的键值关系

```yaml
user1_A_name: zhang
user1_B_name: san
user1_C_name: /home/zhangsan
user2_A_name: li
user2_B_name: si
user2_C_name: /home/lisi 
```



#### 2 字典形式

```bash
vim vari.yml
---
users:
  user1:
    A_name: zhang
    B_name: san
    C_name: /home/zhangsan
  user2:
    A_name: li
    B_name: si
    C_name: /home/lisi 
```



#### 3 调用变量

```bash
例子：调用数组

调用变量方法1：
users.user1.A_name
users.user2.B_name

调用变量方法2：
应用方法2：python字典
users['user1']['A_name']

    
# [student@bastion ansible]$ vim debug.yml
---
- name: useradd
  hosts: dev
  vars_files:
    -- vari.yml
  tasks:
  - name: Add the user
    ansible.builtin.user:
      name: "{{ users.user1.A_name }}{{ users.user1.B_name }}"
      home: "{{ users['user1']['C_name'] }}"
```



### 5 使用已注册变量捕获命令输出

```bash
#register用来捕获命令输出或有关模块执行的其他信息。输出会保存至一个变量中，稍后可用于调试或其他目的。
```

```bash
$ vim register.yml
---
- name: install a packages
  hosts: servera
  tasks:
  - name: install the latest version of Apache
    ansible.builtin.yum:
      name: httpd
      state: latest
    register: install_result  #register字段负责收集变量 install_result自定义变量名被收集变量名
  - name:  message
    ansible.builtin.debug:			
      var: install_result     #debug模块var选项打印register获取install_result变量值

#验证
$ ansible-navigator run -m stdout register.yml 
```



## [二 管理机密-vault]()



**培训目标**
使用Ansible Vault加密敏感变量，并运行引用Vault加密变量文件的playbook。



###  1.介绍Ansible-vault

```bash
若要使用Ansible Vault，可通过一个名为ansible-vault 的命令行工具创建、编辑、加密、解密和查看文件。
```

```bash
$ ansible-vault --help
ansible-vault {create,decrypt,edit,view,encrypt,encrypt_string,rekey}  file
                   
positional arguments:
  {create,decrypt,edit,view,encrypt,encrypt_string,rekey}
    create              Create new vault encrypted file
    decrypt             Decrypt vault encrypted file
    edit                Edit vault encrypted file
    view                View vault encrypted file
    encrypt             Encrypt YAML file
    encrypt_string      Encrypt a string
    rekey               Re-key a vault encrypted file
```

#### 1 创建加密文件

```bash
$ ansible-vault create sec1.txt   
New Vault password: 123456
Confirm New Vault password: 123456

$ cat sec1.txt 
$ANSIBLE_VAULT;1.1;AES256
65303839313034383033333534316136333234643730353634363161363536623465346461353038
6363643662356232396562383639363631333636653763370a366138633933626232643438613632
66313632306232363662323566376235343061313266346163343134333032313037633764363363
3330653838393432620a373632333366633166323661336665336664666535376364353964376335
3662
```

#### 2 查看加密文件

```bash
$ ansible-vault view sec1.txt    
Vault password: 123456
a: 123
```

#### 3 编辑现有的加密文件

```bash
$ ansible-vault edit sec1.txt     
Vault password: 123456
```

#### 4 加密现有的文件

```bash
$ echo heihei > sec2.txt          #创建变量文件
$ ansible-vault  encrypt sec2.txt #加密现有文件
New Vault password: 123456
Confirm New Vault password: 123456
$ cat sec2.txt 
$ ansible-vault  view sec2.txt
Vault password: 123456
```

#### 5 解密现有的文件

```bash
$ ansible-vault decrypt sec2.txt  
Vault password: 123456
```

#### 6 更改加密文件的密码

```bash
$ ansible-vault rekey sec1.txt   					#rekey改密码
Vault password: 123456          					#旧密码 123456
New Vault password: redhat		 					#新密码 redhat	 
Confirm New Vault password: redhat  				#重复新密码 redhat

$ ansible-vault view sec1.txt 
Vault password: redhat
passwd: haha
```

#### 7 使用密码文件

```bash
$ ansible-vault view sec1.txt --help
  --vault-id VAULT_IDS  the vault identity to use
  --vault-password-file VAULT_PASSWORD_FILES
```

```bash
$ echo redhat > secret.txt  #创建密码文件
$ ansible-vault view sec1.txt --vault-id=secret.txt  #通过变量文件指定密码
passwd: haha
```

#### 8 密码文件记录到ansible.cfg配置文件中

```
密码文件记录在ansible.cfg配置文件中的好处是，当执行一个使用了加密文件的playbook时，不必手工指定加密文件密码。
```

```bash
$ vim ansible.cfg  #第一次填写vault路径时，搜索`vault`关键字找该选项
vault_password_file = /home/greg/ansible/secret.txt   #配置文件中指定密码文件位置
[greg@control ansible]$ ansible-vault view sec1.txt   #自动调用配置文件中密码文件
passwd: haha
```



## [三 管理事实 facts]()



**培训目标**
使用Ansible事实引用有关受管主机的数据，并在受管主机上配置自定义事实。



### 1.收集事实

```
收集事实常用两种手段常用是临时命令ad-hoc及Playbook，事实以josn语法格式列出。收集时要找ansible_开头的事实名称。
```

| ansible_*name                            | 描述              |
| :--------------------------------------- | ----------------- |
| ansible_hostname                         | 主机名            |
| ansible_fqdn                             | fqdn完全合格域名  |
| ansible_nodename                         | 节点名称          |
| ansible_default_ipv4.address             | ipv4地址          |
| ansible_lvm                              | lvm逻辑卷         |
| ansible_devices.vda.partitions.vda1.size | /dev/vda1分区大小 |
| ansible_kernel                           | 内核信息          |
| ansible_dns.nameservers                  | dns信息           |



#### 1 临时命令 ad-hoc

```bash
1.使用ad-hoc方式收集事实
$ ansible localhost -m setup
$ ansible localhost -m setup -a  filter=ansible_nodename
$ ansible servera -m setup -a filter=*ipv4*   #模糊匹配方式
$ ansible servera -m setup > 1.txt   #将事实记录到1.txt文件中，方便后期查找事实。
```



#### 2 PLAYBOOK 收集事实

```bash
$ vim debug.yml
---
- name: debug
  hosts: servera
  tasks:
  - ansible.builtin.debug:
      msg: "{{ ansible_default_ipv4.address }}"    #简化后可将ansible_facts去掉，二级变量开头，要保留ansible_,如：ansible_default_ipv4.address
  - ansible.builtin.debug:
      var: ansible_hostname 
$ ansible-navigator run -m stdout debug.yml
```



#### 3 关闭事实

```bash
$ vim debug.yml
---
- name: debug message
  hosts: dev
  gather_facts: on/off  true/false
  tasks:
  - debug:
      msg: "{{ ansible_facts.default_ipv4.address }}"
      
[greg@bastion ansible]$ ansible-playbook debug.yml

如果playbook内容和事实收集没有关系，可以关闭可以大量减少playbook执行时间。
注意：考试时不要关闭
```



### 2. 魔法变量

```
实时变量通常收集的是受管节点的信息，而魔法变量收集的是本机的变量值，先了解课上的4个魔法变量功能，后面再看使用场景。
```

```bash
inventory_hostname  列出在清单中的主机名
group_names         列出当前主机归属于哪个组
groups 			    列出清单中的所有主机名称。以及所在组
hostvars			列出系统中所有魔法变量及所有事实变量

$ https://docs.ansible.com/ansible/latest/reference_appendices/special_variables.html
# dosc.ansible.com中搜索magic或facts
```

#### 1 临时命令收集魔法变量值 ad-hoc

```bash
ansible  servera -m debug -a var=inventory_hostname
ansible  servera -m debug -a var=groups
ansible  all -m debug -a var=group_names
ansible  all -m debug -a var=hostvars
ansible  servera -m debug -a var=groups.all
```

#### 2 PLAYBOOK 收集事实+魔法变量（1文件）

```bash
$ vim debug.yml
---
- hosts: all
  tasks:
  - debug:
      var: hostvars
$ ansible-navigator run  debug.yml -m stdout > 1.txt   #重定向到文件中的意义是方便在1.txt文件中搜索需要的值
```

#### 3 魔法变量hostvars

```
临时命令与playbook收集hostvars变量值是不同的，临时命令不会执行setup模块，所以收集不到事实，PLAYBOOK方法则可以收集到事实和魔法变量。
```



# 第4章 实施任务控制



## [一 编写循环和条件任务]()



**培训目标**
使用循环来编写高效的任务，并使用条件来控制运行任务的时间。



### 1.利用循环迭代任务

```bash
loop字段通常在同一缩进的模块下面，对该模块生效，通过item来加载loop循环中的值或变量。

帮助：搜索loop可以搜到相应语法
$ https://docs.ansible.com/ansible/latest/user_guide/playbooks_loops.html
```



#### 1 简单循环

```bash
#无循环
- name: service * 2
  hosts: servera
  tasks:
  - name: Start service nfs-server
    ansible.builtin.service:
      name: nfs-server
      state: started
      
  - name: Start service chronyd
    ansible.builtin.service:
      name: chronyd
      state: started
      
#使用循环
- name: service * 2
  hosts: dev
  tasks:
  - name: Start service nfs-server
    ansible.builtin.service:
      name: "{{ item }}"
      state: started
    loop:
    - nfs-server
    - chronyd
```



#### 2 循环使用变量

```bash
$ vim loop.yml
- name: service loop
  hosts: servera
  vars:
    servers:
    - nfs-server
    - chronyd
  tasks:
  - name: Start service
    ansible.builtin.service:
      name: "{{ item }}"
      state: stopped
    loop: "{{ servers }}"
    
#vars字段也可以替换为vars_files，将变量保存至文件中，加载到Playbook   
$ mkdir /home/student/ansible/vars/
$ vim /home/student/ansible/vars/var.yml
   servers:
   - nfs-server
   - chronyd
 
$ vim loop.yml
- name: service loop
  hosts: servera
  vars_files:
    - /home/student/ansible/vars/var.yml
  tasks:
  - name: Start service
    ansible.builtin.service:
      name: "{{ item }}"
      state: stopped
    loop: "{{ servers }}"  
```



#### 3 循环字典列表

```bash
#循环字典列表保存在loop字段中
- name: service loop
  hosts: dev
  tasks:
  - name: service loop
    ansible.builtin.user:
      name: "{{ item.name }}"  
      comment: "{{ item.comment }}"  
      state: present
    loop:
      - name: jane
        comment: tom
      - name: joe
        comment: harry      
```

```bash
#循环字典列表保存在play的vars中        
- name: service loop
  hosts: dev
  vars：
    users：
    - name: jane
      comment: tom
    - name: joe
      comment: harry
  tasks:
  - name: service loop
    ansible.builtin.user:
      name: "{{ item.name }}"
      comment: "{{ item.comment }}"
      state: present
    loop: "{{ users }}"
```

```bash
#循环字典列表保存在play的vars_files中 
$ vim vaifile.yml
  users：
    - name: jane
      comment: tom
    - name: joe
      comment: harry
      
$ vim user.yml      
- name: service loop
  hosts: dev
  vars_files:
  - vaifile.yml
  tasks:  
  - name: service loop
    ansible.builtin.user:
      name: "{{ item.name }}"
      state: present
      comment: "{{ item.comment }}"
    loop: "{{ users }}"  
```



### [2.有条件地运行任务]()

```
应用场景：通常主机模式为多个节点时，可以让符合when条件的主机执行模块任务。符合条件则为真，则执行模块。否则为假，跳过模块任务。
```



IP地址是172.25.250

#### 1 例1

```bash
---
- name: service state
  hosts: all
  tasks:
  - name: install the latest version of Apache
    ansible.builtin.yum:
      name: httpd
      state: present
    when: inventory_hostname == 'node1'  
    
    
    (如果使用inventory_hostname这个魔法变量，要参考清单中的主机名称。node1位置，单双引号都可以识别为字符串) 
    
and  与  多个条件同时为真才执行  
or   或  多个条件有一个为真就执行
```



#### 2 例2：

```yaml
---
- name: 安装软件包
  hosts: all
  tasks:
  - name: install the latest version of Apache
    ansible.builtin.yum:
      name: "{{ item }}"
      state: latest
    loop:
    - php
    - mariadb
    when: inventory_hostname in groups.dev or inventory_hostname in groups.test or inventory_hostname in groups.prod

```



#### 3 常用when条件语句

```bash
#变量值 == '字符串'
inventory_hostname == 'node1'
inventory_hostname != 'node1'
'"52:54:00:00:fa:0b" in ansible_default_ipv4.macaddress'
ansible_default_ipv4.address == '172.25.250.11'

#变量值存在  in  第二个变量
inventory_hostname in groups.dev    #可以匹配组
'"dev" in group_names'              #可以匹配


```



#### 4 练习： 

```bash
-课上随堂练习：
1.如何以IP？主机名？为条件
2.收集受管节点是否有某个硬件设备，vdb？


-RHEL9-QA
第三题
issue题


default变量查询方法
搜索引擎中搜索：filters --- Using filters to --- 搜索admin---default('admin', true) 
```



## [二  实施处理程序]()



**培训目标**
实施仅在另一个任务更改托管主机时运行的任务。



### 1.Ansible处理程序

```
对一个Playbook模块改动时，自动执行后续处理动作。

比如一种场景，当修改了服务配置文件时，需要对服务进行重启。可以在配置文件模块位置用notify监视是否修改后，用handlers中的处理程序如：service模块，对其重启服务。达到修改文件便自动重启服务的效果。
```

```bash
$ vim handlers.yml
---
- name:
  hosts: servera
  tasks:
  - name: install the latest version of Apache
    ansible.builtin.yum:
      name: httpd
      state: latest
  - name: Copy using inline content
    ansible.builtin.copy:
      content: 'heihei'
      dest: /var/www/html/index.html
    notify: restart			#notify字段冒号后的名称restart，指向handlers中的描述为restart的模块
  - name: Start firewalld
    ansible.builtin.service:
      name: firewalld
      state: started

  handlers:							#handlers是缩进和tasks对齐
  - name: restart
    ansible.builtin.service:
      name: httpd
      state: restarted
```



## [三 执行中对错误的处理]()



#### 忽略任务失败

```yaml
[greg@bastion ansible]$ vim ignore_errors.yml
---
- name: test error
  hosts: dev
  tasks:
  - name:  touch directory
    ansible.builtin.shell: mkdir  /a/b
#   ignore_errors: yes   第一次执行加#注释，第二次执行取消注释
  - name: Add the user
    ansible.builtin.user:
      name: johnd

#也可以在任务失败时强制执行处理程序,详见教材
---
- name: test error
  force_handlers: yes
  tasks:
  - xxxx
  
  handlers:
  - name: haha
    ansible.builtin.service:
    xxx
    xxx
```



#### block

```yaml
未使用block时：
---
- name: test error
  hosts: all
  tasks:
  - name:  touch file
    ansible.builtin.shell: mkdir  -p /a/b
    when: inventory_hostname == "servera"

  - name: Add the user
    ansible.builtin.user:
      name: johnd
    when: inventory_hostname == "servera"

使用block时
---
- name: test error
  hosts: all
  tasks:
  - block:
    - name:  touch file
      ansible.builtin.shell: mkdir  -p /a/b

    - name: Add the user
      ansible.builtin.user:
        name: johnd
    when: inventory_hostname == "servera"
```



#### block、rescue、always 定义区域中失败的任务(考点)

```yaml
---
- name: block
  hosts: all
  tasks:
  - block:
    - name:
      ansible.builtin.yum:
        name: http
        state: present
    rescue:
    - name:
      ansible.builtin.yum:
        name: httpd
        state: present
    when: inventory_hostname == 'serverb'
    always:
    - name: Start service httpd, if not started
      ansible.builtin.service:
        name: httpd
        state: started

        
Ansible官方文档：搜索：rescue https://docs.ansible.com/ansible/latest/user_guide/playbooks_blocks.html        
```



#### 练习

```bash
-RHEL9-QA
分区题
逻辑卷题
```





# 第5章 将文件部署到受管主机



## [一 修改文件并将其复制到主机]()



**培训目标**
在受管主机上创建、安装、编辑和删除文件，以及管理这些文件的权限、所有权、SELinux 上下文和其他特征。



### 1.文件模块介绍



#### ansible.builtin

| 模块名      | 说明                                                         |
| ----------- | ------------------------------------------------------------ |
| blockinfile | 插入、更新 、删除，自定义标记先包围的多行文本块              |
| *file       | 设置权限、所有者、SElinux上下文及常规文件、符号连接、硬链接等 |
| *copy       | 远程copy，类似file，可以设置文件属性、SElinux上下文          |
| fetch       | 和copy类似，相反工作方式，从远端拷贝到控制节点               |
| *lineinfile | 改文件某一行时使用                                           |
| stat        | 检测文件状态，类似linux 中stat命令                           |
| synchronize | 围绕rsync一个打包程序。                                      |



```bash
查找模块可使用命令
$ ansible-doc -l | grep file
$ ansible-navigator collections -m stdout | grep file$

注意：很多模块已经不在ansible.builtin集合中了，所以需要通过ansible-navigator collections命令搜索。
```



#### ansible.builtin.file

```bash
- name: Change file ownership, group and permissions
  ansible.builtin.file:
    path: /var/www/html/index.html
    owner: apache
    group: apache
    mode: '0644'
    state: touch
    setype: default_t
      
(Choices: absent, directory, file, hard, link, touch)[Default: file]
file：修改文件内容，无该文件则不修改
touch：创建文件
```



#### ansible.builtin.copy 

```yaml
#复制本机文件到受管节点
- name: Copy file with owner and permissions
  ansible.builtin.copy:
    src: /srv/myfiles/foo.conf
    dest: /var/www/html/index.html
    owner: foo
    group: foo
    mode: '0644'
    setype:  httpd_sys_content_t
    
#复制文本内容testweb至目标文件，文件不存在则创建  
- name: Copy using inline content
  ansible.builtin.copy:
    content: "testweb"
    dest: /var/www/html/index.html
```



#### ansible.builtin.lineinfile

```bash
- name: Ensure SELinux is set to enforcing mode
  ansible.builtin.lineinfile:
    path: /etc/selinux/config
    regexp: '^SELINUX='
    line: SELINUX=permissive
      
docs.ansible.com 搜索引擎中搜索：filters --- Using filters to --- 搜索admin---default('admin', true) 
```



#### ansible.builtin.blockinfile

```
- name: Insert/Update HTML surrounded by custom markers after <body> line
  ansible.builtin.blockinfile:
    path: /opt/index.html
    marker: "<!-- {mark} ANSIBLE MANAGED BLOCK -->"
    insertafter: "<body>"
    block: |
      <h1>Welcome to {{ ansible_hostname }}</h1>
      <p>Last updated on {{ ansible_date_time.iso8601 }}</p>
```



#### ansible.builtin.template  

```bash
- name: Template a file to /etc/files.conf
  ansible.builtin.template:
    src: /mytemplates/foo.j2
    dest: /etc/file.conf


- name: Download foo.conf
  ansible.builtin.get_url:   #该模块可以将网络上的文件，直接下载至受管节点上。
    url: http://materials/hosts.j2   #源文件
    dest: /opt/host.txt  #目标文件位置
```





## [二 使用jinja2模板部署自定义文件]()



**培训目标**
将文件部署到使用 Jinia2模板自定义的受管主机。



### 1.Jinja2简介

```bash
1 管理文件通常会使用一些模块，copy，file，blockinfile，lineinfile...
2 更好的配置文件管理方式是使用jinja2语法制作模板文件来生成最终使用的配置文件。
3 jinja2模板文件内，可以通过多种方式编辑或构成，比如魔法变量、事实变量、普通字符、控制语句语法...
4 使用jinja2模板的方法是，先构建jinja2模板，再通过template模块将j2模板同步至受管节点。
5 构建模板文件通常名称自定义，以.j2结尾，类似shell脚本的.sh  python脚本的.py
```



#### 1 使用分隔符

```shell
1、构建jinja2模板
$ vim jin.j2  #文件名用.j2结尾
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
{# host file #}    #描述，为管理员做提示作用。用户不可见
{{ ansible_nodename }} {{ ansible_hostname }}

2、通过templete模块，同步模板文件至受管主机，同时收集事实变量值，将结果生成至相应文件中。
$ vim temp.yml
---
- name: sync file
  hosts: servera
  tasks:
  - name: Template a file to /etc/files.conf
    ansible.builtin.template:
      src: jin.j2
      dest: /etc/myhosts
$ ansible-navigator run temp.yml -m stdout

3、在受管节点上查看文件结果
$ ansible servera -m shell -a 'cat /etc/myhosts'
servera | CHANGED | rc=0 >>
haha
servera heihei servera.lab.example.com

```



#### 2 管理模版文件

```shell
1、在配置文件中定义ansible_managed功能，添加描述信息：“Ansible hahaha”
[greg@control ansible]$ vim ansible.cfg
[defaults]
ansible_managed = Ansible hahaha
变量名 =  变量值

2、在jinja2模板中调用该功能
#vim jinja.j2
{# host file #}     #{##}注释客户端生成文件是不显示
{{ ansible_managed }}  #{{}}描述，客户端生成文件时会显示
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
```



### 2.控制结构-使用循环

```bash
-shell
for user in list;do 
   command
done

-jinja2
{% for user in users %}   #user变量替换为users变量中的所有值，一行一个值。
{{ user }}
{% endfor %}
```

例1:

```bash
$ vim jinja2.j2
{% for host in groups.all %}  #使用for 或if 时控制结构使用{% %}
{{ host }}
{% endfor %}
```

#### 1文件生成方法：

```bash
$ vim debug.yml
---
- name:
  hosts: all
  tasks:
  - ansible.builtin.debug:
      var: hostvars
$ ansible-navigator run debug.yml  -m stdout > 1.txt
```



### 3.例2 生成/etc/hosts

```bash
1
$ vim /home/student/ansible/temp.yml
127.0.0.1 localhost localhost.localdomain localhost4 localhost4.localdomain4
::1 localhost localhost.localdomain localhost6 localhost6.localdomain6

{% for host in groups.all %}
{{ hostvars[host].ansible_default_ipv4.address }} {{ hostvars[host].ansible_nodename }} {{ hostvars[host].ansible_hostname }} 
{% endfor %}

2
vim /home/student/ansible/temp.yml
---
- name: sync file
  hosts: all      #一定要用all，因为要收集5台主机的事实变量
  tasks:
  - name: Template a file to /etc/files.conf
    ansible.builtin.template:
      src: hosts.j2
      dest: /etc/myhosts
    when: inventory_hostname in groups.dev     #匹配dev组
    
$ ansible-navigator run temp.yml -m stdout
$ ansible  servera -m shell -a 'cat /etc/myhosts'
servera | CHANGED | rc=0 >>
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

172.25.250.10 servera.lab.example.com servera
172.25.250.11 serverb.lab.example.com serverb
172.25.250.12 serverc.lab.example.com serverc
172.25.250.13 serverd.lab.example.com serverd

实验完毕

[Discovering variables: facts and magic variables — Ansible Documentation](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_vars_facts.html)

网站中查找关键字：facts  选择：Discovering variables: facts and magic variables   然后搜索hostvars[host]

以json格式查看：
{{ hostvars[i]['ansible_facts'] |  to_nice_json }}
```



# 第6章 管理大项目



**培训目标**
编写复杂的主机模式，以便高效地为 play选择主机。



## [一 引用清单主机]()

```bash
使用主机模式的常见方式：

1.[ad-hoc:]
ansible dev -m shell 

2.[playbook:]
- name:
  hosts: dev
```

```bash
1 清单里使用通配符匹配多个主机
[START:END]
192.168.[0:15].[0:255] 表示 192.168.0.0-192.168.15.255
server[a:c].example.com  表示   a-c
server[01:15].example.com  表示   server01.example.com-server15
ipv6也可以通过[a:f]这种方式
all： 所有主机
ungrouped ： 不属于任何一个组的所有主机

2 主机模式其他方式
hosts： all
hosts： ungrouped

#使用特殊字符时，必须添加单引号，否则不生效
hosts： '*'  和all相同   
hosts：'*.example.com'
hosts：'datacenter*'

#列表形式
hosts：servera,serverb  
hosts：webserver,serverc
hosts：'devops,server*'

#冒号：取代逗号
hosts：lab,&datacenter 匹配lab组同时也属于datacenter组，顺序无所谓&符号时同时也属于的意思
hosts：datacenter,!test2.example.com  表示datacenter组，但不包括test2.。。这个主机
hosts：all,!datacenter1  所有组，但不包含datacenter1组
```



## [二 包含和导入文件]()



**培训目标**
通过无条件或基于条件测试导入或包含来自外部文件的其他 playbook 或任务来管理大型playbook。



### 1.管理大型Playbook

```bash
如果playbook很长很复杂，可以拆分成较小的文件便于管理，以模块化管理，可以将多个不同功能的play，组合成一个主要的playbook，将文件中的任务列表插入play，这样可以将这种模块化的play应用到不同场景。

playbook
- httpd
- php
- mysql
```



#### 1 导入Playbook

```bash
1.将两个playbook加入到主playbook
$ vim one.yml 
---
- name: play1
  hosts: node1
  tasks:
  - name: Install the latest version of Apache
    ansible.builtin.yum:
      name: httpd
      state: latest
      
$ vim two.yml      
---
- name: play2
  hosts: node1
  tasks:
  - name: Make sure a service unit is running
    ansible.builtin.systemd:
      state: started
      name: httpd
      enabled: yes
      

2.在主playbook中和其他play交替使用
$ vim main.yml 
---
- name: four
  hosts: node1
  tasks:
  - ansible.builtin.debug:
      msg: haha
  
- name: one  因为加载的是playbook所以需要顶头写无缩进。
  import_playbook: one.yml
```



### 2.包含与导入

```yaml
Ansible可以支持两种方法将文件放入playbook中：
包含：内容是一个动态操作。在playbook运行期间，Ansible会在内容到达时处理所包含的内容。
导入：内容是一个静态操作。在运行开始之前，Ansible在最初解析playbook时预处理导入的内容。
```



#### 1 包含和导入

```bash
包含：
1.使用include_tasks功能时，包含时设置的when等条件语句将确定任务是否包含在play中
2.如果运行ansible-playbook --list-tasks以列出playbook中的任务，则不会显示已包含任务文件中的任务。将显示包含任务文件的任务。相比之下，import_tasks功能不会列出导入任务文件的任务，而列出已导入任务文件中的各个任务
#（[greg@control ansible]$ ansible-playbook playbook.yml --start-at-task webserver）
3.不能使用ansible-playbook --start-at-task从已包含任务文件中的任务开始执行playbook
#（[greg@control ansible]$ ansible-playbook playbook.yml --start-at-task webinstall）
4.不能使用notify语句触发已包含任务文件中的处理程序名称。可以在包含整个任务文件的主playbook中触发处理程序，在这种情况下，已包含文件中的所有任务都将运行
```

```bash
导入：
1.使用import_tasks功能时，导入时设置的when等条件语句将应用于导入的每个任务
2.无法将循环用于import_tasks功能
3.如果使用变量来指定要导入的文件的名称，则将无法使用主机或组清单变量
```



#### 2 例1

```bash
第一个tasks任务
[greg@bastion ansible]$ mkdir tasks    #tasks目录是自定义的，创建的目的只是方便存储管理tasks任务文件。
[greg@bastion ansible]$ vim tasks/apache.yml  #tasks任务文件，文件中没有主机模式
---
- name:
  ansible.builtin.yum:
    name: httpd
    state: latest
第二个tasks任务
[greg@bastion ansible]$ vim tasks/service.yml 
---
- name:
  ansible.builtin.service:
    name: httpd
    enabled: yes
    state: started
  
包含和导入的方式：
[greg@bastion ansible]$ vim main.yml   #此处main.yml是一个playbook，有主机模式
---
- name:
  hosts: node1
  tasks:
  - include_tasks: tasks/apache.yml  
  - import_tasks: tasks/service.yml
```



# 第7章 使用角色和Ansible collections简化Playbook

## [一 描述角色结构]()



**培训目标**
描述Ansible角色的用途、结构以及playbook中的角色使用。



```bash
1 Ansible中的角色，一个角色是一个结构化的目录组成，可以根据业务需要创建不同的角色，apache的，mysql的等，角色的优势，也是类似将playbook分割成更小的模块，进行模块化管理，简化playbook。

2 除了自行编写、使用、重用和共享角色外，您也可以从其他来源获取角色。您可以使用分发包(如Ansible内容集合)查找角色。
```



## [二 创建角色]()



**培训目标**
在playbook的项目目录中创建一个角色，并将其作为 playbook中某个play的一部分来运行。



### 1.角色创建流程

```bash
创建和使用角色分三步进行:
1.创建角色目录存储路径。
2.定义角色内容。
3.在playbook中使用角色。
```



#### 1 创建角色目录存储路径

```bash
#配置ansible.cfg文件中的roles-path,默认ansible会在roles子目录中查找角色。可以将自己的角色安装在~/ansible/roles子目录中。
1.默认路径：
$ ansible-galaxy role --help   查看角色的子命令帮助
$ ansible-galaxy list
# /usr/share/ansible/roles   -- 系统角色
# /etc/ansible/roles         -- 全局角色路径
[WARNING]: - the configured path /home/student/.ansible/roles does not exist.  --默认该目录不存在，需要的话可以根据需求创建
`如果ansible无法找到该位置角色，会按照ansible.cfg中roles_path指定的目录中查找`

使用自定义工作目录时，我们创建自定义roles（角色）目录，并使用ansible.cfg中roles_path=字段指定角色路径
$ pwd      #进入自己的工作目录
/home/student/ansible
[student@control ansible]$ mkdir roles    #创建存放角色的目录
[student@control ansible]$ vim ansible.cfg    
roles_path    = /home/student/ansible/roles   #指定角色路径
[student@bastion ansible]$ ansible-galaxy list   #一定要在工作目录中使用查看命令，调用当前工作目录配置文件中的角色路径 
# /home/student/ansible/roles      #查询结果应和配置文件的roles_path字段一致
```

```bash
2.[student@control ansible]$ ansible-galaxy init roles/apache   #创建角色
- Role roles/apache was created successfully   #提示创建成功
[student@control ansible]$ ansible-galaxy list    #列出所有角色
# /home/student/ansible/roles
- apache, (unknown version)      			   #角色名称
[student@control ansible]$ tree roles/apache
apache
    ├── defaults　　　　　　　角色默认变量
    │   └── main.yml
    ├── files				引用的静态文件，可以是一些文件，网页模板等。
    ├── handlers　　　　　　　　处理程序，通常通过模块完成的
    │   └── main.yml
    ├── meta				作者，许可、兼容性
    │   └── main.yml
    ├── README.md　　　　　　　
    ├── tasks　　　　　　　　　　任务，任务的组成就是模块应用，也是角色的主要功能
    │   └── main.yml       
    ├── templates　　　　　　　　模板文件，通常使用ｊｉｎｊａ２模板　
    ├── tests					测试
    │   ├── inventory			
    │   └── test.yml
    └── vars				　　角色变量
        └── main.yml
	（用不到的目录可以删除，如defaults、vars、tests）
```



#### 2 定义角色内容

```bash
[student@control ansible]$  tree roles/apache (可选，方便查看路径信息)
[student@control ansible]$  vim roles/apache/tasks/main.yml   #在tasks目录中完善角色内容
---
- name: install apache     #编写角色时，任务中只有模块任务，没有PLAY
  ansible.builtin.yum:
    name: httpd
    state: latest
- name: Start service httpd
  ansible.builtin.service:
    name: httpd
    state: started
    enabled: yes
- name: create web page
  ansible.builtin.template:
    src: jin2.j2    #jin2.j2需要手动创建
    dest: /var/www/html/index.html
  notify:
  - restart   #需要时将调用handlers处理程序，需要手动创建，restart通知中的处理程序名要和handlers/main.yml中处理程序描述一致
- name: Start service firewalld
  ansible.builtin.service:
    name: firewalld
    state: started
    enabled: yes
- name: permit apache
  ansible.posix.firewalld:     #ansible-navigator collections可以查看posix集合 #ansible-navigator doc ansible.posix.firewalld -m stdout 
    service: http
    permanent: yes
    state: enabled
    immediate: yes
    
[student@control ansible]$ vim roles/apache/templates/jin2.j2  #编写模板文件
ipadd={{ ansible_default_ipv4.address }}  hostname={{ ansible_hostname }}

[student@control ansible]$ vim roles/apache/handlers/main.yml   #编写处理程序
---
- name: restart 
  ansible.builtin.service:
    name: httpd
    state: restarted
```



#### 3 在playbook中使用角色

```bash
[student@bastion ansible]$ vim roles.yml
---
- name:
  hosts: servera
  roles:     #roles字段用来调用角色
  - apache   #被调用角色的名称

[student@control ansible]$ curl servera
ipadd=172.25.250.10  hostname=servera
```



## [三 从外部内容源部署角色]()



**培训目标**
从Git存储库或AnsibleGalaxy 等外部资源中选择和检索角色，并在您的 playbook中使用。



#### 1.外部内容来源

```bash
角色有多种获取方式：
本地tar包安装
通过网络地址安装
通过文件同时安装网络中多个地址角色（本课介绍）

roles/requirements.yml
- src： #角色网址
  varsion：#角色版本
  name：#安装在本地的角色名
- src：
  name：


ansible-galaxy role install -r roles/requirements.yml -p roles
-r 指定文件路径
-p 指定角色安装路径
roles/requirements.yml  角色安装信息，包括地址，角色名称等。
```

课上练习：

```bash
软件包参考班级群看板资料里：

1.将phpinfo.tar,haproxy.tar上传到Linux，kiosk家目录下，用kiosk登录
2.将两个角色包拷贝到foundation0的rhel9.0目录中
$ ssh root@localhost cp /home/kiosk/{phpinfo.tar,haproxy.tar} /content/courses/rh294/rhel9.0/materials
3.打开foundation0的浏览器输入http://172.25.254.254/materials/就可以看到两个软件包


实验：
[student@workstation ansible]$ mkdir roles
[student@workstation ansible]$ $ vim ansible.cfg
[defaults]
roles_path=/home/student/ansible/roles
[student@workstation ansible]$ vim roles/requirements.yml
---
- src: http://172.25.254.254/materials/haproxy.tar
  name: balancer
- src: http://172.25.254.254/materials/phpinfo.tar
  name: phpinfo

[student@workstation ansible]$ ansible-galaxy install -r roles/requirements.yml 
[student@workstation ansible]$ ansible-galaxy list
# /home/student/ansible/roles
- apache, (unknown version)
- balancer, (unknown version)
- phpinfo, (unknown version)
```



#### 2 练习

```
P284
```



## [四 从内容集合获取角色和模块]()



**培训目标**
从Ansible 内容集合中获取一组相关角色、补充模块和其他内容，并在 playbook 中使用。



### 1.Ansible内容集合

```
随着模块数量增加，管理困难。模块需要唯一名称，并保持更新。 

借助Ansible内容集合，Ansible代码可以与模块和插件分开更新。Ansible 内容集合可提供一组您可在您的playbook中使用的相关模块、角色和其他插件。这种方法便于供应商和开发人员按照自己的节奏维护和分发集合，而不受Ansible 版本的影响。
```



#### 1 Ansible内容集合的命名空间

```
命名空间是集合名称的第一部分。

由Ansible社区维护的所有集合可能会放入community命名空间中名称类似于:
community.crypto
community.postgresql
community.rabbitmq

红帽直接维护和支持的Ansible内容集合可能使用redhat命名空间有
redhat.rhv
redhat.satellite
redhat.insights等名称。
```



#### 2 选择Ansible内容集合的来源

```bash
无论您是将ansible-navigator 用于最小自动化执行环境，还是在裸机Ansible Core上使用ansible-playbook，您始终至少有一个可用Ansible内容集合:ansible.builtin。

此外，您的自动化执行环境可能还内置了其他自动化执行环境，例如，红帽Ansible 自动化平台2.2使用的默认执行环境ee-supported-rhel8。

如果您需要其他Ansible内容集合，可以将其添加到Ansible 项目的collections 子目录中。您可以从多个来源获取Ansible内容集合:

自动化中心
私有自动化中心
Ansible Galaxy
第三方Git存储库或存档文件
```



#### 3 安装Ansible内容集合

```bash
ansible配置文件ansible.cfg中设置了collections_paths选项来指定集合的默认路径：
~/.ansible/collections;/usr/share/ansible/collections 如果消除这两个目录的指定，则ansible-navigator 无法在这些目录的其版本中找到自动化执行环境内提供的Ansible内容集合。
```

```bash
$ ansible-galaxy collection install 集合 -p collections

-p collections 选项会将该集合安装到本地collections 子目录中。或者不适用-p，而在ansible.cfg文件中collections_paths选项来指定集合的默认路径

集合的来源可以是：
本地  
互联网
git仓库
```



#### 4 使用要求文件安装Ansible内容集合

```bash
1.将三个集合包上传到Linux，kiosk家目录下，用kiosk登录
2.将三个集合包拷贝到foundation0的rhel9.0目录中
$ ssh root@localhost cp /home/kiosk/{community-general-5.5.0.tar.gz,redhat-insights-1.0.7.tar.gz,redhat-rhel_system_roles-1.19.3.tar.gz} /content/courses/rh294/rhel9.0/materials
3.打开foundation0的浏览器输入http://172.25.254.254/materials/就可以看到两个软件包


-------------------------
【workstation】
1.创建存储集合的位置
$ mkdir /home/student/ansible/mycollections  
$ vim ansible.cfg
[defaults]
...        
collections_path=~/.ansible/collections:/usr/share/ansible/collections/home/student/ansible/mycollection: #集合默认路径不删除，额外添加当前用户工作目录中集合路径。
$ vim /home/student/ansible/mycollections/requirements.yml
---
collections:
- name: http://172.25.254.254/materials/community-general-5.5.0.tar.gz
- name: http://172.25.254.254/materials/redhat-insights-1.0.7.tar.gz
- name: http://172.25.254.254/materials/redhat-rhel_system_roles-1.19.3.tar.gz

$ ansible-galaxy collection install -r requirements.yml -p /home/student/ansible/collections/mycollections
$ ansible-galaxy collection list
```



#### 5 练习

```
必做 P297
```



## [五 利用系统角色重用内容]()



**培训目标**
编写利用红帽帽企业 Linux的系统角色执行标准操作的 playbook。



### 1.系统角色

```
linux6时间服务ntpd，linux7，chronyd，管理员必须配置两个服务，如果用系统角色system-roles.timesync角色就可以配置6、7的时间同步。
```



#### 1 内容集合方式安装系统角色

```bash
#角色以两种形式提供：

1.内容集合 redhat.rhel_system_roles

$ mkdir /home/student/ansible/collections
$ vim /home/student/ansible/collections/requirements.yml
---
collections
name: redhat.rhel system roles

$ ansible-galaxy collection install -p collections/ -r home/student/ansible/collections/requirements.yml

此处可以通过文件安装，也可通过直接指定本地tar包安装，可以参考教材练习
```

```bash
$ sudo find ./mycollection/ -name  selinux-playbook.yml #不安装角色包可以使用该条命令搜索
$ cp /usr/share/ansible/collections/ansible_collections/redhat/rhel_system_roles/docs/selinux/selinux-playbook.yml /home/greg/ansible/selinux.yml
```



#### 2 rpm包方式安装系统角色

```bash
系统角色使用流程
1.通过软件安装获得系统角色（rhel-system-roles.noarch）
2.定义系统角色路径（存放角色的位置），并将其记录配置文件
cd /;ansible-galaxy list   , vim ~/ansible/ansible.cfg/;roles_path=xxxx:xxxx
3.使用系统角色，将系统角色添加至playbook，并修改内容
4.应用
```

```bash
1.安装
系统帮助我们定义了一些角色，有不同的功能，需要通过安装软件包。
[greg@control ansible]$ cd /
[greg@control /]$ sudo yum search roles
[greg@control /]$ sudo yum install -y rhel-system-roles.noarch     
[greg@control /]$ ansible-galaxy list    #在根目录下查看角色路径，该路径为默认系统角色路径
# /usr/share/ansible/roles     #系统角色路径
- linux-system-roles.kdump, (unknown version)
....
- rhel-system-roles.timesync, (unknown version)
# /etc/ansible/roles
 [WARNING]: - the configured path /home/greg/.ansible/roles does not exist.
```

```bash
2.定义角色路径
[greg@control /]$ cd ~/ansible/   #在ansible工作目录中查看时属于greg用户定制的角色路径
[greg@control ansible]$ vim ansible.cfg
roles_path    = /home/greg/ansible/roles:/usr/share/ansible/roles #:冒号分割，并添加系统角色路径
[greg@control ansible]$ ansible-galaxy list
# /home/greg/ansible/roles						#之前定义好的自定义角色路径
- apache, (unknown version)
# /usr/share/ansible/roles  					#系统角色路径
- linux-system-roles.kdump, (unknown version)
......
- rhel-system-roles.timesync, (unknown version)
```

```bash
3.使用系统角色，将系统角色添加至playbook并应用
[greg@control ansible]$ rpm -ql rhel-system-roles.noarch | grep timesync #看README.md
[greg@control ansible]$ cp /usr/share/doc/rhel-system-roles/timesync/example-timesync-playbook.yml /home/greg/ansible/timesync.yml   #找例子，并复制到工作目录中
[greg@control ansible]$ vim timesync.yml 	 #编辑角色
---
- hosts: all
  vars:
    timesync_ntp_servers:
      - hostname: 172.25.254.254
        iburst: yes
  roles:
    - rhel-system-roles.timesync

4 使用系统角色
[greg@control ansible]$ ansible-playbook timesync.yml

查询验证
ansible all -a 'chronyc sources -v'
```

### 通过内容集合安装使用系统角色

#### 1 timesync  P313

```bash
$ cp /usr/share/ansible/collections/ansible_collections/redhat/rhel_system_roles/docs/timesync/multiple-ntp-servers.yml  timesync.yml
$ vim timesync.yml
- hosts: all
  vars:
    timesync_ntp_servers:
      - hostname: 172.25.254.254
        iburst: yes
  roles:
    - redhat.rhel_system_roles.timesync
$ ansible-navigator run timesync.yml -m stdout
$ ansible all -m shell -a 'chronyc sources -v '
```

#### 2 selinux

```bash
$ cp /usr/share/ansible/collections/ansible_collections/redhat/rhel_system_roles/docs/selinux/selinux-playbook.yml selinux.yml
$ vim selinux.yml
---
- hosts: all
  vars:
    selinux_policy: targeted
    selinux_state: permissive

  roles:
  - redhat.rhel_system_roles.selinux
$ ansible-navigator run selinux.yml  -m stdout

```



## [六 通过文件安装角色]()

```bash
Ansible官网：https://docs.ansible.com/ansible/2.9/galaxy/user_guide.html#installing-roles-from-galaxy

https://galaxy.ansible.com/nginxinc/nginx_core  #该网站可以查看角色包信息

#实验需要在模拟考试环境下做，普通环境没有角色包
[greg@control ansible]$ vim roles/requirements.yml  #制作角色部署文件
---
- src: http://materials/phpinfo.tar    #角色在网络中的路径
  name: phpinfo						   #指定安装在系统中的角色名称
- src: http://materials/haproxy.tar
  name: balancer
  
[greg@control ansible]$ ansible-galaxy install --help
[greg@control ansible]$ ansible-galaxy install -r roles/requirements.yml  #-r 指定角色文件
[greg@control ansible]$ ansible-galaxy list   #检测
# /home/greg/ansible/roles
- phpinfo, (unknown version)    #查看结果
- balancer, (unknown version)
```



# 第8章 对Ansible进行故障排除



## [一 对 playbook进行故障排除]()



**培训目标**
对新 playbook的一般问题进行故障排除，并修复这些问题。



#### 1 调试Playbook

```bash
ansible-playbook debug.yml -v   #显示详细信息-v -vv -vvv -vvvv
ansible-navigator run playbook.yml -m stdout -v
```



#### 2 Debug

```bash
通过debug模块打印收集到的变量信息，帮助管理员了解模块执行过程及结果
---
- hosts: serverb
  tasks:
  - name: install the latest version of Apache
    ansible.builtin.yum:
      name: vsftpd
      state: latest
    register: web_install   #注册变量收集模块的执行信息
  - ansible.builtin.debug:
      var: web_install      #debug模块可以帮助打印出执行信息

可以一次性将hostvars收集到的所有魔法及事实变量打印出来，并可以记录到文件中
[greg@control ansible]$ vim debug.yml
---
- name: test
  hosts: node1
  tasks:
  - ansible.builtin.debug:
      var: hostvars
[greg@control ansible]$ ansible-playbook  debug.yml > 1.txt 
 

关闭事实收集
如果要关闭收集，可以编辑配置文件
gathering = explicit
或者在playbook里
gather_facts: yes/no  true/false
```



#### 3 检查 Playbook 中的错误

```bash
-RHEL8
$ ansible-playbook playbook.yml --syntax-check
playbook: playbook.yml

-RHEL9
greg@control ansible]$ ansible-navigator run -m stdout debug.yml   --syntax-check
playbook: /home/greg/ansible/debug.yml

```



#### 4 检查 Playbook的问题并遵循良好实践

```bash
#常见语法问题
name：  冒号后面要空格，内容随意
hosts： all 指定的主机不在清单中，报错无partten
syntax-errors：注意格式缩进，2格缩进
变量设置错误，或调用时错误（变量名写错，变量开头要加双引号）  
when条件： （逻辑判断思路错误，或书写错误）
```



#### 5 检查Playbook 工件和日志文件

```bash
ansible-navigator 可以生成 playbook工件，以JSON格式存储与 playbook的运行相关的信息。
您可以将与 playbook运行相关的信息记录到系统上您可写入位置处的文本文件中。
```

```bash
RHEL9中的ansible自动开启日志功能，可通过配置文件设置及手工设置

#配置文件
$ vim ~/.ansible-navigator.yml
---
ansible-navigator:
  execution-environment:
    image: utility.lab.example.com/ee-supported-rhel8:latest
    pull:
      policy: missing
  playbook-artifact:
    enable: false    # false/true 关闭/打开

#手工指定
--pae  --playbook-artifact-enable
```



#### 6 将输出记录到文本文件

```bash
-RHEL8&9

Ansible 提供了一个内置日志基础架构，可通过ansible.cfg 配置文件 [defaults] 部分中的log_path参数进行配置，或通过$ANSIBLE_LOG_PATH环境变量进行配置。在进行上面一项或两项配置后，Ansible会以文本形式将ansible-navigator 命令的输出存储到所配置的日志文件中。此机制也适用于ansible-playbook 等早期的工具。
如果您将Ansible配置为将日志文件写入 /var/log，红帽建议您配置 logrotate来管理文件。
```



## [二 对Ansible受管主机进行故障排除]()



**培训目标**
对运行playbook时受管主机上的故障进行故障排除。



#### 1 对连接进行故障排除

```
1 远程用户（普通，root）
2 远程密码、特权密码问题
3 账号密码有多种设置方案
```

#### 2 使用Ansible运行临时命令

```bash
ad-hoc命令可以用于简单部署场景，比如对一组目标部署一个任务，或检索受管节点的运行情况、状态等。
ansible all -m ping

#注意RHEL9中默认不允许root用户登录，需要修改配置文件允许root登录。所建议使用普通用户作为远程管理用户。
解决方法,服务器端：
$ vim /etc/ssh/sshd_config
#PermitRootLogin prohibit-password
PermitRootLogin yes
$ systemctl  reload sshd
```

#### 3 --step 手动控制执行的步骤

```bash
$ ansible-playbook playbook.yml --step
$ ansible-navigator run playbook.yml  -m stdout --step

PLAY [PLAY1] ********************************************************************************************
Perform task: TASK: Gathering Facts (N)o/(y)es/(c)ontinue: n

手动输入n  y c来控制playbook中执行的步骤
n 不执行该步骤
y 执行该步骤
c 继续自动执行到结束

通过该方法，我们可以让有问题的步骤执行，而无关紧要的步骤可以跳过不执行。达到排错的目的。
```

#### 4 --start-at-task 从指定步骤执行任务

```bash
[greg@control ansible]$ ansible-playbook playbook.yml --start-at-task='Start service httpd'

选项“
--start-at-task    #指定具体执行步骤，等号后面指定模块的描述，如果描述内容中有空格，建议用单或双引号引起来，表示为一个参数。
```

#### 5 -C --check 烟雾测试

```bash
烟雾测试：在管理节点执行剧本，显示剧本的真实执行结果，但是不在受管节点上部署。
$ ansible-playbook playbook.yml --help | grep \\-C
$ ansible-navigator run playbook.yml  -m stdout -C
[greg@control ansible]$ vim playbook.yml
---
- name: PLAY1
  hosts: node2
  tasks:
  - name: install  apache
    ansible.builtin.yum:
      name: httpd
      state: latest
  - name: Copy using inline content
    ansible.builtin.copy:
      content: 'test web page'
      dest: /var/www/html/index.html
  - name: Start service httpd
    ansible.builtin.service:
      name: httpd
      state: started
  - name: Start service firewalld
    ansible.builtin.service:
      name: firewalld
      state: started
  - name: permit apache
    ansible.posix.firewalld:
      service: http
      permanent: yes
      state: enabled
      immediate: yes
[greg@control ansible]$ ansible-playbook playbook.yml -C
```

#### 6 通过发送脚本解决问题

```bash
$ vim test.sh   #自定义脚本名及内容
#!/bin/bash
date
$ vim debug.yml #剧本名自定义
---
- name:
  hosts: servera
  tasks:
  - name: Run a script
    ansible.builtin.script: /home/student/ansible/test.sh    #使用script模块，加脚本文件路径
    register: haha   #register收集上面模块执行结果至 haha（自定义名称）的这个变量中
  - ansible.builtin.debug:
      var: haha   #通过debug模块将haha变量的值打印出来。
$ ansible-playbook debug.yml     

`如果不想写register，可以执行剧本时添加-v 类似：ansible-playbook debug.yml -v`
```





# 第9章 自动执行Linux管理任务



## [一 管理软件和订阅]()



**培训目标**
订阅系统，配置软件通道和存储库，启用模块流以及管理受管主机上的 RPM软件包。



#### 1 优化多软件包安装

```bash
ansible.builtin.---

- name:
  hosts: all
  tasks:
  - name: install the latest version of Apache
    ansible.builtin.yum:
      name: httpd   #httpd
      state: latest
  - name: install the latest version of Apache
    ansible.builtin.yum:
      name: php   #php
      state: latest
---------------------------------------------
- name: install the latest version of Apache
  ansible.builtin.yum:
      name: "{{ item }}"
      state: latest
  loop:　    #用loop方式简化playbook
    - httpd
    - php
      
yum install -y httpd
yum install -y php
---------------------------------------------
- name: install the latest version of Apache
  ansible.builtin.yum:
    name: 
    - httpd
    - php
    state: latest
yum install -y httpd php
--------------------------------------------- 
- name: ensure a list of packages installed
  ansible.builtin.yum:
    name: "{{ packages }}"
  vars:
    packages:
    - httpd
    - httpd-tools
--------------------------------------------- 
      
等同于：yum install -y httpd php
或
loop的这种方式，系统会执行两次独立事务，对每个事务应用一个软件包

yum模块：

state： absent删除, installed,  present确保安装  latest升级到最新版本
latest 等同  yum update

yum remove  yum install 

 name： '*'  代表所有软件包
 name: "@RPM Development tools"   ansible命令里面安装包组要加@
```



#### 2 收集有关已安装软件包的事实

```bash
---
- name:
  hosts: node1
  tasks:
  - name: Gather the package facts
    ansible.builtin.package_facts:
      manager: auto

  #- name: Print the package facts
  #  ansible.builtin.debug:
  #    var: ansible_facts.packages

  - name: Check whether a package called foobar is installed
    ansible.builtin.debug:
      msg: "{{ ansible_facts.packages.zip.0.version }}"

```



#### 3 查看用于管理软件包的其他模块

```bash
- name: Install httpd on RHEL 8 and 9
  ansible.builtin.dnf
    name: httpd
    state: present

- name: Install httpd on RHEL 7 and earlier
  ansible.builtin.yum:
    name:httpd
    state:present
```



#### 4 配置RPM软件包存储库

```bash
$ ansible-doc -l | grep yum

---
- name:
  hosts: node1
  tasks:
  - name: Add multiple repositories into the same file (1/2)
    ansible.builtin.yum_repository:
      name: EX294_BASE #file字段不存在时，默认用name字段代替源文件名称
      description: EX294 base software
      file: rhel  #file选项作用是定义yum源文件名称，无该字段时会用name字段后的值代替文件名
      baseurl: http://content/rhel8.4/x86_64/dvd/BaseOS
      gpgcheck: yes
      gpgkey: http://content/rhel8.4/x86_64/dvd/RPM-GPG-KEY-redhat-release
      enabled: yes
      
  - name: Add multiple repositories into the same file (1/2)
    ansible.builtin.yum_repository:
      name: EX294_STREAM
      description: EX294 stream software
      file: rhel  #多个模块使用同一个file字段的名称时，会将源地址写入到同一个文件中。
      baseurl: http://content/rhel8.4/x86_64/dvd/AppStream
      gpgcheck: yes
      gpgkey: http://content/rhel8.4/x86_64/dvd/RPM-GPG-KEY-redhat-release
      enabled: yes  #enabled默认值为yes，考试时要写该选项。


ansible模块选项和vim配置文件内容对比
  vim：                ansible：
      rhel.repo          file
      [rhel]            * name
      name=rhel         * description
      baseurl=			* baseurl
      gpgcheck=			* gpgcheck
      gpgeky=			* gpgkey
      enabled=			* enabled
      
      
测试方法：
$ ansible all -a 'yum repolist all'

# 受管节点检测
$ cat rhel.repo

```

```bash
$ ansible-doc rpm_key   
- ansible.builtin.rpm_key:
    state: present
    key: http://apt.sw.be/RPM-GPG-KEY.dag.txt
```



## [二 管理用户和身份验证]()



**培训目标**
管理Linux用户和组，配置SSH，以及修改受管主机上的Sudo配置。



#### 1 user 模块

```bash
需求:在servera上创建用户tom 附加组为group1，并设置密码为passwordtom: tom123用sha512方式哈希，uid =2000
user这个模块类似这些功能： useradd userdel usermod

$ cat group.yml     
- name:
  hosts: servera,server
  vars:
  - passwordtom: tom123
  tasks:
  - name:  create group
    ansible.builtin.group:
      name: "{{ item }}"
    loop:
      - group1
      - group2
  - name: Add the
    ansible.builtin.user:
      name: tom     
      comment: student
      uid: 2000
      password_expire_max: 10
      group: group1
      groups: group1,group2
      shell: /bin/bash
      password: "{{ passwordtom | password_hash('sha512') }}"
      #passwordtom该密码位置如果是字符串用单引号引起来，如果是变量则不需要单引号
      
          
#验证用户密码过期时间
chage -l tom
Password expires					: Jun 11, 2024
#验证用户密码是否正确，可以从node2登录node1，测试node1上的tom用户
ssh tom@node1

Ansible网页搜索：password_hash--Using filter--网页中查找password_hash，查看密码哈希方式，一定注意是512
Ansible官网：  https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html
      模块中：append: yes    如果想额外添加附加群组，此选项需要yes(usermod -a -G  grouptest u1) 
```

#### 

#### 2 group模块

```bash
[greg@bastion ansible]$ cat group.yml 
---
- name:
  hosts: dev
  tasks:
  - name: Ensure group "grouptest" exists
    ansible.builtin.group:
      gid: 10000
      name: grouptest
      state: present absent


等同于：groupadd  grouptest
等同于：groupadd -g 10000 grouptest
groupdel
```



## [三 管理启动过程和调度进程](/etc/yum.repes.d/)

#### 1 at 

```yaml
---
- name:
  hosts: dev
  tasks:
  - name: Schedule a command to execute in 20 minutes as root
    ansible.posix.at:
      command: ls -d / >/dev/null
      count: 20
      units: minutes
      

      
默认是创建一个任务，给root，删除的	话使用选项state：absent
```



#### 2 cron （考点）

```bash
[greg@bastion ansible]$ cat cron.yml 
---
- name:
  hosts: all
  tasks:
  - name: Ensure a job that runs at 2 and 5 exists. 
    ansible.builtin.cron:
      name: "check dirs"   
      minute: "*/2"
      hour: "2,5"
      day: 1-10
      user: harry
      job: "ls -alh > /dev/null"
     

[greg@bastion ansible]$ ansible dev -m shell -a 'crontab -u harry -l'
```



#### 3 管理服务

```bash
---
- name:
  hosts: dev
  tasks:
  - name: install the latest version of Apache
    ansible.builtin.yum:
      name: httpd
      state: latest
          
  - name: Start service httpd, if not started
    ansible.builtin.service:
      name: httpd
      state: started
      enabled: yes    #开机自启动
~                  
```



### 4 systemd	

```yaml
[greg@bastion ansible]$ cat systemd.yml 
---
- name:
  hosts: dev
  tasks:
  - name: install the latest version of Apache
    ansible.builtin.yum:
      name: httpd
      state: latest

  - name: Make sure a service is running
    ansible.builtin.systemd:
      name: httpd
      state: started
      enabled: yes
      
测试命令：
ansible dev -m shell -a 'systemctl status httpd'
ansible dev -m shell -a 'systemctl is-enabled httpd'
```



#### 5 reboot

```
- name: Unconditionally reboot the machine with all defaults
  reboot:

```



#### 6 command & shell  支持更多的特殊字符

```bash
$ ansible servera -m command -a 'useradd user1'
$ ansible servera -a 'useradd user2'
$ ansible servera -a 'echo 123456 | passwd --stdin user1'
$ ansible servera -m shell -a 'echo 123456 | passwd --stdin user1'
```



## [四 管理存储]() 



**培训目标**
对存储设备进行分区，配置LM，格式化分区或逻辑卷，挂载文件系统，以及添加交换空间。



| 模块名                       | linux指令  | Linux实施命令                                                |
| ---------------------------- | ---------- | ------------------------------------------------------------ |
| community.general.parted     | parted     | parted /dev/vdb mkpart part1 2048s 1G                        |
| community.general.lvg        | vgcreate   | vgcreate -s 16M vg100 /dev/vdb{1..2}                         |
| community.general.lvol       | lvcreate   | lvcreate -L 100M -n lv100 vg100                              |
| community.general.filesystem | mkfs       | mkfs.ext4 /dev/vg100/lv100                                   |
| ansible.posix.mount          | /etc/fstab | echo "/dev/vg100/lv100 ext4 /mnt/data defaults 0 0" > /etc/fstab |

### 一  通过模块管理存储

```bash
1.分区-格式化-挂载
2.分区-lvm（pv,vg,lv）-格式化-挂载
```

```bash
1-1 分区 community.general.parted
#创建两个分区每个500M
---
- name: parted
  hosts: node2
  tasks:
  - name: part 1 2048s-500M
    community.general.parted:
      device: /dev/vdc
      number: 1
      state: present
      part_end: 500MiB
  - name: part 2 500M-1000M
    community.general.parted:
      device: /dev/vdc
      number: 2
      state: present
      part_start: 500MiB
      part_end: 1GiB


1-2 格式化 community.general.filesystem
#两个分区都格式化为ext4文件系统，如果选择不同文件系统，可以分成两个模块。
  - name: Create a ext4
    community.general.filesystem:
      fstype: ext4
      dev: "{{ item }}" 
    loop:
      - /dev/vdc1
      - /dev/vdc2

1-3 挂载 ansible.posix.mount
#自动创建挂载点/mnt/part1,再将/dev/vdc1写入/etc/fstab文件最后一行，并挂载。
  - name: Mount DVD read-only
    ansible.posix.mount:
      path: /mnt/part1
      src: /dev/vdc1
      fstype: ext4
      state: mounted

```

```bash
2.分区-lvm（pv,vg,lv）-格式化-挂载
2-1 分区 community.general.parted 
#分两个区每个大约500M，/dev/vdb
---
- name: lv
  hosts: node2
  tasks:
  - name: part 1 2048s-500M
    community.general.parted:
      device: /dev/vdb
      number: 1
      state: present
      part_end: 500MiB
  - name: part 2 500M-1000M
    community.general.parted:
      device: /dev/vdb
      number: 2
      state: present
      part_start: 500MiB

2-2 pv+vg community.general.vg
#两个分区都加入vg，名称vg100
#ansible-navigator collections -m stdout | grep vg$ 查看帮助
  - name: Create a volume group vg100
    community.general.lvg:
      vg: vg100
      pvs: /dev/vdb1,/dev/vdb2
      pesize: 32

2-3 lv community.general.lvol
#创建一个lv名为lv100，大小800M
  - name: Create a logical volume lv100 size 800M
    community.general.lvol:
      vg: vg100
      lv: lv100
      size: 800
2-4 格式化 community.general.filesystem
#格式化为xfs文件系统
 - name: Create a xfs
    community.general.filesystem:
      fstype: xfs
      dev: /dev/vg100/lv100
2-5 挂载 ansible.posix.mount
#lv挂载到/mnt/data，并设置为开机自启动。
  - name: /mnt/data
    ansible.posix.mount:
      path: /mnt/data
      src: /dev/vg100/lv100
      fstype: xfs
      state: mounted
      
      
#整体配置
---
- name: lv
  hosts: node2
  tasks:
  - name: part 1 2048s-500M
    community.general.parted:
      device: /dev/vdb
      number: 1
      state: present
      part_end: 500MiB
  - name: part 2 500M-1000M
    community.general.parted:
      device: /dev/vdb
      number: 2
      state: present
      part_start: 500MiB
  - name: Create a volume group vg100
    community.general.lvg:
      vg: vg100
      pvs: /dev/vdb1,/dev/vdb2
      pesize: 32
  - name: Create a logical volume lv100 size 800M
    community.general.lvol:
      vg: vg100
      lv: lv100
      size: 800
  - name: Create a xfs
    community.general.filesystem:
      fstype: xfs
      dev: /dev/vg100/lv100
  - name: /mnt/data
    ansible.posix.mount:
      path: /mnt/data
      src: /dev/vg100/lv100
      fstype: xfs
      state: mounted

#搜索帮助总结
nsible-navigator --help
ansible-navigator collections -m stdout | grep parted$
ansible-navigator collections -m stdout | grep lvg$
ansible-navigator collections -m stdout | grep lvol$
ansible-navigator collections -m stdout | grep filesystem$
ansible-navigator collections -m stdout | grep mount$
ansible all -m shell -a 'lsblk'

ansible-navigator doc -m stdout community.general.parted
ansible-navigator doc -m stdout community.general.filesystem
ansible-navigator doc -m stdout ansible.posix.mount
ansible-navigator doc -m stdout community.general.lvg
```

### 二 通过系统角色管理存储

### 1.磁盘管理角色

```bash
$ vim disk.yml
- hosts: webservers
  roles:
  - name: redhat.rhel_system_roles.storage
    storage_volumes:
      - name: part1
        type: disk
        disks:
        - /dev/vdb
        size: 1500M
        mount_point: /mnt/part1
        fs_label: images
        state: mounted
ansible-navigator run disk.yml  -m stdout
```

### 2.逻辑卷角色

```bash
$ vim storage.yml
- hosts: webservers
  roles:
  - block:
    - name: redhat.rhel_system_roles.storage
      storage_volumes:
        - name: part1
          type: disk
          disks:
          - /dev/vdb
          size: 1500M
          mount_point: /mnt/part1
          fs_label: images
          state: mounted
    rescue:
    - name: redhat.rhel_system_roles.storage
      storage_volumes:
        - name: part1
          type: disk
          disks:
          - /dev/vdb
          size: 500M
          mount_point: /mnt/part1
          fs_label: images
          state: mounted
$ ansible-navigator run storage.yml  -m stdout
```



## [五 管理网络]()



```bash
[greg@control ansible]$ sudo find ./mycollection/ -name '*.md' | grep network
[greg@control ansible]$ vim ./mycollection/ansible_collections/redhat/rhel_system_roles/roles/network/README.md
[greg@control ansible]$ sudo find ./mycollection/ -name '*.yml' | grep network 

$ vim /usr/share/doc/rhel-system-roles/collection/roles/network/README.md
$ vim playbook.yml
---
- name: haha
  hosts: webservers

  roles:
  - redhat.rhel_system_roles.network
  vars:
    network_connections:
      - name: eth1
        type: ethernet
        ip:
          dhcp4: no
          gateway4: 192.0.2.1
          dns:
          - 192.0.2.2
          address:
          - 192.0.2.3/24
        state: up


$ ansible-navigator run playbook.yml  -m stdout
```



# 附加：

## 使用普通用户远程管理受管主机（重点）

```bash
【超级用户远程管理方式】
vim /home/greg/ansible/ansible.cfg
remote_user=root
host_key_checking = False

vim /home/greg/ansible/inventory
[all:vars]
#ansible_user=root
ansible_password=redhat

【普通用户远程管理方式】
需求，请使用greg用户远程管理受管主机
【bastion】控制节点
ansible.cfg
[defaults]
remote_user = greg
host_key_checking = False

[privilege_escalation]
become=True
become_method=sudo 
become_user=root
become_ask_pass=false
----------------------------------------以下配置考试环境下已做
管理节点
ssh-keygen 
三个回车
ssh-copy-id greg@workstation
ssh greg@workstation

其他受管主机上每一个主机都做如下操作
[root@workstation ~]# useradd  greg
[root@workstation ~]# echo redhat | passwd --stdin greg
[root@workstation ~]# visudo
greg    ALL=(ALL)       NOPASSWD: ALL
```

