| 第一天              | 第二天               | 第三天                   | 第四天        | 第五天        |
| :------------------ | -------------------- | :----------------------- | ------------- | ------------- |
| 1、提高命令行生产率 | 4、管理SELinux安全性 | 7、访问网络附加存储      | 10、安装rhel  | 11、容器介绍2 |
| 2、计划将来的任务   | 5、管理基本存储      | 8、控制启动过程          | 11、容器介绍1 | 12、总复习    |
| 3、调优系统性能     | 6、管理逻辑卷        | 9、管理网络安全firewalld |               |               |
|                     |                      |                          |               |               |

# 环境切换

```bash
【foundation0】

$ rht-clearcourse 0
$ rht-setcourse rh134
```



# 第一章 提高命令行生产率

## 脚本：

| 系统    |                                 |      |
| ------- | ------------------------------- | ---- |
| Windows | *.bat,*.cmd,.vbd                |      |
| Linux   | #!/bin/bash ,  chmod +x file.sh |      |

## 1.创建和执行BASH SHELL脚本

### 1.指定命令解释器（考点）

```bash
[root@foundation0 ~]# vim /test.sh     #掌握
#!/bin/bash                            #指定解释器为/bin/bash
date
echo  "hello world"

[root@foundation0 ~]# chmod +x test.sh  #赋予脚本执行权限
[root@foundation0 ~]# cd /
[root@foundation0 ~]# ./test.sh          #可以通过./这种相对路径方式执行脚本
[root@foundation0 ~]# /root/test.sh      #或通过绝对路径方式执行脚本

[root@foundation0 bin]# first.sh 
hello world
[root@foundation0 bin]# sh first.sh 
hello world
[root@foundation0 bin]# bash first.sh 
hello world
[root@foundation0 bin]# source first.sh 
hello world
[root@foundation0 bin]# . first.sh    
hello world

脚本中需要书写解释器#!/bin/bash，脚本内容可以是linux命令。linux系统可以通过/etc/shells查看支持的shell类型。也可以通过echo $SHELL来查看当前系统正在使用的shell类型
```

### 2.执行Bash Shell

```bash
[root@foundation0 /]# which ls

[root@foundation0 /]# echo $PATH
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin
```

练习：

```bash
[root@node1 opt]# echo $PATH
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin
[root@node1 opt]# which ls
alias ls='ls --color=auto'
	/usr/bin/ls
[root@node1 opt]# cd /root/bin
-bash: cd: /root/bin: No such file or directory
[root@node1 opt]# mkdir /root/bin
[root@node1 opt]# mv /test.sh /root/bin/
[root@node1 opt]# ls /root/bin/
test.sh
[root@node1 opt]# test.sh
hello world
```

### 3.对特殊字符加引号

```bash
\, '' , ""   #掌握

[root@servera /]# echo hello    
hello
[root@servera /]# echo $SHELL xxx
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin
[root@servera /]# echo \$SHELL
$PATH
[root@servera /]# echo '$SHELL'   #单引号内容是普通字符串，变量不生效
$PATH
[root@servera /]# echo "$SHELL"   #双引号内变量生效


【servera】
[root@servera ~]# echo $HOSTNAME
[root@servera ~]# ssh root@bastion 'touch /tmp/$HOSTNAME'     
[root@servera ~]# ssh root@bastion 'ls /tmp'
[root@servera ~]# ssh root@bastion "touch /tmp/$HOSTNAME"     
[root@servera ~]# ssh root@bastion 'ls -l /tmp'
```

### 4.``,$()

```bash
`命令`   #掌握  优先执行
$(命令)


在shell中设置一个变量暂时使用
[root@servera /]# var=`date`
[root@servera /]# var=$(date)
[root@servera /]# echo 'to day $var'
to day $var
[root@servera /]# touch $(date +%H%M%S).txt
取消一个变量
[root@servera /]# unset var
[root@servera /]# echo $var

第二个例子
whoami
echo whoami
echo `whoami`
echo $(whoami)
date
echo `date`
man date
date +%y%m%d
date +%Y%m%d
date +%Y-%m-%d
echo $(date +%Y-%m-%d)
touch $(date +%Y-%m-%d).txt
ls
tar -zcvf $(date +%Y-%m-%d).tar.gz /etc/
ls
cd /

既然可以通过``和$()的结果，通过echo来通过标准输出打印到屏幕上，那么我们也可以将其应用到脚本。
vim os.current
#!/bin/bash
  
echo -e "User:\t" $(whoami)
echo -e "HOST:\t" `hostname`
echo -e "ipv4:\t" $(ip a s eth0 | awk '/inet / {print $2}')
echo -e "Memory:\t" $(free -h | awk '/Mem/ {print $2}') 
echo -e "Disk:\t" $(df -ht xfs | awk '/dev/ {print $4}')

chmod +x os.current 
./os.current 
```

## 2.使用循环更高效的运行命令

### 1.for语句

```bash
语法：
for variable in list
	do
		command variable
done

例子：
for i in 1 2 3    
	do 
		echo $i
done

# echo {1..10}
# echo $(seq 1 10)

[root@servera ~]# echo {1..10}      #掌握
1 2 3 4 5 6 7 8 9 10
[root@servera ~]# echo $(seq 1 10)
1 2 3 4 5 6 7 8 9 10

[root@servera ~]# for i in host1 host2 host3;do echo $i;done    #掌握
host1
host2
host3
[root@servera ~]# for num in host{1..3};do echo $num;done 		  #掌握
host1
host2
host3
[root@servera ~]# for num in `ls /`;do echo $num;done			  #掌握

[root@servera ~]# cd /opt/
[root@servera opt]# ls
[root@servera opt]# touch file{1..3}
[root@servera opt]# ls
file1  file2  file3
[root@servera opt]# for i in file*;do ls $i;done
file1
file2
file3

[root@servera opt]# vim for.sh   #(考点)
#!/bin/bash

for i in 1 2 3;do
    echo $i
done


小技巧
[root@servera ~]# echo ${HOSTNAME}O
servera.lab.example.comO
[root@servera ~]# echo $HOSTNAME\O
servera.lab.example.comO
```

```bash
练习：
[root@bastion /]# vim user_list.txt 
user1
user2
user3

[root@servera ~]# vim user.sh
#!/bin/bash
for i in `cat user_list.txt`;do
        useradd test$i 
        echo P@ssw0rd${i}a | passwd --stdin test$i
done
[root@servera ~]# sh user.sh
```



### 2.在脚本中使用退出代码

处理完自己的所有内容后，脚本会退出到调用它的进程。但是，有时候可能需要加载完成之前 退出脚本，比如加载遇到错误条件时。可以通过加载脚本中使用exit命令来实现这一目的。当脚本遇到exit命令时，脚本将立即退出且不会对脚本的其余内容进行处理。

可以使用可选的整数参数（0到255之间，表示退出代码）来执行exit命令。退出代码进程完成后返回的代码。退出代码值0表示没有错误。所有其他非零值都表示存在错误的退出代码。尽可以使用不同的非零值来区分遇到的不同类型错误。此退出代码传回到父进程，后这将它存储在？变量中，并可通过$?进行访问。                                                                      

```bash
[root@servera /]# vim hello.sh
#!/bin/bash

echo "hello world"
exit 1
[root@servera /]# echo $?
```

使用运算符执行测试

### 3.test, []

```bash
test 0 -ne 1 
   36  echo $?
   37  test 0 -ge 0 
   38  echo $?
   39  test 0 -ge 1 
   40  echo $?
   41  test 8 -gt 4
   42  echo $?
   43  [ 0 -ge 0 ]
   44  echo $?

-eq	等于则为真
-ne	不等于则为真
-gt	大于则为真
-lt	小于则为真
-ge	大于等于则为真
-le	小于等于则为真

字符串判断
= ==  != 
[ 字符 == 字符 ]
[ 字符 != 字符 ]

单目，双目
-e 文件和文件夹
-f 普通文件
-d 目录
-c 设备文件
```

### 4.exit ,if  elif  else 

```bash
if [ 条件 ]
  then
       声明
fi

if [ xx ];then
     command
fi
 
一、
vim if.sh
#!/bin/bash
if [ 0 -ge 0 ];then
        echo ok
fi

二、
vim test.sh
#!/bin/bash
if [ -e /file1 ];then
        echo one
        exit 10
fi

三、
#!/bin/bash
if [ -e /file1 ];then
        echo one
        exit 10
else
        echo two
        exit 20
fi

四、
#!/bin/bash
if [ -e /file1 ];then
        echo one
        exit 10
elif [ -e /opt/file1 ];then
        echo two
        exit 20
else
        echo three
        exit 30
fi
```

### 5.$0，$1，$2..$9 ,$# 位置变量

```bash
vim ping.sh
#!/bin/bash
ping -c $2 172.25.254.$1

echo '$0: ' $0
echo '$1: ' $1
echo '$2: ' $2
echo '$3: ' $3
echo '$#: ' $#
[root@servera ~]# chmod +x ping.sh
[root@servera ~]# ./ping.sh 254 5
PING 172.25.254.254 (172.25.254.254) 56(84) bytes of data.
64 bytes from 172.25.254.254: icmp_seq=1 ttl=63 time=3.02 ms
64 bytes from 172.25.254.254: icmp_seq=2 ttl=63 time=0.976 ms
64 bytes from 172.25.254.254: icmp_seq=3 ttl=63 time=1.03 ms
64 bytes from 172.25.254.254: icmp_seq=4 ttl=63 time=1.09 ms
64 bytes from 172.25.254.254: icmp_seq=5 ttl=63 time=2.16 ms

--- 172.25.254.254 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 12ms
rtt min/avg/max/mdev = 0.976/1.654/3.015/0.808 ms
$0:  ./ping.sh       脚本名
$1:  254			 执行脚本时赋予脚本的第一个值
$2:  5				 执行脚本时赋予脚本的第二个值
$3: 
$#:  2				 一共赋予多少个值
```

```
课外内容
-------------------------------------
$@					  具体赋予了什么值
$？					 看上一条命令的返回值0为正常执行，非0是非正常执行
$$ 				      查看子进程进程号
```



```bash
创建一个添加用户的脚本
在system1上创建一个脚本，名为/root/makeusers，此脚本能实现为系统system1创建本地用户，并且这些用户的用户名来自一个包含列表的文件。用户满足下列要求：
（1）	此脚本要求提供一个参数，此参数就是包含用户名列表的文件
（2）	如果没有提供参数，此脚本应该给出下面的提示信息 Usage: /root/makeusers userfile然后退出并返回相应的值
（3）	如果提供一个不存在的文件名，此脚本应该给出下面的提示信息 Input file not found然后退出并返回相应的值
（4）	创建的用户登录shell为/bin/false
（5）	此脚本不需要为用户设置密码
[root@servera ~]# cd /root/
[root@servera ~]# cat /root/userfile
testuser1
testuser2
testuser3

[root@servera ~]# vim /root/makeusers
#!/bin/bash
if [ $# -lt 1 ];then
        echo "Usage: $0 userfile"
        exit 1
elif [ ! -f $1 ];then
        echo "Input file not found"
        exit 1
else
        for i in `cat $1`;do
                /usr/sbin/useradd -s /bin/false $i
        done
fi
[root@servera ~]# ./root/makeusers /root/userlist
[root@servera ~]# 
```

## 3,cut（考grep）

```bash
grep root /etc/passwd   #重要 grep将文本中包含关键字的行打印出来，默认是从上到下的查询顺序。查文件内容用的
grep root /etc/passwd | cut  -d :  -f 1,3-5      #截取文本中的列 -d 指定分隔符号 -f 指定列     
grep root /etc/passwd | cut -c 1,3-5           #-c 指定字符        
grep ^root /etc/passwd
grep nologin$ /etc/passwd
cat -n /etc/passwd									 
grep -n ^# /etc/selinux/config 	
grep ^$ /etc/selinux/config 
grep ^$ /etc/selinux/config  | wc -l
grep -n ^$ /etc/selinux/config  
grep -v ^$ /etc/selinux/config 
grep ^# /etc/selinux/config 
grep -v ^# /etc/selinux/config 


cat /etc/selinux/config | grep -v ^# | grep -v ^$


[root@foundation0 /]# grep ng /usr/share/xml/iso-codes/iso_639_3.xml > /1.txt
[root@foundation0 /]# 
[root@foundation0 /]# 
[root@foundation0 /]# grep ^$ /1.txt
[root@foundation0 /]# grep ^$ /1.txt | wc -l
额外：
# grep -e root -e 0 /etc/passwd       grep -e可以在一个文件内单独匹配多个参数
root:x:0:0:root:/root:/bin/bash
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
# grep -E 'root|0' /etc/passwd

[root@foundation0 ~]# grep 'kiosk|root' /etc/passwd
[root@foundation0 ~]# egrep 'kiosk|root' /etc/passwd
root:x:0:0:root:/root:/bin/bash
operator:x:11:0:operator:/root:/sbin/nologin
kiosk:x:1000:1000::/home/kiosk:/bin/bash
[root@foundation0 ~]# grep  -E 'kiosk|root' /etc/passwd
root:x:0:0:root:/root:/bin/bash
operator:x:11:0:operator:/root:/sbin/nologin
kiosk:x:1000:1000::/home/kiosk:/bin/bash


grep -A  2 root #查看root关键字后两行
grep -B         #前x行
grep -C         #前后x行

grep -A 2 daemon   /etc/passwd

head -5 /etc/passwd
head -5 /etc/passwd > /opt/passwd.tst
ls
cat passwd.tst 
grep -A 2 daemon 
grep -A 2 daemon passwd.tst 
grep -B  2 daemon passwd.tst 
grep -C  2 daemon passwd.tst 
```

### 正则表达式

```bash
[root@foundation0 /]# cat testfile 
cat
cot
cut
cut
cut
dog
dog
concatenate
dogma
category
educated
boondoggle
vindication
chilidog
aacat
caaaaaaaat

匹配行首和行尾
[root@foundation0 /]# grep ^cat testfile 
cat
category 
[root@foundation0 /]# grep cat$ testfile 
cat
aacat
[root@foundation0 /]# grep ^cat$ testfile 
cat

向正则表达式中添加通配符和倍数
1、.匹配换行符以外的任何“单”个字符
[root@servera opt]# grep c.t testfile 

2、选择[]内的一个字符。搜索结果应为c开头、中间a或o或u、结尾t
[root@servera opt]# grep c[aou]t testfile     cat cot cut

3、倍数* 匹配前面的子表达式零次或多次
[root@servera opt]# grep c*t testfile  

4、倍数通常与通配符一起使用，如.*  这将匹配任何以包含c和t中间有0个或多个任意字符的行
[root@servera opt]# grep c.*t testfile 
```

# 第二章 Linux计划任务

## 1.at

```bash
[root@servera /]# rpm -q at
at-3.1.20-11.el8.x86_64
[root@servera /]# systemctl status atd.service 
[root@servera /]# rpm -qc at
/etc/at.deny
/etc/pam.d/atd
/etc/sysconfig/atd


at 选项 参数

创建
13:49分时候执行touch指令
[root@servera /]# at 13:49
warning: commands will be executed using /bin/sh
at> touch /at.txt
at> <EOT>          ctrl+d退出

3分钟后执行
echo `date` >> /home/student/myjob.txt | at now +3min


明天17:20点执行echo
at 17:20 tomorrow
at> echo hello

三天后下午5:10分执行/bin/ls
at 5:20pm+3days
at> /bin/ls

使用时间，和月/日/年的方式指定任务
at 17:20 5/20/2022

在7月的31日上午10点
at 10am Jul 31


查看
[root@servera /]# atq
作业编号  执行日期和时间             队列a			运行作业所有者
7	     Sun Mar  8 13:51:00 2020 a         	 root

[root@servera /]# at -l

查看任务内容
[root@servera /]# at -c 7

删除
[root@servera /]# at -d 9
[root@servera /]# atrm 8

systemctl status atd
man at
at 17:30
man at
at - 1
at -c 1
atq
atrm 1
atq
at 17:20
atq
at 17:30 08/19/2022
at 17:30 2022-7-24
man at
at now +3min
at now +3hour
at now +3days
man at
at 4pm +3days
at 16:02 +3days
man at
at teatime
man at
history
echo 123 > /root/backup | at 17:20
atq
at -c 11
atq

监控任务
[root@servera ~]# watch atq      ctrl+c 退出监控模式

一次性计划任务重启后，任务未执行依然会保存，直至删除或执行完毕后消失。

黑白名单
/etc/at.deny  哪个用户在黑名单里，哪个用户不能使用at，该文件默认存在
/etc/at.allow 在白名单内的用户可以使用at，白名单以外的人不能使用，白名单和黑名单同时存在时优先于黑名单，该文件默认不存在，如使用需自己创建。
```

## 2.crontab （考点）

```bash
[root@servera /]# systemctl status crond.service  #重要
[root@servera /]# systemctl  enable crond
[root@servera /]# systemctl is-enabled crond
[root@servera /]# systemctl enable --now crond

[root@servera /]# rpm -qa cron
[root@servera /]# rpm -qa | grep cron
cronie-anacron-1.5.2-2.el8.x86_64
cronie-1.5.2-2.el8.x86_64
crontabs-1.11-16.20150630git.el8.noarch
[root@servera /]#　rpm -qc crontabs
/etc/crontab

crontab 选项 
-e	编辑计划任务 	 crontab -e
-u	指定用户 	   crontab -u student -e
-r	删除  	    crontab -r
-l	列出  		crontab -l

*       *       *       *       *       command
分		时     日 	 月       周		 任务内容
0-59    0-23   1-31    1-12    0-7

1.每年2月2日上午9点执行echo  hello
0       9       2       2       *       echo hello

2.每天3到6点 第2分 执行一个脚本/root/1.sh
2	3-6  *	*	* /bin/sh /root/1.sh

3.每两个小时的第2分钟，执行一个脚本/root/1.sh
2	*/2   *	*	*	 /bin/sh /root/1.sh

4.每年７月的第１天和第５天，两点２分，执行一个脚本/root/1.sh
2	2   1,5	 7	*	 /bin/sh /root/1.sh

5.配置cron任务，每隔2分钟运行logger “Ex200 in progress”，以harry用户身份运行
[root@servera /]# id harry 
[root@servera /]# crontab -u harry -e
[root@servera /]# crontab -u harry -l  (也可以su - harry切换到任务用户，运行crontab -l)
*/2 	*	*	* 	*    /usr/bin/logger “Ex200 in progress” 

删除某条可以crontab -e 进去编辑
删除用户的所有任务 crontab -r 	

vim /etc/cron.deny  限制用户使用crond服务

白名单：
mandb
man -k cron
man 1 crontab
echo harry >> /etc/cron.allow
白名单启用后，就不用黑名单了，谁在白名单里谁能用该功能
   
测试：
su - harry,crontab -e    #可以使用
exit
su - tom,crontab -e     #不可使用
 
练习：
1、每年2月2日上午9点执行echo hello任务
2、七月每周5的9点至下午5点，每5分钟执行echo hello

3、执行一个任务,该任务每隔2分钟运行以下命令/usr/bin/logger   "hello"，给harry设定这个任务
4、使用root身份为用户tom设置一个计划任务，每天的下午2点43分执行/home/tom/tom.sh脚本
5、每天凌晨2点10分，清除/var/log/abc.log文件
6、每周日下午3点，执行”/bin/sh     /usr/local/sbin/backup.sh“
7、每隔5分钟 通过ping -c 4 192.168.1.1命令来测试连通性
8、每天的，1点，6点，9点，将/var/log/messages文件进行归档，要求使用gzip工具，归档文件保存在/tmp/当前时间.tar.gz

tar -zcvf /tmp/$(date +%Y-%m-%d_%H:%M).tar.gz /var/log/messages
9、每天9点到18点执行 "/bin/sh  /root/test.sh"
10、不允许fred用户使用crontab任务
```

## 3.管理临时文件

rhel6 tmpwatch

systemd-tmpfiles

```bash
1 手动清理临时文件
[root@servera /]# systemctl status systemd-tmpfiles-setup   查看服务状态
[root@servera /]# rpm -qf /usr/lib/tmpfiles.d/tmp.conf 
systemd-239-13.el8.x86_64

cp /usr/lib/tmpfiles.d/tmp.conf /etc/tmpfiles.d/
cd /etc/tmpfiles.d/
vim tmp.conf
q /tmp 1777 root root 5d
systemd-tmpfiles --clean /etc/tmpfiles.d/tmp.conf 
```

```bash
2 清理临时文件小实验
创建一个存放临时文件的目录，并且设置相应权限/run/momentary 0700 root root
[root@servera /]# vim /etc/tmpfiles.d/momentary.conf
d /run/momentary 0700 root root 30s
创建存放临时文件目录
[root@servera /]# systemd-tmpfiles --create /etc/tmpfiles.d/momentary.conf 
[root@servera /]# ll /run/momentary/ -d
drwx------. 2 root root 40 Mar  8 15:08 /run/momentary/
在目录中创建一个文件叫做mom.txt，此文件模拟临时文件
[root@servera /]# touch /run/momentary/mom.txt
[root@servera /]# sleep 30
[root@servera /]# ll /run/momentary/mom.txt 
-rw-r--r--. 1 root root 0 Mar  8 15:08 /run/momentary/mom.txt
输入清除临时文件命令，清除所有的临时文件
[root@servera /]# systemd-tmpfiles --clean /etc/tmpfiles.d/momentary.conf 
[root@servera /]# ll /run/momentary/mom.txt 
ls: cannot access '/run/momentary/mom.txt': No such file or directory
```

# 第三章 系统性能调优

## 1.tuned 

### 查看状态

```bash
[root@servera tmp]# yum install -y tuned
[root@servera tmp]# systemctl enable --now tuned
[root@servera tmp]# systemctl status tuned
```

### tuned-adm管理指令（考点）

```bash
[root@servera tmp]# tuned-adm list  #列出优化方案
[root@servera tmp]# tuned-adm recommend   #系统推荐的
[root@servera tmp]# tuned-adm profile virtual-guest 修改优化方案为virtual-guest
[root@servera tmp]# tuned-adm off #关闭优化，默认不关闭，考试时不要关闭
```

### 配置文件存储路径

```bash
每个优化方案对应要给配置文件，其中描述了修改的内核参数

路径：/usr/lib/tuned/
```

## 2.使用cockpit修改调优的方式

```bash
【servera】
systemctl start cockpit
【foundation】
打开浏览器输入：https://servera:9090--点击右侧高级选项---添加访问---账号密码：root redhat ----左边Overview---找到configure--找到调优

```

## 3.调节nice值

```bash
进程有默认优先级，但是优先级默认不能更改，但是可以通过修改nice值来影响进程优先级。
nice值+old优先级=new优先级
Nice +  80   = 新优先级

Nice值调节范围：
    root：修改范围-20~19    调节优先级的工作由root来执行
    user:   修改范围0~19        调节后无法降级
    
优先级：
        数值越大优先度越低
        数值越小优先级越高

nice 直接给一个新的指令设置优先级
renice 给一个现有的进程调节优先
```

nice值管理

```bash
语法：
nice 选项  command
选项：
-n ： -n 后面添加优先级 
例：
nice -n 10 vim 1.txt 


语法：
renice 选项 进程号
-n：  -n 后面添加优先级 
例：
renice -n 10 pid
```

## 练习：

```bash
[root@clear ~]# cd /opt/
[root@clear opt]# ls
dir1  file1
[root@clear opt]# rm -rf *
[root@clear opt]# vim test.txt  &   #生成一个进程，&放后台
[1] 59090
[root@clear opt]# ps -l
F S   UID     PID    PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
4 S     0    1139    1138  0  80   0 - 59210 -      pts/2    00:00:03 bash
0 T     0   59090    1139  1  80   0 - 60817 -      pts/2    00:00:00 vim  #出现了
0 R     0   59091    1139  0  80   0 - 63799 -      pts/2    00:00:00 ps
[root@clear opt]# nice -n 10 vim 1.txt &  #创建新进程 并且添加nice值为正10
[2] 59097
[root@clear opt]# ps -l
F S   UID     PID    PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
4 S     0    1139    1138  0  80   0 - 59243 -      pts/2    00:00:03 bash
0 T     0   59090    1139  0  80   0 - 60817 -      pts/2    00:00:00 vim
0 T     0   59097    1139  2  90  10 - 60817 -      pts/2    00:00:00 vim  #在这里
0 R     0   59098    1139  0  80   0 - 63799 -      pts/2    00:00:00 ps
[2]+  Stopped                 nice -n 10 vim 1.txt
[root@clear opt]# renice -n -5 59090         #修改现有进程，nice值为负5，指定进程号
59090 (process ID) old priority 0, new priority -5
[root@clear opt]# ps -l
F S   UID     PID    PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
4 S     0    1139    1138  0  80   0 - 59243 -      pts/2    00:00:03 bash
0 T     0   59090    1139  0  75  -5 - 60817 -      pts/2    00:00:00 vim   #已经改变了
0 T     0   59097    1139  0  90  10 - 60817 -      pts/2    00:00:00 vim
0 R     0   59100    1139  0  80   0 - 63799 -      pts/2    00:00:00 ps
[root@clear opt]# 
```



# 第四章 管理SELinux

## 一 介绍selinux

### [1.selinux简介]()

```bash
  SELinux 全称 Security Enhanced Linux (安全强化 Linux),是美国国家安全局2000年以 GNU GPL 发布，是 MAC (Mandatory Access Control，强制访问控制系统)的一个实现，目的在于明确的指明某个进程可以访问哪些资源(文件、网络端口等)。强制访问控制系统 的用途在于增强系统抵御 0-Day 攻击(利用尚未公开的漏洞实现的攻击行为)的能力。所以它不是网络防火墙或 FACL 的替代品，在用途上也不重复。在目前的大多数发行版中，已经默认在内核集成了SELinux。
```

### [2.selinux实现原理]()

```
1、普通权限和selinux权限对比：
​传统的Linux使用用户、文件权限的概念来限制资源的访问，通过对比进程的发起用户和文件权限以此来保证系统资源的安全，这是一种自由访问控制方式（DAC）
​Linux下的一种安全强化机制（SELinux），该机制为进程和文件加入了除权限之外更多的限制来增强访问条件，这种方式为强制访问控制（MAC）。进程和文件都有相应的标签，称为上下文，只有进程上下文和文件上下文对应上，该进程才可以访问文件。
​何为对应？系统中大量的进程和文件上下文已经被定义，如http服务进程上下文为httpd_t域，/var/www/html/目录上下文为httpd_sys_content_t，后者归属于前者的匹配域内，即可匹配。

```

| 分类                | 源             | 目的                       |
| ------------------- | -------------- | -------------------------- |
| 传统文件系统权限DAC | 用户           | 文件系统权限               |
| SELinux权限MAC      | 用户进程上下文 | 文件（或目录端口等）上下文 |

### [3.selinux的限制范围]()

```tcl
SElinux具有多种上下文类型，常见种类：（掌握上下文）
​安全上下文（限制文件的访问）：该种类上  下文存在于内存和文件中，进程访问文件inode时读取到上下文类型进行对比
​布尔值（限制软件功能的访问）：该种类型，主要控制某些进程是否可以访服务常用功能中出现的文件
​安全端口（限制服务的访问）：selinux会限制服务启用的非标准端口号
```

|  ID  |            |                         | SELinux                                                      |
| :--: | ---------- | ----------------------- | ------------------------------------------------------------ |
|  1   | Filesystem | chmod, chown, setfacl   | semanage fcontext ... restorecon ... chcon ... touch /.autorelabel |
|  2   | Service    | vim /etc/*.conf         | setsebool -P ...                                             |
|  3   | Firewall   | firewall-cmd ...        | semanage port ...                                            |
|  4   | SELinux    | vim /etc/selinux/config |                                                              |

### [4.如何查看上下文（掌握，会查看上下文）]()

```bash
[root@servera ~]# dnf install -y httpd
[root@servera ~]# systemctl enable --now httpd

进程：              
ps auxZ            
ps -eZ
[root@servera ~]# ps auxZ | grep httpd
system_u:system_r:httpd_t:s0    root      1971  0.0  1.2 273800 10496 ?        Ss   04:58   0:00 /usr/sbin/httpd -DFOREGROUND

文件：
ls -Z
[root@servera ~]# ll -dZ /var/www/html/
system_u:object_r:httpd_sys_content_t:s0 /var/www/html/

只要进程和文件的安全上下文匹配，该进程就可以访问该文件资源
```

### [5.security context介绍]()

```bash
content一词使我们常说的上下文，安全上下文有5个字段，以：冒号分割，
unconfined_u   :object_r:   httpd_sys_content_t:  	  s0  		   [类别]   

身份 user -u	  角色roles -r  类型 type  -t                             灵敏度（级别）     类别     

1) 身份字段（user)
   用于标识该数据被哪个身份所拥有，相当于权限中的用户身份。这个字段并没有特别的作用，知道就好。常见的身份类型有以下 3 种：
- root：表示安全上下文的身份是 root。
- system_u：表示系统用户身份，其中“_u”代表 user。
- user_u：表示与一般用户账号相关的身份，其中“_u”代表 user。
- 以使用 seinfo 命令来进行查询

2) 角色（role）
主要用来表示此数据是进程还是文件或目录。这个字段在实际使用中也不需要修改，所以了解就好。
常见的角色有以下两种：
- object_r：代表该数据是文件或目录，这里的“_r”代表 role。
- system_r：代表该数据是进程，这里的“_r”代表 role。

3) 类型（type）
   类型字段是安全上下文中最重要的字段，进程是否可以访问文件，主要就是看进程的安全上下文类型字段是否和文件的安全上下文类型字段相匹配，如果匹配则可以访问。#（掌握重点）

注意，类型字段在文件或目录的安全上下文中被称作类型（type），但是在进程的安全上下文中被称作域（domain）。也就是说，在主体（Subject）的安全上下文中，这个字段被称为域；在目标（Object）的安全上下文中，这个字段被称为类型。域和类型需要匹配（进程的类型要和文件的类型相匹配），才能正确访问。
context查询工具seinfo、sesearch
    seinfo -u    # 查询所有的user字段的种类
    seinfo -r    # 查询所有的role字段的种类
    seinfo -t    # 查询所有的type字段的种类 

sesearch -A 可以查询什么类型进程可以读取什么type类型的文件
    sesearch -A -s 进程type    # 查询type类型的进程能够读取的文件type   
    sesearch -A -b 规则（规则的boolean值，所以为-b选项，理解为bool规则）
    
[root@foundation0 ~]# sesearch -A -s httpd_t | grep '^allow httpd_t httpd_sys_content_t'

4) 灵敏度
   灵敏度一般是用 s0、s1、s2 来命名的，数字代表灵敏度的分级。数值越大，代表灵敏度越高。

5) 类别
   类别字段不是必须有的，所以我们使用 ls 和 ps 命令查询的时候并没有看到类别字段。通过seinfo -u -x查看
```

```bash
-servera
systemctl stop firewalld
yum install -y httpd
systemctl enable --now httpd
ps auxZ | grep httpd     #-Z 大写Z就表示上下文，发现http_t字段就是httpd服务进程上下文'类型'字段

cd /var/www/html
ll -dZ /var/www/html    #发现上下文是httpd开头
echo test >  /var/www/html/index.html
ll -Z  #发现所有文件类型字段上下文，都是httpd开头,看上下文的第三段 类型字段


-serverb
ping servera #ping 测试一下连通性，看网络是否正常
root@serverb ~]# curl servera/index.html
test 

-servera
chcon -t default_t /var/www/html/index.html   #更改了file3的上下文
ll -Z  #发现index.html上下文类型字段变成default_t


-serverb
root@serverb ~]# curl servera/index.html
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>403 Forbidden</title>
</head><body>
<h1>Forbidden</h1>
<p>You don't have permission to access this resource.</p>
</body></html>
[root@serverb ~]# curl servera/file3
test messages

-servera
ll -Z #查看file1、2的上下文
chcon -t httpd_sys_content_t /var/www/html/index.html #更改上下文和file1、2相同。

-serverb
ping servera #ping 测试一下连通性，看网络是否正常
[root@serverb ~]# curl servera/index.html
test
```



## 二 更改SElinux强制模式

###  [1.临时开启或关闭selinux]()

```bash
#临时修改意思是重启失效
[root@clear /]# getenforce 
Enforcing
[root@clear /]# setenforce 
usage:  setenforce [ Enforcing | Permissive | 1 | 0 ]    # 强制 1|宽容 0
[root@clear /]# setenforce 0
[root@clear /]# getenforce 
Permissive
[root@clear /]# setenforce Permissive
[root@clear /]# getenforce 
Permissive
[root@clear /]# setenforce 1
[root@clear /]# getenforce 
Enforcing

```

### [2.永久开启或关闭selinux状态]()

```bash
-RHEL<=9
[root@servera ~]# vim /etc/selinux/config
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=enforcing

修改后，重启操作系统生效
```

### [3.继承特性（了解）]()

```bash
继承：
1、在父目录下创建文件会继承selinux上下文touch
2、cp 
不继承：
创建了文件并且移动mv，会保留原来的父目录上下文关系
复制时cp  -a（rp） ，也会保留之前的上下文关系
```

```bash
【servera】 
[root@servera /]# cd /var/www/html/
[root@servera html]# ll -dZ .
drwxr-xr-x. 2 root root system_u:object_r:httpd_sys_content_t:s0 23 May 21 09:13 .
[root@servera html]# touch haha.txt
[root@servera html]# ll -Z haha.txt 
-rw-r--r--. 1 root root unconfined_u:object_r:httpd_sys_content_t:s0 0 May 21 10:06 haha.txt


[root@servera ~]# cd /tmp/
[root@servera tmp]# touch file{1..3}
[root@servera tmp]# ls -Z /tmp/file*
unconfined_u:object_r:user_tmp_t:s0 /tmp/file1 
unconfined_u:object_r:user_tmp_t:s0 /tmp/file2 
unconfined_u:object_r:user_tmp_t:s0 /tmp/file3
[root@servera tmp]# ls -Z /tmp/file*
[root@servera tmp]# cp /tmp/file1 /var/www/html/
[root@servera tmp]# cp -a /tmp/file2 /var/www/html/
[root@servera tmp]# mv /tmp/file3 /var/www/html/
[root@servera tmp]# cd /var/www/html/
[root@servera html]# ls -Z *
unconfined_u:object_r:httpd_sys_content_t:s0 file1           unconfined_u:object_r:user_tmp_t:s0 file3
         unconfined_u:object_r:user_tmp_t:s0 file2
[root@servera html]# touch /var/www/html/file0
[root@servera html]# ll -Z /var/www/html/file0
-rw-r--r--. 1 root root unconfined_u:object_r:httpd_sys_content_t:s0 0 Apr 10 05:08 /var/www/html/file0
</pre>
```

## 三 定义selinux默认文件上下文规则 

```bash
方法1：永久设置，但不记录至数据库
	chcon 设置上下文关系

方法2：永久设置，记录至数据库
	semanage fcontext  添加、修改、查看、删除默认上下文
	restorecon 恢复默认上下文
```

### [1.chcon]()

```bash
chcon 选项 上下文类型  文件

[root@servera html]# ll -Z 
total 0
-rw-r--r--. 1 root root unconfined_u:object_r:httpd_sys_content_t:s0 0 Dec 11 05:36 file1
-rw-r--r--. 1 root root unconfined_u:object_r:httpd_sys_content_t:s0 0 Dec 11 05:38 file2
-rw-r--r--. 1 root root unconfined_u:object_r:user_tmp_t:s0          0 Dec 11 05:37 file3
-rw-r--r--. 1 root root unconfined_u:object_r:user_tmp_t:s0          0 Dec 11 05:37 file4
[root@servera html]# man chcon
[root@servera html]# chcon -t httpd_sys_content_t file3
[root@servera html]# ll -Z 
total 0
-rw-r--r--. 1 root root unconfined_u:object_r:httpd_sys_content_t:s0 0 Dec 11 05:36 file1
-rw-r--r--. 1 root root unconfined_u:object_r:httpd_sys_content_t:s0 0 Dec 11 05:38 file2
-rw-r--r--. 1 root root unconfined_u:object_r:httpd_sys_content_t:s0 0 Dec 11 05:37 file3
-rw-r--r--. 1 root root unconfined_u:object_r:usexr_tmp_t:s0          0 Dec 11 05:37 file4

```

### [2.semanage fcontext ]()

```bash
帮助中查看示例：
man semanage
man semanage fcontext
选项：
semanage fcontext 
-a 	添加   添加至数据库
-d	删除
-l	查看
-t	指定上下文
-m  修改

修改流程：
1、查看selinux上下文类型数据库是否有记录？有就恢复，没有就添加
2、man semanage fcontext添加文件标签类型至数据库. 添加后，文件并不会更改标签类型
3、使用restorecon命令同步和数据库一致

【servera】
-l
[root@clear /]# man semanage fcontext | grep \#
[root@clear /]# semanage  fcontext -l | grep /var/www  看第一行即可
[root@clear /]# semanage  fcontext -l | grep /var/www/html/file3
/var/www/html/file3                                all files          system_u:object_r:default_t:s0 
[root@clear /]# semanage fcontext -a -t httpd_sys_content_t /var/www/html/file3 #数据库没有上下文记录，用-a添加，如已存在则用-m修改数据库中错误的上下文
[root@clear /]# semanage  fcontext -l | grep /var/www/html/file3
/var/www/html/file1                                all files          system_u:object_r:httpd_sys_content_t:s0 

restorecon  恢复文件上下文和数据库一致
-v  显示修改标签内容
-R  递归
[root@clear /]# restorecon -R -v /var/www/html/file3
Relabeled /var/www/html/file3 from system_u:object_r:default_t:s0 to system_u:object_r:httpd_sys_content_t:s0

[root@clear /]# ll -Z /var/www/html/file3
```

```
课上练习：
1.servera上安装httpd软件
2.在/var/www/html目录创建3个文件file0、file1、file2、对应内容test0、test1、test2
3.修改file0上下文为default_t
4.开启httpd服务。关闭防火墙 systemctl stop firewalld
5.在f0上用浏览器访问三个网页文件，测试发现，file0访问不到内容，而file1、file2可以
6.回到servera上修改file0上下文为httpd_sys_content_t
7.再测试
```



## 四 管理布尔值

### [1.管理布尔值]()

```bash
布尔值主要对应的是应用的功能的开启或关闭

getsebool 		列出布尔值状态  user
setsebool 		设置布尔值开启或关闭on 开，off 关
-P				更改布尔值永久生效 setsebool -p
semanage boolean -l 查看布尔值是否永久

[root@servera /]# yum install -y selinux-policy-doc
[root@servera /]# mandb
[root@servera /]# man -k '_selinux' | grep httpd
[root@servera /]# man 8 httpd_selinux
[root@servera /]# man 8 httpd_selinux |  grep -B 1 homedir
[root@servera /]# getsebool -a  
[root@servera /]# getsebool -a |  grep httpd | grep homedir

修改布尔值状态
[root@servera /]# setsebool httpd_enable_homedirs on
[root@servera /]# semanage boolean -l  | grep httpd_enable_home
httpd_enable_homedirs          (off  ,  off)  Allow httpd to enable homedirs
[root@servera /]# setsebool -P httpd_enable_homedirs on  永久生效
[root@servera /]# semanage boolean -l  | grep httpd_enable_home
httpd_enable_homedirs          (on   ,   on)  Allow httpd to enable homedirs
[root@servera /]# setsebool -P httpd_enable_homedirs off
```

### [2.使用布尔值调整]()

```bash
1.关闭selinux正常使用  apache发布页面托管在用户主页上的web内容功能
1.关闭selinux
[root@clear /]# setenforce 0;systemctl stop firewalld   

2.编辑配置文件
[root@clear /]# vim /etc/httpd/conf.d/userdir.conf 
#UserDir disabled   #注释该行
UserDir public_html  #解除该行注释，启用该功能


3.切换student用户，制作发布目录和网页
[root@clear /]# su - student
[student@clear ~]$ mkdir public_html  #创建发布目录
[student@clear ~]$ echo  test_web_page > public_html/index.html #制作网页
[student@clear ~]$ logout  #退出
[root@clear /]# chmod 711 /home/student/  #root用户身份，修改用户家目录权限为711，目的是为了让其他人有权限访问子目录及文件
[root@clear /]# systemctl restart httpd

4.在foundation上做访问测试
firefox  http://172.25.250.10/~student/index.html
test_web_page

5.开启selinux再次访问
【172.25.250.10】
[root@clear /]# setenforce 1
【foundation】
firefox  http://172.25.250.10/~student/index.html
Forbidden
You don't have permission to access /~student/index.html on this server.

6.开启对应布尔值，允许该功能
[root@clear /]# yum install -y selinux-policy-doc
[root@clear /]# mandb
[root@clear /]# man -k 'http'
[root@clear /]# man 8 httpd_selinux    #文档里搜索homedirs，复制该命令，开启该功能
[root@clear /]# setsebool -P httpd_enable_homedirs 1
[root@clear /]# getsebool
[root@clear /]# getsebool -a
[root@clear /]# getsebool -a | grep http | grep homedir  #第一种查询方式
root@clear /]# semanage boolean -l | grep homedirs   #第二种查询方式，查布尔值数据库
httpd_enable_homedirs          (on   ,   on)  Allow httpd to enable homedirs
							  （功能打开，永久生效）

7.在foundation上做访问测试
firefox  http://172.25.250.10/~student/index.html
test_web_page
```

## 五 安全端口

```bash
实验目标：当apache服务发布端口从80修改为82时，保证服务可以启动并开机自启动。

【servera】
[root@servera html]# systemctl stop firewalld
[root@servera html]# yum install -y httpd
[root@servera html]# setenforce 1
[root@servera html]# systemctl enable --now httpd   #服务正常启动
【f0】
curl http://servera/file1 #正常可以访问

【servera】
[root@servera html]# rpm -qc httpd   #倒数第四行
[root@servera html]# vim /etc/httpd/conf/httpd.conf  
Listen 82      #47行 练习时可以从80改为82
[root@servera html]# systemctl restart httpd      #服务起不来
[root@servera html]# cat /var/log/messages  #看日志文件最后的部分
*[root@servera html]# man semanage port | grep \#
       # semanage port -l
       # semanage port -a -t http_port_t -p tcp 81
       # semanage port -a -t ssh_port_t -p tcp 8991
[root@servera html]# semanage port -l | grep http_port_t   
http_port_t                    tcp      80, 81, 443, 488, 8008, 8009, 8443, 9000
*[root@servera html]# semanage port -a -t http_port_t -p tcp 82  #添加安全端口
*[root@servera html]# semanage port -l | grep http_port_t   #并查看是否有82
http_port_t                    tcp      82, 80, 81, 443, 488, 8008, 8009, 8443, 9000
pegasus_http_port_t            tcp      5988
[root@servera html]# echo haha > /var/www/html/index.html  #手动制作一个测试页
[root@servera html]# systemctl restart httpd  #可以启动服务
[root@servera html]# systemctl enable httpd #开机自启动
[root@servera html]# netstat -ntlp | grep 82
【f0】
# curl http://servera:82/index.html  #再次访问
```

## 六 SElinux日志

SELinux 会使用被称为 AVC（Access Vector Cache，访问矢量缓存）的缓存，如果访问被拒绝（也被称为 AVC 拒绝），则会在一个日志文件中记录下拒绝消息。

这些被拒绝的消息可以帮助诊断和解决常规的 SELinux 策略违规行为，至于这些拒绝消息到底被记录在什么位置，则取决于 auditd 和 rsyslogd 守护进程的状态：
	若 auditd 守护进程正在运行，则拒绝消息将被记录与 /var/log/audit/audit.log 中。
	若 auditd 守护进程没有运行，但 rsyslogd 守护进程正在运行，则拒绝消息会记录到 /var/log/messages 中。

```bash
-a：分析指定的日志文件
sealert -a /var/log/audit/audit.log  回显比较慢多等一会
```



# 第五章 管理基本存储

## 一 添加分区、文件系统和持久挂载

### [1.MBR分区方案]()

```bash
mbr叫做主引导记录，IBM公司提出的，它存在于磁盘的0柱面0磁道0扇区中，是磁盘的第一个扇区内，大小为512字节 446字节初始化程序加载器 64字节分区表 2字节校验码，每个分区16字节，所以最多4个分区，最大磁盘空间支持2T   

GPT分区方案
GPT是GUID Partition Table，全局唯一标识磁盘分区表。它由UEFI启动硬盘，这样就有了UEFI取代传动BIOS，而GPT则取代传统的MBR，windows支持最多128个GPT分区。
```

```bash
MBR分区方案
分区表=64字节
1个分区=16字节
4x16=64 最多只能记录4个分区 主分区
4个分区不够用？比如5个以上？

1.主分区最多 4个，占分区编号1-4，可以格式化（可以使用存放数据）
2.扩展分区最多可以有1个，牺牲一个主分区来做扩展分区，它不能格式化（不能使用，不能存放数据），它的作用是装载逻辑分区用的，也就是用扩展分区的空间再划分多个逻辑分区
3.逻辑分区-由扩展分区的空间划分而来的，约可以分15个左右。可以格式化（可以使用，可以存放数据），逻辑分区必定从5号开始

分区方案4P   3P+1E
```

|    0     | mbr  |     dpt     |      |
| :------: | :--: | :---------: | :--: |
| 512 Byte | 446  |     64      |  2   |
|          |      | Primary<=4  |      |
|          |      |  Extend<=1  |      |
|          |      | Logical<=14 |      |

|  ID  |              | Count | Size |           |      |
| :--: | ------------ | ----- | ---- | --------- | ---- |
|  1   | mbr \| msdos | 15    | 2TB  | 3P+1E(nL) | 4P   |
|  2   | gpt          | 128   | 8ZB  | P         |      |

|  ID  |        |       Windows       |                            Linux                             |    MacOS     |
| :--: | ------ | :-----------------: | :----------------------------------------------------------: | :----------: |
|  1   | local  | ntfs, fat32,  exfat | xfs, ext4 \| swap [exfat](https://centos.pkgs.org/8/rpmfusion-free-updates-x86_64/) | apfs,  exfat |
|  2   | remote |      cifs, nfs      |                                                              |              |

### [2.fdisk]()

```bash
使用新磁盘流程规划：
1.分区（可选）fdisk   parted
2.格式化     mkfs
3.挂载       mount
4.使用      

fdisk是一个分区工具，既可以查看磁盘状况，也可以对磁盘进行分区
语法：
fdisk  选项  设备名
选项：
-l 查看所有磁盘状态
例：
fdisk  -l  #查所有磁盘信息
fdisk  -l /dev/vdb   #查某一个磁盘信息
```

#### 练习：

```bash
1 MBR分区方案：
5G磁盘，每个分区1G
3P+1E ， 1E（L5、L6）

[root@clear /]# fdisk -l
[root@clear /]# fdisk -l /dev/vdb
[root@clear /]# fdisk /dev/vdb
n   #创建分区
p   #选择p主  或  e扩展
1	#分区编号
回车，不通过扇区范围分配         
+1G  设置一个1G大小分区
p  查看分区状态

d  删除
w  保存退出

-删除分区
Command (m for help): d
Partition number (1-6, default 6): 6

Partition 6 has been deleted.

#partprobe    磁盘分区正常结束后，此命令可以正常执行，不返回任何信息，主要做刷新分表信息通知内核
[root@clear /]# fdisk -l /dev/vdb


2 GPT 分区方案
Command (m for help): g
Created a new GPT disklabel (GUID: 1BA96F11-6DA0-204D-82D6-0CD15E42851E).
The old dos signature will be removed by a write command.

Command (m for help): n
Partition number (1-128, default 1):  回车
First sector (2048-10485726, default 2048): 回车 
Last sector, +sectors or +size{K,M,G,T,P} (2048-10485726, default 10485726): +1G

Created a new partition 1 of type 'Linux filesystem' and of size 1 GiB.

Command (m for help): n
Partition number (2-128, default 2): 
First sector (2099200-10485726, default 2099200): 
Last sector, +sectors or +size{K,M,G,T,P} (2099200-10485726, default 10485726): +2G

Created a new partition 2 of type 'Linux filesystem' and of size 2 GiB.

Command (m for help): l   #列出分区标识类型

Command (m for help): t   #改变分区标识类型
Partition number (1,2, default 2): 2
Partition type (type L to list all types): 19

Changed type of partition 'Linux filesystem' to 'Linux swap'.

Command (m for help): p
Disk /dev/vdb: 5 GiB, 5368709120 bytes, 10485760 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 1BA96F11-6DA0-204D-82D6-0CD15E42851E

Device       Start     End Sectors Size Type
/dev/vdb1     2048 2099199 2097152   1G Linux filesystem
/dev/vdb2  2099200 6293503 4194304   2G Linux swap

Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.

# partprobe  同步数据至内核
```

### [3.parted]()

#### 3.1 parted使用MBR分区方案

```bash
1.mbr方式
定义分区方案
分区
分3个区 主、扩展 、逻辑。再用非交互式创建第二个逻辑
[root@servera /]# parted /dev/vdc     #直接指定设备，而不是分区
(parted) mklabel        #按回车键  定义分区方案                                                    
New disk label type? msdos     #两一下tab键，填写                                             
Number  Start  End  Size  Type  File system  Flags
(parted) mkpart #分区                                                          
Partition type?  primary/extended? p  #主/扩展/逻辑                                    
File system type?  [ext2]? ext4   #无用，不生效
Start? 2048s#第一个分区，一定从1M或2048s开始                                                          
End? 1000MB #分1G分区，1000M、1G、（10G磁盘，可以写10%，完全使用就是100%）                                                              
(parted) p          #查看                                                      

Number  Start   End     Size   Type     File system  Flags
 1      1049kB  1000MB  999MB  primary  ext4         lba
(parted) quit        #退出                                                    
Information: You may need to update /etc/fstab.
[root@servera ~]#udevadm  settle (更新通知内核，建议敲上，不是必要)

非交互式方式
命令：
[root@servera /]# parted /dev/vdc mkpart p ext4 1000MB 2000MB
```

#### 3.2.parted使用GPT方案：

```bash
[root@clear /]# parted  /dev/vdc  #使用了上一个实验的同样的分区，重复使用了vdc
(parted) mklabel                                                          
New disk label type? gpt  #再更改分区方案时会抹掉之前的分区数据
Warning: The existing disk label on /dev/vdc will be destroyed and all data on this disk will be lost. Do
you want to continue?
Yes/No? yes    #yes                                                            
(parted) p                                                                 B
Partition Table: gpt  #此处显示gpt分区方案
(parted) mkpart                                                           
Partition name?  []? part1   #分区名字                                               
File system type?  [ext2]?     #直接回车                                           
Start? 1M                                                                 
End? 10%                                                                  
(parted) p                                                                
Number  Start   End     Size    File system  Name   Flags
 1      1049kB  1074MB  1073MB  ext2         part1        #注意Number和Name
(parted) mkpart
Partition name?  []? part2                                                
File system type?  [ext2]?                                                
Start? 10%                                                                
End? 20%
(parted) p
Number  Start   End     Size    File system  Name   Flags
 1      1049kB  1074MB  1073MB  ext2         part1
 2      1074MB  2147MB  1074MB  ext2         part2   #第二个分区完成
(parted) rm                                                               
Partition number? 2               #选择第二个分区的Number                                      
(parted) p                                                                
Number  Start   End     Size    File system  Name   Flags
 1      1049kB  1074MB  1073MB  ext2         part1
(parted) q                                                                
#非交互式方式重新分第二个分区
[root@clear /]# parted /dev/vdc mkpart  part2 ext2 10% 20%                
[root@clear /]# parted /dev/vdc p
Number  Start   End     Size    File system  Name   Flags
 1      1049kB  1074MB  1073MB               part1
 2      1074MB  2147MB  1074MB               part2   #成功分配
```

```
lsblk 
lsblk /dev/vdc
blkid 
```



### 4.文件系统

```bash
1.分区后，会给分区定义文件系统类型
windos: fat32 ntfs exfat 
linux : exfat 有ext2 ext3 ext4 xfs vfat 
MacOS : exfat 
2.为什么定义文件系统？给磁盘定义一种存储数据的方法，这样块存储设备才可以记录文件数据。
3.每个分区都可以定义一个独立的文件系统，定义的方法就是格式化。设备有了文件系统后才可以存储数据。
4.操作系统里分区很多，每个分区可能会有相同或不同的文件系统类型，彼此独立
5.VFS  虚拟文件系统：作用，将用户发送的指令给任何文件系统做翻译，对于用户来说，我不必学习不同文件系统的操作方法，而是使用常用shell管理指令，就可以通过VFS传递给不同的文件系统了。
```

### 5.格式化：

```bash
语法：
mkfs  选项  文件系统类型  设备名
mkfs  -t    ext4      /dev/vdb1
选项：
-t： 指定文件系统  -t ext4
例：
[root@servera /]# mkfs -t ext4 /dev/vdc1 	
例2：使用.点代替-t
[root@servera /]# mkfs.ext4 /dev/vdc2
```

### 6.lsblk

```bash
[root@servera /]# lsblk --fs /dev/vdc   #-f  --fs
NAME   FSTYPE LABEL UUID                                 MOUNTPOINT
vdc                                                      
├─vdc1 ext4         af656cc6-80e3-4b05-abcf-a162907c2f0a 
└─vdc2 xfs          64237913-4937-48ff-8afa-28c6fc05124d 
[root@servera /]# parted /dev/vdc p
Model: Virtio Block Device (virtblk)
Disk /dev/vdc: 5369MB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags: 

Number  Start   End     Size   Type     File system  Flags
 1      1049kB  1000MB  999MB  primary  ext4
 2      1000MB  2000MB  999MB  primary  xfs
 
查看文件系统
lsblk  --fs /dev/vdc

blkid 超级管理员可用 也可查文件系统类型

```

### 7.挂载与永久挂载

#### 7.1 mount

```bash
mount挂载也称为手动挂载，或临时挂载，重启失效

语法：
mount   源设备  挂载点      #源设备需要格式化后，成为文件系统才可挂载   #挂载点需要是空目录

例1：
mount  /dev/vdc1    /mnt/disk1

选项：
-t 指定文件系统类型（默认自动识别） mount  -t ext4 /dev/vdc1    /mnt/disk1 
-o 指定挂载权限卸载--》再指定权限  mount  -o ro /dev/vdc1    /mnt/disk1 
例2：
remount 重新挂载 （不卸载的基础上重新挂载）
mount -o remount,ro /dev/vdc2

卸载：
umount 源设备/挂载点
例：
umount /dev/vdc1  #或者 umount /mnt/disk1  #卸载时要退出/mnt/disk1目录
```

```bash
1.临时挂载 
[root@clear dev]# mkdir /mnt/disk1   #创建挂载点
[root@clear dev]# mount /dev/vdc1 /mnt/disk1   #挂载
[root@clear dev]# df -Th
Filesystem     Type      Size  Used Avail Use% Mounted on
/dev/vdc1      ext4      991M  2.6M  922M   1% /mnt/disk1
[root@clear dev]# cd /mnt/disk1  #进入目录  
[root@clear disk1]# touch haha   #使用
[root@clear disk1]# ls
haha  lost+found
[root@clear disk1]# cd /
卸载
[root@clear /]# umount /dev/vdc1
[root@clear /]# df -Th  #查看发现已经没有/dev/vdc1的挂载信息，就算成功了 。

2.使用UUID方式挂载
[root@clear /]# lsblk -f #也可以查看UUID
[root@clear /]# blkid
[root@clear /]# blkid /dev/vdc1
/dev/vdc1: UUID="9bdeba87-5ad0-4c52-b577-0234115df2e1" TYPE="ext4" PARTLABEL="part1" PARTUUID="28f6e868-cd04-434c-9fd3-618f17382797"
[root@clear /]# mount UUID="9bdeba87-5ad0-4c52-b577-0234115df2e1" /mnt/disk1 #鼠标左键选定后按滑轮中间。
[root@clear /]# df -Th
/dev/vdc1      ext4      991M  2.6M  922M   1% /mnt/disk1

mount方式挂载，重启失效
[root@clear /]# df -Th
[root@clear /]# reboot  #重启后再查看挂载
```

#### 7.2 /etc/fstab

```bash
mount -t ext4 -o rw /dev/vdb1 /mnt/disk1


2.开机自动挂载   永久
[root@clear /]# df -h
/dev/vdc1       991M  2.6M  922M   1% /mnt/disk1
[root@clear /]# umount /dev/vdc1
[root@clear /]# df -h
[root@clear /]# vim /etc/fstab 
/dev/vdc1       /mnt/disk1      ext4    defaults    0     0
设备ID或设备名称	 挂载点	文件系统类型   选项（权限，认证设定，其他）   内核日志检测机制0不检测，1、2检测，1比2优先级高  磁盘检测机制0不检测

[root@clear /]# mount -a #挂载/etc/fstab中所有未挂载的设备
[root@clear /]# df -Th
/dev/vdc1      ext4      991M  2.6M  922M   1% /mnt/disk1

#reboot   #模拟考试环境，没做完selinux安全端口别重启，会起不来

`注意`
如果/etc/fstab中配置错误导致不能启动系统，重启时会自动进入紧急模式.
解决方案流程：
1 此时输入管理员root密码
2 编辑挂载权限 #mount -o remount,rw / 
3 vi /etc/fstab更改正内容或将其用#井号注释挂载内容后，
4 reboot即可
```

## 二 管理交换分区 

```bash
创建swap 
流程：
1、分区    512M
2、格式化为swap
3、给系统加载swap空间
	 1 第一种 临时加载，重启失效 	
	 2 第二种 写入/etc/fstab文件永久生效
流程： 
1.分区
[root@clear /]# parted  /dev/vdc mkpart part3 ext2 20% 30% 
Information: You may need to update /etc/fstab.
[root@clear /]# parted  /dev/vdc p
Number  Start   End     Size    File system  Name   Flags
 1      1049kB  1074MB  1073MB  ext4         part1
 2      1074MB  2147MB  1074MB  xfs          part2
 3      2147MB  3221MB  1074MB               part3
 
2.格式化
[root@clear /]# mkswap /dev/vdc3
Setting up swapspace version 1, size = 1024 MiB (1073737728 bytes)
no label, UUID=d66e7c4a-152a-4900-9801-bef1e23a021b

3.挂载
[root@clear /]# lsblk -f
└─vdc3 swap         d66e7c4a-152a-4900-9801-bef1e23a021b                                  [root@clear /]# free -m  
              total        used        free      shared  buff/cache   available
Mem:           1826         239        1059          16         528        1415
Swap:             0           0           0       #<-----提前检查以下系统默认swap空间，发现都是0,未添加
[root@clear /]# swapon /dev/vdc3    #临时加载
[root@clear /]# free -m
              total        used        free      shared  buff/cache   available
Mem:           1826         240        1058          16         528        1414
Swap:          1023           0        1023   #有数值了，添加成功
[root@clear /]# swapoff /dev/vdc3    #卸载
[root@clear /]# free -m
              total        used        free      shared  buff/cache   available
Mem:           1826         239        1058          16         528        1415
Swap:             0           0           0

写入fstab开机自启
[root@clear /]# vim /etc/fstab 
/dev/vdc3 swap swap defaults 0 0
[root@clear /]# cat /etc/fstab
/dev/vdc3 swap swap defaults 0 0

[root@clear /]# free -m
              total        used        free      shared  buff/cache   available
Mem:           1826         239        1059          16         528        1415
Swap:             0           0           0
[root@clear /]# swapon -a   #swap空间用swapon -a挂载
[root@clear /]# free -m
              total        used        free      shared  buff/cache   available
Mem:           1826         240        1058          16         528        1414
Swap:          1023           0        1023



使用本地存储创建swap（了解）
swap-file
# df -h
# dd if=/dev/zero of=/pagefile.sys bs=512M count=1
# mkswap /pagefile.sys 
# chmod 0600 /pagefile.sys 
# vim /etc/fstab 
...
/pagefile.sys swap swap defaults 0 0 
# swapon -a
# free -h
```



# 第六章 逻辑卷管理LVM

## 一 创建和扩展逻辑卷

### 1.逻辑卷管理概述

```
逻辑卷（Logical Volume Manager）是基于内核的一种逻辑卷管理器，LVM适合于管理大存储设备，并允许用户动态调整文件系统的大小。
```

```
物理卷（Physical Volume，PV）：物理卷是LVM的最底层概念，是LVM的逻辑存储块，物理卷与磁盘分区是逻辑的对应关系。
卷组（Volume Group，VG）：卷组是LVM逻辑概念上的磁盘设备，通过将单个或多个物理卷组合后生成卷组。卷组的大小取决于物理卷的容量以及个数。
物理长度（Physical Extent，PE）：物理长度是将物理卷组合为卷组后，所划分的最小存储单位，即逻辑意义上磁盘的最小存储单元。LVM默认PE大小为4MB.
逻辑卷（Logical Volume，LV）:逻辑卷就是LVM逻辑意义上的分区，我们可以指定从卷组中提取多少容量来创建逻辑卷，最后对逻辑卷格式化并挂载使用。
```

### 2 构建LVM存储

1.创建逻辑卷的流程

```bash
1.分区或者添加物理硬盘 			  fdisk,parted (mbr,gpt)
2.pv让物理磁盘或分区变成lvm可用的卷   pvcreate 
3.创建vg同时指定pe块大小  		vgcreate -s 
4.在vg中划分lv空间   lvcreate -L | -l  -n vgname
5.格式化lv空间
6.挂载或者永久挂载。

PV语法：
pvcreate  /dev/vdb1  /dev/vdb   #分区名称，或磁盘名
VG语法：
vgcreate -s 16M vgname pvname   #-s不写默认4M，扩大的话可以是2的次方，比如4、8、16、32...，vgname是vg名称自定义，pvname 就是创建pv时的物理分区名，就是pv名称
LV语法：
lvcreate -L 容量 -n lvname vgname #指定容量可以使用-L 容量 或 -l 块数
```

```bash
1.分区     至少分3个分区，模拟物理磁盘，作为pv的底层物理块设备
[root@clear ~]# fdisk -l /dev/vdb
Device     Boot   Start      End  Sectors Size Id Type
/dev/vdb1          2048  2099199  2097152   1G 83 Linux
/dev/vdb2       2099200  4196351  2097152   1G 83 Linux
/dev/vdb3       4196352  6293503  2097152   1G 83 Linux
/dev/vdb4       6293504 20971519 14678016   7G  5 Extended
/dev/vdb5       6295552  8392703  2097152   1G 83 Linux

2.PV阶段
[root@clear ~]# man pvcreate ,搜索/EXAMPLES
[root@clear ~]# pvcreate /dev/vdb{1,3,5}
[root@clear ~]# pvs 或者  pvscan   或者  pvdisplay 进行查询

3.VG阶段
[root@clear ~]# man vgcreate ,搜索/myvg
[root@clear ~]# man vgcreate | grep myvg
[root@clear ~]# vgcreate -s 16M vg100 /dev/vdb{1..3}    # -s指定物理扩展块16M一个
[root@clear ~]# vgs         #主要查看下vg的大小，拥有的pv和lv数量
[root@clear ~]# vgdisplay   #vg名，容量，块大小，块的使用量

pv vg  pe 65535 固定 16M  x 65535 = 270G  2^2  


4.LV阶段
[root@clear ~]# man lvcreate ,搜索/64m
[root@clear ~]# man lvcreate | grep 64m
[root@clear ~]# lvcreate -L 1G -n lv100 vg100　　　　（-L　指定具体容量-L 1G 　，　-l　指定PE块数量  -l 32）
[root@clear ~]# lvs
[root@clear ~]# lvdisplay 

5.格式化
# mkfs.xfs /dev/vg100/lv100 
# lsblk --fs /dev/vg100/lv100

6.创建挂载点
# mkdir /mnt/lvm1
# mount /dev/vg100/lv100 /mnt/lvm1
# df -h
# cd /mnt/lvm/

7.永久挂载
[root@clear ~]# cd /
[root@clear /]# umount /mnt/lvm1
[root@clear /]# echo "/dev/vg100/lv100 /mnt/lvm1 xfs defaults 0 0" >> /etc/fstab 
[root@clear /]# mount -a
[root@clear /]# df -Th
Filesystem              Type      Size  Used Avail Use% Mounted on
/dev/mapper/vg100-lv100 xfs       794M   38M  757M   5% /mnt/lvm1

8.reboot重启验证


删除逻辑卷思路：
创建顺序---分区--lv（pv-vg-lv）--格式化---挂载
卸载顺序---卸载--删lv--删vg--删pv--删分区
vgremove vg100
```

### 3 扩展逻辑卷（考点）

```bash
扩展流程：
1、扩展lv大小增加容量
2、重新定义文件系统大小
```

```bash
1 扩展语法：
lvextend 【-L| -l】【size|PE】 lv_name

选项：
-L：指定容量   直接指定最终大小 -L 1500M ，或者在原有基础上增加多少容量 -L +700M 
-l：指定块数   -l 60     ， -l +30

$ lvextend -L 1500M /dev/vg100/lv100
$ lvextend -l 60 /dev/vg100/lv100
```

```bash
2 重新定义文件系统大小

ext 文件系统 ”resize2fs 设备名/挂载点“   resize2fs /dev/vg100/lv100
xfs 文件系统 “xfs_growfs 设备名/挂载点”  xfs_growfs /dev/vg100/lv100 或 xfs_growfs 挂载点
	
例：
如果是xfs文件系统使用：
[root@servera /]# xfs_growfs /mnt/lvm (挂载点)
[root@servera /]# df -h

如果是ext4文件系统使用：
[root@servera /]# resize2fs /dev/myvg/mylv （lv设备名）
创建的lv名称为myly，属于myvg卷组，lv需要30个pe，每个pe16M，需要开机自动挂载到/mnt/mylvdir. 并且使用xfs文件系统
```

### 4 缩小lv（了解）

```bash
ext文件系统可以缩小，xfs不支持
流程：
1、卸载
2、resize2fs 定义缩小后的大小
3、磁盘检测
4、lvresize -L 1G 逻辑卷名

# umount /dev/myvg/mylv
# resize2fs /dev/myvg/mylv 1G
# e2fsck -f /dev/myvg/mylv
# resize2fs /dev/myvg/mylv 1G
# lvdisplay 
# lvresize -L 1G /dev/myvg/mylv 
# lvdisplay
```



## 二 管理分层存储

### 1.stratis

~~~bash
1.1 安装

$ yum install -y stratis-cli stratisd
$ systemctl enable --now stratisd
```
1.2 部署存储池

1.为磁盘分区(可选用磁盘或分区)，模拟块设备（磁盘）         
[root@servera ~]# stratis pool create pool1 /dev/vdb    #创建池名为pool1
[root@servera ~]# stratis pool list    #列出池信息包括大小
[root@servera ~]# stratis blockdev list  #列出池有哪些物理块设备组成
[root@servera ~]# stratis pool add-data pool1 /dev/vdc #添加额外存储
[root@servera ~]# stratis pool list
[root@servera ~]# stratis blockdev list 
ool Name   Device Node   Physical Size   Tier
pool1       /dev/vdb              5 GiB   Data
pool1       /dev/vdc              5 GiB   Data

1.3 创建文件系统

[root@servera ~]# man stratis   #搜/example
[root@servera ~]# stratis filesystem create pool1 fs1   #在pool1中创建文件系统fs1
[root@servera ~]# stratis filesystem list  #列出文件系统信息
Pool Name   Name   Used      Created             Device                   UUID                                
pool1       fs1    546 MiB   Sep 16 2023 21:30   /dev/stratis/pool1/fs1   0e40b338-6178-4bee-87b3-6e13b8be5ad0
 6d1ed6e714a6428eb374549b4fdd8d2b  
[root@servera ~]# mkdir /mnt/stratisdisk   #创建挂载点
如何查看uuid
[root@servera ~]# lsblk --output=UUID  /dev/stratis/pool1/fs1
[root@servera ~]# mount /dev/stratis/pool1/fs1 /mnt/stratisdisk/   #临时挂载
```
1.4 备份
-创建测试文件
[root@servera ~]# touch /mnt/stratisdisk/haha.txt

-备份
[root@servera ~]# stratis filesystem snapshot pool1 fs1 fs1.bak
[root@servera ~]# rm -f /mnt/stratisdisk/haha.txt 
[root@servera ~]# umount /mnt/stratisdisk 

[root@servera ~]# mount /dev/stratis/pool1/fs1.bak /mnt/stratisdisk
[root@servera ~]# ls /mnt/stratisdisk
haha.txt

1.5 开机自动挂载

```bash
vim /etc/fstab
/dev/stratis/pool1/fs1.bak /mnt/stratisdisk xfs _netdev 0 0 #_netdev 延迟挂载，先连接网络再挂载文件系统
mount -a

[root@clear ~]# systemctl start stratis
[root@clear ~]# stratis pool create mypool /dev/vdb1
[root@clear ~]# stratis pool add-cache mypool /dev/vdb2
[root@clear ~]# stratis blockdev list mypool 
Pool Name  Device Node  Physical Size   Tier
mypool     /dev/vdb1            1 GiB   Data
mypool     /dev/vdb2            1 GiB  Cache
~~~

### 2.VDO

~~~bash
## 2 VDO

### 1.1安装

```bash
该实验需要在普通环境上做，恢复init快照，默认是294，要切换至rh134课程
切换方法：
【foundation】
[kiosk@foundation0 ~]$ rht-clearcourse 0
[kiosk@foundation0 ~]$ rht-setcourse rh134
[kiosk@foundation0 ~]$ cat /etc/rht | grep RHT_COURSE   
RHT_COURSE=rh134		#必须保证课程是rh134才可以
[kiosk@foundation0 ~]$ for i in classroom bastion workstation servera;do rht-vmctl start $i;done  #开这4台虚拟机，打开时稍等3分钟左右，让其保证都开启，然后ping一下测试连通性
[kiosk@foundation0 ~]$ for i in classroom bastion workstation servera;do ping -c 4  $i;done  
结果都是以下结果即可，应出现icmp_seq=1 ttl=64 time=1.13 ms字样：
PING bastion.lab.example.com (172.25.250.254) 56(84) bytes of data.
64 bytes from bastion.lab.example.com (172.25.250.254): icmp_seq=1 ttl=64 time=1.13 ms
64 bytes from bastion.lab.example.com (172.25.250.254): icmp_seq=2 ttl=64 time=0.180 ms
64 bytes from bastion.lab.example.com (172.25.250.254): icmp_seq=3 ttl=64 time=0.234 ms
64 bytes from bastion.lab.example.com (172.25.250.254): icmp_seq=4 ttl=64 time=0.265 m

【foundation】
ssh student@workstation
【workstation】
lab advstorage-vdo start   #运行脚本，运行前，保证servera已打开
ssh student@servera
【servera】
[root@servera ~]# sudo -i
[root@servera ~]# yum search vdo
[root@servera ~]# yum install -y vdo.x86_64 kmod-kvdo.x86_64
[root@servera ~]# systemctl enable --now vdo
```

### 1.2 部署vdo

```bash
[root@servera ~]# man vdo   #/EXAMPLE
*[root@servera ~]# vdo create --name=vdo1 --device=/dev/vdd --vdoLogicalSize=50G
[root@servera ~]# vdo list
[root@servera ~]# vdo status --name=vdo1
[root@servera ~]# vdo status --name=vdo1 | grep Dedu
[root@servera ~]# vdo status --name=vdo1 | grep Comp
```

### 1.3 格式化及临时挂载测试

```bash
格式化
*[root@servera ~]#  mkfs.xfs -K /dev/mapper/vdo1      #-K让命令可以快速返回，效果类似快速格式化 ，如果已有文件系统 可以使用 -f强制执行 

挂载
*[root@servera ~]# mkdir /mnt/vdo1
[root@servera ~]# mount /dev/mapper/vdo1 /mnt/vdo1
[root@servera ~]# df -Th
[root@servera ~]# vdostats --human-readable
[root@servera ~]# cp /root/install.img /mnt/vdo1/install.img.1
[root@servera ~]# vdostats --human-readable
[root@servera ~]# vdostats --human-readable
[root@servera ~]# cp /root/install.img /mnt/vdo1/install.img.2
[root@servera ~]# vdostats --human-readable vdostats --human-readable
ls /mnt/vdo1/
lab advstorage-vdo finish
```

### 1.4 开机自动挂载

```bash
开机自动挂载方法：教材推荐
*[root@servera ~]# man vdo- | grep x-systemd
/dev/mapper/vdo1 /mnt/vdo1 xfs defaults,x-systemd.requires=vdo.service 0 0

mount -a

方法二：
/dev/mapper/vdo1 /mnt/vdo1 xfs defaults,_netdev 0 0

~~~



# 第七章 访问网络存储

## 1.通过NFS挂载网络附加存储

| NAS  | NFS  |
| ---- | ---- |
|      | CIFS |

###  1.1 查看NFS共享

```bash
普通环境
[root@servera ~]# showmount -e 172.25.254.250  #后面IP地址是NFS服务器的地址，环境默认已经部署好
Export list for 172.25.254.250:     #来自250的共享
/content 172.25.0.0/255.255.0.0     #服务器共享的目录
```

### 1.2 客户端挂载 mount

```bash
语法：
mount -t nfs  ip/域名:共享目录  本地挂载点
练习：
[root@servera ~]# mkdir /mnt/nfs1
[root@servera ~]# mount -t nfs -o ro 172.25.254.250:/content  /mnt/nfs1
```

### 1.3 开机自动挂载 /etc/fstab

```bash
[root@servera ~]# umount /mnt/nfs1
[root@servera ~]# vim /etc/fstab
172.25.254.250:/content /mnt/nfs1       nfs  defaults 0 0
[root@servera ~]# mount -a
[root@servera ~]# df -h
[root@servera ~]# reboot  （重启后使用df -h查看挂载状态）模拟考试环境没做selinux题，先别重启，起不来
```

## 二 自动挂载网络附加存储autofs

### 2.1 自动挂载 示例：

```bash
[root@clear home]# yum search autofs
[root@clear home]# yum install -y autofs
[root@clear home]# rpm -qc autofs
/etc/auto.master
/etc/auto.misc
```

### 2.2  普通方式1

```bash
servera上做该练习，挂载f0，172.25.254.250上的/content到servera上的/mnt/nfs10，读写权限
-mount
mkdir /mnt/nfs10
mount 172.25.254.250:/content /mnt/nfs10

-fstab
mkdir /mnt/nfs10
vim  /etc/fstab
172.25.254.250:/content /mnt/nfs10  nfs4  defaults 0 0

-autofs
[root@servera ~]# yum install -y autofs
[root@servera ~]# rpm -qc autofs
/etc/auto.master
/etc/auto.misc 
[root@servera ~]# vim /etc/auto.master
/mnt   /etc/auto.misc    #/data是基础挂载点，表示挂载是从这里开始的，系统自动创建/data。auto.misc是映射文件，内容为映射挂载点、挂载权限、共享目录路径等。

[root@servera ~]# vim /etc/auto.misc 
#nfs10 		-fstype=nfs4,rw     172.25.254.250:/content
nfs10 		-rw     172.25.254.250:/content

[root@servera ~]# systemctl restart autofs #重启让以上配置生效
[root@servera ~]# ls /mnt/nfs10/    #ls是使用挂载点目录
boot     ks         rhel7.0  rhel8.2  rhtops  ucf
courses  manifests  rhel8.0  rhel8.4  slides
[root@servera ~]# df -h | tail -1    #才会出现挂载项
172.25.254.250:/content  491G   70G  422G  15% /data/nfs10
```

### 2.3 普通方式2

```bash
1. 安装
servera上做该练习，挂载f0，172.25.254.250上的/content到servera上的/mnt/nfs10，读写权限
普通环境需要自己安装，
[root@servera ~]# yum install -y autofs
[root@servera ~]# systemctl enable --now autofs

2. 主配置文件	
[root@servera ~]# vim /etc/auto.master.d/nfs.autofs  #文件名自定义根据业务而定，且.autofs扩展名结尾
/mnt /etc/auto.nfs		#/mnt是基础挂载点，名称自定义,系统会自动创建。auto.nfs为映射文件，

3.映射文件
[root@servera ~]# cp /etc/auto.misc /etc/auto.nfs  #通过misc文件复制一份，并改名,文件名用auto.开头 结尾名称根据业务自定义
[root@servera ~]# vim /etc/auto.nfs
nfs10                         -rw     172.25.254.250:/content
#映射挂载点，是基础挂载点下的一个子目录名称。  -fsype指定文件系统类型(此选项可选择不添加，默认识别文件系统)，简化为 -rw。  172.25.254.250:/content  此段为nfs共享服务器地址及共享目录


[root@servera ~]# systemctl restart autofs
[root@servera ~]# ls /mnt/nfs10    
boot     ks         rhel7.0  rhel8.2  rhtops  ucf
courses  manifests  rhel8.0  rhel8.4  slides
[root@servera ~]# df -h | tail -1 
172.25.254.250:/content  491G   70G  422G  15% /data/nfs10
```

### 2.4 autofs 直接映射

```bash
[root@clear /]# umount /mnt/nfs10
1.主配置文件
[root@clear /]# vim /etc/auto.master.d/nfs.autofs  #nfs.autofs 名称规则和上个实验一样
/- /etc/auto.nfs     #所有直接映射条目使用/-作为基础目录
2.映射文件
[root@clear /]# vim /etc/auto.nfs   #固定文件名
/mnt/nfs10  -rw 172.25.254.250:/content
3.重启服务
[root@clear /]# systemctl restart autofs
4.测试
[root@clear /]# ll /mnt/nfs1   #cd或ll方式都可以访问共享目录内容，即可挂载
[root@servera /]# df -h
Filesystem               Size  Used Avail Use% Mounted on
172.25.254.250:/content  491G   66G  426G  14% /mnt/nfs1  
```

### 2.5 autofs 间接映射

```bash
1.主配置文件
[root@clear /]# vim /etc/auto.master.d/nfs.autofs 
/mnt /etc/auto.nfs
2.映射文件
[root@clear /]# vim /etc/auto.nfs 
*	-rw	 172.25.254.250:/&   #如果共享导出了多个子目录可以用&代替子目录的名称，而星*号会自动匹配共享的目录名称
*	-rw	 172.25.254.250:/&  /content /public  /share      



3.重启服务
[root@clear /]# systemctl restart autofs
4.测试
[root@clear /]# cd /mnt/content;ls
[root@clear remoteuser1]# df -h  #应该能看到挂载信息
```



# 第八章 启动流程

## 一 选择启动目标

### [1.Linux 9 启动过程]()

```bash
1、计算机接通电源。系统固件（现代UEFI或更旧的BIOS）运行开机自检（POST），并开始
初始化部分硬件。
    使用系统BIOS或UEFI配置屏幕（早期按F2可进入设置）
2、系统固件会搜索可启动设备，可能是在UEFI启动固件中配置的，也可能按照BIOS中配置的顺序搜索所有磁盘上的主引导记录（MBR）
   使用系统BIOS或UEFI配置屏幕（早期按F2可进入设置）
3、系统固件会从磁盘读取启动加载器，然后将系统控制权交给启动加载器。红帽企业版Linux8中，启动加载器为GRand Unified Bootloader version2（GRUB2）
   使用grub2-install命令进行配置，它将安装GRUB2作为磁盘上的启动加载器。
4、GRUB2将从/boot/grub2/grub.cfg文件加载配置并显示一个菜单，从中可以选择要启动的内核。
   使用/etc/grub.d/目录、/etc/default/grub文件和grub2-mkconfig命令进行配置，以生成/boot/grub2/grub.cfg文件。
5、选择内核超时到期后，启动加载器从磁盘中加载内核和initramfs，并将他们放入内存中。initramfs是一个存档，其中包含启动时所有必要硬件的内核模块、初始化脚本等等。在redhat8中，initramfs包含自身可用的整个系统。
       使用/etc/dracut.conf.d/目录、dracut命令和lsinitrd命令进行配置，以检查initramfs文件。
6、启动加载器将控制权交给内核，从而传递启动加载器的内核命令行中指定的任何选项，以及initramfs在内存中的位置
7、对于内核可在initramfs中找到驱动程序的所有硬件，内核会初始化这些硬件，然后作为PID1从initramfs执行/sbin/init。在redhat8中/sbin/init是一个指向systemd的链接。
8、initramfs中的systemd进程会执行initrd.target目标的所有单元。这包括将磁盘上的root文件系统挂载于/sysroot目录。
    使用/etc/fstab进行配置
9、内核将root文件系统从initramfs切换回/sysroot中的root文件系统。随后，systemd会使磁盘中安装的systemd副本来重新执行。
10、systemd会查找从内核命令行传递或系统中配置的默认目标，然后启动或停止单元，以符合该目标的配置，从而自动解决单元之间依赖关系。本质上，systemd进程是一组系统应激活以达到所需状态的单元。这些进程通常启动一个基于文本的登录或图形登录屏幕
     可使用/etc/systemd/system/default.target和/etc/systemd/system/进行配置。
```

### [2.重启和关机]()

```bash
-RHEL9<=
关机：
systemctl poweroff    
init 0				  
重启：
systemctl reboot     
init 6
reboot
```

### [3.选择SYSTEMD TERGET]()

下表列出了systemd启动达到的systemd单元

| 目标              | 用途                                           |
| ----------------- | ---------------------------------------------- |
| graphical.target  | 多用户、图形、文本登录                         |
| multi-user.target | 多用户、文本登录                               |
| rescue.target     | 救援                                           |
| emergency.target  | 紧急，进入initramfs环境，root只读形式挂载于/上 |

```bash
systemctl list-units --type=target --all
systemctl list-unit-files --state=enabled
systemctl list-dependencies graphical.target | grep target
```

### [4.在运行时选择target]()

```bash
切换图形或字符
[root@workstation ~]# systemctl isolate multi-user.target  或   init 3 
[root@workstation ~]# systemctl isolate graphical.target   或   init 5  
[root@workstation ~]# grep AllowIsolate /usr/lib/systemd/system/multi-user.target
AllowIsolate=yes    单元文件中包含AllowIsolate=yes才可以进行切换
```

### [5.设置默认target]()

```bash
设置默认的启动目标
[root@servera ~]# systemctl get-default 
[root@servera ~]# syetemctl set-default  multi-user.target
[root@servera ~]# ll /etc/systemd/system/default.target
```

### [6.在启动时选择其他目标]()

```bash
【f0】
grub 账号：root
grub 密码： Asimov

reboot
选择内核的位置按e
linux.....<按键end或者ctrl+e> systemd.unit=emergency.target  #multi-user.target 或graphical.target 
ctrl+x 
如果进入了紧急模式，需要输入root密码
mount -o remount,rw /
vim /etc/fstab
exit 或 reboot
```

## 二 重置root密码

### 1.修改登录密码

```bash
[root@foundation0]# ssh workstation
[student@workstation ~]# lab start boot-resetting

-servera  
#打开servera的视图模式，-Activites-所有应用-Virtaul Machine Manager-双击servera-点击左上角三个小方块-ctrl+alt+del
1.重新启动系统。
$ reboot（Send key 选择ctrl+alt+del）
2.按任意键(Enter除外)中断启动加载器倒计时。
$ 按方向键 ↓ 下
3.将光标移到要引导的救援内核条目 (名称中带有rescue一词的条目)。
$ 光标停留第二个内核rescue位置
4.按e编辑选定的条目。
$ e
5.将光标移到内核命令行 (以inux开头的行)。
$ linux... 按ctrl+e移动光标至最后
6.附加rd.break。利用该选项，就在系统从initramfs 向实际系统移交控制权前，系统将会中断。
$ rd.break console=tty0
7.按ctrl+x使用这些更改来启动。
$ ctrl+x
8.提示时，按Enter执行维护。
$ 救援模式下回车
9.给根目录设置rw权限
$ mount -o remount,rw /sysroot/
10.进入根目录
$ chroot /sysroot/      #cd / ,ls -a /
11.修改密码
$ echo redhat | passwd --stdin root
12.添加/.autorelabel文件，刷新selinux标记
$ touch /.autorelabel   #[root@servera ~]# systemctl status selinux-autorelabel
13.退出当前shell，会自动重启
$ exit
$ exit
```

## 五  诊断和修复文件系统问题（考点）

```bash
当/etc/fstab中挂载信息写错可能会导致系统起不来，会自动进入紧急模式
下表为常见错误：
```

| 问题                             | 结果                                                         |
| -------------------------------- | ------------------------------------------------------------ |
| 文件系统损坏                     | systemd尝试修复，无法修复进入紧急(emergency)模式             |
| /etc/fstab 引用的设备/UUID不存在 | systemd等待一定时间，等设备变得可用。如不可用，则进入紧急模式 |
| /etc/fstab 挂载点不存在          | 直接进入紧急模式                                             |
| /etc/fstab挂载点错误             | 直接进入紧急模式                                             |
| 文件系统、选项错误（权限）       | 直接进入紧急模式                                             |

```bash
版本建议：编辑/etc/fstab后使用systemd daemon-reload加载。否则systemd可能会继续使用用旧版本。
解决方法：
#当进入紧急模式后
Give root password for maintenance
(or pressControl-D to continue): redhat     #输入root密码后按回车进入系统
1 输入root密码
2 mount -o remount,rw /						#RHEL9中/根目录权限是可写的，所以无需敲该指令
3 vim /etc/fstab 							#可将错误字段需改，或在挂载行前，直接添加#井号注释掉该行。然后reboot重启
或者将之前编写的挂载项注释掉，让系统先正常启动，然后再修改调试。
```



#   第九章 firewalld

|                     |                            |
| ------------------- | -------------------------- |
| firewalld、iptables | 策略限制MAC、IP、PORT、APP |
| selinux             | 上下文、布尔值，端口       |
| 软件权限            | 读写执行等                 |
| 文件系统            | rwx，隐藏权限，acl         |

## **一 防火墙架构概念**

```bash
1 现代的计算机环境不仅需要流畅的网络连通，更需要具备安全性的网络，所以我们除了及时更新系统补丁以外事件，可以针对自己的计算机环境设置接收哪些数据和不接收哪些数据。我们可以使用软件来实现这一目的，这种软件就叫做防火墙。防火墙这种软件会制定一些规则，我们也叫做策略，管理数据包的通过。

 
2 firewalld优点  iptables
在于可使用zone、service简化配置任务，易于管理。而且属于动态管理（修改规则后不必重启整个防火墙服务）。

3.zone区域
firewalld预先提供了多个区域，不同区域拥有不同设置好的规则，类似不同的管理方案。系统默认区域为public。
所有区域默认在系统中会同时开启但是 如何判断我们的数据包在哪个区域中实现了数据限制，通过以下三种方法来判断：
- source，源地址
- interface，接收请求的网卡（ 一个网卡，只能绑定一个区域。多个网卡，可以绑定同一区域）
- firewalld.conf中配置的默认zone
三个优先级按次序降低。匹配后不再向下匹配。
```

### 1 nftables增强了netfilter

```bash
nftables--->替代--->netfilter
```

### 2 firewalld简介

```bash
rhel7之前---iptables（静态）
rhel7之后---firewalld（动态）
```

### 3 预定义区域

```bash
[root@foundation0 ~]# firewall-cmd --get-zones
block dmz drop external home internal libvirt public trusted work
```

### 4 预定义服务

```bash
[root@foundation0 ~]# firewall-cmd --get-service
```



## 二 配置防火墙

| 管理方法                          | 解释                |
| --------------------------------- | ------------------- |
| /etc/firewalld                    | 直接修改配置文件    |
| Web控制台图形界面                 | 通过cockpit         |
| firewalld-cmd命令行工具（重点考） | shell命令行直接执行 |
| firewall-config                   | 图形                |

### 1 使用Web控制台配置防火墙服务

```bash
【servera】
[root@servera ~]# systemctl start cockpit  #开启cockpit功能
[root@servera ~]# netstat -ntlp  		   #查看是否开启了9090端口
[root@servera ~]# firewall-cmd --list-all  #查看防火墙策略
services: cockpit dhcpv6-client http ssh   #策略中包含了cockpit，表示允许访问cockpit


【f0】
firefox https://172.25.250.10:9090     	   #可以访问

【servera】
[root@servera ~]# firewall-cmd --permanent --remove-service=cockpit
success
[root@servera ~]# firewall-cmd --reload
success
[root@servera ~]# firewall-cmd --list-all

【f0】
firefox https://172.25.250.10:9090     	   #不可以访问

【servera】
[root@servera ~]# firewall-cmd --permanent --add-service=cockpit  #添加cockpit服务，客户端再次访问即可
success
[root@servera ~]# firewall-cmd --reload
success
[root@servera ~]# firewall-cmd --list-all

```

### 2 从命令行配置防火墙

```
通用指令（会用）
```

| 防火墙命令                         | 说明                                                        |
| ---------------------------------- | ----------------------------------------------------------- |
| --get-default-zone                 | 查看默认区域                                                |
| --set-default-zone=public          | 设置默认区域为public，永久设置                              |
| --get-zones                        | 列出所有可用区域                                            |
| --get-active-zones                 | 列出当前正在使用所有区域                                    |
| --add-source=CIDR [--zone=ZONE]    | 将IP或网络到指定区域，如果未提供--zone=选项，则使用默认区域 |
| --remove-source=CIDR [--zone=ZONE] | 删除IP或网络.....，如果未提供--zone=选项，...则删除默认区域 |
| --add-interface=                   | 在某个区域中添加端口                                        |
| --change-interface=                | 改变端口至某个区域                                          |
| --list-all-zones                   | 列出所有区域状态                                            |

```
必会指令（考点）
```

| 防火墙命令                       | 解释             |
| -------------------------------- | ---------------- |
| --list-all [--zone=ZONE]         | 查看区域所有策略 |
| --add-service=  [--zone=ZONE]    | 允许一个服务     |
| --add-port=  [--zone=ZONE]       | 允许一个端口     |
| --remove-service=  [--zone=ZONE] | 移除一个服务     |
| --remove-port=  [--zone=ZONE]    | 移除一个端口     |
| --permanent                      | 永久生效         |
| --reload                         | 立即加载         |

### 3 练习

```bash
1、管理防火墙服务状态
systemctl status firewalld 
systemctl start firewalld 
systemctl stop firewalld 
systemctl restart firewalld 
systemctl enable --now firewalld 
```

```bash
2、查看与设置默认区域
man firewall-cmd #/Ex
firewall-cmd --get-service
firewall-cmd --get-zones
firewall-cmd --get-default-zone 
firewall-cmd --set-default-zone=home     设置默认区域，了解就可以，不是必须要设置
firewall-cmd --list-all
man 5 firewalld.zones
```

```bash
3.配置防火墙（考）
允许服务
[root@servera ~]# firewall-cmd --list-all  #查看区域所有配置
[root@servera ~]# firewall-cmd --permanent --add-service=http #允许http服务永久生效，要结合reload
[root@servera ~]# firewall-cmd --reload
[root@servera ~]# firewall-cmd --list-all
移除服务
[root@servera ~]# firewall-cmd --permanent --remove-service=http
[root@servera ~]# firewall-cmd --reload
[root@servera ~]# firewall-cmd --list-all
允许端口
[root@servera ~]# firewall-cmd --permanent --add-port=8000/tcp
[root@servera ~]# firewall-cmd --reload
[root@servera ~]# firewall-cmd --list-all
移除端口
[root@servera ~]# firewall-cmd --permanent --remove-port=8000/tcp
[root@servera ~]# firewall-cmd --reload
[root@servera ~]# firewall-cmd --list-all
允许源地址到某个区域（了解）
firewall-cmd --add-source=172.25.250.100 --zone=trusted --permanent
firewall-cmd --reload
  
firewall-cmd --add-interface=enp2s0 --zone=trusted --permanent
firewall-cmd --reload
```

### 4 典型举例

  ```bash
允许apache服务
foundation-----访问-----servera的web服务
思路

【servera】
$ firewall-cmd --permanent --remove-service=http  
$ firewall-cmd --reload  #立即生效
$ firewall-cmd --list-all

$ yum install -y httpd
$ echo test_page > /var/www/html/index.html
$ systemctl enable --now httpd

$ curl localhost     访问成功后，证明本地可以正常访问
test_page

【foundation】
$ curl http://servera  发现不能访问

【servera】
systemctl enable --now firewalld
systemctl status firewalld

firewall-cmd --permanent --add-service=http    #--permanent 永久生效，必须添加
firewall-cmd --reload  #立即生效
firewall-cmd --list-all

【foundation】
curl http://servera  发现可以访问


练习：允许ftp服务
servera端：
yum install -y vsftpd    #安装vsftpd软件
systemctl enable --now vsftpd

vim /etc/vsftpd/vsftpd.conf   #进入配置文件 
anonymou_enable=NO  改为YES    #开启匿名访问

systemctl restart vsftpd

yum install -y ftp     #安装ftp可客户端

$ ftp localhost     #访问本机ftp服务器
Trying ::1...
Connected to localhost (::1).
220 (vsFTPd 3.0.3)
Name (localhost:root): ftp          #ftp表示匿名用户登录
331 Please specify the password.
Password: 直接回车

f0充当client端测试：
yum install -y ftp
ftp localhost   发现不能登录，需要在server端允许ftp服务。
  ```

##  二  SElinux安全端口（考点）

```bash
【servera】
[root@servera ~]# setenforce 1
[root@servera ~]# vim /etc/httpd/conf/httpd.conf 
Listen 80 改成了Listen 82
[root@servera ~]# systemctl restart httpd   重启不了，因为selinux
```

```bash
[root@servera ~]# man semanage port    #/EX
[root@servera ~]# semanage port -a -t http_port_t -p tcp 82  #数据库没有标签内容则-a，有则-m
[root@servera ~]# semanage port -l | grep http
重启web服务
[root@servera ~]# systemctl restart httpd
[root@servera ~]# systemctl enable --now httpd
```

```bash
[root@servera ~]# firewall-cmd --permanent --add-port=82/tcp   #本地访问无须配置，远端主机访问需要配置
[root@servera ~]# firewall-cmd --reload
[root@servera ~]# firewall-cmd --list-all

测试
[serverb curl http://servea:82] 
```

  ```bash
富规则参考
[root@servera ~]# man 5 firewalld.richlanguage
  ```

# 第 十 章 kickstart

**提示: 在workstation(172.25.250.9)上面做服务器端，servera做客户端**

| 安装方式  | 简介                           |
| --------- | ------------------------------ |
| DVD 光盘  | 物理系统光盘                   |
| ISO镜像   | 光盘制作成为的镜像.iso文件     |
| QCOW2镜像 | 云环境或虚拟环境中部署为虚拟机 |

## 一 手动安装系统

## 二 使用KICKSTART自动安装

```
P293 Kickstart练习
[kiosk@foundation0 ~]$ ssh workstation
[student@workstation ~]$ lab start installing kickstart
....
[student@workstation ~]$ ssh servera
[student@workstation ~]$ sudo -i 
redhat

1.后面按照教材要求进行配置
[root@servera ks-config]# vim /var/www/html/ks-config/kickstart.cfg
bootloader --append="console=ttyS0 console=ttyS0,115200n8 no_timer_check net.ifnames=0  crashkernel=auto" --location=mbr --timeout=1 --boot-drive=vda
graphical
keyboard --vckeymap=us
lang en_US.UTF-8
repo --name="BaseOS" --baseurl="http://classroom.example.com/content/rhel9.0/x86_64/dvd/BaseOS/"
repo --name="Appstream" --baseurl="http://classroom.example.com/content/rhel9.0/x86_64/dvd/AppStream"
url --url="http://classroom.example.com/content/rhel9.0/x86_64/dvd/"
rootpw --plaintext redhat
authselect select sssd
selinux --enforcing
services --disabled="kdump,rhsmcertd" --enabled="sshd,rngd,chronyd"
timezone America/New_York --utc
ignoredisk --only-use=vda
autopart
%pre --erroronfail 
echo "Kickstarted on $(date)" >> /etc/issue
%end
%post --erroronfail
echo -n "Setting default runlevel to multiuser text mode"
rm -f /etc/systemd/system/default.target
ln -s /lib/systemd/system/multi-user.target /etc/systemd/system/default.target
echo .
echo "Removing linux-firmware package."
dnf -C -y remove linux-firmware
echo "virtual-guest" > /etc/tuned/active_profile
cat > /etc/hosts << EOF
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
EOF
%end
%packages
@core 
chrony
dracut-config-generic
dracut-norescue
firewalld
grub2
kernel
rsync
tar 
httpd
-plymouth
%end

2.重启httpd服务，并开机自启动。
3.关闭防火墙，selinux。
```

一 dhcp设置

```bash
【root@servera】
[root@servera ~]# systemctl stop firewalld;setenforce 0

一、dhcp
1 安装
[root@servera ~]# yum install -y dhcp-server
2 建立配置文件
[root@servera ~]# cp /usr/share/doc/dhcp-server/dhcpd.conf.example /etc/dhcp/dhcpd.conf
cp: overwrite '/etc/dhcp/dhcpd.conf'? y
[root@servera ~]# vim /etc/dhcp/dhcpd.conf   #man 5 dhcpd.conf   /next-server
3 配置服务
[root@servera ~]# vim /etc/dhcp/dhcpd.conf
allow bootp;
allow booting;
subnet 172.25.250.0 netmask 255.255.255.0 {
  range 172.25.250.100 172.25.250.200;
  option routers 172.25.250.254;
  default-lease-time 600;
  max-lease-time 7200;
  filename "/pxelinux.0";
  next-server 172.25.250.10;
}
4 启动并测试
[root@servera ~]# systemctl enable --now dhcpd

5 测试dhcp功能：设置serverb开机启动为网卡启动，当做客户端，测试能够获取ip即可
```

二、tftp and syslinux

```bash
1、安装tftp服务
[root@servera ~]# yum install -y tftp-server tftp
[root@servera ~]# rpm -ql tftp-server

2、安装syslinux-tftpboot该软件后提供了/tftpboot目录其中包含了一些引导文件、内核文件及pxelinux.0文件等，建立pxelinux.cfg目录，准备存放default文件。
[root@servera ~]# yum install -y syslinux-tftpboot.noarch
[root@servera ~]# rpm -ql syslinux-tftpboot
[root@servera ~]# mkdir /tftpboot/pxelinux.cfg/

3、将254.250上的光盘镜像相关文件挂载至本机
[root@servera ~]# mkdir /content
[root@servera ~]# mount 172.25.254.250:/content /content/
[root@servera ~]# df -h
[root@serveran ~]# cp /content/rhel9.0/x86_64/dvd/images/pxeboot/{initrd.img,vmlinuz} /tftpboot/
[root@servera ~]# cp /content/rhel9.0/x86_64/dvd/isolinux/boot.msg /tftpboot/
[root@servera ~]# cp /content/rhel9.0/x86_64/dvd/isolinux/isolinux.cfg /tftpboot/pxelinux.cfg/default
[root@servera ~]# vim /tftpboot/pxelinux.cfg/default 
default vesamenu.c32 
timeout 60  可以改为60，就是6秒
display boot.msg

label linux
  menu label ^Install Red Hat Enterprise Linux 9.0.0
  menu default
  kernel vmlinuz
  append initrd=initrd.img inst.stage2=ftp://172.25.250.10/dvd  inst.ks=http://172.25.250.10/ks-config/kickstart.cfg quiet

4、修改tftp发布目录，并启动服务及测试
[root@servera ~]# vim /usr/lib/systemd/system/tftp.service 
[Service]
ExecStart=/usr/sbin/in.tftpd -s /tftpboot  (将-s /var/lib/tftpboot，更成-s /tftpboot)

[root@servera ~]# systemctl enable --now tftp
测试：
登录servera，yum install -y tftp ,tftp 172.25.250.10,get ls.c32 quit
```

三、ftp


```bash
1、安装
[root@servera ~]# yum install -y vsftpd.x86_64 ftp
[root@servera ~]# rpm -qc vsftpd

2、配置服务
[root@servera ~]# vim /etc/vsftpd/vsftpd.conf 
anonymous_enable=YES

[root@servera ~]# mkdir /var/ftp/dvd
[root@servera ~]# mount /content/rhel9.0/x86_64/isos/rhel-baseos-9.0-x86_64-dvd.iso /var/ftp/dvd/

3、启动及测试
df -h
[root@servera ~]# systemctl enable --now vsftpd

4 从serverb上测试一下tftp功能是否正常（可选）
【serverb】cleint
以网卡方式启动--安装1、指定ftp路径172.25.250.9/dvd 2 最小化 3 设置lvm分区
进度条走完-重启，添加硬盘启动 ， 从本地硬盘启动。
```

四 、保证所有服务测试正常

```bash
1.dhcpd   分配IP，指导访问tftp
2.tftp    分配启动所需文件及pxelinux安装程序
3.vsftpd  共享光盘镜像
4.httpd   共享ks文件

其实vsftpd和httpd可以二选一，例如只用httpd服务共享光盘镜像、ks文件。
```

## 三 使用cockpit安装虚拟机

```
[root@servera ~]# yum install -y cockpit-machines
```

```
以服务的形式启动http，一旦启动就会长期运行，在内存中产生相应的进程systemct start httpd

容器以进程的方式运行http，一旦运行完毕容器就会自动关闭，自动退出，结束运行。当再想运行http的时候需要新开启一个容器

rhel8查看一个文件，
容器

容器：1个应用  -- 好管理    apache   php   mysql +linux= lamp -- 支持php网站
容器：多个应用 -- 不好管理   （apache   php   mysql）
```





# 第十一章 运行容器

总目标：获取简单的轻量型服务，并在单个红帽企业 Linux服务器上作为容器运行和管理。

目标：

- 解释容器概念，以及用于构建、存储和运行容器的核心技术。
- 讨论使用注册表存储和检索镜像以及部署、查询和访问容器的容器管理工具。
- 通过从容器主机共享存储来为容器数据提供持久存储，并配置容器网络。
- 将容器配置为 systemd 服务，并将容器服务配置为在系统启动时启动。

章节：

1. ​	容器概念
2. ​	部署容器
3. ​	管理容器存储和网络资源
4. ​	作为系统服务来管理容器



## 第一节 容器概念

![podman](https://gitee.com/suzhen99/redhat/raw/master/images/podman.svg)

### **1.1 容器技术介绍**

- 软件应用通常依赖于运行时环境（**runtime environment**）提供的系统库、配置文件或服务。传统上，软件应用的运行时环境安装在物理主机或虚拟机上运行的操作系统中。然后，管理员在操作系统上安装应用依赖项。

- 在红帽企业Linux中，诸如RPM等打包系统可协助管理员管理应用依赖项。安装httpd软件包时，RPM系统会确保同时安装该软件包的正确库和其他依赖项

- 以传统方式部署的软件应用的主要弊端是这些依赖项会受到运行时环境的束缚。应用需要的支持软件的版本可能比操作系统提供的软件更旧或更新。同样，同一系统上的两个应用可能需要同一软件互不兼容的不同版本。

- 解决这些冲突的方式之一是将应用打包并作为容器进行部署。容器是由一个或多个与系统其余部分隔离的进程组成的集合。软件容器是打包应用以简化其部署和管理的一种方式。

- 以实体集装箱为例。集装箱是打包和装运货物的标准方式。它作为一个箱子进行标记、装载、卸载，以及从一个位置运输到另一个位置。集装箱中的内容与其他集装箱的内容隔离，因此互不影响。这些基本原则也适用于软件容器。

- 


### **1.2** **容器的核心技术**

- 红帽企业 Linux通过运用以下核心技术来支持容器
  用于资源管理的控制组(cgroups)。
  用于进程隔离的命名空间。
  加强安全边界的SELinux和 Seccomp安全计算模式)



### 1.3 **容器和虚拟机之间的差异**

- 容器提供许多与虚拟机相同的益处，如安全、存储和网络隔离等。
- 这两种技术都将其应用库和运行时资源与主机操作系统或虚拟机监控程序隔离开，反之亦然。

![image-20230908181705833](C:\Users\guoyu\AppData\Roaming\Typora\typora-user-images\image-20230908181705833.png)

![image-20230908181716017](C:\Users\guoyu\AppData\Roaming\Typora\typora-user-images\image-20230908181716017.png)

容器和虚拟机以不同的方式与硬件和底层操作系统交互。
虚拟机具有以下特征:

-  使多个操作系统能够同时在一个硬件平台上运行
-  使用虚拟机监控程序将硬件分为多个虚拟硬件系统
-  需要一个完整的操作系统环境来支持该应用

容器具有以下特征:

- 直接在操作系统上运行，从而跨系统上的所有容器共享资源
- 共享主机的内核，但它将应用进程与系统其余部分隔离开来
- 与虚拟机相比，它需要的硬件资源要少得多，因此容器的启动速度也更快
- 包括所有依赖项，如系统和编程依赖项，以及配置设置

### 1.4 Rootless和 Rootful 容器

在容器主机上，您可以 root 用户或普通非特权用户身份运行容器。由特权用户运行的容器称为Rootful容器。由非特权用户运行的容器称为 Rootless 容器。

Rootless容器不允许使用通常为特权用户保留的系统资源，例如访问受限目录，或在受限端口(1024以下的端口)上发布网络服务。此功能可防止潜在攻击者获取容器主机上的root特权

如有必要，您可以root 用户身份直接运行容器，但如果有漏洞允许攻击者破坏容器，这样做会削弱系统的安全性。

### 1.5 **设计基于容器的架构**

容器是重复利用托管应用并使其可以移植的有效方式。容器可以轻松地从一个环境迁移到另一个环境，如从开发环境迁移到生产环境。您可以保存一个容器的多个版本，并根据需要快速访问每个版本。

容器通常是临时的。您可以将运行中容器所生成的数据永久保存到持久存储中，但容器本身通常会在需要时运行，然后停止并被删除。下次需要该特定容器时，将启动新的容器进程。

您可以在单个容器中安装含有多个服务的复杂软件应用。例如，Web 服务器可能需要使用数据库和消息传递系统。不过，将一个容器用于多个服务会难以管理。

更好的设计是在单独的容器中运行每个组件、Web 服务器、数据库和消息传递系统。这样，更新和维护单个应用组件不会影响其他组件或应用堆栈。

### 1.6 **容器镜像和注册表**

要运行容器，您必须使用容器镜像。容器镜像是包含编码步骤的静态文件，它充当创建容器的蓝图。容器镜像打包应用及其所有依赖项，如系统库、编程语言运行时和库，以及其他配置设置。

容器镜像根据规范构建，如开放容器项目(OCI)镜像格式规范。这些规范定义容器镜像的格式，以及镜像支持的容器主机操作系统和硬件架构的元数据。

容器注册表是用于存储和检索容器镜像的存储库。开发人员将容器镜像推送或上传到容器注册表中。您可以从注册表中将这些容器镜像拉取或下载到本地系统，以用于运行容器。

可使用包含第三方镜像的公共注册表，也可使用贵组织控制的私有注册表。容器镜像来源很重要。和任何其他软件包一样，您必须知道是否可以信任容器镜像中的代码。对于是否及如何提供、评估和测试提交给它们的容器镜像，不同的注册表具有不同的策略。

红帽通过两个主容器注册表分发认证容器镜像，您可以使用红帽登录凭据来访问这两个注册表。

- registry.redhat.io: 适用于基于官方红帽产品的容器。
- registryconnect.redhat,com:适用于基于第三方产品的容器。
- 红帽容器目录(https://access.redhat.com/containers)提供了一个基于Web的界面，通过它可以搜索这些注册表中的认证内容。
- 培训环境中添加了红帽Quay,官方为收费版，培训环境中为免费版registry.lab.example.com



您需要红帽开发人员账户才能从红帽注册表下载镜像。您可以使用podman login命令对注册表进行身份验证。如果您不向podman login 命令提供注册表URL，它会向默认配置的注册表进行身份验证

```bash
#登录servera请使用ssh方式，不要使用su切换。
[root@foundation0 ~]# ssh root@servera
[root@servera ~]# ssh student@localhost
[student@servera ~]$ sudo dnf -y install container-tools 
[student@servera ~]$ podman login -u admin -p redhat321 registry.lab.example.com 
```

```bash
$ sudo dnf -y install container-tools  #安装podman容器
$ podman --version


$ podman login --help
登录方法一（交互）：
$ podman login registry.lab.example.com 
Username: admin
Password: redhat321
Login Succeeded!

登录方法二（非交互）： #推荐
$ podman login registry.lab.example.com -u admin -p redhat321 
Login Succeeded!
```

```bash
# podman login -u adminexample.comError: authenticating creds for "registry.lab.example.com": pingincontainer registry registry.lab.example.com: Get "https://registy.lab.example.com/v2/": x509: certificate relies on legacy CommonName field，use SANs instead

如果出现以上报错，是要求https验证，需要通过选项--tls-verify进行手动关闭
podman login registry.lab.example.com -u admin -p redhat321 --tls-verify=false
```

您还可以使用podman login命令的 --username和--password-stdin 选项，指定用于登录注册表的用户和密码。--password-stdin 选项从 stdin 读取密码。红帽建议不要使用--password选项直接提供密码，因为此选项会将密码存储在日志文件中。

```bash
登录方法三（非交互）
$ echo redhat321 | podman login -u admin --password-stdin  registry.lab.example.com
Login Succeeded!
```

要验证您是否已登录到某一注册表，请使用 podman login命令 --get-login选项。及退出登录podman logout

```bash
$ podman login --get-login   #查看登录的用户
admin

$ podman login registry.lab.example.com --get-login   #指定仓库地址，查看登录用户
admin

$ podman logout registry.lab.example.com  			  #登出
Removed login credentials for registry.lab.example.com

后续实验需要登录，请重新进入登录状态
```

### 1.7 **配置容器注册表**

容器注册表的默认配置文件是 /etc/containers/registries.conf 文件。

```bash
$ sudo vim /etc/containers/registries.conf
[sudo] password for student: student

#第22行 指定可搜索的镜像仓库地址，如果使用完全合格域名，此处可以留空
unqualified-search-registries = ["registry.lab.example.com"] 

#第24行
[[registry]]   解除注释开启以下功能
#第37行
insecure = true   #false/true 开启https安全验证/关闭安全验证
#第40行
blocked = false    #需要过滤掉的镜像仓库地址
location = "registry.lab.example.com" 指定容器注册表位置

$注意
~/.config/containers/registries.conf目录设置会覆盖/etc/containers/registries.conf

推荐：
【student】
$ mkdir -p ~/.config/containers
$ cp /etc/containers/registries.conf ~/.config/containers/registries.conf
$ vim ~/.config/containers/registries.conf
unqualified-search-registries = ["registry.lab.example.com"] 
[[registry]]   
insecure = true 
blocked = false    
location = "registry.lab.example.com" 


注意：如果只访问本地仓库，unqualified-search-registries = ["registry.lab.example.com"]默认即可，但要需要访问外网，需要用root用户修改vim /etc/resolv.conf文件内容添加nameserver 8.8.8.8 优先解析。
阿里容器
i2kldsde.mirror.aliyuncs.com
```

### 1.8 **使用容器文件构建容器镜像**

------

容器文件是一种文本文件，内含用于构建容器镜像的指令。容器文件通常具有定义其文件和目录所在路径或URL的上下文。生成的容器镜像由只读层组成，每一层代表容器文件中的一条指令。

以下输出是一个容器文件示例，它使用registry.access.redhat.com注册表中的UBI镜像,安装 python3 软件包，并将 hello 字符串打印到控制台。

```bash
$ cat Containerfile
FROM registry.access.redhat.com/ubi8/ubi:latest
RUN dnf install -y python3
CMD["/bin/bash"，"-c"，"echo hello"]
```

创建容器文件及其使用说明超出了本课程的范畴。有关容器文件的更多信息，请参阅DO180课程。



### 1.9 **规模化容器管理**

------

新应用越来越多地使用容器来实施功能组件。这些容器提供应用的其他部分使用的服务。组织管理越来越多的容器，可能很快就会不堪重负。

在生产中大规模部署容器需要一个能够应对以下挑战的环境:
- 平台必须确保提供必要服务的容器的可用性。
- 环境必须通过增加或减少运行中的容器实例，并对流量进行负载平衡，从而应对应用的使用高峰。
- 平台必须检测容器或主机的故障，并相应地作出反应。
- 开发人员可能需要自动工作流，以便透明、安全地向客户交付新的应用版本。

Kubernetes是一项编排服务，可以使在容器主机集群中部署、管理和扩展基于容器的应用变得更加轻而易举。Kubernetes 通过负载平衡器将流量重定向到容器，以便您可以扩展提供服务的容器数量。Kubernetes 还支持用户定义的健康检查，以便监控您的容器，并在容器出现故障时将其重新启动。

红帽提供了一个名为红帽OpenShift 的kubernetes 发行版。红帽OpenShift是基于 Kubernetes基础架构构建的一组模块化组件和服务。它为开发人员提供的额外功能包括基于 Web 的远程管理、多租户、监控与审计、高级安全功能、应用生命周期管理和自助服务实例等。

红帽OpenShift不在本课程讨论范围之内，但您可以通过https://www.openshift.com了解更多相关信息。



## 第二节 部署容器



### **培训目标**

讨论使用注册表存储和检索镜像以及部署、查询和访问容器的容器管理工具。



### 2.1 **Podman 实用程序**

------

Podman 是来自container-tools元数据包的全功能容器引警，用于管理开放容器计划(0C)容器和镜像。podman 实用程序的运作不使用守护进程，因此开发人员无需系统上的特权用户帐户来启动或停止容器。Podman提供多个子命令来与容器和镜像交互。以下列表显示了本节中使用的子命令:

**Podman 命令**

| 命令           | 描述                                     |
| -------------- | ---------------------------------------- |
| podman build   | 使用容器文件构建容器镜像。               |
| podman run     | 在新容器中运行命令。                     |
| podman images  | 列出本地存储中的镜像。                   |
| podman ps      | 打印有关容器的信息。                     |
| podman inspect | 显示容器、镜像、卷、网络或容器集的配置。 |
| podman pull    | 从注册表下载镜像。                       |
| podman cp      | 在容器和本地文件系统之间复制文件或目录   |
| podman exec    | 在运行中的容器内执行命令。               |
| podman rm      | 删除一个或多个容器。                     |
| podman rmi     | 删除一个或多个本地存储的镜像。           |
| podman search  | 在注册表中搜索镜像。                     |

有关各个子命令使用帮助手册的更多信息，请将子命令附加到 podman 命令，并用连字符将两者分隔。例如，podman-build 帮助手册介绍了 podman build 子命令的用法。



为阐述本课中的主题，请想象以下情景。

作为系统管理员，您被委任了一项任务，使用 python-38 软件包运行基于名为 python38的RHEL8UBI 容器镜像的容器。您还有一项任务，从容器文件创建容器镜像，并从该容器镜像运行名为python36的容器。使用容器文件创建的容器镜像必须具有python36:1.0标签。识别两个容器之间的差异。另外，请确保容器中安装的 python 软件包与本地计算机中安装的 Python 版本不相冲突。



### 2.2 **安装容器实用工具**

------

container-tools 软件包包含与容器和容器镜像交互所需的实用程序。若要在系统上下载、运行和比较容器，请使用dnf install命令来安装container-tools 元软件包。使用dnf info命令查看 container-tools 软件包的版本和内容。

```bash
$ dnf install container-tools
$ dnf info container-tools
```

container-tools 元数据包提供所需的 podman和skopeo 实用程序，用于完成分配的任务。



### 2.3 **从注册表下载容器镜像文件**

------

首先，确保podman 实用程序已配置为从registry.redhat,io注册表搜索和下载容器。podman info命令显示podman 实用程序的配置信息，包括其配置的注册表。

```bash
$ podman info
```

podman search命令使用registries.conf文件中指定的注册表列表搜索匹配的名称镜像。默
认情况下，Podman在所有非限定搜索注册表中执行搜索。



### **让servera联网方法：**

------



```bash
1、f0宿主机上ens192是第二块网卡，vmware上选择nat模式-选择‘已连接’，再获取IP，保证可以上网
2、在f0宿主机上执行ssh root@172.25.254.254 rht-config-nat
3、servera上ping www.baidu.com测试	
```

使用podman search命令，显示包含python-38软件包的已配置注册表的镜像列表

```bash
$ podman search registry.lab.example.com/
$ podman pull registry.lab.example.com/ubi8/python-38
$ podman images
```

```bash
[student@servera ~]$ podman images
REPOSITORY                               TAG         IMAGE ID      CREATED        SIZE
registry.lab.example.com/ubi8/python-38  latest      b335f517d3b5  16 months ago  892 MB

REPOSITORY   :仓库地址
TAG 		 ：标记，latest最近版本
IMAGE ID     ：镜像ID，ID号唯一，保证镜像唯一性
CREATED      ：创建时间；
SIZE    	 ：镜像大小
```

```bash
练习：
以普通用户身份远程登录系统
# ssh student@localhost
1.登录容器镜像仓库服务器registry.lab.example.com
$ podman login  -u admin -p redhat321 registry.lab.example.com
2.搜索镜像服务器仓库registry.lab.example.com所有镜像
$ podman search  registry.lab.example.com/
3.下载rhel7、8、9的ubi系统到本地
$ podman pull registry.lab.example.com/ubi7/ubi
$ podman pull registry.lab.example.com/ubi8/ubi
$ podman pull registry.lab.example.com/ubi9-beta/ubi
4.查看本地镜像
$ podman images
5.通过rhel7的ubi运行一个交互式容器
$ podman run -ti registry.lab.example.com/ubi7/ubi
6.通过rhel8的ubi运行一个长期挂载到后台的容器
$ podman run -di registry.lab.example.com/ubi8/ubi
7.通过rhel9的ubi运行一个长期挂载到后台的容器，并且指定容器名称为rhel9
$ podman run -di --name rhel9  registry.lab.example.com/ubi9-beta/ubi
8.进入rhel9容器进行交互
$ podman exec -ti rhel9 /bin/bash
9.使用httpd-24镜像运行一个web服务。并且访问测试
podman run -d --name web1 -p 8090:8080 registry.lab.example.com/rhel8/httpd-24
$ curl localhost:8090  #本地测试，如果在其他主机上访问servera容器web服务，需要防火墙允许主机端口8090
$ sudo firewall-cmd --permanent --add-port=8090/tcp
$ sudo firewall-cmd --reload
$ sudo firewall-cmd --list-all
10.使用nginx镜像运行一个web服务。并且访问测试，nginx容器内端口为80。


```





### 2.4 **从容器文件创建容器镜像**

------

您获得了以下容器文件，用于在 python36-app 目录中创建容器镜像

```bash
$ cat Containerfile 
FROM registry.access.redhat.com/ubi8/ubi:latest
RUN dnf install -y python36
CMD["/bin/bash"，"-c"，"sleep infinity"]
此容器文件是教材中例子默认报错，可以使用下面的容器文件
```

```bash
$ cat Containerfile 
FROM registry.lab.example.com/ubi9-beta/ubi:latest
RUN echo -e '[rhel-9.0-for-x86_64-baseos-rpms]\nbaseurl = http://content.example.com/rhel9.0/x86_64/dvd/BaseOS\nenabled = true\ngpgcheck = false\nname = Red Hat Enterprise Linux 9.0 BaseOS (dvd)\n[rhel-9.0-for-x86_64-appstream-rpms]\nbaseurl = http://content.example.com/rhel9.0/x86_64/dvd/AppStream\nenabled = true\ngpgcheck = false\nname = Red Hat Enterprise Linux 9.0 Appstream (dvd)'>/etc/yum.repos.d/rhel_dvd.repo
RUN yum install --disablerepo=* --enablerepo=rhel-9.0-for-x86_64-baseos-rpms --enablerepo=rhel-9.0-for-x86_64-appstream-rpms -y python3
CMD ["/bin/bash", "-c", "sleep infinity"]
```



- 以上容器文件使用registry.access.redhat.com/ubi8/ubi:latest 镜像作为基础镜像。容器文件而后将安装python36 软件包，并运行sleep infinity bash 命令来防止容器退出
- 通常，容器运行一个进程，然后在该进程完成后退出。sleep nfinity 命令可防止容器退出因为该进程永远不会完成。然后，您可以在容器内进行测试、开发和调试。
- 在检查容器文件后，您可以使用podman build命令来构建镜像。podman build命令的语法如下所示:

```bash
$ podman build -t NAME:TAG  DIR
```

NAME：新镜像的名称。
标签： 新镜像的标签。如果未指定标签，则镜像自动标记为 latest。
DIR： 工作目录路径。容器文件必须位于工作目录中。如果工作目录是当前目录，则您可以用点(.)来指定它。使用-f标志指定与当前目录不同的目录。

```bash
$ podman build -t python39:1.0 .
```

以上输出的最后一行显示了容器镜像ID。大多数Podman 命令使用容器镜像ID的前12个字符来指代容器镜像。您可以将此短ID 或者容器或容器镜像的名称，作为大多数 Podman 命令的参数。

```bash
$ podman images
```

然后，使用 podman inspect 命令来查看容器镜像的低级别信息，并验证其内容是否符合容器要
求。

```bash
（podman pull  先下载后再使用inspect）
$ podman inspect localhost/python36:1.0
```

podman inspect命令的输出显示reqistry.access.redhat.com/ubi8/ubi:latest基础镜像、用于安装 python36 软件包的 dnf命令，以及在运行时执行以防止容器退出的sleep infinity bash命令。



### 运行容器

------

现在，您已拥有所需的容器镜像，可以使用它们来运行容器。容器可以处于以下状态之一!
Created
	已创建好但尚未启动的容器。
运行中
	与其进程一起运行的容器。
已停止
	其进程已停止的容器。
Paused
	其进程已暂停的容器。不支持 Rootless 容器。
Deleted
	其进程处于已死状态的容器。

podman ps命令列出系统上正在运行的容器。使用podman ps -a来命令查看计算机中的所有容器 (已创建、已停止、已暂停或正在运行)。
您可使用podman create 命令来创建容器，以便稍后运行。若要创建容器，请使用 localhost/python36容器镜像的ID。也可以使用--name 选项设置名称来标识容器。此命令的输出是容器的长ID,如果不指定--name选项，会自动生成一个容器名称。

```bash
$ podman create --name python36 dd6ca291f097
```

然后，您可使用podman ps和podman ps -a命令来验证容器是否已创建但尚未启动。您可以查看有关 python36 容器的信息，如容器的短ID、名称和状态，容器在启动时运行的命令，以及用于
创建容器的镜像。

```bash
$ podman ps 
$ podman ps -a
```

现在您已验证容器已正确创建，您决定启动容器，所以要运行 podman start 命令。您可以使用名称或容器ID来启动容器。此命令的输出是容器的名称。

```bash
$ podman start python36
$ podman ps
```



### 2.6 从远程存储库运行容器

您可使用podman run命令，在一个步骤中创建并运行容器。podman run 命令在容器内运行进程，此进程将启动新容器。
您可以使用podman run 命令-d选项以分离模式运行容器，这将在后台运行容器，而不是在会话的前台运行。在 python36 容器的示例中，您不需要提供容器运行所提的命令，原因是为该容器创建镜像的容器文件中已提供了sleep infinity 命令。

为了创建python38容器，您决定使用podman run 命令并引用registry.access.redhat,com/ubi8/python-38镜像。您还决定使用sleep infinity命令来防止容器退出。

```bash
podman run 
-t  终端
-i  交互
-d  放在后台
--name  指定容器的名称，如果不指定，会自从产生名称

#实验前可以提前下载镜像至本地 
#podman search registry.lab.example.com/
#podman pull registry.lab.example.com/ubi8/ubi
#podman images

$ podman run -ti --name rhel8 registry.lab.example.com/ubi8/ubi  
$ podman run -di --name rhel8-1 registry.lab.example.com/ubi8/ubi
$ podman exec -ti rhel8-1 /bin/bash
```



```bash
$ podman run -d --name python38 registry.access.redhat.com/ubi8/python-38 sleep infinity
$ podman ps 
```



### 2.7 容器中的环境隔离

容器隔离应用的环境。每个容器都有自己的文件系统、网络和进程。如果您查看ps 命令的输出,并在主机和运行中容器之间进行比较，就会注意到隔离功能。
首先在本地计算机上运行 ps -ax 命令，该命令将返回具有许多进程的预期结果。

```bash
$ ps -ax
```

podman exec 命令可在运行中的容器内执行命令。该命令取容器的名称或ID作为第一个参数，并将下列参数作为要在容器内运行的命令。您可以使用podman exec命令查看python36容器中正在运行的进程。ps aux命令的输出看起来有所不同，因为它运行与本地计算机不同的进程。

```bash
$ podman exec python38 ps -ax
#因为上面registry.access.redhat.com/ubi8镜像没有下载成功，所以教材中的python38我们无法执行，但可以使用练习环境中的rhel8镜像，执行一些其他命令，比如ls /等。
```

您可以使用sh -c命令来封装要在容器中执行的命令。在以下示例中，ps ax > /tmp/process-data.log命令被解释为要在容器中执行的命令。如果您不封装命令，则Podman 可能会将大于号字符(>)解释为 podman 命令的一部分，而不是podman exec 选项的参数。

```bash
$ podman exec python38 sh -c 'ps -ax > /tmp/process-data.log'
#同上，可以测试一些其他指令比如：
#podman exec rhel8 sh -c 'echo haha > /test.txt'
#podman exec -ti rhel8 /bin/bash
#ls /
```

您决定将主机系统上安装的python版本与容器上安装的python版本进行比较

```bash
$ python3 --version
Python 3.9.10
$ podman exec python36 python3 --version
Python 3.6.8
$ podman exec python38 python3 --version
Python 3.8.8
```



### 2.8 容器中的文件系统隔离

------

开发人员可以使用文件系统隔离功能，为不同版本的编程语言编写和测试应用，无需使用多个物理机或虚拟机。

您将在终端上的/tmp目录中创建一个显示hello world的简单bash 脚本。

```bash
$ echo "echo hello world!'" > /tmp/hello.sh
```

/tmp/hello.sh 文件位于主机计算机上，而不存在于容器内的文件系统上。如果您尝试使用podman exec来执行脚本，则会出现错误，因为容器中不存在/tmp/hello.sh脚本。

```bash
$ stat /tmp/hello.sh
$ podman exec python38 stat /tmp/hello.sh
```

podman cp 命令在主机和容器文件系统之间复制文件和文件夹。您可以使用 podman cp 命令将/tmp/hello.sh文件复制到python38容器。

```bash
$ podman cp /tmp/hello.sh python38:/tmp/hello.sh
$ podman exec python38 stat /tmp/hello.sh
```

脚本复制到容器文件系统后，即可从容器内执行。

```bash
$ podman exec python38 bash /tmp/hello.sh
hello world
```



### 2.9 删除容器和镜像

------

您可以分别使用podman rm和 podman rmi命令来删除容器和镜像。在删除容器镜像之前，必须先从该镜像移除任何现有的运行中容器。

您决定删除python38容器及其相关镜像。如果您在python38容器存在时尝试删除egistry.access.redhat.com/ubi8/python-38 镜像，这会出现错误。

```bash
$ podman rmi registry.access.redhat.com/ubi8/python-38
Error: Image used by
a60f71a1dc1b997f5ef244aaed232e5de71dd1e8a2565428ccfebde73a2f9462: mage is in useby a container
```

您必须先停止容器，然后才能删除它。若要停止容器，请使用 podman stop 命令。

```bash
$ podman stop python38
```

停止容器后，使用 podman rm 命令来删除容器

```bash
$ podman rm --help
$ podman rm python38
a60f71a1dc1b997f5ef244aaed232e5de71dd1e8a2565428ccfebde73a2f9462
```

当容器不再存在时，可以使用podman rmi 命令删除registry.access.redhat.com/ubi8/python-38

```bash
$ podman rmi registry.access.redhat.com/ubi8/python-38Untagged: registry.access.redhat.com/ubi8/python-38:latestDeleted: a33d92f90990c9b1bad9aa98fe017e48f30c711b49527dcc797135352ea57d12
```

### 练习

```
P331
```



## 第三节 管理容器存储和网络资源



### 目标

通过从容器主机共享存储来为容器数据提供持久存储，并配置容器网络。

练习：P347



### 1  **管理容器资源**

------

您可以使用容器来运行简单的进程，然后退出。

还可以配置容器以连续运行某一服务，如数据库服务器。如果您持续运行服务，您最终可能需要向容器添加更多资源，如持久存储或对其他网络的访问权限。

您可以使用不同的策略为容器配置持久存储:
- 对于红帽OpenShift 等企业容器平台上的大型部署，您可以使用复杂的存储解决方案为容器提供存储，而无需了解底层基础架构。
- 对于单个容器主机上且无需扩展的小型部署，您可以通过在运行中的容器上创建要挂载的目录，从容器主机创建持久存储。

当Web服务器或数据库服务器等容器为容器主机外部的客户端提供内容时，您必须为这些客户端设置通信通道，以访问容器的内容。您可以配置端口映射，以启用与容器的通信。通过端口映射，目的地为容器主机上端口的请求将被转发到容器内的端口。

若要理解本讲座中的主题，设想您必须执行以下任务

- 基于MariaDB，创建名为db01的容器化数据库。

- 配置容器端口映射和主机防火墙，以允许端口3306/tcp 上的流量。

- 配置db01容器，以使用具有适当SELinux 上下文的持久存储。

- 添加适当的网络配置，以便client01容器可以使用DNS与db01容器通信。

  

### **2 容器的环境变量**

------

些容器镜像允许在创建时传递环境变量以自定义容器。您可以使用环境变量为容器设置参数，以根据您的环境进行定制，无需创建自己的自定义镜像。通常，您不会修改容器镜像，因为这会向镜像添加层，或许更加难以维护。

您使用podman run -d registry.lab.example.com/rhel8/mariadb-105命令运行容器化数据库，但发现容器无法启动。

```bash
$ podman run -d --name db01 registry.lab.example.com/rhel8/mariadb-105
```

您将使用 podman container logs 命令调查容器状态的原因。

```bash
$ podman container logs db01
```

```bash
$ skopeo inspect docker://registry.lab.example.com/rhel8/mariadb-105 | grep -B 1 Usage
#练习过程中可以将usage中提示的示例指令作为参考。
```

输出中的usage 标签提供了如何运行镜像的示例。url标签指向红帽容器目录中的一个Web页面，其中记录了环境变量以及有关如何使用容器镜像的其他信息。



此镜像的文档显示容器将3306 端口用于数据库服务。文档中还显示了以下环境变量可用于配置数据库服务:

mariadb镜像的环境变量:

| 变量                | 描述                      |
| ------------------- | ------------------------- |
| MYSQL USER          | 要创建的MySQL帐户的用户名 |
| MYSQL_PASSWORD      | 用户帐户的密码            |
| MYSQL_DATABASE      | 数据库名称                |
| MYSQL_ROOT_PASSWORD | root用户的密码 (可选)     |

在检查了镜像的可用环境变量后，您使用 podman run 命令-e选项将环境变量传递给容器，并使用podman ps命令来验证它是否正在运行。

```bash
$ podman run -d --name db01 \
-e MYSQL_USER=user \
-e MYSQL_PASSWORD=pass \
-e MYSQL_DATABASE=db \
-e MYSQL_ROOT_PASSWORD=redhat \
registry.lab.example.com/rhel8/mariadb-105

$ podman ps -a
```



### **3  容器持久存储**

------

默认情况下，在您运行容器时，其所有内容都使用基于容器的镜像。鉴于容器镜像的寿命短，用户或应用写入的所有新数据在移除容器后都会丢失

若要持久保留数据，您可以将容器中的主机文件系统内容与 --volume (-v)选项搭配使用。在容器中使用此卷类型时，您必须考虑文件系统级别的权限。

在MariaDB容器镜像中，mysql用户必须拥有/var/lib/mysql目录，就如同MariaDB在主机上运行时一样。您打算挂载到容器中的目录必须具有mysql作为用户和组所有者(或mysql用户的UID/GID，如果主机上没有安装 MariaDB)。如果以root 用户身份运行容器，则主机上的UID和GID 与容器内的UID和GID 匹配。

您可以使用podman unshare命令在用户命名空间内运行命令。要获取用户命名空间的UID映射，请使用podman unshare cat命令。

```bash
$ podman unshare cat /proc/self/uid_map
		0	1000	1
		1	100000	65535
$ podman unshare cat /proc/self/gid_map
		0	1000	1
		1	100000	65536
```

以上输出显示，容器中的root用户 (UID和GID为0)映射到主机计算机上的用户(UID和GID为1000)。容器中的UID和GID1映射到主机计算机上的UID和GID 100000。1后的每个UID和GID以1增量递增。例如，容器内的UID和GID30映射到主机计算机上的UID和GID100029。

您可以使用podman exec命令查看使用临时存储运行的容器内的mysql用户UID和GID。

```bash
$ podman exec -it db1 grep mysql /etc/passwd 
mysql:x:27:27:MySQL Server:/var/lib/mysql:/sbin/nologin
```

您决定将/home/user/db_data目录挂载到db01容器中，以在容器的/var/lib/mysql目录中提供持久存储。然后创建/home/user/db_data目录，并使用podman unshare命令将27的用户命名空间UID和GID设置为该目录的所有者。

```bash
$ mkdir /home/student/db_data
$ podman unshare chown 27:27 /home/student/db_data
```

容器中的UID和GID 27映射到主机计算机上的UID和GID100026。您可以使用ls 命令查看/home/student/db_data 目录的所有权来验证映射。

```bash
$ ls -l /home/student/total 0
drwxrwxr-x.3 100026 100026 18 May 5 14:37 db data
```

现在已设置了正确的文件系统级权限，您可使用 podman run 命令-v选项来挂载目录。

```bash
$ podman run -d --name db01 \
-e MYSQL_USER=user \
-e MYSQL_PASSWORD=pass \
-e MYSQL_DATABASE=db \
-e MYSQL_ROOT_PASSWORD=redhat \
-v /home/student/db_data:/var/lib/mysql \
registry.lab.example.com/rhel8/mariadb-105
```

您注意到 db01容器未在运行。

```bash
$ podman ps -a
CONTAINER IDIMAGE CREATED STATUS PORTS NAMES COMMAND
dfdc20cf9a7e registry,labexample.com/rhel8/mariadb-105:latest run-mysqld29 seconds ago Exited (1) 29 seconds ago  db01
```

podman container logs 命令显示/var/lib/mysql/data目录的权限错误。

```bash
$ podman container logs db01
```

发生此错误的原因是，主机上/home/user/db_data目录中设置的SELinux上下文不正确。



### 4 容器存储的SELinux上下文

------

您必须先设置container_file_t SELinux 上下文类型，然后才能将该目录作为持久存储挂载到容器。如果目录没有container_file_t SELinux 上下文，则容器无法访问该目录。您可以将Z选项附加到podman run命令-v选项的参数，以自动设置目录的SELinux上下文。

因此，当您将/home/student/db_data 目录挂载为/var/lib/mysql目录的持久存储时，您可以使用podman run -v  /home/student/db_data:/var/lib/mysql:Z 命令设置该目录的SELinux上下文。

```bash
$ podman run -d --name db01 \
-e MYSQL_USER=user \
-e MYSQL_PASSWORD=pass \
-e MYSQL_DATABASE=db \
-e MYSQL_ROOT_PASSWORD=redhat \
-v /home/student/db_data:/var/lib/mysql:Z \
registry.lab.example.com/rhel8/mariadb-105

$ podman ps -a
$ ll -dZ /home/student/db_data
```



### 5 分配端口映射到容器

------

要提供对容器的网络访问权限，客户端必须连接到容器主机上的端口，这些端口将网络流量传递到容器中的端口。将容器主机上的网络端口映射到容器中的端口时，容器将接收发送到主机网络端口的网络流量。

例如，您可以将容器主机上的13306端口映射到容器上的3306端口，以便与 MariaDB容器通信。因此，发送到容器主机端口13306的流量将由容器中运行的MariaDB 接收。

您可以使用 podman run命令 -p 选项设置从容器主机上13306端口到db01容器上3306端口的端口映射。

```bash
$ podman run -d --name db01 \
-e MYSQL_USER=user \
-e MYSQL_PASSWORD=pass \
-e MYSQL_DATABASE=db \
-e MYSQL_ROOT_PASSWORD=redhat \
-p 13306:3306 \
-v /home/student/db_data:/var/lib/mysql:Z \
registry.lab.example.com/rhel8/mariadb-105

$ podman ps -a
$ ll -dZ /home/student/
unconfined_u:object_r:user_home_dir_t:s0 /home/student/
```

使用podman port命令-a选项可显示正在使用的所有容器端口映射。您还可以使用podmanport db01命令显示 db01容器的映射端口。

```bash
$ podman port -a
1c22fd905120 3306/tcp -> 0.0.0.:13306
$ podman port db01
13306/tcp ->0.0.0.0:13306
```

您可以使用firewall-cmd 命令允许端口13306流量传入容器主机，以便它可以重定向到容器。

```bash
# firewall-cmd --add-port=13306/tcp --permanent
# firewall-cmd --reload
```

Rootless(特权)容器无法打开主机上特权端口1024有 以下的端口。比如-p 80:8000 ，比必须使用root才可以对其进行调整。

### 附加测试

通过主机mariadb数据库客户端软件访问容器数据库服务，需要安装mariadb软件后指定-u，账号-p密码，-h数据库服务器所在主机名或IP，及主机映射端口

```bash
$ sudo dnf -y install mariadb
$ mysql -u user -ppass -h serverb -P 13306   # -h 后面填写您当前实验的主机名或IP
```



### 6 容器中的DNS配置

Podmanv4.0支持两种容器网络后端，即Netavark和CNI。自RHEL9起，系统默认使用Netavark。若要验证所用的网络后端，请运行以下podman info命令。

```bash
$ podman info --format {{.Host.NetworkBackend}}
netavark
```

主机上使用默认Podman网络的现有容器无法解析彼此的主机名，因为默认网络上未启用DNS。



使用podman network create命令创建一个支持DNS的网络。您可使用podman network create命令创建名为db_net 的网络，并将子网指定为10.87.0.0/16，网关指定为10.87.0.1。

```bash
$ podman network create --gateway 10.87.0.1 --subnet 10.87.0.0/16 db_net

$ podman network ls #列出容器网络
```

如果不指定--gateway 或--subnet 选项，则会使用默认值创建它们。



podman network inspect 命令显示关于特定网络的信息。您可以使用podman networkinspect 命令验证网关和子网的设置是否正确，以及新的dbnet 网络是否启用了DNS。

```bash
$ podman network inspect db_net
```

您可以使用podman run命令--network选项将启用DNS的db_net 网络添加到新容器。您可以使用podman run命令--network选项创建连接到db_net 网络的db01和client01容器

```bash
$ podman run -d --name db01 \
--network db_net \
-e MYSQL_USER=user \
-e MYSQL_PASSWORD=pass \
-e MYSQL_DATABASE=db \
-e MYSQL_ROOT_PASSWORD=redhat \
-v /home/student/db_data:/var/lib/mysql:Z \
-p 13306:3306 \
registry.lab.example.com/rhel8/mariadb-105


$ podman run -d --name client01 \
> --network db_net \
> -v /etc/yum.repos.d:/etc/yum.repos.d/ \
> registry.lab.example.com/ubi9-beta/ubi \
> sleep infinity

$ podman ps -a
```

由于容器设计为仅具有所需的最少软件包，因此容器可能不具有测试通信所需的实用程序，如ping和ip命令。您可以使用 podman exec 命令在容器中安装这些实用程序。

```bash
$ podman exec client01 dnf install -y iputils iproute
..output omitted..
```

容器现在可以通过容器名称互相 ping。您可以使用podman exec 命令来测试DNS解析。名称解析到为db_net网络手动设置的子网内的IP。

```bash
$ podman exec client01 ping -c4 db01
PING db01.dns.podman (10.87.0.2) 56(84) bytes of data.
64 bytes from 10.87.0.2 (10.87.0.2): icmp_seq=1 ttl=64 time=1.08 ms
64 bytes from 10.87.0.2 (10.87.0.2): icmp_seq=2 ttl=64 time=0.082 ms
64 bytes from 10.87.0.2 (10.87.0.2): icmp_seq=3 ttl=64 time=0.063 ms
64 bytes from 10.87.0.2 (10.87.0.2): icmp_seq=4 ttl=64 time=0.070 ms

```

您可以使用 podman exec命令验证每个容器中的IP地址是否与DNS解析匹配。

```bash
$ podman exec client01 ip a | grep 10.8
    inet 10.87.0.3/16 brd 10.87.255.255 scope global eth0

```



### 7 多个网络连接到单个容器

多个网络可以同时连接到一个容器，以帮助分隔不同类型的流量。
您可以使用 podman network create命令创建backend 网络

```bash
$ podman network create backend
```

然后，使用podman network ls 命令查看所有 Podman 网络。

```bash
$ podman network ls
NETWORK ID    NAME    DRIVER  
a7fea510a6d1  backend bridge
fe680efc5276  db01    bridge
2f259bab93aa  podman  bridge
```

没有通过podman network create 命令的--gateway和--subnet 选项指定子网和网关。
使用podman network inspect 命令来获取backend 网络的IP信息。

```bash
$ podman network inspect backend
```

在容器运行时，您可以使用podman network connect 命令将其他网络连接到容器。您可以使用podman network connect命令，将backend网络连接到db01和client01容器。

```bash
$ podman network connect backend db01
$ podman network connect backend client01
```

您可以使用podman inspect 命令验证两个网络是否都已连接到各个容器并显示IP信息。

```bash
$ podman inspect db01

$ podman inspect db01 | grep -A 34 Networks > db01
$ cat db01  #查看db01的两个网络IP为10.89.0.2 ， 10.87.0.2
```

client01容器现在可以与两个网络上的db01容器通信。您可以使用podman exec 命令从cliento1容器pingdb01容器上的两个网络。

```bash
$ podman exec -ti client01 ping -c4 10.89.0.2
PING 10.89.0.2 (10.89.0.2) 56(84) bytes of data.
64 bytes from 10.89.0.2: icmp_seq=1 ttl=64 time=0.352 ms
$ podman exec -ti client01 ping -c4 10.87.0.2
PING 10.87.0.2 (10.87.0.2) 56(84) bytes of data.
64 bytes from 10.87.0.2: icmp_seq=1 ttl=64 time=0.594 ms
```

容器内安装mariadb客户端访问容器数据库

```bash
$ podman exec client01 dnf -y install mariadb
podman
$ podman exec -ti client01 mysql -u user -ppass -h db01
```



```
练习P347
```



## 四 作为系统服务来管理容器



### 培训目标

将容器配置为systemd 服务，并将容器服务配置为在系统启动时启动。



### 1 使用systemd单元管理小型容器环境

您可以运行容器来完成系统任务，或获取一系列命令的输出。您可能还希望运行无限期运行服务的容器，如Web 服务器或数据库。在传统环境中，特权用户通常将这些服务配置为在系统启动时运行，并使用 systemctl 命令进行管理。

作为普通用户，您可以创建 systemd 单元来配置您的 Rootless 容器。利用此配置，您可以通过systemctl命令将容器作为常规系统服务进行管理。

基于systemd单元管理容器主要用于不需要扩展的基本和小型部署。对于许多基于容器的应用和服务的更复杂扩展和编排，可以使用基于 Kubernetes的企业编排平台，如红帽OpenShift容器平台。



### **1.1** **为探讨本课中的主题，使用以下场景测试**

作为系统管理员，您被委任了一项任务，将基于nginx 器镜像的nginx容器配置为在系统启动时启动。您还必须为Web服务器内容挂载/home/appdev-adm/nginx_web目录，在目录中制作内容为 “nginx_web_page”的索引页面，挂载至容器的nginx发布目录/usr/share/nginx/html，并将8080端口从本地计算机映射到容器80。将容器配置为通过systemctl命令来启动和停止。

```
ssh student@localhost
mkdir /home/student/nginx_web
echo nginx_web_page > /home/student/nginx_web/index.html
podman run -d --name nginx -v /home/student/nginx_web:/usr/share/nginx/html -p 8080:80 nginx   
```



### 2 systemd用户服务要求

作为普通用户，您可以使用 systemctl 命令来启用服务。该服务在您打开会话(图形界面、文本控制台或SSH)时启动，并在您关闭最后一个会话时停止。此行为与系统服务有所不同，系统服务是在系统启动时启动并在系统关机时停止。

默认情况下，当您使用useradd 命令创建用户帐户时，系统将使用普通用户 ID 范围中的下一个可用ID。系统还在/etc/subuid文件中为用户的容器保留一系列ID。如果您使用useradd命令--system选项创建用户帐户，则系统不会为用户容器保留范围。因此，无法使用系统帐户启动Rootless容器。

您决定创建一个专门的用户帐户来管理容器。使用useradd 命令创建appdev-adm用户，并将redhat用作密码。

```bash
$ sudo useradd appdev-adm
[sudo] password for student:student
$ sudo passwd appdev-adm
Changing password for user appdev-adm.
New password:redhat
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:redhart
passwd: all authentication tokens updated successfully.
```

然后，使用 su命令切换到appdev-adm 用户，并使用podman 命令来启动。

```bash
$ su appdev-adm
Password:redhat
$ podman info
ERRO[0000] XDG RUNTIME DIR directory "/run/user/1000" is not owned by the current
user
```

Podman是一款无状态实用程序，需要完整的登录会话。Podman 必须在SSH会话中使用，不能在
sudo或su shell中使用。因此，您将退出 su  shell，并通过 SSH登录计算机。

无状态应用:Stateless Application 是指并不会在会话中保存下次会话中去要的客户端数据。 每一个会话都像首次执行一样,不会依赖之前的数据进行响应。

```bash
$ exit
$ ssh appdev-adm@localhost
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
appdev-adm@localhost's password:redhat

$ podman info
```

然后，配置容器注册表并使用您的凭据进行身份验证。您可以使用以下命令运行 http 容器。

```bash
[appdev-adm@serverb ~]$ mkdir ~/nginx_web
[appdev-adm@serverb ~]$ echo "nginx_web_page" > ~/nginx_web/index.html
[appdev-adm@serverb ~]$ podman run -d --name nginx -p 8080:80 -v /home/appdev-adm/nginx_web/:/usr/share/nginx/html/:Z registry.lab.example.com/library/nginx
```



### 3 为容器创建systemd用户文件

您可以在~/.confiq/svstemd/user/目录中手动定义systemd服务。用户服务的文件语法与系统服务文件的相同。有关更多详细信息，请查看systemd.unit(5)和systemd.service(5)man 手册。

使用podman generate systemd 命令为现有容器生成systemd 服务文件podman generate systemd 命令使用容器作为模型来创建配置文件。

podman generate systemd 命令--new选项指示podman 实用程序对systemd 服务进行配置，以便在该服务启动时创建容器并在该服务停止时删除容器。

您可以使用podman generate systemd 命令和--name 选项来显示为nginx容器建模的systemd 服务文件。

```bash
*$ MANWIDTH=160  man systemd.unit | grep config.*user
 ~/.config/systemd/user/*
*$ mkdir -p ~/.config/systemd/user/

*$ cd ~/.config/systemd/user/
$ podman generate systemd -n nginx
ExecStart=/usr/bin/podman start nginx
ExecStop=/usr/bin/podman stop -t 10 nginx

*$ podman generate systemd -n nginx -f
```

- 启动时，systemd守护进程执行 podman start 命令来启动现有容器
- 停止时，systemd守护进程执行 podman stop 命令来停止容器。请注意，systemd守护进程不会删除该容器。

然后，使用上一命令并加上--new选项来比较systemd 配置。

```bash
$ podman generate systemd -n nginx --new

ExecStartPre=/bin/rm -f %t/%n.ctr-id
ExecStart=/usr/bin/podman run --cidfile=%t/%n.ctr-id --cgroups=no-conmon --rm --sdnotify=conmon --replace -d --name nginx -p 8080:80 -v /home/appdev-adm/nginx_web/:/usr/share/nginx/html/:Z registry.lab.example.com/library/nginx
ExecStop=/usr/bin/podman stop --ignore --cidfile=%t/%n.ctr-id
ExecStopPost=/usr/bin/podman rm -f --ignore --cidfile=%t/%n.ctr-id

```

- 启动时，systemd守护进程执行 podman run 命令以创建并启动新容器。此操作使用podman run 命令--rm选项，它将在停止时删除容器。
- 停止时，systemd 执行 podman stop 命令以停止容器。
- 在systemd停止容器后，systemd 将使用podman rm -f命令将其移除。

验证 podman generate systemd 命令的输出，并使用--files 选项运行上一命令，以在当前目录中创建systemd用户文件。由于nginx容器使用持久存储，因此您选择使用带有--new选项的podman generate systemd命令。然后，创建~/config/systemd/user/目录并将文件移到此位置上。

```bash
$ podman ps -a
CONTAINER ID  IMAGE                                          COMMAND               CREATED        STATUS            PORTS                 NAMES
bf3b986d2490  registry.lab.example.com/library/nginx:latest  nginx......

$ cd ~/.config/systemd/user/
$ podman generate systemd -n nginx --new -f
/home/appdev-adm/.config/systemd/user/container-nginx.service

$ podman stop nginx 
$ systemctl --user enable --now container-nginx.service

$ podman ps -a
CONTAINER ID  IMAGE                                          COMMAND               CREATED       STATUS           PORTS                 NAMES
3eef13c2c88d  registry.lab.example.com/library/nginx:latest  nginx ....
```



### 4  为容器管理systemd用户文件

现在，您已创建了 systemd 用户文件，可以使用 systemctl命令 --user 选项来管理nginx容器。
首先，重新加载systemd守护进程，使systemctl命令知道新的用户文件。使用systemctl--user start命令启动nginx容器。使用为容器生成的 systemd用户文件的名称。

```bash
$ podman stop nginx 
$ systemctl --user enable --now container-nginx.service
$ systemctl --user status container-nginx.service
$ systemctl --user stop container-nginx.service

#建议重启验证容器是否可以 开机自启动
```

下表总结了 systemd 系统和用户服务之间使用的不同目录和命令。



| 比较系统和用户服务     |          |                                     |
| ---------------------- | -------- | ----------------------------------- |
| 存储自定义单元文件     | 系统服务 | /etc/systemd/system/unit.service    |
|                        | 用户服务 | ~/.config/systemd/user/unit.service |
| 重新加载单元文件       | 系统服务 | # systemctl daemon-reload           |
|                        | 用户服务 | $ systemctl --user daemon-reload    |
| 启动和停止服务         | 系统服务 | # systemctl start UNIT              |
|                        |          | # systemctl stop UNIT               |
|                        | 用户服务 | $ systemctl --user start UNIT       |
|                        |          | $ systemctl --user stop UNIT        |
| 在计算机启动时启动服务 | 系统服务 | # systemctl enable UNIT             |
|                        | 用户服务 | $ loginctl enable-linger            |
|                        |          | $ systemctl --user enable UNIT      |



### 5 将容器配置为在系统引导时启动

此时，systemd服务配置已就绪，可以为给定的用户运行容器。但是，如果用户从系统注销，systemd服务会在特定时间后停止容器。出现此行为的原因是，systemd 服务单元是使用.user 选项创建的，它在用户登录时启动服务，并在用户注销时停止服务。
不过，您可以通过运行loginctl enable-linger 命令来更改此默认行为，并强制已启用的服务在服务器启动时启动，并在服务器关闭期间停止。
您可以使用 loginctl命令将systemd用户服务配置为在所配置服务的最后一个用户会话关闭后保留。然后，使用 loginctl show-user 命令验证配置是否成功。

```bash
$ loginctl show-user appdev-adm
Linger=no
$ loginctl enable-linger
$ loginctl show-user appdev-adm
Linger=yes
```



### 6 以Root 用户身份使用 Systemd 管理容器

您还可以将容器配置为以root身份运行，并使用systemd 服务文件进行管理。这种方法的一个优势是，您可以将这些服务文件配置为像常见systemd 单元文件那样工作，而不是以特定用户身份来运行。

将服务文件设置为 root的过程与前面概述的 Rootless容器过程类似，但以下例外:
	不要创建专门的用户来管理容器。
	服务文件必须在/etc/systemd/system 目录中，而不是在~/config/systemd/user 目录中
	使用systemctl命令管理容器，但不使用 --user 选项。
	不要以root用户身份运行 loginctl enable-linger命令。

有关演示，请参见本节末尾参考资料中所列红帽视频频道中的YouTube视频

```
练习：P358
```



## 五 容器模考练习：

```bash
-RH134
-foundation0
$ for i in classroom utility bastion workstation servera;do rht-vmctl start $i;done
$ ssh workstation


-workstation
$ lab start containers-deploy
$ ssh servera

#第一题
-servera
$ podman login registry.lab.example.com -u admin -p redhat321 #应提示登录成功

$ sudo vim ~/Containerfile 
FROM registry.lab.example.com/ubi9-beta/ubi:latest
RUN mkdir /dir{1,2}
RUN echo -e '[rhel-9.0-for-x86_64-baseos-rpms]\nbaseurl = http://content.example.com/rhel9.0/x86_64/dvd/BaseOS\nenabled = true\ngpgcheck = false\nname = Red Hat Enterprise Linux 9.0 BaseOS (dvd)\n[rhel-9.0-for-x86_64-appstream-rpms]\nbaseurl = http://content.example.com/rhel9.0/x86_64/dvd/AppStream\nenabled = true\ngpgcheck = false\nname = Red Hat Enterprise Linux 9.0 Appstream (dvd)'>/etc/yum.repos.d/rhel_dvd.repo
RUN yum install --disablerepo=* --enablerepo=rhel-9.0-for-x86_64-baseos-rpms --enablerepo=rhel-9.0-for-x86_64-appstream-rpms -y python3
CMD ["/bin/bash", "-c", "sleep infinity"]

*$ podman build -t pdf .
$ podman images

#继续做第二题
$ sudo -i
student
# mkdir /opt/{file,progress}
# chown -R student /opt 
# ll /opt
# exit
$ podman run \
    -d \
    --name ascii2pdf \
    -v /opt/file:/dir1:Z \
    -v /opt/progress:/dir2:Z \
    pdf

$ podman stop ascii2pdf

$ loginctl enable-linger
$ loginctl show-user wallah

$ MANWIDTH=160; man systemd.unit | grep config.*user
...
       `│$HOME/.config/systemd/user`                      │...

*$ mkdir -p ~/.config/systemd/user/
*$ cd ~/.config/systemd/user/
*$ podman generate systemd -n ascii2pdf -f
*$ systemctl --user enable --now container-ascii2pdf
$ systemctl --user status container-logserver
```

```bash
$ <Ctrl-D>
# cp /etc/fstab /opt/file
# ls /opt/progress
# file /opt/progress/fstab
```



# 附加：

## ACL访问控制列表

~~~bash
# 

```
firewalld
selinux
文件系统权限； rwx 
软件权限


文件系统权限； rwx 
特殊权限：
acl权限：
隐藏权限：
```

## 1 ACL概念

```bash
给单个或多个用户或组针对文件/目录设置权限。

acl可以针对以下对象设置权限：
user（用户）
group（用户组）
mask(使用环境很少)
other(使用环境很少)
```

## 2 查看文件是否设置了acl

```bash
示例：
[root@servera /]# ll os.current 
-rwxrwxr-x+ 1 root root 246 Mar  8 10:14 os.current    #有+加号，证明设置了facl权限

提示：
#xfs ext3 ext4默认支持ACL
```

### 示例：

```bash
[root@servera opt]# touch file{1..3}
[root@servera opt]# ls
file1  file2  file3
[root@servera opt]# ll 
total 0
-rw-r--r--. 1 root root 0 Dec 11 03:51 file1
-rw-r--r--. 1 root root 0 Dec 11 03:51 file2
-rw-r--r--. 1 root root 0 Dec 11 03:51 file3
[root@servera opt]#  setfacl -m u:student:r file1   
[root@servera opt]# ll 
total 0
-rw-r--r--+ 1 root root 0 Dec 11 03:51 file1
-rw-r--r--. 1 root root 0 Dec 11 03:51 file2
-rw-r--r--. 1 root root 0 Dec 11 03:51 file3
[root@servera opt]# getfacl file1
# file: file1
# owner: root
# group: root
user::rw-
user:student:r--
group::r--
mask::r--
other::r--

```

### 练习：

```bash
语法：
setfacl [选项] ugmo:user/group:权限 文件名
#ugmo：u 所有者权限（针对用户）  g 所属组权限（针对用户组）  m mask掩码  o 其他人权限    前两个常用，如果针对单个用户一般用u，如果针对组就用g
#user/group  ：如果针对用户设置权限，就要写用户名。针对组设置acl权限就写组名
#权限： 字符或数字表示法来书写权限。rwx --- -  7 0

选项
-m 设置acl权限   setfacl -m u:studnet:rwx file1
-x 删除某条acl	  setfacl -x u:student file1
-b 删除所有      setfacl -b file1
-R 递归（如果给目录设置权限也会递归传递给子目录及文件）         setfacl -Rm u:studnet:rwx file1  


getfacl
-R 递归         getfacl file1

-m 设置
[root@servera /]# touch file{1..3}
[root@servera /]# man setfacl | grep lisa  #搜索例子 
[root@servera /]# setfacl -m u:harry:rwx file1
[root@servera /]# getfacl  file1   #查看
# file: file1
# owner: root
# group: root
user::rwx
user:harry:rwx
group::r-x
mask::rwx
other::r-x

设置acl组权限
[root@servera /]# grep wheel /etc/group
[root@servera /]# setfacl -m g:wheel:rwx /opt/file1
[root@servera opt]# id student
uid=1000(student) gid=1000(student) groups=1000(student),10(wheel)
[root@servera opt]# su - student
[student@servera opt]# vim /opt/file1   应该可以编辑保存文件

了解mask值
[root@servera /]# setfacl -m m::rw- acltest.txt 
[root@servera /]# getfacl acltest.txt 
# file: acltest.txt
# owner: root
# group: root
user::rw-
group::r--
group:east:rwx			#effective:rw-  #user和group权限只有和mask权限重叠时才生效
mask::rw-
other::r--

设置原本组权限：
[root@servera opt]# setfacl -m g::rw acl.txt 

-x
[root@servera /]# setfacl -x g:east acltest.txt 
[root@servera /]# setfacl -x m: acltest.txt    #使用该条命令删除mask值时，不能存在其他用户
[root@servera /]# setfacl -x u:harry os.current 
[root@servera /]# getfacl os.current 
# file: os.current
# owner: root
# group: root
user::rwx
user:lisa:---
group::r-x
mask::r-x
other::r-x

-b
[root@servera /]# setfacl -b os.current


-R ，-Rb
[root@servera opt]# mkdir acldir
[root@servera opt]# touch acldir/acl.txt
[root@servera opt]# ls acldir/
acl.txt
[root@servera opt]# setfacl -Rm u:harry:rwx acldir/
[root@servera opt]# getfacl -R acldir/
# file: acldir/
# owner: root
# group: root
user::rwx
user:harry:rwx
group::r-x
mask::rwx
other::r-x

# file: acldir//acl.txt
# owner: root
# group: root
user::rw-
user:harry:rwx
group::r--
mask::rwx
other::r--

[root@servera opt]# setfacl -Rb acldir/
[root@servera opt]# getfacl -R acldir/
# file: acldir/
# owner: root
# group: root
user::rwx
group::r-x
other::r-x

# file: acldir//acl.txt
# owner: root
# group: root
user::rw-
group::r--
other::r--
~~~



## 实施高级存储功能

### 1 stratis(了解)

~~~bash


### 1.1 安装

```bash
# yum install -y stratis-cli stratisd
# systemctl enable --now stratisd
```

### 1.2 部署存储池

```bash
1.为磁盘分区，模拟块设备（磁盘）
man stratis，搜索examples
[root@servera ~]# fdisk -l /dev/vdb
Disk /dev/vdb: 5 GiB, 5368709120 bytes, 10485760 sectors
Device     Boot   Start     End Sectors Size Id Type
/dev/vdb1          2048 2099199 2097152   1G 83 Linux    #分两个区每个1G
/dev/vdb2       2099200 4196351 2097152   1G 83 Linux              

[root@servera ~]# stratis pool create pool1 /dev/vdb1    #创建池名为pool1
[root@servera ~]# stratis pool list    #列出池信息包括大小
[root@servera ~]# stratis blockdev list  #列出池有哪些物理块设备组成
[root@servera ~]# stratis pool add-data pool1 /dev/vdb2  #添加额外存储
[root@servera ~]# stratis pool list
Name     Total Physical Size  Total Physical Used
pool1                  2 GiB               56 MiB
[root@servera ~]# stratis blockdev list 
Pool Name  Device Node    Physical Size   State  Tier
pool1      /dev/vdb1              1 GiB  In-use  Data
pool1      /dev/vdb2              1 GiB  In-use  Data
```

### 1.3 创建文件系统

```bash
[root@servera ~]# man stratis   #搜/example
[root@servera ~]# stratis filesystem create pool1 filesystem1   #在pool1中创建文件系统filesystem1
[root@servera ~]# 
[root@servera ~]# stratis filesystem list  #列出文件系统信息
Pool Name  Name         Used     Created            Device                      UUID                         
pool1      filesystem1  546 MiB  Mar 16 2020 21:47  /stratis/pool1/filesystem1  6d1ed6e714a6428eb374549b4fdd8d2b  
[root@servera ~]# mkdir /mnt/stratisvol   #创建挂载点
如何查看uuid
[root@servera ~]# lsblk --output=UUID  /stratis/pool1/filesystem1
[root@servera ~]# mount /stratis/pool1/filesystem1 /mnt/stratisvol/   #临时挂载
```

### 1.4 备份

```bash
创建测试文件
[root@servera /]# cd /mnt/stratisvol/
[root@servera stratisvol]# dd if=/dev/zero of=myfile bs=1M count=100  #该命令意义是在挂载点中创建一个文件，也可以使用touch来替代该功能
[root@servera stratisvol]# du -sh myfile 
备份
[root@servera /]# stratis filesystem snapshot pool1 filesystem1 filesystembak1  #创建快照
模拟故障
[root@servera /]# cd /mnt/stratisvol/    #进入挂载点，并查看有数据
[root@servera stratisvol]# ls
file1  myfile
[root@servera stratisvol]# rm -f *       #删除所有数据模拟损坏故障
[root@servera stratisvol]# cd /
[root@servera /]# umount /mnt/stratisvol  #卸载挂载点
恢复
[root@servera /]# mkdir /mnt/stratisvolbak  #挂载备份设备
[root@servera /]# mount /stratis/pool1/filesystembak1 /mnt/stratisvolbak
[root@servera /]# ls /mnt/stratisvolbak
```

### 1.5 开机自动挂载

```bash
vim /etc/fstab
/stratis/pool1/filesystembak1 /mnt/stratisvolbak/ xfs _netdev 0 0 #_netdev 延迟挂载，先连接网络再挂载文件系统
mount -a

[root@clear ~]# systemctl start stratis
[root@clear ~]# stratis pool create mypool /dev/vdb1
[root@clear ~]# stratis pool add-cache mypool /dev/vdb2
[root@clear ~]# stratis blockdev list mypool 
Pool Name  Device Node  Physical Size   Tier
mypool     /dev/vdb1            1 GiB   Data
mypool     /dev/vdb2            1 GiB  Cache
``
~~~

### 2 VDO

~~~bash
### 1.1安装

```bash
该实验需要在普通环境上做，恢复init快照，默认是294，要切换至rh134课程
切换方法：
【foundation】
[kiosk@foundation0 ~]$ rht-clearcourse 0
[kiosk@foundation0 ~]$ rht-setcourse rh134
[kiosk@foundation0 ~]$ cat /etc/rht | grep RHT_COURSE   
RHT_COURSE=rh134		#必须保证课程是rh134才可以
[kiosk@foundation0 ~]$ for i in classroom bastion workstation servera;do rht-vmctl start $i;done  #开这4台虚拟机，打开时稍等3分钟左右，让其保证都开启，然后ping一下测试连通性
[kiosk@foundation0 ~]$ for i in classroom bastion workstation servera;do ping -c 4  $i;done  
结果都是以下结果即可，应出现icmp_seq=1 ttl=64 time=1.13 ms字样：
PING bastion.lab.example.com (172.25.250.254) 56(84) bytes of data.
64 bytes from bastion.lab.example.com (172.25.250.254): icmp_seq=1 ttl=64 time=1.13 ms
64 bytes from bastion.lab.example.com (172.25.250.254): icmp_seq=2 ttl=64 time=0.180 ms
64 bytes from bastion.lab.example.com (172.25.250.254): icmp_seq=3 ttl=64 time=0.234 ms
64 bytes from bastion.lab.example.com (172.25.250.254): icmp_seq=4 ttl=64 time=0.265 m

【foundation】
ssh student@workstation
【workstation】
lab advstorage-vdo start   #运行脚本，运行前，保证servera已打开
ssh student@servera
【servera】
[root@servera ~]# sudo -i
[root@servera ~]# yum search vdo
[root@servera ~]# yum install -y vdo.x86_64 kmod-kvdo.x86_64
[root@servera ~]# systemctl enable --now vdo
```

### 1.2 部署vdo

```bash
[root@servera ~]# man vdo   #/EXAMPLE
*[root@servera ~]# vdo create --name=vdo1 --device=/dev/vdd --vdoLogicalSize=50G
[root@servera ~]# vdo list
[root@servera ~]# vdo status --name=vdo1
[root@servera ~]# vdo status --name=vdo1 | grep Dedu
[root@servera ~]# vdo status --name=vdo1 | grep Comp
```

### 1.3 格式化及临时挂载测试

```bash
格式化
*[root@servera ~]#  mkfs.xfs -K /dev/mapper/vdo1      #-K让命令可以快速返回，效果类似快速格式化 ，如果已有文件系统 可以使用 -f强制执行 

挂载
*[root@servera ~]# mkdir /mnt/vdo1
[root@servera ~]# mount /dev/mapper/vdo1 /mnt/vdo1
[root@servera ~]# df -Th
[root@servera ~]# vdostats --human-readable
[root@servera ~]# cp /root/install.img /mnt/vdo1/install.img.1
[root@servera ~]# vdostats --human-readable
[root@servera ~]# vdostats --human-readable
[root@servera ~]# cp /root/install.img /mnt/vdo1/install.img.2
[root@servera ~]# vdostats --human-readable vdostats --human-readable
ls /mnt/vdo1/
lab advstorage-vdo finish
```

### 1.4 开机自动挂载

```bash
开机自动挂载方法：教材推荐
*[root@servera ~]# man vdo- | grep x-systemd
/dev/mapper/vdo1 /mnt/vdo1 xfs defaults,x-systemd.requires=vdo.service 0 0

mount -a

方法二：
/dev/mapper/vdo1 /mnt/vdo1 xfs defaults,_netdev 0 0
```
~~~





















































































































































































































































 
