[toc]

## Introduction

苏振 su

[认证](https://www.redhat.com/zh/services/training/all-courses-exams)

- RHCI-Instructor 讲师

- RHCA-高级

  |                            Course                            |                       Content                       | EXAM                                                         |
  | :----------------------------------------------------------: | :-------------------------------------------------: | ------------------------------------------------------------ |
  | [CL210](https://www.redhat.com/zh/services/training/cl210-red-hat-openstack-administration) | 红帽 OpenStack 管理二：云管理员应了解的基础架构配置 | [EX210](https://www.redhat.com/zh/services/training/ex210-red-hat-certified-system-administrator-red-hat-openstack-exam) |
  | [CL260](https://www.redhat.com/zh/services/training/cloud-storage-red-hat-ceph-storage-cl260) |             利用红帽 Ceph 存储的云存储              | [EX260](https://www.redhat.com/zh/services/training/ex260-red-hat-certified-specialist-in-ceph-cloud-storage-exam) |
  | [DO280](https://www.redhat.com/zh/services/training/do280-red-hat-openshift-administration-i) |  红帽 OpenShift 管理二：操作生产性 Kubernetes 集群  | [EX280](https://www.redhat.com/zh/services/training/red-hat-certified-openshift-administrator-exam) |
  | [RH358](https://www.redhat.com/zh/services/training/rh358-red-hat-services-management-automation) |                红帽服务管理与自动化                 | [EX358](https://www.redhat.com/zh/services/training/ex358-red-hat-certified-specialist-services-management-automation-exam) |
  | [DO374](https://www.redhat.com/zh/services/training/do374-developing-advanced-automation-red-hat-ansible-automation-platform) |      利用红帽 Ansible 自动化平台开发高级自动化      | [EX374](https://www.redhat.com/zh/services/training/red-hat-certified-specialist-developing-automation-ansible-automation-platform-exam) |

- RHCE-中级 系统管理 III

  |                            Course                            |                           Content                            |                             EXAM                             |
  | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
  | [RH294](https://www.redhat.com/zh/services/training/rh294-red-hat-system-administration-iii-linux-automation) | ansible [core 2.13.0]<br>ansible-automation-platform-22/ee-supported-rhel8 | [EX294](https://www.redhat.com/zh/services/training/ex294-red-hat-certified-engineer-rhce-exam-red-hat-enterprise-linux-9?section=%E8%80%83%E8%AF%95%E7%9B%AE%E6%A0%87) |
  
- RHCSA-初级  系统管理I, II

  |                            Course                            | Content |                             EXAM                             |
  | :----------------------------------------------------------: | :-----: | :----------------------------------------------------------: |
  | [RH134](https://www.redhat.com/zh/services/training/rh134-red-hat-system-administration-ii) | system  | [EX200](https://www.redhat.com/zh/services/training/ex200-red-hat-certified-system-administrator-rhcsa-exam) |
  | [RH124](https://www.redhat.com/zh/services/training/rh124-red-hat-system-administration-i) |  base   | [EX200](https://www.redhat.com/zh/services/training/ex200-red-hat-certified-system-administrator-rhcsa-exam) |
### 环境做准备
|  ID  | REQUIREMENTS                                                 |
| :--: | ------------------------------------------------------------ |
| 硬件 | * at least **16** GB of RAM<br>* at least **4** CPUs (VT-x)<br>* at least **250** GB of local storage |
| 软件 | OS ARCH：**x86_64<br>**APP：[VMware](https://www.vmware.com/cn.html) [ workstation \| fustion \| player ] |
| 文件 | FOLDER: RH294-RHEL9.vmwarevm/*.vmx                           |

<div style="background: #dbfaf4; padding: 12px; line-height: 24px; margin-bottom: 24px;">
<dt style="background: #1abc9c; padding: 6px 12px; font-weight: bold; display: block; color: #fff; margin: -12px; margin-bottom: -12px; margin-bottom: 12px;" >Hint - 提示</dt>
  解压缩<a href="https://www.7-zip.org/">7z</a>/windows、<a href="https://www.keka.io/zh-cn">keka</a>/macos
</div>

### 线下环境

**[kiosk@foundation]**

```bash
当前课程
$ cat /etc/rht
RHT_COURSE=rh294
...输出省略...

查看当前可以切换到哪些环境
$ ssh root@localhost rht-usb f0list
INFO     Configuration file: /root/.icrm/config.yml
Active course  : RH294-RHEL9.0-1.r202302150940-ILT+RAV-7-en_US.icmf
Infrastructure : RHCIfoundation-RHEL85-7.r2023030714-ILT-7-en_US.icmf
Quiesced course: RH124-RHEL9.0-5.r2023051908-ILT+RAV-7-en_US.icmf_quiesced
Quiesced course: RH134-RHEL9.0-5.r2023051908-ILT+RAV-7-en_US.icmf_quiesced
INFO     Listing deployed manifests: /content/manifests
INFO     Found 4 manifests deployed.
INFO     f0list completed.

删除当前环境配置
$ rht-clearcourse 0

切换课程环境至rh124
$ rht-setcourse rh124
$ cat /etc/rht
RHT_COURSE=rh124
...输出省略...
```

| MACHINE |     VM      |                        | 必须启动 |  |
| :-----: | :---------: | :--------------------: | :------: | :-----: |
| VMware  | foundation  |          平台 - GUI          |    \*    | 0 |
|   KVM   |  classroom  |  dns, yum repo, **rc.local**...  |    \*    | 1 |
|   KVM   |   bastion   | Gateway system(router) |    \*    | 2 |
| KVM | utility | podman Server || 3 |
|   KVM   | workstation |      Client - GUI      |          | 4 |
|   KVM   |   servera   |      Client - CLI      |    \*    | 5 |
|   KVM   | server{b-e} |      Client - CLI      |          | 6 |



### 环境配置

- 关闭VMware dhcp：

  - `编辑`菜单/`虚拟网络编辑器...`/`vmnet1`

- 舒服：`查看`菜单
  - <kbd>F9</kbd> 隐藏库
  - `拉伸客户机`
  - 自定义 / `选项卡`

- 强烈建议关闭`360`或`安全管家`

  

### 技巧
| STEP |                             |                           COMMENT                            | EXAMPLE                                                 |
| :--: | :-------------------------: | :----------------------------------------------------------: | :------------------------------------------------------ |
|  1   |           `word`            |               单词，记头 2-3 个字母 (翻译软件)               | user                                                    |
|  2   |       <kbd>Tab</kbd>        |                  列出（两下）、补全（一下）                  | user<kbd>Tab</kbd><kbd>Tab</kbd><br>usera<kbd>Tab</kbd> |
|  3   | `man CMD` <br> CMD `--help` | 查帮助，<kbd>q</kbd>退出<br><kbd>/</kbd>word<kbd>Enter</kbd>,<kbd>n</kbd>\|<kbd>N</kbd> | man find                                                |
|  4   |          `echo $?`          |                  ==0（TRUE）, find 命令除外                  |                                                         |

| VM 虚拟机名称    | 功能                             | 是否必须 | 普通用户：密码  | 超级用户密码 |
| ---------------- | -------------------------------- | -------- | --------------- | ------------ |
| foundation0      | 宿主机                           | 1        | kiosk：redhat   | root：Asimov |
| classroom        | 分配dhcp、dns、软件              | 1        |                 | root：Asimov |
| utility          | 容器镜像服务器                   | 1        | student:student | root:redhat  |
| bastion          | 网关设备，连接其他机器的网络     | 1        | student:student | root:redhat  |
| workstation      | 提供图形界面连接其他机器：如abcd | 0        | student:student | root:redhat  |
| servera、b、c、d | 练习机，nic/mac+IP+hostname      | 1        | student:student | root:redhat  |

### 虚拟机控制命令

<kbd>Win</kbd> / `Terminal`

​		<kbd>Win</kbd>-<kbd>:arrow_up:</kbd>
​		<kbd>Ctrl</kbd>-<kbd>+</kbd> | <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>=</kbd>

**[kiosk@foundation0]**

```bash
$ rht-clearcourse 0

$ ssh root@localhost rht-usb f0list
INFO     Configuration file: /root/.icrm/config.yml
INFO     Listing deployed manifests: /content/manifests
INFO     Found 4 manifests deployed.
INFO     f0list completed.
Active course  : RH124-RHEL9.0-5.r2023051908-ILT+RAV-7-en_US.icmf
Infrastructure : RHCIfoundation-RHEL85-7.r2023030714-ILT-7-en_US.icmf
Quiesced course: RH134-RHEL9.0-5.r2023051908-ILT+RAV-7-en_US.icmf_quiesced
Quiesced course: RH294-RHEL9.0-1.r202302150940-ILT+RAV-7-en_US.icmf_quiesced

$ rht-setcourse rh124

$ cat /etc/rht
RHT_VENUE=ilt
RHT_ENROLLMENT=0
RHT_ROLE=foundation
RHT_COURSE=rh124
RHT_TITLE="Red Hat System Administration I"
RHT_VMS="bastion workstation utility servera serverb "
RHT_VM0="classroom "
RHT_GVMS=""
RHT_PRIVUSEOVS=""
RHT_NOSTARTVMS=""
RHT_NOFINISHVMS=""
RHT_VMTREE=rhel9.0/x86_64
```

（红帽培训提供）

```bash
$ rht-vmctl status classroom
$ rht-vmctl start classroom
$ rht-vmctl status classroom

$ rht-vmview view classroom
$ rht-vmctl reset classroom

$ rht-vmctl status all
```

```bash
【fountation0】 #开机后默认登录f0，按顺序开启环境

第一个启动 classroom
$ rht-vmctl start classroom

第二个启动 bastion
$ rht-vmctl start bastion

第三个启动 utility
$ rht-vmctl start utility

第四个启动servera
$ rht-vmctl start servera

KVM LOCAL console
$ rht-vmview view servera

command line
$ ssh student@servera
$ sudo -i    #输入密码student
```
（系统自带）

```bash
虚拟机管理器 - GUI/Graphical User Interface
$ virt-manager

必须切换到 root
$ su -
Password: `Asimov`

本地控制台 - GUI/Graphical User Interface
# virt-viewer classroom

本地控制台 - CLI/Command Line Interface
# virsh console servera
Escape character is `^]`
`<Enter>`
servera login: `root`
Password: `redhat`
# <Ctrl-D>
```

<kbd>Ctrl</kbd>-<kbd>]</kbd> 退出本地控制台

```bash
$ su -
Asimov
# yum -y install langpacks-zh_CN
```

> - [x] 不要再次询问我
>
>   <kbd>保留旧的名称</kbd>

```bash
$ ping classroom
<Ctrl+C>
$ ping -c 4 bastion
$ ping -c 4 servera

ssh REMOTE console（系统自带）
$ ssh root@servera
$ <Ctrl-D> | $ logout | $ exit
```

打开RHEL9-VMware复制功能

```bash
【foundation0】
$ su - root 
Asimov
$ yum -y install open-vm-tools-desktop.x86_64
$ reboot
```



## 1. [红帽企业 Linux 入门](http://foundation0.ilt.example.com/slides/)

- 网络

  ![](https://k8s.ruitong.cn:8080/Redhat/RH124-RHEL9.0-en-5-20230516/images/classroom/RH124-Classroom-Architecture.svg)

- **OS**=GNU(tools)/Linux(kernel)

> - <img align=left width=200 height=200 src="https://gitee.com/suzhen99/redhat/raw/master/images/5882b2b7d0a20cf4f664615276094b36adaf9943.png">linus + x86/DOS | Minix(UNIX) = Linux /[kernel](https://www.kernel.org/)
>
> 
>
> 
>
> - 
> - 
> - 
> - <img align=left width=200 src="https://bkimg.cdn.bcebos.com/pic/1ad5ad6eddc451dab5799d66b6fd5266d11632ab?x-bce-process=image/watermark,image_d2F0ZXIvYmFpa2UyNzI=,g_7,xp_5,yp_5/format,f_auto">stallman ![](https://www.gnu.org/graphics/heckert_gnu.transp.small.png)[GNU](http://www.gnu.org/) = tools



- **redhat** 7.. 9 -=> [fedora](https://getfedora.org/)/[EPEL](https://fedoraproject.org/wiki/EPEL)
- **RHEL** 4, 5, 6 | 7, 8, **9**
- **CentOS** (Software)

> - [阿里巴巴开源镜像站](https://developer.aliyun.com/mirror/)
> - [清华大学开源软件镜像站](https://mirrors.tuna.tsinghua.edu.cn/)
> - [USTC Open Source Software Mirror](http://mirrors.ustc.edu.cn/)
> - [网易开源镜像站](http://mirrors.163.com/)

- **Ubuntu** 个人版本，桌面漂亮（dos2unix）
- **Debian** 操作系统

### Linux软件？生态？

```bash
`rpm` - RPM Package Manager/ RedHat Package Manager
# rpm -ivh ...

# wget ...
# tar -xf ...
# cd ...
# vim README
# yum -y install gcc ...
# ./configure --?
# make
# make install
```



## 2. [访问命令行](http://foundation0.ilt.example.com/slides/)

### 终端切换 RHEL>=7

|      |                   shortcut                   |  comment   |                                               |
| :--: | :------------------------------------------: | :--------: | --------------------------------------------- |
| CLI  | <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>Fx</kbd> | x in (2,6) |                                               |
| GUI  | <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>F1</kbd> |            | <kbd>Alt</kbd>-<kbd>F2</kbd> / gnome-terminal |

### SHELL(bash, dash, zsh, sh)

> Linux严格区分大小写
>
> $ COMMAND --option arg

```bash
$ echo $SHELL

$ cat /etc/shells

查内部命令
$ man bash
<F>/<B>/<Q>

外部命令
$ echo $PATH
$ which passwd
$ whereis passwd
```

### login

|        |                                                              |                                                              |
| :----: | ------------------------------------------------------------ | ------------------------------------------------------------ |
| local  | CLI<br><kbd>Ctl</kbd>-<kbd>Alt</kbd>-<kbd>F2</kbd><br>(ssh, telnet) | GUI<br><kbd>Ctl</kbd>-<kbd>Alt</kbd>-<kbd>F2</kbd><br>(`x`rdp, vnc) |
| remote | ssh root@172.25.250.9                                        | ssh -X student@w<kbd>Tab</kbd>                               |

> - ![](https://www.gnome.org/wp-content/uploads/2020/08/logo-big.png)[gnome3](https://www.gnome.org/) RHEL
> - [kde](https://kde.org/zh-cn/)
> - x-window
> - ![](https://mate-desktop.org/assets/img/icons/mate.png)[mate](https://mate-desktop.org/)麒麟软件

### prompt 前导符

```bash
[student@workstation ~]$
[root@workstation ~]# shutown -h 0
```

> [whoami@hostname pwd]permission

### Locking the Screen

> <kbd>Win</kbd>+<kbd>L</kbd>
>
> <kbd>Ctrl</kbd>+<kbd>D</kbd>  Done`本地文本终端`
>
> 桌面右上角托盘栏，关机按钮，锁屏按钮



### logout 命令行

```bash
$ logout

$ exit
```

<kbd>Ctrl</kbd>-<kbd>D</kbd>	Done

<kbd>Ctrl</kbd>-<kbd>C</kbd>	Cancel



内存不够：
CLI=512M, GUI=1024M

### poweroff, reboot

|      |        关机        |                     重启                      |         |
| :--: | :----------------: | :-------------------------------------------: | :-----: |
|  1   |       init 0       |                    init 6                     |  RHEL   |
|  2   |      poweroff      |                   `reboot`                    |  RHEL   |
|  3   | systemctl poweroff |               systemctl reboot                | RHEL>=7 |
|  4   |   shutdown -h 15   |                shutdown -r 15                 |  RHEL   |
|  5   |      halt -p       | <kbd>Ctrl</kbd>-<kbd>Alt</kbd>-<kbd>Del</kbd> |  RHEL   |

> - 0，关机
> - 1，Single
> - 2，==3（少 nfs） 
>
> - 3 ，CLI
> - 4，NONE
>
> - 5， GUI
> - 6，重启

### syntax

```bash
# cmd [-option] [arg1] [arg2]
```

```bash
# ls
# ls -a
# ls --all
# ls -al
# ls -a -l
# ls /home
# ls -a /home
```

- 回显式命令

```bash
$ date
$ date +%R
$ date +%y%m%d
```

- 交互式命令

```bash
$ passwd
Current password: `redhat`
New password: `P@33w0rd`
Retype new password: `P@33w0rd`
```

### workspace

|      |                           shortcut                           | version |           COMMENT           |
| :--: | :----------------------------------------------------------: | :-----: | :-------------------------: |
|  1   | <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>:arrow_up:</kbd> \| <kbd>:arrow_down:</kbd> | RHEL>=7 |          切换桌面           |
|  2   | <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>:arrow_left:</kbd> \| <kbd>:arrow_right:</kbd> | RHEL<=6 |          切换桌面           |
|  3   | <kbd>Win</kbd>+<kbd>:arrow_left:</kbd> \| <kbd>:arrow_right:</kbd> |         |          窗口分屏           |
|  4   |                 <kbd>Alt</kbd>+<kbd>F2</kbd>                 |         | <kbd>Win</kbd>+<kbd>R</kbd> |
|  5   |                 <kbd>Alt</kbd>+<kbd>F1</kbd>                 |         |       <kbd>Win</kbd>        |



### 查看文件内容

| command |              example               |                          reference                           | Comment |
| :-----: | :--------------------------------: | :----------------------------------------------------------: | :-----: |
|   cat   |    $ cat -n /etc/selinux/config    |                                                              | 内容少  |
|  more   |      # more /var/log/messages      | <kbd>  </kbd>, <kbd>q</kbd>, <kbd>Ctrl</kbd>+<kbd>B</kbd>\|<kbd>Ctrl</kbd>+<kbd>F</kbd> | 很少用  |
|  less   |      # less /var/log/messages      | <kbd>  </kbd>, <kbd>q</kbd>, <kbd>Ctrl</kbd>+<kbd>B</kbd>\|<kbd>Ctrl</kbd>+<kbd>F</kbd>,<br> <kbd>⬆️</kbd>, <kbd>⬇️</kbd> ，<kbd>/</kbd> `word` | 用得多  |
|  head   | # cat -n /var/log/messages \| head |                             -n 2                             |  帮助   |
|  tail   | # cat -n /var/log/messages \| tail |                             -n 3                             |  日志   |
|   vim   |      # vim /var/log/messages       | <kbd>Ctrl</kbd>+<kbd>B</kbd>\|<kbd>Ctrl</kbd>+<kbd>F</kbd>,<br> <kbd>⬆️</kbd>, <kbd>⬇️</kbd> ，<kbd>/</kbd> `word`<br><kbd>:</kbd><kbd>q</kbd><kbd>!</kbd> |  修改   |

### <kbd>Tab</kbd>

功能：一下补全，两下列出

对象：命令、路径、选项

```bash
# pas<Tab><Tab>
# pass<Tab>
# passwd -<Tab><Tab>
# passwd --std<Tab>
# passwd --stdin ki<Tab>
# ls /v<Tab>
# ls /var/l<Tab><Tab>
```

### Continuing a Long Command on Another Line

|  ID  |                                                              | COMMENT                                              |
| :--: | :----------------------------------------------------------: | ---------------------------------------------------- |
|  1   |                 <kbd>Alt</kbd>+<kbd>.</kbd>                  | 自动复制上一条命令，最后一个项目，粘贴到光标所在位置 |
|  2   |                  <kbd>Esc</kbd><kbd>.</kbd>                  | 同上                                                 |
|  3   | <img height=30 src="https://gitee.com/suzhen99/redhat/raw/master/images/mouse-left.png">,  <img height=30 src="https://gitee.com/suzhen99/redhat/raw/master/images/mouse-mid.png"> | 左键双击选择\==复制，中键单击\==粘贴                 |

### 文本缓冲区

```bash
# echo haha

反斜杠代表手动换行
# echo wo men ye ke yi, \
shou dong \
huan hang

分号代表回车。一行可执行多条命令
# echo haha; echo hehe

两个&，代表条件为真执行
# echo haha && echo hehe
两个|，代表条件为假执行
# Echo haha || echo hehe
```

### history

```bash
# history
# history -a 或者 <Ctrl+D>
# ls -a ~/.bash_history 
# echo $HISTSIZE

# <ArrowUp>/<ArrowDown>

*# !cha
*# !num
# <Ctrl+R>keyword
```

### shortcut-terminal

```bash
# sYstemctl restart sshd
```

|      |                 shortcut                 |    word     |           COMMENT            |
| :--: | :--------------------------------------: | :---------: | :--------------------------: |
|  1   |       <kbd>Ctrl</kbd>+<kbd>A</kbd>       |    heAd     |       光标跳到行**首**       |
|  2   |       <kbd>Ctrl</kbd>+<kbd>E</kbd>       |     End     |         光标跳到行尾         |
|  5   | <kbd>Ctrl</kbd>+<kbd>:arrow_left:</kbd>  |    Word     |     光标跳到**前**一个词     |
|  6   | <kbd>Ctrl</kbd>+<kbd>:arrow_right:</kbd> |    Word     |     光标跳到**后**一个词     |
|  3   |       <kbd>Ctrl</kbd>+<kbd>U</kbd>       |    Undo     |    删除光标**前**所有字母    |
|  4   |       <kbd>Ctrl</kbd>+<kbd>K</kbd>       |    Kill     |    删除光标**后**所有字母    |
|  7   |       <kbd>Ctrl</kbd>+<kbd>W</kbd>       |    Word     | 删除光标**前**的单词（一组） |
|  8   |       <kbd>Alt</kbd>+<kbd>D</kbd>        | Delete word |       删除光标后的单词       |
|  9   |   <kbd>Alt</kbd>+<kbd>Backspace</kbd>    |             | 删除光标**前**的单词（一个） |



## 3. [从命令行管理文件](http://foundation0.ilt.example.com/slides/)

### 目录

```bash
$ ls -ld /root /home/kiosk
$ ls -ld /etc/httpd/conf/httpd.conf /var/www
$ ls -ld /dev/nvme0n1p2 /mnt/boot
$ ls -ld /bin /usr/bin /sbin /usr/sbin
$ ls -ld /boot /tmp
$ ls -ld /run /proc
$ ls -l /usr/local
```

|  ID  |      目录      | 说明                                                     |
| :--: | :------------: | -------------------------------------------------------- |
|  1   |     /root      | 用户目录-管理员                                          |
|  1   | /home/username | 用户目录-普通用户                                        |
|  2   |      /etc      | 等等 - 配置文件（系统、服务）                            |
|  2   |      /var      | 参数 - log、ftp、http                                    |
|  3   |      /dev      | 设备                                                     |
|  3   |  /mnt, /media  | 挂载点                                                   |
|  4   |      /bin      | 命令 - 用户                                              |
|  4   |     /sbin      | 命令 - 管理员                                            |
|  5   |     /boot      | 启动 - 系统（kernel, initram, grub2-efi.cfg\|grub2.cfg） |
|  5   |      /tmp      | 临时 - 系统                                              |
|  6   |      /run      | 关机后为空 - 系统 （pid）                                |
|  6   |     /proc      | 关机后为空 - 系统（进程目录, 系统信息）                  |
|  7   |   /usr/local   | 本地相关（编译，手册）                                   |

### 绝对相对路径

> 路径是否以**根**开头

```bash
# cd /
# cd /etc
# cd /var
```

```bash
# cd log
# cd ~			#当前用户家目录
# cd ~kiosk	#kiosk用户家目录
# cd -			#上一次工作目录
# cd .			#当前目录
# cd ..			#上一级工作目录
```

> 一切皆文件。folder, /dev/vda, /dev/vda1
>
> partition = inode信息 + block块/簇（4k）

<div style="background: #dbfaf4; padding: 12px; line-height: 24px; margin-bottom: 24px;">
<dt style="background: #1abc9c; padding: 6px 12px; font-weight: bold; display: block; color: #fff; margin: -12px; margin-bottom: -12px; margin-bottom: 12px;" >Hint - 提示</dt>
make directory， --parents<br>
move, remove， --recursive<br>
copy
</div>

|      | file  |  folder  |                                    |
| :--: | :---: | :------: | :--------------------------------: |
| 创建 | touch | mkdir -p |                                    |
| 拷贝 |  cp   |  cp -r   |                                    |
| 删除 |  rm   |  rm -r   |            # alias<br>             |
| 移动 |  mv   |    mv    | 路径改变<br>注意：目标目录是否存在 |
| 改名 |  mv   |    mv    |              路径不变              |

```bash
# mkdir -p /folder/f1/f2
# ls -d /folder
# touch /folder/file
# ls /folder/file

# cp -r /folder /tmp
# ls -d /tmp/folder
*# cp /folder/file /tmp
# ls /tmp/file

# mv /tmp/folder /home
# ls -d /tmp/folder /home/folder
# mv /tmp/file /home
# ls /home/file
# mv /home/folder /home/wjj
# mv /home/file /home/wj

# rm -r /home/wjj
```
<kbd>y</kbd>*n

```bash
# rm /home/wj
```
<kbd>y</kbd>

> \# alias
>
> alias rm='rm -i'
>
> --interactive

```bash
-f,force
# rm -rf /folder

\转意，不使用别名
# \rm -r /folder
```



### ln

|              |      软链接      |         硬链接         |
| :----------: | :--------------: | :--------------------: |
|     大小     | 路径中字符的个数 | 和原文件一样，共用空间 |
|    节点号    |   和原文件不同   |      和原文件相同      |
| 是同一个文件 |       不是       |           是           |
|     功能     |     快捷方式     |        简单备份        |
|    跨分区    |       可以       |         不可以         |
|     文件     |       可以       |          可以          |
|    文件夹    |       可以       |       **不可以**       |

> link
>
> Partition = inode节点号 + block/空间

```bash
# dd if=/dev/zero of=/home/bigfile bs=1M count=3
# ls -lh /home/bigfile
# man ls
# du -sh /home

# ln /home/bigfile /home/hard
# ls -lh /home/
# du -sh /home
# ls -lhi /home/

# ln -s /home/bigfile /home/soft
# du -sh /home
# ls -lhi /home/

# ll /etc/grub.cfg
# ll /etc/grub2.cfg
```

### 通配符
```bash
# mkdir /folder/
# ls /folder/
# touch /folder/{a..c}{1..3}.txt
# ls /folder/
# ls /folder/*.txt
# ls /folder/?3*
# ls /folder/a*

# ls /folder/?[23]*

# ls /folder/?[^23]*
# ls /folder/?[!23]*

# ls /folder/{a1,b3}*

# touch folder/{A..C}{1..3}.bat
# ls /folder/[a-z]*
# ls /folder/[[:alpha:]]*
# ls /folder/[a-zA-Z]*
# ls /folder/[[:lower:]]*
```

### 命令的替换

```bash
- $(cmd)
# touch /root/$(date +%y%m%d).bk

- `cmd`
# touch /root/`date +%Y%m%d`.bk
```

## 4. [在RHEL中获得帮助](http://foundation0.ilt.example.com/slides/)

### man 

```bash
# man man

--keywords
*# mandb	# 日计划；刚开机未执行，需要手动执行
*# man -k passwd

# man passwd
# man 5 passwd
# man passwd.5

# man shadow 
# man 5 shadow

# man bash
`/^\ {7}if`	man手册中，查找`7个空格开头if`
`/if.*then`	其中`.*`代表任意长度，且存在
```

> <kbd>n</kbd>, <kbd>N</kbd>
> <kbd>q</kbd>

### info

```bash
# info ls
# info passwd
# info ls
```

### pinfo

```bash
# pinfo ls
```

### /usr/share/doc

```bash
# whereis passwd
passwd: /usr/bin/passwd /etc/passwd /usr/share/man/man1/passwd.1.gz /usr/share/man/man5/passwd.5.gz

# grep -r BOOTPROTO /usr/share/doc/
# vim /usr/share/doc/initscripts/sysconfig.txt
/ifcfg

# man -k ifcfg
# man nm-settings-ifcfg-rh
# ls /usr/share/man/man5/nm-settings-ifcfg-rh.5.gz
```

### --help

```bash
# passwd -h

# passwd --help
```

###  [在线帮助](https://access.redhat.com/)



## 5. [创建视图和编辑视图](http://foundation0.ilt.example.com/slides/)

|                |  默认  | 指定 |      |
| :------------: | :----: | :--: | :--: |
| 标准输入设备/0 |  键盘  |  <   |  <<  |
| 标准输出设备/1 | 显示器 | `>`  |  >>  |
| 标准错误输出/2 | 显示器 |  2>  | 2>>  |

### ; && ||

```bash
-没有条件判断，前面执行是否成功，执行后面
# echo haha; echo hehe

-简单的条件判断，前面执行成功了，执行后面
# echo haha && echo hehe

-简单的条件判断，前面执行失败了，执行后面
# Echo haha || echo hehe
```

### PPT图解

```bash
# wc < /etc/fstab 
# ls > 1.txt
# Ls 2> err.txt
# ls > 1.txt >&2
# find / -user student 2> err.txt
# find / -user student 2> /dev/null
# ls > 1.txt 2>&1
# find / -user student > 1.txt 2>&1
```

### >, >> 输出重定向

```bash
# echo haha
# Echo haha

# mkdir /folder
# cd /folder
# ls

生成文件
# echo haha > 1.txt
# echo haha >> 2.txt
# ls
# cat 1.txt 
# cat 2.txt 

一个是更新、覆盖
两个是追加
# echo hehe > 1.txt 
# echo hehe >> 2.txt 
# cat 1.txt 
# cat 2.txt 

1=stand, 2=error, &=1+2
# echo haha > new.txt
# cat new.txt 
# Echo haha > new.txt || echo no
# cat new.txt 

# Echo haha 2> new.txt
# cat new.txt 
# echo zheng &> all.txt
# cat all.txt 
# Echo erro &>> all.txt
# cat all.txt

# find / -user kiosk > true.txt 2>&1

# find / -user kiosk > true.txt 2> err.txt
# cat true.txt 
# cat err.txt 
# find / -user kiosk 2> /dev/null
# du -sh /dev/null

-echo
# echo -e "haha\thehe"
# echo -e "haha\nhehe"
# echo -e "haha\nhehe" > my.txt
# cat my.txt
# echo -e " \033[1;32mINFO:\033[0;39m"
# echo -n 'qian dao fu'
```

### <, << 输入重定向

```bash
< == 0
# mysql -u root -predhat contacts < inventory.dump
```

```bash
# wc
ni hao
wo hao
da jia hao
<Ctrl+D>
# cat 
ni hao
wo hao
<Ctrl+D>
# cat > input.txt
ni hao
wo hao
da jia hao
<Ctrl+D>
# cat input.txt 
# wc < input.txt 

定义终止符
# cat > /etc/new.txt << EOT
ding yi
zhong zhi fu
EOT
# cat new.txt
```

```bash
# yum -y install postfix mailx
# systemctl enable --now postfix

# mail kiosk
bt1
nr1
.

# mail -s bt2 kiosk
nr2
<Ctrl-D>

# mail -s bt1 kiosk < my.txt
# su - kiosk

$ mail
<1>
<2>
<q>
```



### | 管道

> 将前面命令执行结果，传递给后面的命令，再次计算

```bash
# ls
# ls | wc -l

# (echo mima; echo mima) | passwd kiosk
# echo mima | passwd --stdin kiosk

管道可以多次使用
# cat -n /var/log/messages 
# cat -n /var/log/messages | head -n 8
# cat -n /var/log/messages | head -n 8 | tail -n 1

tee 三通保存文件,用于排错
# cat -n /var/log/messages | head -n 8 | tee save.txt | tail -n 1
# cat save.txt 

# <Ctrl+Alt+F2>
foundation0 login: `root`
Password: `mima`
# tty
/dev/tty
# <Ctrl+Alt+F1>
# cat -n /var/log/messages | head -n 8 | tee /dev/tty2 | tail -n 1
# <Ctrl+Alt+F2>

$ echo haha | sudo tee /home/s.txt
[sudo] password for student: `student`
```

### vim 编辑器

> vi, `vim`, nano, emacs, gedit

三个模式：命令模式（默认）、输入模式、末行模式

```bash
# vim chuangjian.txt
1- <i>
ni hao
wo hao
2- <Esc>
3- :wq!
```

|                                            | character                                                    | word                                                         | line                                                         |
| :----------------------------------------: | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|                    :set                    | :set number<br>显示 行号<br>:set cuc<br>显示 光标列          | :set nonumber<br>取消显示行号                                | :set all<br>查看所有设置变量                                 |
|                     Go                     | <kbd>G</kbd>, n<kbd>G</kbd><br>光标跳至尾行，第n行           | <kbd>g</kbd><kbd>g</kbd>, n<kbd>g</kbd><kbd>g</kbd><br>光标跳至首行，第n行 | :`line-numbe`<br>光标跳至第n行                               |
|                   Arrow                    | <kbd>h</kbd><kbd><u>j</u></kbd><kbd>k</kbd><kbd>l</kbd><br>光标移动（方向键） | :arrow_left: :arrow_down: :arrow_up: :arrow_right:           |                                                              |
|                    line                    | <kbd>^</kbd><br>光标跳至行首                                 | <kbd>$</kbd><br>光标跳至行尾                                 |                                                              |
|                    word                    | <kbd>w</kbd><br>光标跳至下一个单词                           | <kbd>b</kbd><br>光标跳至上一个单词                           |                                                              |
|                   delete                   | <kbd>x</kbd> ==  <kbd>d</kbd><kbd>l</kbd><br>删除光标所在字母 | <kbd>d</kbd><kbd>w</kbd>, <kbd>d</kbd><kbd>t</kbd><kbd> </kbd><br>光标删除一个词，光标删除至空格 | <kbd>d</kbd><kbd>d</kbd><br>删除一行                         |
|                    yank                    | <kbd>v</kbd><kbd>y</kbd><br>复制一个字母                     | <kbd>y</kbd><kbd>w</kbd><br>复制一个单词                     | <kbd>y</kbd><kbd>y</kbd><br>复制一行                         |
|                   paste                    | <kbd>P</kbd><br>粘贴至光标前                                 | <kbd>p</kbd><br>粘贴至光标后                                 | 4<kbd>P</kbd><br>粘贴4遍，在光标前                           |
|                    muti                    |                                                              |                                                              | 3<kbd>y</kbd><kbd>y</kbd><br>复制3行                         |
|                                            | :3,5d<br>删除第3行至第5行                                    |                                                              |                                                              |
| <strong style="color: red">VISUAL</strong> | <kbd>v</kbd><br>连续选择                                     | <kbd>Ctrl</kbd>+<kbd>v</kbd>, <kbd>j</kbd>n, <kbd>I</kbd>, #, <kbd>Esc</kbd><br>块选择，方向键下n次，光标行首插入，<br>插入#号，退出输入模式<br><kbd>Ctrl</kbd>+<kbd>v</kbd>, <kbd>j</kbd>n, <kbd>x</kbd><br>块选择，方向键下n次，删除选择字符 | <kbd>V</kbd>选择行，再使用方向键，<kbd>\<</kbd>左缩进对齐，<kbd>></kbd>右缩进对齐 |
|                    Undo                    | <kbd>u</kbd><br>撤消上一步                                   | <kbd>U</kbd><br>撤消当前行所有操作                           | <kbd>:</kbd><kbd>e</kbd><kbd>!</kbd><br>撤消当前文件所有操作 |
|                    Redo                    | <kbd>Ctrl</kbd>+<kbd>R</kbd><br>恢复撤消操作                 |                                                              |                                                              |
|                  Replace                   | <kbd>r</kbd>x<br>替换单个字母为x                             | <kbd>R</kbd> <kbd>Esc</kbd>\|<kbd>:back:</kbd><br>连续替换模式，退出连续替换模式，恢复 |                                                              |
|                   search                   | <kbd>/</kbd>word<br>向下查找word<br><kbd>?</kbd>word<br>向上查找word | <kbd>n</kbd>, <kbd>N</kbd><br>/ - 下一个，上一个<br>? - 上一个，下一个 |                                                              |
|                   write                    | <kbd>:</kbd><kbd>w</kbd><kbd>q</kbd> == <kbd>Z</kbd><kbd>Z</kbd><br>保存退出 | <kbd>:</kbd><kbd>w</kbd><kbd> </kbd> path/file<br>另存为     | <kbd>:</kbd><kbd>w</kbd><kbd>q</kbd><kbd>!</kbd><br>强制保存退出 |
|                    quit                    | <kbd>:</kbd><kbd>q</kbd><kbd>!</kbd><br>强制退出不保存       |                                                              |                                                              |
|                   Change                   | <kbd>C</kbd> == <kbd>c</kbd><kbd>$</kbd>​<br>删除至行尾，输入模式 | <kbd>D</kbd> == <kbd>d</kbd><kbd>\$</kbd><br>删除至行，命令模式 |                                                              |
|                   switch                   | :%s/^/# <kbd>Enter</kbd><br>替换所有行，行首为#              | :3,5s/#//<kbd>Enter</kbd><br>替换第3行至第5行#号为空，相当于删除# | :s/old/new/g<br>替换当前行所有old为new                       |
|                   :help                    | :help usr_08.txt                                             |                                                              |                                                              |
|                                            | :split<br>当前文件上下分屏                                   | :vsplit<br>当前文件左右分屏                                  | <kbd>Ctrl</kbd>-<kbd>w</kbd><br>方向键                       |
|                                            | :nohl<br>取消搜索后的高亮显示                                | /suzhen<br>取消搜索后的高亮显示，搜个不存在的词              |                                                              |
|                                            | :syntax off<br>关闭语法高亮显示                              | :syntax on<br>开启语法高亮显示                               |                                                              |
|                    edit                    | :e /path/file<br>不退出编辑器，关闭当前文件，打开新文件      |                                                              |                                                              |

> Insert, Append, Open
>
> 
>
> OOO
> III12iii<u>3</u>aaa45AAA
> ooo

```bash
末行模式：
:set all
:set tabstop=2
:set autoindent
```



### 变量

```bash
# env
# set
# echo $HISTSIZE
# echo $PS1
# PS1="C:\> "
# PS1='[\u@\h \W]\$ '
# PS1='[\u@\h \w]\$ '

设置局部变量
# NEW_FOLDER=/fx/fy/fz
# echo $NEW_FOLDER
# mkdir -p $NEW_FOLDER
# ls -d /fx
# ls -R /fx

设置局部变量
# NEW_FOLDER="fa fb fc"
# echo $NEW_FOLDER
# mkdir -p $NEW_FOLDER
# ls 
# echo $NEW_FOLDER
取消局部变量 unset
# unset NEW_FOLDER
# echo $NEW_FOLDER

export
设置全局变量
# export NEW_FOLDER='fa fb fc'
# echo $NEW_FOLDER
# bash
# echo $NEW_FOLDER
# exit
# echo $NEW_FOLDER
取消全局变量 export -n
# export -n NEW_FOLDER

用户登陆脚本
profile 内容主要写配置
bashrc 内容主要写脚本命令函数
-all，范围大
# ls /etc/profile /etc/bashrc 
-user，范围小
# ls ~/.bash_profile ~/.bashrc 

RHCSA7.0,RHCA
# psa || echo no
# alias 
# echo "alias psa='ps -aux'" >> ~/.bashrc 
# psa || echo no
# <Ctrl+D> 或者 # source ~/.bashrc
# psa && echo OK
```



## 6. [管理本地用户和组](http://foundation0.ilt.example.com/slides/)

### User - <strong style="color:red">useradd</strong>, usermod, userdel

### 				id, <strong style="color:red">su</strong>, <strong style="color:red">sudo</strong>

### 				<strong style="color:red">passwd</strong>, chage

### Group - <strong style="color:red">groupadd</strong>, groupmod, groupdel

```bash
# id
# id 1000
# id kiosk

su, sudo
- sudo
$ tail /var/log/messages || echo no
$ su -
# grep wheel /etc/sudoers
# visudo 
# usermod -aG wheel kiosk	#附加组增加
# exit

$ id
$ ssh kiosk@localhost
$ id
$ tail /var/log/messages || echo no
$ sudo tail /var/log/messages && echo yes

# usermod -G libvirt kiosk	#附加组是
# visudo
...
kiosk	ALL=(ALL)       /usr/bin/tail -n 2 /var/log/messages
# ssh kiosk@localhost
$ tail /var/log/messages || echo no
$ sudo tail /var/log/messages || echo no
$ sudo tail -n 2 /var/log/messages && echo yes


- su
$ su -
Asimov
# su - kiosk
$ echo $PATH
$ <Ctrl-D>
# echo $PATH
# su kiosk
$ echo $PATH
$ <Ctrl-D>

# grep kiosk /etc/{passwd,shadow}
# chage -l kiosk 

useradd, usermod, userdel
# useradd tom
# grep tom /etc/{passwd,shadow}
# usermod -u 1111 tom
# grep tom /etc/{passwd,shadow}
# usermod -g 1111 tom || echo no

groupadd, groupmod, groupdel
# grep tom /etc/group
# groupadd cat
# grep cat /etc/group
# groupmod -g 1111 cat
# grep cat /etc/group
# groupmod -n mao cat
# grep 1111 /etc/group

# usermod -g 1111 tom
# grep tom /etc/{passwd,shadow}
# usermod -c xiaomao tom
# grep tom /etc/{passwd,shadow}
# usermod -d /home/mao tom
# grep tom /etc/{passwd,shadow}
# ls /home/
# mv /home/tom /home/mao
# ls /home/ -l
# cat /etc/shells 
# usermod -s /bin/sh tom
# grep tom /etc/{passwd,shadow}
# echo mima | passwd --stdin tom
# grep tom /etc/{passwd,shadow}
# usermod -l mao tom
# id mao
# usermod -L mao		# passwd -l mao
# grep mao /etc/{passwd,shadow}
# usermod -U mao		# passwd -u mao
# grep mao /etc/{passwd,shadow}
# userdel mao
# id mao
# ls -l /home/mao

# useradd jerry
# ls /home/ -l
# userdel -r jerry
# id jerry
# ls /home/ -l

# groupdel tom

chage
# chage -l kiosk
设置用户初始登陆必须改密码
# chage -d 0 kiosk
# ssh kiosk@localhost
mima
mima
P@33w0rd
P@33w0rd
# chage -l kiosk
# chage -m 9 -M 30 -W 14 kiosk
# chage -l kiosk
# grep kiosk /etc/shadow

# usermod -s /bin/false kiosk

-gpasswd
gpasswd 
-a： 将用户添加到组，等效于useradd -G；usermod -G
-d： 将用户从组中删除

gpasswd -d harry wheel   将harry从wheel组中删除
gpasswd -a harry wheel   将harry添加到wheel组中
```

|  ID  |    FILE     | COMMENT  |
| :--: | :---------: | :------: |
|  1   | /etc/passwd | account  |
|  2   | /etc/shadow | password |
|  3   | /etc/group  |  group   |

|  ID  | FILE                 |                                                           |
| :--: | -------------------- | --------------------------------------------------------- |
|  1   | /etc/default/useradd | useradd defaults file                                     |
| 1.a  | /etc/skel/           | 模板文件件夹{.bash_logout,.bash_profile,.bashrc,.mozilla} |
|  2   | **/etc/login.defs**  | 创建用户默认的配置                                        |



## 7. [控制文件访问](http://foundation0.ilt.example.com/slides/)

### permission

|      |             |   file   |               folder                |
| :--: | :---------: | :------: | :---------------------------------: |
|  r   |  **R**ead   |   cat    |                 ls                  |
|  w   |  **W**rite  |   vim    |            touch，mkdir             |
|  x   | e**X**ecute | ./script | <font style="color: blue">cd</font> |

### ls -l

```bash
# ls -lh /etc/fstab 
# ll -h /etc/fstab
-rw-r--r--. 1 root root 1.1K Aug  3 22:33 /etc/fstab

看用户对文件有何权限：
先看用户是否是文件所有者，是则使用所有者权限，否则向下匹配
看用户是否是所属组成员，是则使用所属组权限，否则向下匹配
如不匹配以上权限，就属于其他人权限

root
useradd -G root tom
harry
```
> `-` type, `uuugggooo`, `.`|`+` facl, `1` ln, `user` `group`, `size`,`date`, `file`

- **what**
| rwx  | 二进制 |        计算过程         |   7   |
| :--: | :----: | :---------------------: | :---: |
| r--  |  100   | 1*2^2^+0\*2^1^+0\*2^0^= | 4 = r |
| -w-  |  010   | 0*2^2^+1\*2^1^+0\*2^0^= | 2 = w |
| --x  |  001   | 0*2^2^+0\*2^1^+1\*2^0^= | 1 = x |
| ---  |  000   |                         | 0 = - |

### <strong style="color:red">chmod</strong>

who which what
> - **who**
> | `u`  | User  |             用户             |
> | :--: | :---: | :--------------------------: |
> | `g`  | Group |              组              |
> | `o`  | Other | 其他人[ ! user ]\[ ! group ] |
> | `a`  |  All  |         所有人=u+g+o         |

- **which** `+` , `-`, `=`

```bash
# mkdir /folder
# touch /folder/file

-symbolic (letters) 
# ls -ld /folder/{.,*}
# chmod g+w,o=- /folder
# ls -ld /folder/{.,*}
# chmod go=rx /folder/		# chmod g=rx,o=rx /folder/
# ls -ld /folder/{.,*}
# chmod a=- /folder/
# ls -ld /folder/{.,*}
# chmod o+r /folder/
# ls -ld /folder/{.,*}

-numeric (digits)
# chmod 755 /folder/
# ls -ld /folder/{.,*}
# chmod 700 /folder/
# ls -ld /folder/{.,*}

-R
# ls -ld /folder/{.,*}
# chmod -R 755 /folder/
# ls -ld /folder/{.,*}
```

### <strong style="color:red">chown</strong>

```bash
# id root
# id 1
# id 2

# chown bin:daemon /folder/
# ls -ld /folder/{.,*}
# chown kiosk /folder/
# ls -ld /folder/{.,*}

方法一
# chown :root /folder/{.,*}
方法二
# chgrp root /folder/{.,*}
# ls -ld /folder/{.,*}


-R
# chown -R bin /folder
# ls -ld /folder/{.,*}
```



### 特殊权限(x)

|            User             |         Group          |          Other          |  number  |      |
| :-------------------------: | :--------------------: | :---------------------: | :------: | :--: |
|             rwx             |          rwx           |           rwx           |   0777   |      |
|           rw**s**           |        rw**s**         |         rw**t**         | **7**777 | 字母 |
|   **suid=4**<br>**4**777    | **sgid=2**<br>**2**777 | **stick=1**<br>**1**777 |  **7**   | 数值 |
| rw-+suid == rw**X**<br>4677 |                        |                         |          |      |

```bash
suid=执行命令，使用该命令所属用户的身份执行 4
- kiosk root /etc/shadow
# whereis passwd
# ll /usr/bin/passwd /etc/shadow

- root kiosk /tmp/root.mo
# ll /usr/bin/touch
# touch /tmp/root.txt
# ll /tmp/root.txt
# cp /usr/bin/touch /usr/bin/mo
# chown kiosk /usr/bin/mo
1# chmod u+s /usr/bin/mo
2# chmod 4755 /usr/bin/mo
# ll /usr/bin/mo
# mo /tmp/root.mo
# ll /tmp/root.mo

sgid=该目录中新建文件，继承文件夹所属组 2 
# touch /folder/root.txt
# ll /folder/{.,root.txt}
# ll -d /folder/{.,root.txt}
# chown :kiosk /folder/
# ll -d /folder/{.,root.txt}
m1# chmod 2755 /folder/
m2# chmod g+s /folder
# ll -d /folder/{.,root.txt}
# touch /folder/root.new
# ll -d /folder/{.,root.*}

 
stick=只有拥有者和root，可以删除或修改 1
1# chmod o+t folder
2# chmod 1755 folder
# su - kiosk -c 'touch /tmp/k{1..3}.txt'
# ll /tmp/k*.txt
# rm -rf /tmp/k1.txt 
# su - kiosk -c 'rm -rf /tmp/k2.txt'
# ll /tmp/k*.txt
# su - bin -s /bin/bash -c 'rm -rf /tmp/k3.txt' || echo no
# ll /tmp/k*.txt
```

### umask

> - 新创建文件和文件夹时，默认的权限
> - 111 == xxx ( file+x = script; folder+x = cd)

|        |             |      |
| :----: | :---------: | ---- |
| folder | 777 - umask |      |
|  file  | 666 - umask |      |


```bash
# umask
0022
# mkdir wjj; touch wj; ls -ld wj*

# su - kiosk
$ umask
0002
$ mkdir wjj; touch wj; ls -ld wj*
```

```bash
练习：
新创建的文件默认权限-r--r--r--，新创建目录权限为dr-xr-xr-x。只给用户harry设置。

文件默认权限 （字符）  文件默认权限 （字符）     666/777        rw-rw-rw-
uasmk值             文件及目录权限           444/555        r--r--r--
-----------------   -----------------      -------------  -------
文件及目录权限         uasmk值               222/222        -w--w--w-  = 0222 

$ su - harry
$ /home/harry/
$ vim .bashrc
umask 0222  #在最后一行添加
$ source .bashrc
$ umask
$ touch file1;mkdir dir100;ll -d file1 dir100
$ logout
# su - harry
$ umask 
```



## 8. [监控和管理进程](http://foundation0.ilt.example.com/slides/)

|                          |         |                                |                  |
| ------------------------ | ------- | ------------------------------ | ---------------- |
| 分时多任务多用户系统     | Windows | 一个程序未响应，一个程序未响应 | 十字路口，红绿灯 |
| **实时**多任务多用户系统 | Linux   | <Ctrl-Alt-F1><Ctrl-Alt-F2>     | 立交桥           |



### GUI: gnome-system-monitor

### CLI: ps

```bash
# ps -aux
# ps -aux | head -n 2

# pstree
# pstree -p
```

### CLI: top

```bash
# top
```

> <kbd>h</kbd>, <kbd>q</kbd>
<kbd>f</kbd>, <kbd>ArrowDown</kbd>
<kbd>Space</kbd>, <kbd>s</kbd>
<kbd>k</kbd>, <kbd>Enter</kbd>, <kbd>9</kbd>
<kbd>q</kbd>

### kill

```bash
# kill -l

# kill -9 1234
```

### killall

```bash
# killall dhcpd
```

### jobs, bg（background,）, fg（foreground）

<kbd>Ctrl</kbd>-<kbd>z</kbd>, <kbd>&</kbd>

```bash
# jobs
# dd if=/dev/zero of=/dev/null bs=1K
<Ctrl-Z>	# ZombieZ
# jobs

# bg
# jobs

# dd if=/dev/zero of=/dev/null bs=2K &
# jobs

# fg 2
<Ctrl-C>
# kill %1
```

### nice, renice

```bash
# ps -efo pid,command,nice
# renice -n 19 4901
# ps -efo pid,command,nice

# nice -n 10 dd bs=1K if=/dev/zero of=/dev/null &
# ps -efo pid,command,nice

4621 是shell的进程尖
# renice -n -10 4621 
# ps -efo pid,command,nice
# nice -n 8 dd bs=2K if=/dev/zero of=/dev/null &
# ps -efo pid,command,nice
```

###  uptime

```bash
# top
# uptime
# w
```



## 9. [控制服务和守护程序](http://foundation0.ilt.example.com/slides/)

### 9.1 systemd

```bash
# ps -axu | head -n 2
```

```bash
-kernel/RH442
# sysctl -a | grep -i ipv4.*forward
```
```bash
# systemctl help
# systemctl help list-units
# systemctl -t help
# systemctl list-units 
# systemctl list-units -t service
# systemctl list-unit-files | wc -l
# systemctl list-units | wc -l

# systemctl list-unit-files | grep autofs
# systemctl list-unit-files | grep vdo
# systemctl list-unit-files | grep atd
# systemctl list-unit-files | grep crond
```

### 9.2 systemctl

```bash
- status
RHEL>=7# systemctl status sshd
RHEL<=6# service sshd status

- is-enabled, is-active
# systemctl is-enabled sshd
# systemctl is-active sshd

- start, stop
# systemctl is-active sshd
# systemctl stop sshd
# systemctl is-active sshd
# systemctl start sshd
# systemctl is-active sshd

- enable, disable
# systemctl is-enabled sshd
# systemctl disable sshd
# systemctl is-enabled sshd
# systemctl enable sshd
# systemctl is-enabled sshd

- restart(stop && start), reload
# systemctl status sshd
# systemctl restart sshd
# systemctl status sshd
# systemctl reload sshd
# systemctl status sshd

- list-dependencies
# systemctl list-dependencies 
# systemctl list-dependencies | grep target

- mask, unmask
# systemctl mask atd
# systemctl stop atd
# systemctl start atd || echo no
# systemctl status atd
# systemctl unmask atd
# systemctl start atd
# systemctl status atd
```

|         |           RHEL<=6           |                           RHEL>=7                            |
| :-----: | :-------------------------: | :----------------------------------------------------------: |
| restart | service DAEMON **restart**  |                 systemctl **restart** DAEMON                 |
|  stop   |     service DAEMON stop     |                    systemctl stop DAEMON                     |
| status  |  service DAEMON **status**  |                 systemctl **status** DAEMON                  |
| enable  | **chkconfig** DAEMON **on** | systemctl <strong style='color: #A30000'>enable</strong> DAEMON |
| disable |  **chkconfig** DAEMON off   |                   systemctl disable DAEMON                   |



## 10. [OpenSSH服务](http://foundation0.ilt.example.com/slides/)

|      |   Windows   |                           Linux                            |
| :--: | :---------: | :--------------------------------------------------------: |
| CLI  | telnet, ssh |                          **ssh**                           |
| GUI  |  rdp, vnc   | [xrdp](https://github.com/neutrinolabs/xrdp/releases), vnc |

![[OpenSSH]](https://gitee.com/suzhen99/redhat/raw/master/images/openssh.gif)

|  ID  |                      Server                      |   Client   |
| :--: | :----------------------------------------------: | :--------: |
|  1   |         classroom \| servera \| serverb          | Foundation |
|  2   | 172.25.254.254 \| 172.25.250.10 \| 172.25.250.11 |            |

| FILE                   |                  |           CMD           |  ROLE  |
| :--------------------- | :--------------: | :---------------------: | :----: |
| ~/.ssh/known_hosts     | 访问过的主机指纹 |                         | client |
| ~/.ssh/id_rsa          |       私钥       |       ssh-keygen        | client |
| ~/.ssh/id_rsa.pub      |       公钥       | ssh-keygen, ssh-copy-id | client |
| ~/.ssh/authorized_keys |     多条公钥     |                         | server |



### 10.1 ssh

```bash
[foundation]
$ rht-vmctl start classroom 
$ ping -c 4 172.25.254.254
$ mv .ssh .ssh.bk

$ whoami

🚩1
$ ssh 172.25.254.254
yes
<Ctrl-C>
$ cat .ssh/known_hosts

🚩2
$ ssh instructor@172.25.254.254
Asimov
$ <Ctrl-D>

🚩3
$ ssh instructor@172.25.254.254 'mkdir ~/newfolder'
Asimov
$ ssh instructor@172.25.254.254 'ls -l'
Asimov

# ll /etc/ssh/ssh_host_*
```

### 10.2 ssh - keygen, ssh-copy-id

> PreferredAuthentications	{ publickey | password }

```bash
$ ssh-keygen 
Generating public/private rsa key pair.
Enter file in which to save the key (/home/kiosk/.ssh/id_rsa): `<Enter>`
Enter passphrase (empty for no passphrase): `<Enter>`
Enter same passphrase again: `<Enter>`
Your identification has been saved in /home/kiosk/.ssh/id_rsa.
Your public key has been saved in /home/kiosk/.ssh/id_rsa.pub.
...
$ ls .ssh/
$ cat -n .ssh/id_rsa.pub 

$ ssh-copy-id instructor@classroom
instructor@classroom\'s password: `Asimov`
Number of key(s) added: `1`

Now try logging into the machine, with:   "ssh 'instructor@classroom'"
...

$ ssh 'instructor@classroom'
[instructor@classroom ~]$ cat -n .ssh/authorized_keys
[instructor@classroom ~]$ <Ctrl-D>
```

>  -i identity_file

```bash
$ ssh-keygen -f .ssh/id_new -N ''
$ ls ~/.ssh/id*

$ ssh-copy-id -i .ssh/id_new.pub root@classroom
root@classroom\'s password: `Asimov`
...
Number of key(s) added: `1`
...
$ ssh -i .ssh/id_new root@classroom
```



### 10.3 sshd_config - Prohibit the Superuser

**[classroom]**

```bash
# systemctl status sshd
# man sshd_config

# vim /etc/ssh/sshd_config
```
```ini
...
PermitRootLogin no
```
> OPERATION: <br>`/Root`, `w`, <br>`C`, `no`, <kbd>Esc</kbd>,<br> <kbd>Z</kbd><kbd>Z</kbd>
 ```bash
# systemctl list-unit-files | grep ssh

# systemctl restart sshd
# systemctl enable sshd
 ```

**[foudation]**

```bash
$ ssh root@classsroom
root@classroom\'s password: `Asimov`
Permission denied, please try again.
root@classroom\'s password: `Asimov`
Permission denied, please try again.
root@classroom\'s password: `Asimov`
root@classroom: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).

$ ssh instructor@classroom
$ su -
Password: `Asimov`
# 
```

### 10.3 sshd_config - Prohibiting Password

**[classroom]**

```bash
# vim /etc/ssh/sshd_config 
```
```ini
...
PasswordAuthentication no
```
> OPERATION: <br>`/Pass`, `2`<kbd>n</kbd>, <kbd>w</kbd>, <br><kbd>C</kbd>, `no`, <kbd>Esc</kbd>, <br><kbd>:</kbd><kbd>x</kbd>
```bash
# systemctl restart sshd
# systemctl enable sshd

# useradd tom
# echo ttt | passwd --stdin tom
```

> <kbd>Ctrl</kbd>-<kbd>R</kbd>, `re`<kbd>Enter</kbd>
>
> <kbd>Ctrl</kbd>-<kbd>R</kbd>, `en`<kbd>Enter</kbd>

**[foundation]**

```bash
$ ssh tom@classroom
tom@classroom: Permission denied (publickey,gssapi-keyex,gssapi-with-mic).

$ ssh instructor@classroom
$ <Ctrl-D>
```

```bash
课上练习：
servera以root身份远程拷贝/etc/yum.repos.d/* 所有文件至serverb的/etc/yum.repos.d/

【servera】
1. root 登录servera
2. vim /etc/ssh/sshd_config
#PermitRootLogin prohibit-passwd #no选项 就是禁止 root 用户的远程 SSH 登录,而 prohibit-password 则是把 SSH 服务器的认证从密码(password)认证改为私钥(key)认证。
PermitRootLogin yes
PasswordAuthentication yes
3. systemctl restart sshd

【serverb】
1. root 登录serverb
2. vim /etc/ssh/sshd_config
PermitRootLogin yes  #40行
PasswordAuthentication yes   #67行
3. systemctl restart sshd

【servera】
scp  /etc/yum.repos.d/* root@serverb:/etc/yum.repos.d/
```



## 11. [分析和存储日志](http://foundation0.ilt.example.com/slides/)

### 11.1 [rsyslog](http://www.rsyslog.com)

```bash
lab0
# systemctl list-unit-files -t service | grep log
# systemctl status rsyslog.service
# systemctl status syslog.service
# man rsyslogd
# man rsyslog.conf
# vim /etc/rsyslog.conf
# ll /var/log/messages
# vim /var/log/messages

lab1
# vim /etc/ssh/sshd_config
...
PermitRootLogin nn
...
# systemctl restart sshd
# systemctl status sshd
# journalctl -xe
# vim /var/log/messages

# vim /etc/ssh/sshd_config
...
PermitRootLogin no
...
# systemctl restart sshd
# systemctl status sshd
# vim /var/log/messages
```

```bash
# yum search log | egrep 'daemon|server'
# yum info rsyslog

--query
--configure
# rpm -qc rsyslog
# vim /etc/rsyslog.conf
```
```ini
...
#### RULES ####
*.info;mail.none;authpriv.none;cron.none                /var/log/messages
authpriv.*                                              /var/log/secure
mail.*                                                  -/var/log/maillog
cron.*                                                  /var/log/cron
*.emerg                                                 :omusrmsg:*
uucp,news.crit                                          /var/log/spooler
local7.*                                                /var/log/boot.log
```
```bash
# vim /var/log/messages
Aug 21 05:40:54 clear dnf[1217]: There are no enabled repositories in "/etc/yum.repos.d", "/etc/yum/repos.d", "/etc/distro.repos.d".
`时间` `主机名` `进程`: `该进程的消息`

:set number, <G>, <Ctrl-B>, <Ctrl-F>

# ls /etc/yum.repos.d /etc/yum/repos.d /etc/distro.repos.d
# yum repolist 
No repositories available


# ls -l /var/log

-logger
# tail -f -n 1 /var/log/secure

<Ctrl+Shift+T>	# /dev/pts/0
# mandb
# man logger
/-p
/facility
/level
# logger -p authpriv.info today is sat

<Alt+1>					# /dev/pts/1
Aug 21 06:56:19 clear root[24627]: today is sat
<Ctrl-C>

# ls /etc/logrotate.d/syslog
```

### 11.1 messages

```bash
# tail -f /var/log/messages

<Ctrl-Shift-T>
# systemctl restart sshd

<Alt-1>
```

### 11.2 troubleshoot

**[foundation]**

```bash
# systemctl restart httpd
# systemctl status httpd

# rpm -qc httpd
# vim /etc/httpd/conf/httpd.conf 
...
Listen 83
# systemctl restart httpd || echo no
# systemctl status httpd
1# journalctl -xe
2# vim /var/log/messages 
<G>
TIP: `semanage port -a -t PORT_TYPE -p tcp 83`

# man semanage port | grep semanage.*port
# semanage port -l
# semanage port -l | grep -w 80
# semanage port -a -t http_port_t -p tcp 83
# systemctl restart httpd && echo ok
# systemctl status httpd
```

### 11.4 systemd-journald

```bash
# systemctl status systemd-journald.service

# man journalctl

# journalctl 

-e, --pager-end
-x, --catalog
# journalctl -xe

-n, --lines=
# journalctl -n 5

-f, --follow
# journalctl -f
<Ctrl-C>
# tail -f /var/log/messages
<Ctrl-C>

-p, --priority=
# journalctl -p err

# journalctl --since today

# journalctl --since "-1 hour" 

# journalctl \             
--since "2021-11-07 ha ha ha  09:00:00" \
--until "2021-11-07 12:00:00"




_SYSTEMD_UNIT
# journalctl _SYSTEMD_UNIT=sshd.service
# journalctl -u sshd
```

```bash
lab3
# ls /run/log/journal

# systemctl list-unit-files -t service | grep journal
# systemctl status systemd-journald.service
# man journald.conf
# vim /etc/systemd/journald.conf
# journald.conf
# ls /run/log/journal /var/log/journal

*# mkdir /var/log/journal
# ls -ld /run/log/journal
*# chown root:systemd-journal /var/log/journal
*1# chmod 2755 /var/log/journal
*2# chmod u=rwx,g=rxs,o=rx /var/log/journal
# ls -ld /var/log/journal
*# systemctl restart systemd-journald.service 
# systemctl status systemd-journald.service 

# ls /var/log/journal/
```

```bash
方法二：
$ cp -a /run/log/journal/ /var/log/journal/
$ systemctl restart systemd-journald
$ ls /var/log/journal/
```



### 11.5 chrony

- set-timezone

```bash
# timedatectl 
# timedatectl list-timezones | grep -i hong
# timedatectl set-timezone Asia/Hong_Kong
# timedatectl 
```
- set-time

```bash
# timedatectl set-ntp false 
# timedatectl set-time 14:00
# timedatectl 
```

- chrony

**[servera]**

```bash
确认连通性
# ping -c 4 materials.example.com

# vim /etc/chrony.conf
```
```ini
...
server materials.example.com iburst
```
> OPERATION:
>
> 方法1 <kbd>:</kbd><kbd>3</kbd><kbd>,</kbd><kbd>5</kbd><kbd>s</kbd><kbd>/</kbd>`^`<kbd>/</kbd>`#`, 
>
> 方法2 <kbd>j</kbd><kbd>j</kbd>,  <kbd>Ctrl</kbd>+<kbd>V</kbd>, <kbd>j</kbd><kbd>j</kbd>, <kbd>I</kbd>, `#`, <kbd>Esc</kbd>
>
> <kbd>/</kbd><kbd>3</kbd>,  <kbd>c</kbd><kbd>t</kbd><kbd>Space</kbd>, <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>V</kbd>, <kbd>Esc</kbd>, <kbd>Z</kbd><kbd>Z</kbd>
```bash
# systemctl restart chronyd
# systemctl enable chronyd

# timedatectl set-ntp true
1# timedatectl
System clock synchronized: `yes`
              NTP service: `active`
2# chronyc sources -v
`^``*` classroom.example.com
```



## 12. [RHEL网络管理](http://foundation0.ilt.example.com/slides/)

### 12.1 TCP/IP

![tcp-ip-model-vs-osi-model](https://gitee.com/suzhen99/redhat/raw/master/images/tcp-ip-model-vs-osi-model.svg)

|  ID  |    ITEM    |  EXAMPLE  |                                                 |                   |
| :--: | :--------: | :-------: | :---------------------------------------------: | :---------------: |
|  4   |   应用层   |   HTTP    |                 真正使用的协议                  |        URL        |
|  3   |   传输层   | TCP、UDP  |                    传输控制                     | # ss<br># netstat |
|  2   |   网络层   |    IP     |                    逻辑地址                     |     # ip a s      |
|  1   | 网络接口层 | 网线、MAC | 有线、无线，物理地址<br/>判断网卡厂商，全球唯一 |     # ip a s      |

> NetworkManager
>
> - WIFI
> - IPv6

```bash
-RHEL>=8 NetworkManager
-RHEL<=7 network+[NetworkManager]
-RHEL>=6 netowrk+NetworkManager
# systemctl status NetworkManager

# nmcli device status 
DEVICE  TYPE      STATE         CONNECTION         
enp1s0  ethernet  connected     Wired connection 1 
enp2s0  ethernet  disconnected  --                 
lo      loopback  unmanaged     --
```

|  ID  |                        |                           COMMENT                            |       EXAMPLE       |          |
| :--: | :--------------------: | :----------------------------------------------------------: | :-----------------: | :------: |
|  1   | IP / [NETMASK\|PREFIX] |           172.25.0.9/255.255.0.0 \| 172.25.0.9/16            |        红警         |   LAN    |
|  2   |        GATEWAY         |                          172.25.x.x                          |         QQ          | Internet |
|  3   |          DNS           | **正向解析** # host servera，<br />反向解析 # host 172.25.0.9 | IE, firefox, chrome |   解析   |

网段：IP与掩码二进制与运算 

|                     |                |           |
| :-----------------: | :------------: | :-------: |
|      网络地址       |   172.25.0.0   | 主机位全0 |
| 广播地址 \| netbios | 172.25.255.255 | 主机位全1 |

ip4,ip6

```bash
# yum -y install net-tools
# ifconfig

-Minimal
# ip address show enp1s0
2: enp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether `52:54:00:00:fa:0a` brd ff:ff:ff:ff:ff:ff
    inet `172.25.250.10`/`24` brd 172.25.250.255 scope global noprefixroute enp1s0
       valid_lft forever preferred_lft forever
    inet6 `fe80::e6c5:468e:edb6:9b52`/`64` ...
```

|              |       ipv4        |             ipv6              |          mac          |
| :----------: | :---------------: | :---------------------------: | :-------------------: |
| 二进制（位） |        32         |              128              |          48           |
|  符号（分）  |         .         |               :               |           :           |
|     进制     |  十进制<BR>[0-9]  |     十六进制<BR>[0-9A-F]      | 十六进制<br>[0-9A-F]  |
|      组      |         4         |               8               |           6           |
| **EXAMPLE**  | **172.25.250.10** | **fe80::e6c5:468e:edb6:9b52** | **52:54:00:00:fa:0a** |

### ping, ping6

```bash
# ping -c4 172.25.250.10

RHEL<=7
# ping6 2001::1
# ping6 fe80::e6c5:468e:edb6:9b52 || echo no
%
# ping6 fe80::e6c5:468e:edb6:9b52%enp1s0
-i
# ping6 fe80::e6c5:468e:edb6:9b52 -I enp1s0

RHEL8
# ping6 2001::1
# ping6 fe80::e6c5:468e:edb6:9b52
```

### gateway

```bash
1# nmcli connection show 'Wired connection 1' | grep -i gateway
...
ipv4.gateway:                           --
...
IP4.GATEWAY:                            172.25.250.254
...

2# ip route
default via `172.25.250.254` dev eth0 proto dhcp metric 100
...

3# route -n
...
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         `172.25.250.254`  0.0.0.0         UG    100    0        0 eth0

4# netstat -nr
Kernel IP routing table
Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
0.0.0.0         `a`  0.0.0.0         UG        0 0          0 eth0
172.25.250.0    0.0.0.0         255.255.255.0   U         0 0          0 eth0
```

### dns

```bash
# cat /etc/resolv.conf			    #立即生效，临时生效
# Generated by NetworkManager		#该文件通过NetworkManager服务生成
search domain250.example.com		#ping node2 #ping node2.domain250.example.com
nameserver 172.25.250.254		    #DNS服务器地址

chattr -a /etc/resolv.conf          #可以取消隐藏权限
```

### hostname

```bash
RHEL<=6
# hostname
# ls /etc/sysconfig/network

RHEL>=7
# hostnamectl										# 永久生效、立即生效
# ls /etc/hostname
```



### 1. nmcli

```bash
# nmcli dev status 
# nmcli connection show

# ls /etc/sysconfig/network-scripts/ifcfg-*

-dhcp
# nmcli connection modify 'Wired connection 1' \
connection.autoconnect yes \
ipv4.method auto
# systemctl restart NetworkManager
# ip a s
-static
# nmcli con mod 'Wired connection 1' \
connection.autoconnect yes \
ipv4.method manual \
ipv4.addresses 172.25.250.11/24 \
ipv4.gateway 172.25.250.254 \
ipv4.dns 8.8.8.8
# nmcli connection up 'Wired connection 1' 
# ip a s

# mandb
# man -k nmcli
# man nmcli | grep -A 2 nmcli.*mod

+ipv4.addresses,-ipv4.dns
# nmcli con mod 'Wired connection 1' +ipv4.addresses 172.25.250.10/24
# ifdown 'Wired_connection_1' && ifup 'Wired_connection_1'
# ip a s
# nmcli con mod 'Wired connection 1' -ipv4.addresses 172.25.250.10/24
# ifdown 'Wired_connection_1' && ifup 'Wired_connection_1'
# ip a s
```

```bash
# nmcli connection add \
type ethernet \
ifname eth0 \
connection.autoconnect yes \
ipv4.method manual \
ipv4.addresses 172.25.250.11/24 \
ipv4.gateway 172.25.250.254 \
ipv4.dns 8.8.8.8
```



### 2. nmtui

```bash
# nmtui
# nmcli connection up 'Wired connection 1'
```

### 3. nm-connection-editor

**[foundation|workstation]**

```bash
$ nm-connection-editor
```

### 4. ifcfg-*

```bash
-方法1
# grep -r BOOTPROTO /usr/share/doc
# vim /usr/share/doc/initscripts/sysconfig.txt 
/ifcfg, <Ctrl+F>/<Ctrl+B>

-方法2
# man -k ifcfg
ifcfg (8)            - simplistic script which replaces ifconfi...
nm-settings-ifcfg-rh (5) - Description of ifcfg-rh settings plugin
# man nm-settings-ifcfg-rh

RHEL<8
# vim /etc/sysconfig/network-scripts/ifcfg-Wired_connection_1	#永久生效
...
#PREFIX=16
NETMASK=255.255.0.0

# nmcli connection up 'Wired connection 1' 

# ip a s

RHEL=9
$ vim /etc/NetworkManager/system-connection/链接名
[ipv4]
address1=172.25.254.250/24
route1=172.25.0.0/16,172.25.254.254
address2=172.25.0.250/24
dns=172.25.254.250;8.8.8.8；
dns-search=ilt.example.com;example.com;heihei;  
method=manual

加载方法1：更新IP/mask
$ nmcli con reload
$ nmcli con up 链接名

加载方法2：更新IP/mask/domain_name/dns
$ chattr -i /etc/resolv.conf   #取消隐藏权限
$ nmcli con reload
$ systemctl restart NetworkManager
```

### tracepath

```bash
# tracepath 172.25.0.250
# tracepath 172.25.250.250
```

### services

```bash
# systemctl status sshd

-os linux, windows, macOS
# netstat -antup | grep 22
-short
# ss -antup | grep 22

# grep ssh.*22 /etc/services
ssh             22/tcp                          # The Secure Shell (SSH) Protocol
```

### hostnamectl

```bash
RHEL>=7
# hostnamectl set-hostname sa.example.com
# hostname
# cat /etc/hostname

RHEL<7
# hostname sa.example.com
# vim /etc/sysconfig/network
25

RHEL=9
# hostnamectl hostname sa.example.com
# hostname
# cat /etc/hostname
```

### /etc/hosts

> - 本地解析
> - ip hostname 静态对应列表

```bash
# grep ^hosts /etc/nsswitch.conf
hosts:      files dns myhostname
```

> - files - /etc/hosts
>- dns - /etc/resolv.conf

### /etc/resolv.conf

```bash
# cat /etc/resolv.conf 
# grep -i dns /etc/sysconfig/network-scripts/ifcfg-Wired_connection_1 
# nmcli con mod 'Wired connection 1' ipv4.dns 8.8.8.8
# grep -i dns /etc/sysconfig/network-scripts/ifcfg-Wired_connection_1
 
# cat /etc/resolv.conf && echo no
1-down
# nmcli connection up 'Wired connection 1' 
2-up
# systemctl restart NetworkManager
# cat /etc/resolv.conf && echo change
```



## 13. [归档与系统间复制文件](http://foundation0.ilt.example.com/slides/)

> `Partition`/分区 = `inode`/节点区（文件个数） + `block`/块区（文件容量）
>
> |          |      |       COMMENT        |       |
> | :------: | :--: | :------------------: | :---: |
> | 文件个数 | 打包 | 多个文件变成一个文件 | inode |
> | 文件容量 | 压缩 |   大文件变成小文件   | block |

### 13. 1 tar

```bash
-c `c`reate
-t lis`t`
-x e`x`tract
# dd if=/dev/zero of=./file1.txt bs=1M count=1
# dd if=/dev/zero of=./file2.txt bs=1M count=2
# ls -lh file*
# du -sh /boot

# man tar
# tar -cf file.tar file[12].txt /boot
# echo $?

# tar -tf file.tar 
# du -sh /boot
# ls -lh file[12]*
# ls -lh file*

# rm -rf file[12]*
# ls -lh file*
# tar -xf file.tar 
# ls file*
# ls -ld boot/
# du -sh boot/
# tar -xf file.tar -C /tmp
# ls -ldh /tmp/{file*,boot}

-z gzip
-j bzip2
-J xz
# man tar
# tar -czf file.tar.gz file[12]*
# tar -cjf file.tar.bz2 file[12]*
# tar -cJf file.tar.zx file[12]*
# ls -lh file*
# file file.tar.zx 
# tar -tf file.tar.zx
# tar -xf file.tar.zx -C /home
```

```bash
# man tar
# man tar | grep tar.*-c
# MANWIDTH=120 man tar | grep tar.*-c
# MANWIDTH=120 man tar | grep gzip
# MANWIDTH=120 man tar | grep bz2
# MANWIDTH=120 man tar | grep bzip2

# tar -czf /root/backup.tar.gz /usr/local
# echo $?

# tar -tf /root/backup.tar.gz
# file /root/backup.tar.gz

# ls -lh /boot/vmlinuz-4.18.0-193.el8.x86_64 /boot/initramfs-4.18.0-193.el8.x86_64.img
# tar -cf f4.tar /boot/vmlinuz-4.18.0-193.el8.x86_64 /boot/initramfs-4.18.0-193.el8.x86_64.img
# tar -tf f4.tar
# ls -lh /boot/vmlinuz-4.18.0-193.el8.x86_64 /boot/initramfs-4.18.0-193.el8.x86_64.img f4.tar

# tar -xf f4.tar
# ls
# tree boot/

-C
# tar -xf f4.tar -C /tmp
# tree /tmp/boot/

-z,-j,-J
# tar -czf f4.tar.txt /boot/vmlinuz-4.18.0-193.el8.x86_64 /boot/initramfs-4.18.0-193.el8.x86_64.img
# tar -cjf f4.tar.bat /boot/vmlinuz-4.18.0-193.el8.x86_64 /boot/initramfs-4.18.0-193.el8.x86_64.img
# tar -cJf f4.tar.exe /boot/vmlinuz-4.18.0-193.el8.x86_64 /boot/initramfs-4.18.0-193.el8.x86_64.img
# ls -lh f4.tar.txt f4.tar.bat f4.tar.exe
# file f4.tar.txt f4.tar.bat f4.tar.exe
```



|  CMD  | BASE |      DIFF      |                        |
| :---: | :--: | :------------: | :--------------------: |
|  scp  | ssh  |    回显 式     |      文件路径明确      |
| sftp  | ssh  |    交互 式     |     文件路径不明确     |
| rsync | ssh  | 大量重复的文件 | 反复拷贝，源和目标不变 |

|      |  Server   |   Client   |
| ---- | :-------: | :--------: |
|      | Classroom | foundation |

### 13.2 scp -CLI [ Linux | macOS | win10 ] | [winscp](https://winscp.net/eng/docs/interfaces) -GUI [ Windows ]![img](https://gitee.com/suzhen99/redhat/raw/master/images/commander.png)

**[kiosk@foundation]**

```bash
$ man scp 

-local to remote
$ scp /etc/yum.repos.d/rhel-dvd.repo root@node1:/etc/yum.repos.d
root@node1\'s password: `flectrag`
$ ssh root@node1 ls /etc/yum.repos.d
root@node1\'s password: `flectrag`

-remote to remote
$ scp root@node1:/etc/yum.repos.d/rhel-dvd.repo root@node2:/etc/yum.repos.d
root@node1\'s password: `flectrag`
root@node2\'s password: `P@33w0rd`
$ ssh root@node2 ls /etc/yum.repos.d
root@node2\'s password: `P@33w0rd`

-remote to local
$ scp root@node2:/etc/yum.repos.d/rhel-dvd.repo .
root@node2\'s password: `P@33w0rd`
$ ls
```

### 13.4 sftp

|  ID  |                       |    COMMENT     |                                    |
| :--: | :-------------------: | :------------: | ---------------------------------- |
|  1   |      ftp<br>lftp      |   ftp client   | 交互式<br>写脚本                   |
|  2   |         sftp          | ssh SubService |                                    |
|  3   | **vsftpd**<br> wu-ftp |    service     | Very Secure Ftp 安全性<br>功能丰富 |

> |     COMMENT      |  SERVER   |    CLIENT    |
> | :--------------: | :-------: | :----------: |
> |     hostname     | classroom |  Foundation  |
> |     切换目录     |    cd     |  lcd<br>!cd  |
> | 查看目录中的文件 |    ls     |  lls<br>!ls  |
> | 查看当前所在目录 |    pwd    | lpwd<br>!pwd |
> |     上传文件     |           |     put      |
> |     下载文件     |    get    |              |
> |       退出       |   exit    |              |

**[root@foundation]**

```bash
# sftp instructor@classroom.example.com 
instructor@classroom\'s password: `Asimov`
sftp> `help`
sftp> `pwd`
Remote working directory: /home/instructor
sftp> `!pwd`
/root
sftp> `cd /tmp`
sftp> `lcd /home/kiosk`
sftp> `pwd`
Remote working directory: /tmp
sftp> `!pwd`
/home/kiosk
sftp> `!ls`
ClassPrep.txt		      ...
sftp> `put ClassPrep.txt`
Uploading ClassPrep.txt to /tmp/ClassPrep.txt
ClassPrep.txt    100%   31KB   9.4MB/s   00:00    
sftp> `ls`
ClassPrep.txt     NIC1       ...
sftp> `get NIC1`
Fetching /tmp/NIC1 to NIC1
/tmp/NIC1    100%    7     1.6KB/s   00:00    
sftp> `!ls N*`
NIC1
sftp> `exit`
```

### 13.4 rsync

**[root@foundation]**

```bash
# man sync
# man rsync

# rsync -av /boot/ instructor@classroom:~
instructor@classroom\'s password: `Asimov`
# rsync -av /boot/ instructor@classroom:~
instructor@classroom\'s password: `Asimov`

-append
# echo haha > /boot/new.file
# rsync -av /boot/ instructor@classroom:~
instructor@classroom\'s password: `Asimov`
sending incremental file list
`./`
`new.file`
...

-modify
# echo hehe > /boot/new.file
# rsync -av /boot/ instructor@classroom:~
instructor@classroom\'s password: `Asimov`
`new.file
...
# ssh instructor@classroom cat new.file
instructor@classroom\'s password: `Asimov`
hehe

-delete
# rm -rf /boot/new.file
# rsync -av /boot/ instructor@classroom:~
instructor@classroom\'s password: `Asimov`
`./`
...
# ssh instructor@classroom ls new.file
```



## 14. [安装与升级软件包](http://foundation0.ilt.example.com/slides/)

> 安装方式
>
> - rpm，无基础	redhat, centos, kylinos
> - 编译，有基础
> - deb                    debian, ubuntu, kylinos

| RPM包安装方式 |   Install   |          Diff          |              |                     | query  |    uninstall    |
| :-----------: | :---------: | :--------------------: | :----------: | ------------------- | :----: | :-------------: |
|      rpm      |     -i      |      不支持依赖包      |              | RPM Package Manager | **-q** |       -e        |
|      yum      | **install** | 软件仓库<br>支持依赖包 |   向下兼容   | Yellow dog Updater  | search | erase<br>remove |
|      dnf      |   install   | 软件仓库<br>支持依赖包 | Yum 主要分支 | Dandified Yum       | search | erase<br>remove |

> /etc/yum.repos.d/\*.repo

### 14.1 rpm

**[root@foundation]**

```bash
-q
# rpm -q vdo

-q query all
# rpm -qa
# rpm -qa | grep vdo
# rpm -qa | grep samba


-p package
-i infomation
未安装，RHEL7开始可省略
[root@foundation]
# find /content/ -name vdo*rpm
# rpm -qip /content/rhel8.0/x86_64/dvd/BaseOS/Packages/vdo-6.2.0.293-10.el8.x86_64.rpm
已安装
# rpm -qi vdo

-l list
# rpm -qlp /content/rhel8.0/x86_64/dvd/BaseOS/Packages/vdo-6.2.0.293-10.el8.x86_64.rpm
# rpm -ql vdo
-f from
注意：必须是绝对路径
# whereis vdo
# rpm -qf /usr/bin/vdo
-c config
# rpm -qcp /content/rhel8.0/x86_64/dvd/BaseOS/Packages/openssh-server-7.8p1-4.el8.x86_64.rpm
# rpm -qc openssh-server
-d document
# rpm -qdp /content/rhel8.0/x86_64/dvd/BaseOS/Packages/vdo-6.2.0.293-10.el8.x86_64.rpm
# rpm -qd vdo

--scripts
# rpm -qp --scripts /content/rhel8.0/x86_64/dvd/BaseOS/Packages/vdo-6.2.0.293-10.el8.x86_64.rpm
# rpm -q --scripts vdo

-i install, verbose, hash
### error ###
# rpm -ivh /content/rhel8.0/x86_64/dvd/BaseOS/Packages/samba-4.9.1-8.el8.x86_64.rpm
error: Failed dependencies:
	libxattr-tdb-samba4.so()(64bit) is needed by samba-0:4.9.1-8.el8.x86_64
	libxattr-tdb-samba4.so(SAMBA_4.9.1)(64bit) is needed by samba-0:4.9.1-8.el8.x86_64
	samba-common-tools = 4.9.1-8.el8 is needed by samba-0:4.9.1-8.el8.x86_64
	samba-libs = 4.9.1-8.el8 is needed by samba-0:4.9.1-8.el8.x86_64
# echo $?
1
# rpm -q samba
package samba is not installed
# rpm -ivh \
/content/rhel8.0/x86_64/dvd/BaseOS/Packages/samba-common-tools-4.9.1-8.el8.x86_64.rpm \
/content/rhel8.0/x86_64/dvd/BaseOS/Packages/samba-4.9.1-8.el8.x86_64.rpm \
/content/rhel8.0/x86_64/dvd/BaseOS/Packages/samba-libs-4.9.1-8.el8.x86_64.rpm
# echo $?
0
# rpm -q samba
samba-4.9.1-8.el8.x86_64

##### 查"文件"来源包，方法一
http://rpm.pbone.net/
	单击`Advanced RPM Search`
	复选`RedHat EL 8`
	Please enter searched expression `libxattr-tdb-samba4.so`
	单击`SEARCH`
##### 查"文件"来源包，方法二
# 存在yum仓库时
# yum provides libxattr-tdb-samba4.so
...
`samba-libs-4.9.1-8.el8.i686` : Samba libraries
Repo        : ucf-rhel-8-for-x86_64-baseos-rpms
Matched from:
Provide    : libxattr-tdb-samba4.so

# find /content -name samba-common-tools*.rpm
/content/rhel8.0/x86_64/dvd/BaseOS/Packages/samba-common-tools-4.9.1-8.el8.x86_64.rpm
# find /content/ -name  samba-libs*.rpm
/content/rhel8.0/x86_64/dvd/BaseOS/Packages/samba-libs-4.9.1-8.el8.x86_64.rpm
# rpm -ivh /content/rhel8.0/x86_64/dvd/BaseOS/Packages/samba-common-tools-4.9.1-8.el8.x86_64.rpm /content/rhel8.0/x86_64/dvd/BaseOS/Packages/samba-libs-4.9.1-8.el8.x86_64.rpm /content/rhel8.0/x86_64/dvd/BaseOS/Packages/samba-4.9.1-8.el8.x86_64.rpm

# find /content/ -name dnf-utils*rpm
/content/rhel8.0/x86_64/dvd/BaseOS/Packages/dnf-utils-4.0.2.2-3.el8.noarch.rpm
/content/rhel8.2/x86_64/rhel8-additional/rhel-8-for-x86_64-baseos-rpms/Packages/d/dnf-utils-4.0.2.2-3.el8.noarch.rpm
### OK ###
# rpm -ivh /content/rhel8.0/x86_64/dvd/BaseOS/Packages/dnf-utils-4.0.2.2-3.el8.noarch.rpm
...
Updating / installing...
   1:dnf-utils-4.0.2.2-3.el8          ################################# [100%]
# echo $?
0
# rpm -q dnf-utils
dnf-utils-4.0.2.2-3.el8.noarch

-e erase
# rpm -e dnf-utils
# rpm -q dnf-utils
```

### 14.2 yum | dnf

> **url**	= protocol + path
>
> ​	http://x/y
>
> ​	ftp://x/y
>
> ​	file:///path

```bash
yum配置文件
# man yum.conf
yum仓库文件
# ls /etc/yum.repos.d/
查看生效的yum仓库
# yum repolist 
No repositories available

默认启用签名检查
# vim /etc/yum.conf
gpgcheck=1

通过命令做仓库
-repo: cmd/yum-config-manager
[kiosk@foundation]
$ yum provides yum-config-manager	#RHEL8.0
`dnf-utils-4.0.2.2-3.el8.noarch` : Yum-utils CLI compatibility layer
...
Filename    : /usr/bin/yum-config-manager
$ firefox http://foundation0.ilt.example.com/dvd/BaseOS
		Packages/
		<Ctrl-F> / `yum-utils`				#RHEL8.2
		鼠标右键`copy link location`
[root@node1]
-rpm
# rpm -ivh http://foundation0.ilt.example.com/dvd/BaseOS/Packages/yum-utils-4.0.12-3.el8.noarch.rpm
-yum
# yum -y install http://foundation0.ilt.example.com/dvd/BaseOS/Packages/yum-utils-4.0.12-3.el8.noarch.rpm

-yum-config-manager
# yum-config-manager -h
...
Config-manager command-specific options:
  --add-repo URL        add (and enable) the repo from the specified file or url
# yum-config-manager --add-repo http://foundation0.ilt.example.com/rhel8.0/x86_64/dvd/BaseOS/
# ls /etc/yum.repos.d/
# yum repolist

编辑文件做仓库
-repo: file/X.repo
[kiosk@foundation]
$ scp /etc/yum.repos.d/rhel-dvd.repo root@node1:/etc/yum.repos.d/app.repo
[root@node1]
# vim /etc/yum.repos.d/app.repo
[AppStream]
name=AppStream
baseurl=http://foundation0.ilt.example.com/dvd/AppStream
# ls /etc/yum.repos.d/
# yum repolist

# yum -y install ftp
...
Error: GPG check FAILED

--GPG-KEY1：考试时建议，通过导入文件件检查
[kiosk@foundation]
$ ls \
/etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release \
/content/rhel8.2/x86_64/dvd/RPM-GPG-KEY-redhat-release

*# rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
# yum -y install ftp

# rpm -qa | grep pub
# rpm -e \
gpg-pubkey-fd431d51-4ae0493b \
gpg-pubkey-d4082792-5b32db75
# yum -y erase ftp

--GPG-KEY2：考试时建议，编辑仓库文件，安装软件时导入检查
*# vim /etc/yum.repos.d/foundation0.ilt.example.com_dvd_BaseOS.repo 
...
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
# vim /etc/yum.repos.d/app.repo 
...
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
# yum -y install ftp

# rpm -qa | grep pub
# rpm -e \
gpg-pubkey-fd431d51-4ae0493b \
gpg-pubkey-d4082792-5b32db75
# yum -y erase ftp

--GPG-KEY3:考试时不建议，8版本考试时此方法不给分；生产中不建议；测试环境随意
# vim /etc/yum.conf
...
gpgcheck=0
# vim /etc/yum.repos.d/app.repo 
...
#gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
# yum -y install ftp
```

```bash
库库的地址，如果有问题，修改后，请clean一下
# yum clean all
```

```bash
INTERNET

-EPEL
# yum-config-manager --add-repo https://mirrors.tuna.tsinghua.edu.cn/epel/8/Everything/x86_64/
# rpm --import https://mirrors.tuna.tsinghua.edu.cn/epel/RPM-GPG-KEY-EPEL-8

-CENTOS
# yum-config-manager --add-repo https://mirrors.tuna.tsinghua.edu.cn/centos/8/AppStream/x86_64/os/
# yum-config-manager --add-repo https://mirrors.tuna.tsinghua.edu.cn/centos/8/BaseOS/x86_64/os/
# rpm --import https://mirrors.tuna.tsinghua.edu.cn/centos/RPM-GPG-KEY-CentOS-Official

-下载单独的安装包
https://pkgs.org/
```

```bash
查询安装包是否安装
# yum list httpd
# yum list http*

通过关键词搜索安装包名
# yum search ntp

== rpm -qi chrony
# yum info chrony.x86_64

== rpm -qf /etc/samba/smb.conf
# yum provides *smb.conf

== rpm -e autofs
# yum list autofs
# yum erase autofs
# yum list autofs
# yum -y erase autofs

== rpm -ivh ...
# yum install autofs
# yum install -y autofs
# yum list autofs
# yum history
# yum clean all

# yum grouplist
# yum groupinstall 'Virtualization Host'
```



## 15. [访问linux文件系统](http://foundation0.ilt.example.com/slides/)

### Disk

|                   |      INTERFACE       |                    |
| :---------------: | :------------------: | :----------------: |
| /dev/**sd**[a-zz] | SATA, SAS, SCSI, USB |       热插拔       |
| /dev/**hd**[a-zz] |         IDE          |                    |
| /dev/**vd**[a-zz] |        VirtIO        | /dev/**x**vd[a-zz] |
| /dev/**nvme**0n1  |         nvme         |                    |

### Filesystem

|   OS    |       NEW        |                             ORG                              |                            |
| :-----: | :--------------: | :----------------------------------------------------------: | :------------------------: |
| Windows |     **ntfs**     | fat16 (single partition <=2GB), <br>fat32 (single file <=4GB) |       exfat*(>=Win8)       |
|  Linux  | **xfs**(RHEL>=7) |                    ext3, **ext4**(RHEL<7)                    | [exfat](https://pkgs.org/) |
|  MacOS  |     **apfs**     |                             hfs                              |           exfat*           |

```bash
# df -hT
# df -ht xfs -t vfat
```



### mount, umount

> Linux：挂载，卸载（临时生效、立即生效）
>
> windows：指派盘符，删除盘符

**[root@foundation]**

```bash
-local partiton
本地分区
# lsblk 
# blkid  /dev/nvme0n1p1

设备名称
# mkdir /mnt/dev
# mount /dev/nvme0n1p1 /mnt/dev
# ls /mnt/dev

UUID
# mkdir /mnt/uuid
# mount UUID="E9D0-6DD5" /mnt/uuid
# ls /mnt/uuid

# mount | grep mnt
# umount /mnt/dev
# umount /mnt/uuid
# mount | grep mnt

-local *.iso
本地光盘镜像 == 虚拟光驱
# find /content -name *.iso
# mkdir /mnt/iso
# mount /content/rhel8.0/x86_64/isos/rhel-8.0-x86_64-dvd.iso /mnt/iso

-remote nfs/RH358
远程 nfs 共享, == 映射网络驱动器
# ping -c 4 172.25.254.250
# showmount -e 172.25.254.250
# mkdir /mnt/nfs
# mount 172.25.254.250:/content /mnt/nfs
# ls /mnt/nfs
# umount /mnt/nfs

-remote samba/RH358
远程 windows 共享, == 映射网络驱动器
pysical machine vmnet1: `172.25.254.1`
# ping -c 4 172.25.254.1
guest # smbclient -L 172.25.254.1 -N										#Nopassword
user  # smbclient -L 172.25.254.1 -U tom%mima
# mkdir /mnt/OneDrive
user  # mount -o username=tom,password=mima //172.25.254.1/OneDrive /mnt/OneDrive
guest # mount -o guest //172.25.254.1/OneDrive /mnt/OneDrive
# umount /mnt/OneDrive

# smbclient -L //172.25.254.1 -U tom%mima
# smbclient //172.25.254.1/OneDrive -U tom%mima
Try "help" to get a list of possible commands.
smb: \> `help`
smb: \> `ls`
smb: \> `exit`
```

### locate, find

```bash
# ls file*
# locate file.tar || echo no
# updatedb &
# jobs
# locate file.tar && echo ok
 
# find / -name file.tar
# find / -name file.tar 2>/dev/null
# man find
/TEST
# find / -user kiosk
# man find | grep find.*exec
# find /home/ -user kiosk -exec cp -a {} /mnt/dev \;
# ls -l /mnt/dev/
```

### ln

```bash
# ls -l file2.txt 
# du -sh .

# ln file2.txt hard
# ls -lh file2.txt hard
# du -sh .
# ls -lih file2.txt hard
# man find | grep inum
# find / -inum 540914070

# ln -s file2.txt soft
# ls -lih file2.txt soft 
```

### dd, df, du

```bash
-dd disk dump == win/ghost
# dd if=rhel-8.2-x86_64-dvd.iso of=/dev/sda
# dd if=/dev/zero of=file1 bs=1M count=2

-df disk filesystem
# df -h

-du disk unit
# du -sh /boot
```



## 16. [分析服务和获得支持](http://foundation0.ilt.example.com/slides/)

### cockpit

```bash
# yum search cock
# yum list cockpit
# systemctl list-unit-files | grep cock
# systemctl status cockpit.service
# systemctl start cockpit.service
# systemctl enable cockpit.service
# echo $?
# ss -antup | grep cock
# firewall-cmd --list-all
```

<kbd>Alt</kbd>+<kbd>F2</kbd> / `firefox http://127.0.0.1:9090`

​	root%Asimov



## A. 附录

### A.0 动手操作

### A. 1 [Typora](https://typora.io/) Markdown编辑器

### A.2 VMware network

|  ID  | VMware<br>foundation0 |           |                  物理机<br>PC                  |
| :--: | :--: | :-------: | :---------------------------------------: |
|  1   | - |  Bridge   |                 真实网卡                  |
|  2   | 00:0c:29:c7:4a:11<br>VMware `连接网络适配器`<br>**ens192<br>**\$ `nmcli connection up ens192`<br> $ `ip a s ens192` |    NAT<br>已复制    |  **VMnet8**<br>192.168.121.1<br>（自动配置）<br>X:\\> `ssh kiosk@192.168.121.X`  |
|  3   | 00:0c:29:c7:4a:07<br>**ens160** == br0<br>172.25.254.250/24 | Host-only<br>`已移动` | VMnet1<br>172.25.254.`1`/24<br>（手动配置）<br>X:\\> `ssh kiosk@192.168.254.250` |

### A.3 alias

```bash
$ alias 

$ alias copy='cp'

$ copy /etc/fstab /tmp

$ unalias copy

```
```bash
# ls ~/.bashrc /etc/bashrc
```

### A.5 Terminal

|  ID  |                                               |
| :--: | --------------------------------------------- |
|  1   | <kbd>Ctrl</kbd>+<kbd>+</kbd>                  |
|  2   | <kbd>Ctrl</kbd>+<kbd>-</kbd>                  |
|  3   | <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>T</kbd> |
|  4   | <kbd>Alt</kbd>+<kbd>1</kbd>                   |

### A.6 SERVICE

| step | CMD                                                   |         DEMO          |                 COMMENT                  |
| :--: | :---------------------------------------------------: | :-------------------: | :--------------------------------------: |
|  1   | yum search KEYWORD                                    |          ssh          | server\|daemon -=><br> `openssh-server.x86_64` |
| 1.1 | yum list PKG<br>rpm -q PKG |openssh-server|Installed Packages|
| 1.2 | yum -y install PKG <br>\| rpm -ivh PATH/PKG |openssh-server||
|  2   | rpm -qc PKG                                           | openssh-server | config\|cfg\|conf -=><br>`/etc/ssh/sshd_config` |
| **3** | vim CFG                                               | /etc/ssh/sshd_config  |                                          |
|  4   | **A**<br>rpm -ql PKG \| grep service                  | openssh-server | **B**<br>systemctl list-unit-files \| grep ssh<br>systemctl status sshd.service |
|  5   | systemctl restart DAEMON                              |         sshd          |  |
|  6   | systemctl enable DAEMON                               |                       |                                          |
| **7** | firewall-cmd --permanent ...<br>firewall-cmd --reload | --add-service=ssh<br>--add-port=82/tcp | port |
| **8** | security<br>(filesystem<br>+ selinux)              | chmod, chown, setfacl<br>semange fcontext ...<br>semange port ...<br>setsebool -P ... | selinux(filesystem+port+service) |

### A.7 Simulation - 『培训环境 』转成 『练习环境，模拟考试环境』

|  NO  | OPERATION                                                    | COMMENT                        |
| :--: | ------------------------------------------------------------ | ------------------------------ |
|  1   | VMware/<br>  snapshot :arrow_right: `INIT`               | 快照恢复             |
|  1.a   | VMware/ <br> `CPU*?` + `MEM=?` | 建议修改配置,<br>启动虚拟机 |
|  2   | VMware /<br>  Step1. CDROM / `EX200.iso`,<br>  Step2. 复选`连接 CD/DVD 驱动器`<br>$ df /dev/sr0 | 插入光盘镜像<br>连接光盘，确认 |
|  3   | \$ ssh root@localhost yum -y install /run/media/kiosk/CDROM/* | root 身份安装                  |
|  4   | 上步完成后，关机 |                   |
|  5   | VMware/<br/>  snapshot :arrow_right: 拍摄快照`EX200` | 创建快照 |
|  E  | <kbd>Win</kbd> / <kbd>View Exam</kbd>                        | 练习题                         |
|  E  | \$ exam-grade                                             | 判分脚本（练习环境）                  |

<div style="background: #dbfaf4; padding: 12px; line-height: 24px; margin-bottom: 24px;">
<dt style="background: #1abc9c; padding: 6px 12px; font-weight: bold; display: block; color: #fff; margin: -12px; margin-bottom: -12px; margin-bottom: 12px;" >Hint - 提示</dt>
$ whereis exam-grade<br>
$ vim /usr/local/bin/exam-grade<br>
/
</div>
|     VM     |   root   |      |
| :--------: | :------: | ---- |
| foundation | P@33w0rd |      |
| classroom  | P@33w0rd |      |
|   node1    | flectrag |      |
|   node2    | P@33w0rd |      |



### A.8 ACCOUNT

|     VM      |  root  | student | Instructor | kiosk  |
| :---------: | :----: | :-----: | :--------: | :----: |
| foundation  | Asimov |    -    |     -      | redhat |
|  classroom  | Asimov |    -    |   Asimov   |   -    |
|   bastion   | redhat | student |     -      |   -    |
| workstation | redhat | student |     -      |   -    |
| server{a-e} | redhat | student |     -      |   -    |

### A9. X11（== Win/RemoteApp）

**server - workstation**

```bash
# grep ^X11 /etc/ssh/sshd_config
X11Forwarding yes
```

**client - foundation**

```bash
$ ssh -X student@workstation
student
$ firefox
```

### A10. 挂载优盘

物理机插入优盘

VMware / 连接到『Linux虚拟机』

**[root@foundation]**

```bash
# lsblk
# umount /dev/sda1
# umount /run/media/kiosk/EFI
# lsblk

# mkdir /mnt/usb
# mount /dev/sda1 /mnt/usb
# ls /mnt/usb
# umount /mnt/usb
# eject /dev/sda
```

VMware 虚拟机USB，移除优盘

物理机拔掉优盘

### A11. windows

```cmd
help
part=inode+block
```

### A12. Software

|      | URL                                              |  Name  | Comment                   |
| :--: | ------------------------------------------------ | :----: | ------------------------- |
|  1   | https://www.vmware.com/cn.html                   | VMware |                           |
|  2   | https://ark.intel.com/content/www/cn/zh/ark.html |  CPU   | 英特尔® 虚拟化技术 (VT-x) |
|  3   | https://typora.io/                               | Typora | markdown                  |
|  4   |                                                  | Xmind  | 思维导图                  |
|  5   | https://sunlogin.oray.com/download/              | 向日葵 | 远程                      |

### A13. master

|  ID  | Master | Branch  |
| :--: | :----: | :-----: |
|  1   |  yum   |   dnf   |
|  2   | mysql  | mariadb |
|  3   | syslog | rsyslog |

### A14. 编译安装

```bash
# wget ...
# tar -xf ...
# cd ...
# ls
README.md configure ...
# vim README.md
# gcc ...
R# ./configure ...
R# make
R# make install
```





+-+*/

-----0-=1·	··········································+- 

-
